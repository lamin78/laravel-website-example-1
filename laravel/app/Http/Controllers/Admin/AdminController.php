<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

use Session;
use Admin;

class AdminController extends Controller {
	
	protected $filters;
	protected $response = [];
	
	public function __construct(Guard $guard) {
		
		$this->middleware('auth');

		Admin::register($guard);

		/**
		 * The state of the javascript array may change depending on the needs of the project front end
		 * However this will eventually be compressed into one file using prepros
		 * For now the prettyified state is necessary for line dubbuging
		 *
		 */
		
		$this->context = array(
			'page_css_array' 	=> []
			,'pageViewJS'		=> ""
		);

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}


		$this->filters = [];
	}

	public function getFilters($key, $default) {
		//Session::flush();
		return Session::get('filters.'.$key, $default);
	}

	public function setFilters($key,$value) {
		Session::put('filters.'.$key,$value);
	}

	public function prepDefaultFilters($key, $options) {
		$filters = $this->getFilters($key, []);

		foreach ($options as $k => $v) {
			$filters[$k] = (isset($filters[$k]) ? $filters[$k] : $v);
		}

		return $filters;
	}
}