<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use App\Http\Requests\Admin\SocialImagesRequest as SocialImagesRequest;
use DB;
use Carbon\Carbon;

use App\Models\SocialImages as SocialImages;

use Intervention\Image\ImageManagerStatic as Image;


class ImagesController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, SocialImages $images)
	{
		parent::__construct($guard);

		$this->images = $images;
	}

	/**
	 * Show the Social Images dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['title'] = 'Social - Images';

		$this->context['pageViewJS'] = 'admin/images-main-min';

		$this->context['images'] = $this->images->orderby("created_at", "desc")->get();

		return view('admin.social.images', $this->context);
	}

	/**
	 * Show the create form to the user.
	 *
	 * @return Response
	 */
	public function create()
	{

		$this->context['title'] = 'Social - Images - Add New Image';

		$this->context['pageViewJS'] = '';

		return view('admin.social.images-create', $this->context);
	}

	public function store(SocialImagesRequest $request) {

		$data['status'] = 'ready';

		$filename = str_slug($request->title) . '.' . $request->file('image')->getClientOriginalExtension();

	    $request->file('image')->move(
	        base_path() . '/html/uploads/', $filename
	    );

	    // open file a image resource
		$img = Image::make('uploads/'.$filename);

		$img->fit(500)->save();

	    $data = [
	    		'title' => $request->title,
	    		'media_local' => $filename,
	    		'status' => 'ready'
	    		];

	  	$this->images->fill($data);
		$this->images->save();

		$this->response = array('response_status' => 'success', 'message' => 'This Project has been added successfully.');

		return redirect('admin/social/images')->with('response', $this->response);
	}		

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$count = $this->images->where('status','ready')->count();

		if ($count <= 2) {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least two images live.');
			return redirect('admin/social/images')->with('response', $this->response);
		} else {
			$img = $this->images->findOrFail($id);
			$img->status = "disabled";
			$img->save();
			return redirect('admin/social/images');
		}
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$img = $this->images->findOrFail($id);
		$img->status = "ready";
		$img->save();
		return redirect('admin/social/images');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->images->where('status','ready')->count();
		$img = $this->images->findOrFail($id);
		if ($count <= 2 && $img['status'] == 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least two images live.');
			return redirect('admin/social/images')->with('response', $this->response);
		} else {
			
			$img->delete();
			return redirect('admin/social/images');
		}
	}
}
