<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use App\Http\Requests\Admin\LinkedInRequest as LinkedInRequest;
use DB;
use Carbon\Carbon;

use App\Models\LinkedIn as LinkedIn;


class LinkedInController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, LinkedIn $linkedin)
	{
		parent::__construct($guard);

		$this->linkedin = $linkedin;

	}

	/**
	 * Show the linkedin dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['title'] = 'Social - LinkedIn';

		$this->context['pageViewJS'] = '';

		$this->context['posts'] = $this->linkedin->orderby("created_at", "desc")->get();

		return view('admin.social.linkedin', $this->context);
	}

	/**
	 * Show the create form to the user.
	 *
	 * @return Response
	 */
	public function create()
	{

		$this->context['title'] = 'Social - LinkedIn - Add New Post';

		$this->context['pageViewJS'] = '';

		return view('admin.social.linkedin-create', $this->context);
	}

	public function store(LinkedInRequest $request) {

		$request['status'] = 'ready';

	  	$this->linkedin->fill($request->all());
		$this->linkedin->save();

		$this->response = array('response_status' => 'success', 'message' => 'This LinkedIn post has been added successfully.');

		return redirect('admin/social/linkedin')->with('response', $this->response);
	}		

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$count = $this->linkedin->where('status','ready')->count();
		if ($count <= 2) {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least two linkedin posts live.');
			return redirect('admin/social/linkedin')->with('response', $this->response);
		} else {
			$ld = $this->linkedin->findOrFail($id);
			$ld->status = "disabled";
			$ld->save();
			return redirect('admin/social/linkedin');
		}
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$ld = $this->linkedin->findOrFail($id);
		$ld->status = "ready";
		$ld->save();
		return redirect('admin/social/linkedin');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->linkedin->where('status','ready')->count();
		$ld = $this->linkedin->findOrFail($id);
		if ($count <= 2 && $ld['status'] == 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least two linkedin posts live.');
			return redirect('admin/social/linkedin')->with('response', $this->response);
		} else {
			
			$ld->delete();
			return redirect('admin/social/linkedin');
		}
	}
}
