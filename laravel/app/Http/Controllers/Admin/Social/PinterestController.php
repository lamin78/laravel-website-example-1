<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use App\Http\Requests\Admin\PinterestRequest as PinterestRequest;
use DB;
use Carbon\Carbon;
use Storage;

use App\Models\Pinterest as Pinterest;
use Intervention\Image\ImageManagerStatic as Image;


class PinterestController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Pinterest $pinterest)
	{
		parent::__construct($guard);

		$this->pinterest = $pinterest;

	}

	/**
	 * Show the pinterest dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'Social - Pinterest';

		$this->context['pageViewJS'] = 'admin/pinterest-main-min';

		$this->context['pins'] = $this->pinterest->orderby("created_at", "desc")->get();

		return view('admin.social.pinterest', $this->context);
	}

	public function store(PinterestRequest $request) {

		$request['status'] = 'pending';

	  	$this->pinterest->fill($request->all());
		$this->pinterest->save();

		$this->download_images();

		$this->response = array('response_status' => 'success', 'message' => 'This Pinterest post has been added successfully.');

		return redirect('admin/social/pinterest')->with('response', $this->response);
	}		

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$pin = $this->pinterest->findOrFail($id);
		$pin->status = "disabled";
		$pin->save();
		return redirect('admin/social/pinterest');
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$pin = $this->pinterest->findOrFail($id);
		$pin->status = "ready";
		$pin->save();
		return redirect('admin/social/pinterest');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->pinterest->where('status','ready')->count();
		$pin = $this->pinterest->findOrFail($id);
		if ($count <= 1 && $pin['status'] == 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least one pin live.');
			return redirect('admin/social/pinterest')->with('response', $this->response);
		} else {
			unlink(base_path().'\html\uploads\\'.$pin->media_local);
			$pin->delete();
			return redirect('admin/social/pinterest');
		}

		
	}



	private function download_images()
	{
		try {
			$pin = $this->pinterest->where('media_url', '!=', '')->where('status','pending')->take(1)->get()->first();
			if ($pin) {
				$url = $pin->media_url;
				$extension = pathinfo($url, PATHINFO_EXTENSION);

				$filename = 'pinterest-'.$pin->id . '.' . $extension;
				$file = file_get_contents($url);

				$save = file_put_contents('uploads/'.$filename, $file);
				if ($save) {
					

					// open file a image resource
					$img = Image::make('uploads/'.$filename);

					$img->trim('top-left', null, 25)->rectangle(0, 0, $img->width(), $img->height(), function ($draw) {
					    $draw->background('rgba(0, 0, 0, 0.5)');
					})->fit(500)->save();


					$pin->media_local = $filename;
					$pin->status = 'ready';
					$pin->save();
					//echo "Processed Image: " . $filename . "<br/>";
				}
				$this->download_images();
			}
		} catch (Exception $e) {

		}
	}
}
