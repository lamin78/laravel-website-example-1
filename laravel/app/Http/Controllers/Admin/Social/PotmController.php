<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use App\Http\Requests\Admin\PotmRequest as PotmRequest;
use DB;
use Carbon\Carbon;

use App\Models\Potm as Potm;

use Intervention\Image\ImageManagerStatic as Image;


class PotmController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Potm $potm)
	{
		parent::__construct($guard);

		$this->potm = $potm;

		$this->case_studies 		= Config::get('projects');
	}

	/**
	 * Show the potm dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['title'] = 'Social - Potm';

		$this->context['pageViewJS'] = '';

		$this->context['projects'] = $this->potm->orderby("created_at", "desc")->get();

		return view('admin.social.potm', $this->context);
	}

	/**
	 * Show the create form to the user.
	 *
	 * @return Response
	 */
	public function create()
	{

		$this->context['title'] = 'Social - Potm - Add New Project';

		$this->context['pageViewJS'] = '';

		$avail_projects = ["" => "Select a Project"];
		foreach($this->case_studies as $key => $value)
		{
			$avail_projects[$key] = ucwords(str_replace("-"," ",$key));
		}

		$this->context['projects'] = $avail_projects;


		return view('admin.social.potm-create', $this->context);
	}

	public function store(PotmRequest $request) {

		$data['status'] = 'ready';

		$filename = $request->uri . '.' . $request->file('image')->getClientOriginalExtension();

	    $request->file('image')->move(
	        base_path() . '/html/uploads/', $filename
	    );

	    // open file a image resource
		$img = Image::make('uploads/'.$filename);

		$img->fit(500)->save();

	    $data = [
	    		'uri' => $request->uri,
	    		'media_local' => $filename,
	    		'status' => 'ready'
	    		];

	  	$this->potm->fill($data);
		$this->potm->save();

		$this->response = array('response_status' => 'success', 'message' => 'This Project has been added successfully.');

		return redirect('admin/social/potm')->with('response', $this->response);
	}		

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$count = $this->potm->where('status','ready')->count();

		if ($count <= 1) {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least one POTM live.');
			return redirect('admin/social/potm')->with('response', $this->response);
		} else {
			$potm = $this->potm->findOrFail($id);
			$potm->status = "disabled";
			$potm->save();
			return redirect('admin/social/potm');
		}
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$potm = $this->potm->findOrFail($id);
		$potm->status = "ready";
		$potm->save();
		return redirect('admin/social/potm');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->potm->where('status','ready')->count();
		$potm = $this->potm->findOrFail($id);

		if ($count <= 1 && $potm['status'] != 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least one POTM live.');
			return redirect('admin/social/potm')->with('response', $this->response);
		} else {
			
			$potm->delete();
			return redirect('admin/social/potm');
		}
	}
}
