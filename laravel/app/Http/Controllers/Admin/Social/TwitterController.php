<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use DB;
use Carbon\Carbon;

use App\Models\Tweets as Tweets;

class TwitterController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, Tweets $tweets)
	{
		parent::__construct($guard);

		$this->tweets				= $tweets;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'Social - Twitter';

		$this->context['pageViewJS'] = '';

		$this->context['tweets'] = Tweets::orderby("tweeted_at", "desc")->get();

		return view('admin.social.twitter', $this->context);
	}

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$count = $this->tweets->where('status','ready')->count();
		if ($count <= 5) {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least five tweets live.');
			return redirect('admin/social/twitter')->with('response', $this->response);
		} else {
			$tweet = $this->tweets->findOrFail($id);
			$tweet->status = "disabled";
			$tweet->save();
			return redirect('admin/social/twitter');
		}
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$tweet = $this->tweets->findOrFail($id);
		$tweet->status = "ready";
		$tweet->save();
		return redirect('admin/social/twitter');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->tweets->where('status','ready')->count();
		$tweet = $this->tweets->findOrFail($id);
		if ($count <= 5 && $tweet['status'] == 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least five tweets live.');
			return redirect('admin/social/twitter')->with('response', $this->response);
		} else {
			unlink(base_path().'\html\uploads\\'.$tweet->media_local);
			$tweet->delete();
			return redirect('admin/social/twitter');
		}
	}
}
