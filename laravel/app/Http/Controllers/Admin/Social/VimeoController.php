<?php namespace App\Http\Controllers\Admin\Social;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use App\Http\Requests\Admin\VimeoRequest as VimeoRequest;
use DB;
use Carbon\Carbon;

use App\Models\VimeoVids;
use Vimeo\Vimeo as Vimeo;

use Intervention\Image\ImageManagerStatic as Image;

class VimeoController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, VimeoVids $vimeovids)
	{
		parent::__construct($guard);

		$this->vimeovids = $vimeovids;

		$this->client_id = "2e54adf96efa2ad25a8beb1598d04845c939fa4b";
		$this->client_secret = "Uwqr89d5vXx4cstg9RKoD0+8/0zx4EM6EVPAN2K3BwKRVYB/8VIjdD8U7uv3rAoMhUKegHLtGZCILmwfxfw5NF4FA+J+uoz9R3HLtkm1rDPp8fQc5KDQcdYQ96y58pF6";
		$this->auth_token = "814454e57744de748e572819dff675b6";
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['title'] = 'Social - Vimeo';

		$this->context['pageViewJS'] = '';

		$this->context['vids'] = VimeoVids::orderby("created_time", "desc")->get();

		return view('admin.social.vimeo', $this->context);
	}

	public function store(VimeoRequest $request) {
		$id = $request->id;

		$lib = new Vimeo($this->client_id, $this->client_secret);
	  	$lib->setToken($this->auth_token);

	  	$response = $lib->request('/videos/'.$id, [], 'GET');
	  	if ($response['status'] == 200) {
	  		//dd($response['body']);
	  		
	  		$biggest_image = array_pop($response['body']['pictures']['sizes']);
	  		//dd($biggest_image);
	  		
		  	$data = [
		  			'name' => $response['body']['name'],
		  			'uri' => $response['body']['uri'],
		  			'link' => $response['body']['link'],
		  			'embed' => $response['body']['embed']['html'],
		  			'media_url' => $biggest_image['link'],
		  			'status' => 'pending',
		  			'created_time' => new Carbon($response['body']['created_time'])
		  			];
		  	$this->vimeovids->fill($data);
			$this->vimeovids->save();

			$this->download_images();

			$this->response = array('response_status' => 'success', 'message' => 'This vimeo has been added successfully.');

			return redirect('admin/social/vimeo')->with('response', $this->response);
		} else {
			$this->response = array('response_status' => 'alert', 'message' => 'This vimeo video could not be found.');

			return redirect('admin/social/vimeo')->with('response', $this->response);
		}

	}

	/**
	 * Disable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function disable($id)
	{	
		$count = $this->vimeovids->where('status','ready')->count();

		if ($count <= 1) {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least one Vimeo video live.');
			return redirect('admin/social/vimeo')->with('response', $this->response);
		} else {

			$vid = $this->vimeovids->findOrFail($id);
			$vid->status = "disabled";
			$vid->save();
			return redirect('admin/social/vimeo');
		}
	}

	/**
	 * Enable the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function enable($id)
	{	
		$vid = $this->vimeovids->findOrFail($id);
		$vid->status = "ready";
		$vid->save();
		return redirect('admin/social/vimeo');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$count = $this->vimeovids->where('status','ready')->count();
		$vid = $this->vimeovids->findOrFail($id);
		if ($count <= 1 && $vid['status'] == 'ready') {
			$this->response = array('response_status' => 'alert', 'message' => 'There must always be at least one Vimeo video live.');
			return redirect('admin/social/vimeo')->with('response', $this->response);
		} else {
			$vid->delete();
			if (file_exists(base_path().'\html\uploads\\'.$vid->media_local)) {
				unlink(base_path().'\html\uploads\\'.$vid->media_local);
			}
			return redirect('admin/social/vimeo');
		}
	}



	private function download_images()
	{
		try {
			$vid = VimeoVids::where('media_url', '!=', '')->where('status','pending')->take(1)->get()->first();
			if ($vid) {
				$url = $vid->media_url;

				if (stripos($url,"?")) {
					$parts = explode("?",$url);
					$url = $parts[0];
				}


				$extension = pathinfo($url, PATHINFO_EXTENSION);

				$filename = 'vimeo-'.$vid->id . '.' . $extension;
				$file = file_get_contents($url);

				$save = file_put_contents('uploads/'.$filename, $file);
				if ($save) {
					

					// open file a image resource
					$img = Image::make('uploads/'.$filename);

					$img->trim('top-left', null, 25)->fit(500)->save();


					$vid->media_local = $filename;
					$vid->status = 'ready';
					$vid->save();
					//echo "Processed Image: " . $filename . "<br/>";
				}
				$this->download_images();
			}
		} catch (Exception $e) {

		}
	}
}
