<?php namespace App\Http\Controllers\Admin\Summary;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;

use Config;
use Request;
use DB;
use Carbon\Carbon;

class SummaryController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard)
	{
		parent::__construct($guard);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'System Summary';

		$this->context['pageViewJS'] = '';

		return view('admin.summary.home', $this->context);
	}

}
