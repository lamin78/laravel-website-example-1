<?php namespace App\Http\Controllers\Admin\Users;


use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests;

use App\Models\User as User;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

use App\Models\Roles;

use App\Http\Requests\Admin\UserRequest			as UserRequest;


use Config;
use Admin;

class UserController extends AdminController {

	/**
	 * Create a new controller instance with dependency injection
	 *
	 * @return void
	 */
	public function __construct(Guard $guard, User $user) {

		parent::__construct($guard);

		Admin::checkPagePermission("menu_users");

		$this->user = $user;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'User Management';

		$users = $this->user->get();

		$users->each(function($user)
		{
		    $user->role;
		});

		$this->context['users'] = $users;

		return view('admin.users.main', $this->context);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->context['title']				= 'Create User';
		$this->context['roles']				= Roles::all();

		return view('admin.users.create', $this->context);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRequest $request)
	{

		$request['password'] = bcrypt($request->password);

		$this->user->fill($request->all());

		// save form data if passed validation rules
		$this->user->save();

		$this->response = array('response_status' => 'success', 'message' => 'This user has been created successfully.');

		return redirect('admin/users')->with('response', $this->response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->findOrFail($id);

		$this->context['title'] = 'User Management : Editing ' . $user->name;

		$this->context['user'] = $user;

		$this->context['roles']				= Roles::all();

		return view('admin.users.edit', $this->context);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UserRequest $request)
	{
		$user = $this->user->findOrFail($id);

		if($request->password != '') {
			$request['password'] = bcrypt($request->password);
		} else {
			unset($request['password']);
		}

		$user->update($request->all());

		$this->response = array('response_status' => 'success', 'message' => 'This user has been updated successfully.');

		return redirect('admin/users')->with('response', $this->response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = $this->user->findOrFail($id);
		$user->delete();
		return redirect('admin/users');
	}

}
