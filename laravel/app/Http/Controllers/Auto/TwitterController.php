<?php namespace App\Http\Controllers\Auto;

use App\Http\Controllers\Auto\AutoController;

use Twitter;
use Carbon\Carbon;

use App\Models\Searches;
use App\Models\Tweets;

use Intervention\Image\ImageManagerStatic as Image;

class TwitterController extends AutoController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$last_search = Searches::take(1)->orderBy('created_at', 'desc')->get();
		$last_id = 10000000000;
		$max_id_str = 0;

		if ($last_search->count() != 0) {
			$last_id = $last_search->first()->tweet_id;
		}

		$results_raw = Twitter::getUserTimeline(['screen_name' => 'Article10', 'count' => 20, 'format' => 'json', 'since_id' => $last_id, 'tweet_mode' => 'extended']);

		$results  = json_decode($results_raw);

		if (count($results) > 0) {
			//dd($results);

			$insert_count = 0;

			foreach($results as $key => $value) {
				//echo $value->text . "<br/>";

				$tweet = new Tweets;
				
				$tweet->tweet_id = $value->id_str;
				$tweet->text = $value->full_text;
				$tweet->user_id = $value->user->id_str;
				if (isset($value->entities->media)) {
					$tweet->media_url = $value->entities->media[0]->media_url;
					$tweet->status = 'pending';
				} else {
					$tweet->status = 'ready';
				}
				$carbon_date = Carbon::parse($value->created_at);
				$tweet->tweeted_at = $carbon_date;

				$tweet->save();

				$insert_count++;
			}

			echo "Found " . $insert_count . " new tweets.<br/>";
			echo "New Highest ID is: " . $results[0]->id_str . "<br/>";


			$new_search = new Searches;
			$new_search->tweet_id = $results[0]->id_str;
			$new_search->save();
		}

		$this->download_images();
	}

	private function download_images()
	{

		try {
			$tweet = Tweets::where('media_url', '!=', '')->where('status','pending')->take(1)->get()->first();
			if ($tweet) {
				$url = $tweet->media_url;
				$extension = pathinfo($url, PATHINFO_EXTENSION);

				$filename = $tweet->tweet_id . '.' . $extension;
				$file = file_get_contents($url);

				if (file_exists(base_path().'\html\uploads\\'.$filename)) {
					unlink(base_path().'\html\uploads\\'.$filename);
				}
				
				$save = file_put_contents('uploads/'.$filename, $file);
				if ($save) {
					

					// open file a image resource
					$img = Image::make('uploads/'.$filename);

					$img->trim('top-left', null, 25)->rectangle(0, 0, $img->width(), $img->height(), function ($draw) {
					    $draw->background('rgba(0, 0, 0, 0.5)');
					})->fit(500)->save();


					$tweet->media_local = $filename;
					$tweet->status = 'ready';
					$tweet->save();
					echo "Processed Image: " . $filename . "<br/>";
				}
				$this->download_images();
			}
		} catch (Exception $e) {

		}
	}

}
