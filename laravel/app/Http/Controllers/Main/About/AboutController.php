<?php namespace App\Http\Controllers\Main\About;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Request;

class AboutController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['section'] = "about";
		$this->video_config		  = Config::get('video_config');
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['pageViewJS']	= 'main/about-home-min';
		$this->context['pageViewCSS'] 	= 'main/sections/about';
		$this->context['videoObj']		= $this->video_config[Request::segment(1)];
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];

		$this->context['subpage'] = '';

		return view('main.about.home', $this->context);
	}
}
