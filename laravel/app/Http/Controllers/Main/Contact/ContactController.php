<?php namespace App\Http\Controllers\Main\Contact;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Mail;
use Request;

use App\Http\Requests\Main\ContactRequest			as ContactRequest;

use App\Models\Contact 								as Contact;

class ContactController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['section'] = "contact";
		$this->video_config = Config::get('video_config');
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/contact-home-min';
		$this->context['pageViewCSS']	= 'main/sections/contact';
		$this->context['videoObj']		= $this->video_config['home'];
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];
		$this->context['subpage']		= '';

		return view('main.contact.home', $this->context);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ContactRequest $request, Contact $contact)
	{
		$contact->fill($request->all());
		// save form data if passed validation rules
		$contact->save();

		// Prepare the email to send.
		$details = array(
						'site_url'	=> $this->context['site_url'],
						'name'		=> $request['name'],
						'company'	=> $request['company'],
						'number'	=> $request['number'],
						'email'		=> $request['email'],
						'requirement' => $request['requirement'],
						'when'		=> $request['when'],
						'extra'		=> $request['extra']
					);

		$this->sendMail($details);

		$response = array('response_status' => 'success', 'message' => 'Thank you your message has been sent.');

		return redirect('contact#form')->with('response', $response);
	}

	private function sendMail($details)
	{
		Mail::send('emails.contact', $details, function($message) use($details)
		{
		    $message->from(env('SES_FROM_ADDRESS'), 'No Reply');
		   	if (App::environment() == 'production') {
		    	$message->to(env('SES_TO_ADDRESS'));
		    } else {
		    	$message->to(env('SES_TO_ADDRESS'));
		    }
		    $message->subject('Article 10 Enquiry');
		});
	}
}
