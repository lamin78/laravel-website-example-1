<?php namespace App\Http\Controllers\Main;

use App;
use Config;
use Request;
use Log;
use App\Http\Controllers\Main\MainController;
use App\Models\CuiConversationText as CUI_Text;
use App\Models\CuiConversationOptions as CUI_Options;

class ConversationUIController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section']	= "home";
		$this->video_config = Config::get('video_config');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * Optimize initial data object to have mulitple text nodes until first choice node encountered
	 *
	 * @return Response
	 */
	public function index($source = 'none')
	{

		$this->context['pageViewJS']	= 'main/cui-app-min';
		$this->context['pageViewCSS']	= 'main/sections/cui';
		$this->context['videoObj'] 		= $this->video_config['home'];
		$this->context['source'] 			= $source;
		$this->context['show'] 				= true;

		return view('main.home.cui', $this->context);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getNextLine(CUI_Text $cuiText, CUI_Options $optionsArray, $id)
	{
		$cui = $cuiText->where('index', $id)->first();

		// dd($cui);

		if(isset($cui)) {

			// $cui->conversationText;
			$options = $cui->options;

			// Log::info($options);
			// dd($cui);
			// dd($options);

			$data['cui']['conversationText'] = $cui->conversation_text;

			$data['cui']['options'] = [];
			foreach($options->toArray() as $key=>$value) {
				if($value['type'] === 'link') {
					$optionLink = $optionsArray->where('id', $value['id'])->first();
					// $value['linkData'] = $optionLink->links->toArray();
					// Log::info("Out ID");
					// Log::info("=======================================================");
					// Log::info($value);
					$value['linkData'] = ['link_text'=>$value['out_id']];
				}
				// dd($value);

				$data['cui']['options'][] = $value;
			}

			Log::info("options");
			Log::info("==============================================");
			Log::info($data['cui']['options']);
		}
		else {
			$data['empty'] = true;
		}

		// dd($data);

		return response()->json($data);
	}

	public function storeInput()
	{
		# code...
	}
}
