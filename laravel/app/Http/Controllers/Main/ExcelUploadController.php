<?php namespace App\Http\Controllers\Main;

use Requests;
use Excel;
use DB;
use Log;
use App\Http\Controllers\Main\MainController;
use App\Models\CuiConversationText;
use App\Models\CuiConversationTextOptionsLinkages;
use App\Models\CuiConversationOptions;
use App\Models\CuiConversationUrlData;

class ExcelUploadController extends MainController {

  public function index(
    CuiConversationText $cuiConversationText,
    CuiConversationTextOptionsLinkages $cuiConversationTextOptionsLinkages,
    CuiConversationOptions $cuiConversationOptions)
  {

    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    $cuiConversationText->truncate();
    $cuiConversationTextOptionsLinkages->truncate();
    $cuiConversationOptions->truncate();

    $csv = 'html/csv-upload/cui-data.csv';

    Log::info('');
    Log::info('parsing start');
    Log::info('======================================>>>');

    Excel::load($csv, function($reader) use($cuiConversationText, $cuiConversationTextOptionsLinkages, $cuiConversationOptions) {

      $results = $reader->all();
      $convesationText = array();
      $reader->each(function($row) use($convesationText, $cuiConversationText, $cuiConversationTextOptionsLinkages, $cuiConversationOptions) {

        $cuiTextId = $cuiConversationText->insertGetId(['index'=>$row->id, 'conversation_text'=> $row->text]);

        if(isset($row->go_to_line) && $row->go_to_line !== NULL) {
          Log::info("Goto Line");
          Log::info("===========================================================");
          Log::info($row->go_to_line);
          Log::info("  ");

          $cuiOptId = $cuiConversationOptions->insertGetId(['out_id'=>strtolower(str_replace(' ', '',trim($row->go_to_line))), 'type'=>'goto']);
          $cuiConversationTextOptionsLinkages->insertGetId(['cid'=>$cuiTextId, 'opid'=>$cuiOptId]);
        }
        else {
          $option = array();
          $out = array();
          foreach($row as $key => $value) {
            if(isset($value) && strpos($key, 'option_') !== false)
              array_push($option, $value);
            if(isset($value) && strpos($key, 'out_') !== false)
              array_push($out, $value);
          }

          foreach($option as $key => $value) {
            Log::info("Option");
            Log::info("===========================================================");
            Log::info($value);
            Log::info($out[$key]);
            Log::info("  ");
            if(strpos($value, 'input') !== false) {
              $opt0 = 'input';
              $value = str_replace(' ', '', $value);
              $value = str_replace('input|', '', $value);
              Log::info($value);
              $opt1 = $value;
            }
            else {
              if(strpos($value, 'condition') === false) {
                $opt = explode('|', $value);
                $opt0 = trim($opt[0]);
                $opt1 = trim($opt[1]);
              }
              else {
                $value = str_replace(' ', '', $value);
                $opt = explode('|', $value);
                $opt0 = trim($opt[0]);
                $patterns = array('/button\|/', '/link\|/');
                $opt1 = preg_replace($patterns, '', $value);
                Log::info($opt1);
              }
            }

            // ERROR checks
            if(isset($opt0) && isset($opt1) && isset($key)) {
              if(!isset($opt0) || !isset($opt1) || !isset($out[$key])) {
                Log::info($row);
              }

              $cuiOptId = $cuiConversationOptions->insertGetId(['option_text'=>$opt1, 'out_id'=> strtolower(str_replace(' ', '',trim($out[$key]))), 'type'=>strtolower(trim($opt0))]);
              $cuiConversationTextOptionsLinkages->insertGetId(['cid'=>$cuiTextId, 'opid'=>$cuiOptId]);
            }
            else {
              Log::info($row);
            }
          }
        }
        echo 'CSV row:'.$row->id.' parse successful<br/>';
      });
    });

    Log::info('parsing end');
    Log::info('======================================>>>');
    Log::info('');

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
}
