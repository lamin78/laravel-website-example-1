<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Request;
use Carbon\Carbon;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->context['section']	= "home";
		$this->case_studies_banner	= Config::get('case_studies');
		$this->carousel_config		= Config::get('carousel_config');
		$this->client_config		= Config::get('client_config');
		$this->video_config			= Config::get('video_config');
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index($lang = null)
	{
		$this->context['pageViewJS']	= 'main/home-min';
		$this->context['pageViewCSS']	= 'main/sections/home';
		$this->context['caseStudies']	= $this->case_studies_banner['home'];
		$this->context['corouselArray'] = $this->carousel_config['home'];
		$this->context['videoObj'] 		= $this->video_config['home'];
		$this->context['metaData'] 		= $this->meta_config['home'];
		$this->context['clientsArray']  = $this->client_config['clients'];
		$this->context['subpage']		= '';

		// dd($this->context);

		return view('main.home.home', $this->context);
	}
}
