<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Config;
use App;

use App\Http\Helpers\DBIP;

class MainController extends Controller {
	
	public function __construct() {
		
		$this->context['section'] = "";
		$this->meta_config = Config::get('meta_config');

		$this->context = array(
			'page_css_array' 	=> []
			,'pageViewJS'		=> ""
			,'pageViewCSS'		=> ""
			,'site_url'			=> url('',[])
		);

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}
	}
}