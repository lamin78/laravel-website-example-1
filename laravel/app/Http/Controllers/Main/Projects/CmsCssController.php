<?php namespace App\Http\Controllers\Main\Projects;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Models\CMS_Case_Study;
// use App\Models\CMS_Carousel;
use Log;
use Session;

class CmsCssController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$csSessionData = Session::get('session_data_css');
		return response()
						->view('main.projects._partials.cms_style', $csSessionData)
						->header('Content-Type', 'text/css');
	}
}
