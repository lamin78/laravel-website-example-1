<?php namespace App\Http\Controllers\Main\Projects;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Request;
use Log;
use Session;

use App\Models\CMS_Case_Study;

class ProjectsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(CMS_Case_Study $cmsCaseStudy)
	{
		parent::__construct();

		$this->context['section']	= "projects";
		$this->colours							= Config::get('colours');
		$this->case_studies 				= Config::get('projects');
		$this->projects_banner			= Config::get('projects_banner');
		$this->carousel_config			= Config::get('carousel_config');
		$this->case_studies_banner	= Config::get('case_studies');
		$this->video_config					= Config::get('video_config');
		$this->alias_config					= Config::get('alias_config');
		$this->cmsCaseStudy					= $cmsCaseStudy;
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/projects-home-min';
		$this->context['pageViewCSS']	= 'main/sections/projects';
		// $this->context['videoObj'] 		= $this->video_config['home'];
		$this->context['videoObj'] 		= (isset($this->video_config[Request::segment(1)]) ? $this->video_config[Request::segment(1)] : $this->video_config['home']);
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];
		$this->context['subpage']		= '';

		return view('main.projects.home', $this->context);
	}

	/**
	 * [view description]
	 * @param  [type]         $casestudy      [description]
	 * @param  CMS_Case_Study $cms_Case_Study [description]
	 * @return [type]                         [description]
	 */
	public function view($casestudy, CMS_Case_Study $cms_Case_Study)
	{

		$pageName = (isset($this->alias_config[Request::segment(2)]) ? $this->alias_config[Request::segment(2)] : $casestudy);

		if (array_key_exists($pageName, $this->case_studies)) {

			$this->calc_prev_next($pageName);

			$case_study_details							= $this->case_studies[$pageName];
			$this->context['bannerArray']		= (isset($this->projects_banner[$pageName]) ? $this->projects_banner[$pageName] : $this->projects_banner['omni-channel']);
			$this->context['corouselArray'] = (isset($this->carousel_config[$pageName]) ? $this->carousel_config[$pageName] : []);
			$this->context['caseStudies']		= (isset($this->case_studies_banner[$pageName]) ? $this->case_studies_banner[$pageName] : []);
			$this->context['videoObj'] 			= (isset($this->video_config[$pageName]) ? $this->video_config[$pageName][0] : [$this->video_config['home']]);
			$this->context['videoPageObj']	= (isset($this->video_config[$pageName]) ? $this->video_config[$pageName] : []);
			$this->context['metaData']			= (isset($this->meta_config[$pageName]) ? $this->meta_config[$pageName] : $this->meta_config['home']);

			$colour = "";

			if (Session('service') !== NULL) {
				if (array_key_exists(Session('service'),$this->colours)) {
					$colour = $this->colours[Session('service')];
				}
			}

			if((array_key_exists($casestudy,$this->colours))) {
				$colour = $this->colours[$casestudy];
			}
			else if (!in_array($colour,$case_study_details["colours"])) {
				$colour = $case_study_details["colours"][0];
			}

			$this->context['colour']	= $colour;
			$this->context['imageDir']	= $pageName;
			$this->context['potm'] = $case_study_details["potm"];
			$this->context['noProjectNav']		= false;
			$this->context['pageViewJS']  = 'main/projects-view-min';
			$this->context['pageViewCSS'] = 'main/sections/projects';

			return view('main.projects.view-'.$pageName, $this->context);

		} else {

			$cms_Case_Study = $cms_Case_Study
													->where('casestudy_slug', $pageName)
													->where('status', 'live')
													->first();


			$this->calc_prev_next($pageName);

			if(isset($cms_Case_Study)) {

				$cms_Case_Study->textfield;
				$cms_Case_Study->parallax;
				$cms_Case_Study->slide;
				$cms_Case_Study->video;
				$cms_Case_Study->carousel;
				$cms_Case_Study->order;

				$this->context['imageDir']				= $cms_Case_Study->casestudy_slug;
				$this->context['colour']					= "";
				$this->context['section']					= "home";
				$this->context['pageViewJS'] 			= 'main/projects-view-min';
				$this->context['pageViewCSS'] 		= 'main/sections/projects';
				$this->context['cmsData'] 				= $cms_Case_Study;
				$this->context['site_url']				= url('',[]);
				$this->context['bannerArray']			= [];
				$this->context['cmsPageViewCSS']	= true;
				$this->context['potm'] 						= false;
				$this->context['noFollow'] 				= false;
				$this->context['noProjectNav']		= false;

				$carouselArray = $cms_Case_Study->carousel->groupBy('csid')->all();

				$count = 0;
				$imgOuterArr = array();
				foreach($carouselArray as $key => $array) {
					$imgArr = array();
					foreach($array as $k => $arr) {
						$imgArr[] = $arr['image'];
					}
					$imgOuterArr[] = $imgArr;
					$this->context['carouselCMSArray'][] = [
						'container'					=> 'case-'.$count
			 			,'resizeFlag'				=> false
			 			,'fullFlag'					=> true
			 			,'fullHeightCms'		=> true
			 			,'fadeTrans'				=> false
			 			,'playWhenVisible'	=> false
			 			,'imageRoute'				=> '/images/cms-case-studies/'.$cms_Case_Study->casestudy_slug
			 			,'imageArray'				=> $imgArr
					];

					$count++;
				}

				// Store css session for use in page
				Session::forget('session_data_css');
				Session::put('session_data_css', [
					'imageDir' => $cms_Case_Study->casestudy_slug,
					'headerImageSmall' => str_replace('.', '@0.3x.', $cms_Case_Study->casestudy_header_image),
					'headerImage' => $cms_Case_Study->casestudy_header_image,
					'headerImageLarge' => str_replace('.', '@2x.', $cms_Case_Study->casestudy_header_image),
					'parallaxArray' => $cms_Case_Study->parallax,
					'carouselArray' => $imgOuterArr,
				]);

				$this->context['caseStudies']		= [];
				$this->context['videoObj'] 			= [];
				$this->context['videoPageObj']	= [];
				$this->context['metaData']			= [];

				// dd($cms_Case_Study->toArray());
				return view('main.projects.view-cms-project-basic', $this->context);
			}
			else {
				return redirect('/');
			}
		}
	}

	/**
	 * [preview description]
	 * @param  [type] $hash [description]
	 * @return [type]       [description]
	 */
	public function preview($hash, CMS_Case_Study $cmsCaseStudy)
	{
		$cms_Case_Study = $cmsCaseStudy->where('casestudy_hashed_url', $hash)->first();
		$cms_Case_Study->textfield;
		$cms_Case_Study->parallax;
		$cms_Case_Study->slide;
		$cms_Case_Study->video;
		$cms_Case_Study->carousel;
		$cms_Case_Study->order;

		$this->context['imageDir']				= $cms_Case_Study->casestudy_slug;
		$this->context['colour']					= "";
		$this->context['section']					= "home";
		$this->context['pageViewJS'] 			= 'main/projects-view-min';
		$this->context['pageViewCSS'] 		= 'main/sections/projects';
		$this->context['cmsData'] 				= $cms_Case_Study;
		$this->context['site_url']				= url('',[]);
		$this->context['bannerArray']			= [];
		$this->context['cmsPageViewCSS']	= true;
		$this->context['potm'] 						= false;
		$this->context['noFollow'] 				= true;
		$this->context['noProjectNav']		= true;

		// Log::info('CAROUSEL DATA ========================================');
		// Log::info($cms_Case_Study->carousel->groupBy('csid')->toArray());
		$carouselArray = $cms_Case_Study->carousel->groupBy('csid')->all();

		$count = 0;
		$imgOuterArr = array();
		foreach($carouselArray as $key => $array) {
			$imgArr = array();
			foreach($array as $k => $arr) {
				$imgArr[] = $arr['image'];
			}
			$imgOuterArr[] = $imgArr;
			$this->context['carouselCMSArray'][] = [
				'container'					=> 'case-'.$count
	 			,'resizeFlag'				=> false
	 			,'fullFlag'					=> true
	 			,'fullHeightCms'		=> true
	 			,'fadeTrans'				=> false
	 			,'playWhenVisible'	=> false
	 			,'imageRoute'				=> '/images/cms-case-studies/'.$cms_Case_Study->casestudy_slug
	 			,'imageArray'				=> $imgArr
			];

			Log::info($this->context['carouselCMSArray']);

			$count++;
		}

		// Store css session for use in page
		Session::forget('session_data_css');
		Session::put('session_data_css', [
			'imageDir' => $cms_Case_Study->casestudy_slug,
			'headerImageSmall' => str_replace('.', '@0.3x.', $cms_Case_Study->casestudy_header_image),
			'headerImage' => $cms_Case_Study->casestudy_header_image,
			'headerImageLarge' => str_replace('.', '@2x.', $cms_Case_Study->casestudy_header_image),
			'parallaxArray' => $cms_Case_Study->parallax,
			'carouselArray' => $imgOuterArr,
		]);

		$this->context['caseStudies']		= [];
		$this->context['videoObj'] 			= [];
		$this->context['videoPageObj']	= [];
		$this->context['metaData']			= [];

		// dd($cms_Case_Study->toArray());
		return view('main.projects.view-cms-project-basic', $this->context);
	}

	/**
	 * [calc_prev_next description]
	 * @param  [type] $casestudy [description]
	 * @return [type]            [description]
	 */
	private function calc_prev_next($casestudy)
	{
		$prev = null;
		$next = null;
		$current = null;

		reset($this->case_studies);

		Log::info('CASE STUDIES ========================================');
		Log::info($casestudy);

		Log::info('PROJECT SEQUENCE ========================================');
		$result = $this->cmsCaseStudy
								->select('casestudy_slug')
								->where('status', 'live')
								->get();
		// Log::info($result);

		$newArr = array();
		foreach($result as $key => $project) {
			$newArr[$project['casestudy_slug']] = [
				"colours" => ["pink"],
				"potm" => false
			];
		}

		$this->case_studies = array_merge($this->case_studies, $newArr);

		// Log::info('NEW ARRAY ========================================');
		// Log::info($this->case_studies);

		while ( !is_null($key = key($this->case_studies)) ) {
		    if ($key == $casestudy) {
		    	$current = key($this->case_studies);
		    	prev($this->case_studies);
		    	$prev = key($this->case_studies);

		    	// We were actually at the start so we probably just went to -1;
		    	if (is_null($prev)) {
		    		reset($this->case_studies);
		    		next($this->case_studies);
		    	} else {
		    		next($this->case_studies);
		    		next($this->case_studies);
		    	}
		    	$next = key($this->case_studies);
		    	break;
		    }
		    next($this->case_studies);
		}

		if (is_null($next)) {
			reset($this->case_studies);
			$next = key($this->case_studies);
		}

		if (is_null($prev)) {
			end($this->case_studies);
			$prev = key($this->case_studies);
		}

		Log::info('PREV ========================================');
		Log::info($prev);
		Log::info('NEXT ========================================');
		Log::info($prev);
		Log::info('CURRENT ========================================');
		Log::info($prev);

		$this->context["nav_urls"] = [
										"prev" => "/projects/" . $prev,
										"current" => "/projects/" . $current,
										"next" => "/projects/" . $next
									];

		//dd($this->context["nav_urls"]);
	}

}
