<?php namespace App\Http\Controllers\Main\Services;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Request;

class ServicesController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['section']		= "services";
		$this->client_config				= Config::get('client_config');
		$this->carousel_config			= Config::get('carousel_config');
		$this->case_studies_banner	= Config::get('case_studies');
		$this->colours							= Config::get('colours');
		$this->video_config					= Config::get('video_config');
	}

	/**
	 * Show the services home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/services-home-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['subpage']		= '';
		$this->context['clientsArray']  = $this->client_config['clients'];
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];

		return view('main.services.home', $this->context);
	}


	/**
	 * Show the services presentation solutions screen to the user.
	 *
	 * @return Response
	 */
	public function presentations()
	{

		$this->context['pageViewJS']		= 'main/services-section-min';
		$this->context['pageViewCSS']		= 'main/sections/services';
		$this->context['caseStudies']		= $this->case_studies_banner[Request::segment(2)];
		$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']				= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 			= $this->video_config[Request::segment(2)];
		$this->context['clientsArray']  = $this->client_config['clients'];
		$this->context['subpage']				= Request::segment(2);
		$this->context['metaData']			= $this->meta_config[Request::segment(2)];

		return view('main.services.presentations', $this->context);
	}

	/**
	 * Show the services digital screen to the user.
	 *
	 * @return Response
	 */
	public function digital()
	{
		$this->context['pageViewJS']	= 'main/services-section-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['caseStudies']	= $this->case_studies_banner[Request::segment(2)];
		//$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']		= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 		= $this->video_config['default'];
		$this->context['subpage']		= Request::segment(2);
		$this->context['metaData']		= $this->meta_config[Request::segment(2)];

		return view('main.services.digital', $this->context);
	}

	/**
	 * Show the services infosec screen to the user.
	 *
	 * @return Response
	 */
	public function infosec()
	{
		$this->context['pageViewJS']	= 'main/services-section-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['caseStudies']	= $this->case_studies_banner[Request::segment(2)];
		//$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']		= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 		= $this->video_config['default'];
		$this->context['subpage']		= Request::segment(2);
		$this->context['metaData']		= $this->meta_config[Request::segment(2)];

		return view('main.services.infosec', $this->context);
	}

	/**
	 * Show the services motion screen to the user.
	 *
	 * @return Response
	 */
	public function motion()
	{

		$this->context['pageViewJS']	= 'main/services-section-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['caseStudies']	= $this->case_studies_banner[Request::segment(2)];
		//$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']			= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 		= $this->video_config[Request::segment(2)];
		$this->context['subpage']			= Request::segment(2);
		$this->context['metaData']		= $this->meta_config[Request::segment(2)];

		return view('main.services.motion', $this->context);
	}

	/**
	 * Show the services events screen to the user.
	 *
	 * @return Response
	 */
	public function events()
	{
		$this->context['pageViewJS']	= 'main/services-section-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['caseStudies']	= $this->case_studies_banner[Request::segment(2)];
		//$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']		= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 		= $this->video_config[Request::segment(2)];
		$this->context['subpage']		= Request::segment(2);
		$this->context['metaData']		= $this->meta_config[Request::segment(2)];

		return view('main.services.events', $this->context);
	}

	/**
	 * Show the services campaigns screen to the user.
	 *
	 * @return Response
	 */
	public function campaigns()
	{
		$this->context['pageViewJS']	= 'main/services-section-min';
		$this->context['pageViewCSS']	= 'main/sections/services';
		$this->context['caseStudies']	= $this->case_studies_banner[Request::segment(2)];
		$this->context['corouselArray'] = $this->carousel_config[Request::segment(2)];
		$this->context['colour']		= $this->colours[Request::segment(2)];
		$this->context['videoObj'] 		= $this->video_config['default'];
		$this->context['subpage']		= Request::segment(2);
		$this->context['metaData']		= $this->meta_config[Request::segment(2)];

		return view('main.services.campaigns', $this->context);
	}

	public function redirect($service,$casestudy) {
		return redirect('projects/'.$casestudy)->with('service', $service);
	}
}
