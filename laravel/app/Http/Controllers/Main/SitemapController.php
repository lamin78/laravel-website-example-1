<?php namespace App\Http\Controllers\Main;

use App;
use URL;
use App\Http\Requests;
use App\Http\Controllers\Main\MainController;
use Illuminate\Http\Request;
use Config;
use Carbon\Carbon as Carbon;

class SitemapController extends MainController {

	public function __construct() {
		
		$this->projects = Config::get('projects');
	}
	
	public function index() {
		
		$sitemap = App::make("sitemap");	
		//$sitemap->setCache('laravel.sitemap', 1);

		//if (!$sitemap->isCached()) {
			$date = Carbon::now();
			$projects = $this->projects;
			
			$sitemap->add(URL::to('/'), $date, '1.0', 'daily');
			
			$sitemap->add(URL::to('/projects'), $date, '0.9', 'monthly');
			foreach ($projects as $project => $key) {
				$sitemap->add(URL::to('/projects/'.$project), $date, '0.9', 'monthly');
			}
			
			$sitemap->add(URL::to('/services/presentation-solutions'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services/digital-solutions'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services/information-security'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services/video-animation-3d'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services/exhibitions'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services/campaigns'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/services'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/about'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/social'), $date, '0.9', 'monthly');
			$sitemap->add(URL::to('/contact'), $date, '0.9', 'monthly');
			
			//$images = [
			//	['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
			//];
			//$sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'monthly', $images);

			//$tweets = DB::table('tweets')->orderBy('created_at', 'desc')->get();
			//
			//foreach ($tweet as $tweets) {
			//	$sitemap->add($post->slug, $post->modified, $post->priority, $post->freq);
			//}
		//}

		return $sitemap->render('xml');
	}
}
