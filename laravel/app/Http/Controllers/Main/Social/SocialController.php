<?php namespace App\Http\Controllers\Main\Social;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Request;

use App\Models\Tweets;
use App\Models\VimeoVids;
use App\Models\LinkedIn;
use App\Models\Potm;
use App\Models\SocialImages;
use App\Models\Pinterest;

class SocialController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['section'] = "social";
		$this->video_config = Config::get('video_config');
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->context['pageViewJS']	= 'main/social-home-min';
		$this->context['pageViewCSS']	= 'main/sections/social';
		$this->context['videoObj']		= $this->video_config['home'];
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];
		$this->context['subpage']		= '';

		// Fetch 12 Tweest from our database, we'll use a minimum of 5.
		//
		$this->context['tweets'] = Tweets::where('status','ready')->take(12)->orderby("tweeted_at", "desc")->get();

		$this->context['vids'] = VimeoVids::where('status','ready')->take(2)->orderby("created_time", "desc")->get();

		$this->context['linkedins'] = LinkedIn::where('status','ready')->take(2)->orderby("created_at", "desc")->get();

		$this->context['potm'] = Potm::where('status','ready')->take(1)->orderby("created_at", "desc")->get();

		$this->context['pinterest'] = Pinterest::where('status','ready')->take(1)->orderby("created_at", "desc")->get();

		$this->context['socialimages'] = SocialImages::where('status','ready')->take(2)->orderby("created_at", "desc")->get();


		return view('main.social.home', $this->context);
	}
}
