<?php namespace App\Http\Controllers\Main\tERMS;

use App\Http\Controllers\Main\MainController;

use App;
use Config;
use Mail;
use Request;

class TermsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/terms-home-min';
		$this->context['pageViewCSS']	= 'main/sections/terms';
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];
		$this->context['subpage']		= '';
		$this->context['section'] = "terms";

		return view('main.terms.home', $this->context);
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function privacy()
	{
		$this->context['pageViewJS']	= 'main/terms-home-min';
		$this->context['pageViewCSS']	= 'main/sections/terms';
		$this->context['metaData']		= $this->meta_config[Request::segment(1)];
		$this->context['subpage']		= '';
		$this->context['section'] = "terms";

		return view('main.terms.privacy', $this->context);
	}
}
