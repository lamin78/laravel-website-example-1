<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Helpers\ClientsHelper;
use Validator;
use Request;

class CaseStudyRequest extends FormRequest {

	public function __construct() {}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			'casestudy_title'			=> 'required|min:2|max:128',
			'casestudy_slug'			=> 'required|min:2|max:128',
			'casestudy_subtitle'	=> 'required|min:2|max:255',
			'casestudy_category'	=> 'required|min:2|max:128'
		];
	}
}
