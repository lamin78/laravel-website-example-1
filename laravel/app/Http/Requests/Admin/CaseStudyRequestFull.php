<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Helpers\ClientsHelper;
use Validator;
use Request;

class CaseStudyRequestFull extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			'header_image' 				 		=> 'mimes:png,jpeg,jpg|max:5000',
			'casestudy_title'					=> 'required|min:2|max:128',
			'casestudy_slug'					=> 'required|min:2|max:128',
			'casestudy_subtitle'			=> 'required|min:2|max:255',
			'casestudy_header'				=> 'required|min:2|max:128',
			'casestudy_sub_header'		=> 'required|min:2|max:128',
			'casestudy_client'				=> 'required|min:2|max:128',
			'casestudy_year'					=> 'required|min:2|max:128',
			'casestudy_location'			=> 'required|min:2|max:128',
			'casestudy_category'			=> 'required|min:2|max:128',
			'status'									=> 'required|min:2|max:128',
		];
	}
}
