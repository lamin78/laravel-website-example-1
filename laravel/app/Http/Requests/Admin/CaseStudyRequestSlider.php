<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Helpers\ClientsHelper;
use Validator;
use Request;

class CaseStudyRequestSlider extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			'slide_1_holder' => 'required|mimes:png,jpeg,jpg|max:5000',
			'slide_2_holder' => 'required|mimes:png,jpeg,jpg|max:5000',
		];
	}
}
