<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class PinterestRequest extends FormRequest {
	
	public function __construct() {
		
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		return [
			'pin_id'			=> 'required|string|max:255',
			'note'				=> 'required|string|max:255',
			'link'				=> 'required|string|max:255',
			'media_url'      	=> 'required|string|max:255'
		];
	}

}
