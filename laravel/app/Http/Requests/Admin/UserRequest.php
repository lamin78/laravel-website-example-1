<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Helpers\ClientsHelper;
use Validator;
use Request;

class UserRequest extends FormRequest {

	public function __construct() {}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		return [
			'name'			=> 'required|min:2|max:80',
			'email'			=> 'required|between:3,64|email',
			'password'	=> 'confirmed|min:8',
			'active'		=> 'required|boolean'
		];
	}

}
