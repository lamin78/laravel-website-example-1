<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class ContactRequest extends FormRequest {
	
	public function __construct() {
		
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		return [
			'name'			=> 'required|min:2|max:80',
			'company'		=> 'required',
			'number'		=> 'required',
			'email'			=> 'required|email',
			'requirement'	=> 'required',
			'when'			=> 'required',
			'extra'			=> ''
		];
	}

}
