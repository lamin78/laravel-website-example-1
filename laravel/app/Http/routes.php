<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use JonnyW\PhantomJs\Client;

Route::group(['prefix' => 'admin'], function() {

	Route::get('/', function() {
		return redirect('admin/summary');
	});

	Route::controllers([
		'auth'		=> 'Admin\Auth\AuthController',
		'password'	=> 'Admin\Auth\PasswordController',
	]);

	Route::resource('users', 'Admin\Users\UserController');

	/* Case Study Route */
	Route::resource('case-study', 'Admin\CaseStudy\CaseStudyController');

	// Route::get(
	// 	'case-study/preview/{hash}',
	// 	array(
	// 		 'as'	=> 'case-study-preview.{hash}'
	// 		,'uses'	=> 'Admin\CaseStudy\CaseStudyController@preview'
	// 	)
	// );

	Route::post(
		'case-study/generate-slug',
		array(
			 'as'	=> 'case-study-generate-slug'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@generateSlug'
		)
	);

	Route::post(
		'case-study/add-section',
		array(
			 'as'	=> 'case-study-add-section'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@addSection'
		)
	);
	Route::post(
		'case-study/delete-section',
		array(
			'as'	=> 'case-study-delete-section'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@deleteSection'
		)
	);
	Route::post(
		'case-study/update-order/{id}',
		array(
			 'as'	=> 'case-study-update-order.{id}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateOrder'
		)
	);
	Route::post(
		'case-study/update-order-ident',
		array(
			 'as'	=> 'case-study-update-order'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateOrderIdent'
		)
	);
	Route::post(
		'case-study/edit-status',
		array(
			 'as'	=> 'case-study-edit-status'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editStatus'
		)
	);

	// HEADER ROUTE
	Route::get(
		'case-study/edit-header/{id}',
		array(
			'as'	=> 'case-study-edit-header.{id}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editHeader'
		)
	);
	Route::put(
		'case-study/update-header/{id}',
		array(
			 'as'	=> 'case-study-update-header.{id}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateHeader'
		)
	);

	// STATISTICS ROUTE
	Route::get(
		'case-study/edit-statistics/{id}',
		array(
			'as'	=> 'case-study-edit-statistics.{id}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editStatistics'
		)
	);
	Route::put(
		'case-study/update-statistics/{id}',
		array(
			 'as'	=> 'case-study-update-statistics.{id}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateStatistics'
		)
	);

	// TEXT AREA ROUTE
	Route::get(
		'case-study/edit-textarea/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-textarea.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editTextArea'
		)
	);
	Route::put(
		'case-study/update-textarea/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-textarea-update.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateTextArea'
		)
	);

	// PARALLAX ROUTE
	Route::get(
		'case-study/edit-parallax/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-parallax.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editParallax'
		)
	);
	Route::put(
		'case-study/update-parallax/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-update-parallax.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateParallax'
		)
	);

	// SLIDER ROUTE
	Route::get(
		'case-study/edit-slider/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-slider.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editSlider'
		)
	);
	Route::put(
		'case-study/update-slider/{cid}/{sid}',
		array(
			'as'	=> 'case-study-update-slider.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateSlider'
		)
	);

	// VIDEO ROUTE
	Route::get(
		'case-study/edit-video/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-video.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editVideo'
		)
	);
	Route::put(
		'case-study/update-video/{cid}/{sid}',
		array(
			'as'	=> 'case-study-update-video.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateVideo'
		)
	);

	// CAROUSEL ROUTE
	Route::get(
		'case-study/edit-carousel/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-edit-carousel.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@editCarousel'
		)
	);

	Route::post(
		'case-study/add-carousel-image',
		array(
			 'as'	=> 'case-study-add-carousel.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@addCarousel'
		)
	);

	Route::post(
		'case-study/update-theme',
		array(
			 'as'	=> 'update-theme.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateTheme'
		)
	);

	Route::put(
		'case-study/update-carousel-image/{cid}/{sid}',
		array(
			 'as'	=> 'case-study-update-carousel.{cid}.{sid}'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateCarousel'
		)
	);

	Route::post(
		'case-study/update-carousel-order',
		array(
			 'as'	=> 'case-study-update-carousel-order'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@updateCarouselOrder'
		)
	);

	Route::post(
		'case-study/delete-carousel-section',
		array(
			 'as'	=> 'case-study-delete-carousel-section'
			,'uses'	=> 'Admin\CaseStudy\CaseStudyController@deleteCarouseSectionCarousel'
		)
	);


	Route::get('summary', 'Admin\Summary\SummaryController@index');

	Route::group(['prefix' => 'social'], function() {

		Route::get('twitter', 'Admin\Social\TwitterController@index');
		Route::post('twitter/disable/{id}', 'Admin\Social\TwitterController@disable');
		Route::post('twitter/enable/{id}', 'Admin\Social\TwitterController@enable');
		Route::delete('twitter/destroy/{id}', 'Admin\Social\TwitterController@destroy');

		Route::get('vimeo', 'Admin\Social\VimeoController@index');
		Route::post('vimeo/store', 'Admin\Social\VimeoController@store');
		Route::post('vimeo/disable/{id}', 'Admin\Social\VimeoController@disable');
		Route::post('vimeo/enable/{id}', 'Admin\Social\VimeoController@enable');
		Route::delete('vimeo/destroy/{id}', 'Admin\Social\VimeoController@destroy');

		Route::get('linkedin', 'Admin\Social\LinkedInController@index');
		Route::get('linkedin/create', 'Admin\Social\LinkedInController@create');
		Route::post('linkedin/store', 'Admin\Social\LinkedInController@store');
		Route::post('linkedin/disable/{id}', 'Admin\Social\LinkedInController@disable');
		Route::post('linkedin/enable/{id}', 'Admin\Social\LinkedInController@enable');
		Route::delete('linkedin/destroy/{id}', 'Admin\Social\LinkedInController@destroy');

		Route::get('pinterest', 'Admin\Social\PinterestController@index');
		Route::post('pinterest/store', 'Admin\Social\PinterestController@store');
		Route::post('pinterest/disable/{id}', 'Admin\Social\PinterestController@disable');
		Route::post('pinterest/enable/{id}', 'Admin\Social\PinterestController@enable');
		Route::delete('pinterest/destroy/{id}', 'Admin\Social\PinterestController@destroy');

		Route::get('potm', 'Admin\Social\PotmController@index');
		Route::get('potm/create', 'Admin\Social\PotmController@create');
		Route::post('potm/store', 'Admin\Social\PotmController@store');
		Route::post('potm/disable/{id}', 'Admin\Social\PotmController@disable');
		Route::post('potm/enable/{id}', 'Admin\Social\PotmController@enable');
		Route::delete('potm/destroy/{id}', 'Admin\Social\PotmController@destroy');

		Route::get('images', 'Admin\Social\ImagesController@index');
		Route::get('images/create', 'Admin\Social\ImagesController@create');
		Route::post('images/store', 'Admin\Social\ImagesController@store');
		Route::post('images/disable/{id}', 'Admin\Social\ImagesController@disable');
		Route::post('images/enable/{id}', 'Admin\Social\ImagesController@enable');
		Route::delete('images/destroy/{id}', 'Admin\Social\ImagesController@destroy');
	});
});

Route::get('/', 'Main\HomeController@index');

Route::get('/cui', 'Main\ConversationUIController@index');
Route::get('/cui/upload-csv', 'Main\ExcelUploadController@index');
Route::get('/cui/{source}', 'Main\ConversationUIController@index');
Route::get('/cui/get-next-line/{id}', 'Main\ConversationUIController@getNextLine'); // CREATE SOCIAL MEDIA END POINTS

Route::get('/projects',				'Main\Projects\ProjectsController@index');
Route::get('/projects/{casestudy}', 'Main\Projects\ProjectsController@view');
Route::get('/css/cms_style/{id}', 'Main\Projects\CmsCssController@index');
Route::get(
	'projects/preview/{hash}',
	array(
		 'as'		=> 'case-study-preview.{hash}'
		,'uses'	=> 'Main\Projects\ProjectsController@preview'
	)
);
// SCREEN CAPTURE
Route::get('/screenshot', function() {

	$url = Input::get('url');
	$width  = 1280;
  $height = 6000;

	$client = Client::getInstance();
	$client->getEngine()->setPath(dirname(dirname(__FILE__)).'../../bin/phantomjs.exe');
	$client->isLazy();

  $request  = $client->getMessageFactory()->createCaptureRequest($url);
	$request->setViewportSize($width, $height);
	$file = '../bin/file.jpg';
	$request->setOutputFile($file);
	$request->setTimeout(30000);

  $response = $client->getMessageFactory()->createResponse();

  $client->send($request, $response);

	rename('../bin/file.jpg', 'screenshots/file.jpg');

	echo '<img src="screenshots/file.jpg" />';
});

Route::get('/services',							'Main\Services\ServicesController@index');
Route::get('/services/presentation-solutions',	'Main\Services\ServicesController@presentations');
Route::get('/services/digital-solutions',		'Main\Services\ServicesController@digital');
Route::get('/services/digital',					function(){
	return redirect('/services/digital-solutions');
});
Route::get('/services/information-security',	'Main\Services\ServicesController@infosec');
Route::get('/services/video-animation-3d',		'Main\Services\ServicesController@motion');
Route::get('/services/exhibitions',				'Main\Services\ServicesController@events');
Route::get('/services/campaigns',				'Main\Services\ServicesController@campaigns');

Route::get('/services/{service}/view/{casestudy}', 'Main\Services\ServicesController@redirect');

Route::get('/about', 'Main\About\AboutController@index');

Route::get('/social', 'Main\Social\SocialController@index');

Route::get('/contact',	'Main\Contact\ContactController@index');
Route::post('/contact', 'Main\Contact\ContactController@store');

Route::get('/terms-of-business',	'Main\Terms\TermsController@index');
Route::get('/privacy-notice',	'Main\Terms\TermsController@privacy');

Route::group(['prefix' => 'auto'], function() {
	Route::get('/twitter', 'Auto\TwitterController@index');
});

Route::get('sitemap', 'Main\SitemapController@index');

/**
 * Catch all
 * Route::any('/services/{all}',	'Main\Services\ServicesController@index')->where('all', '.*');
 * Route::any('/projects/{all}',	'Main\Projects\ProjectsController@index')->where('all', '.*');
 * Route::any('/work/{all}',		'Main\Projects\ProjectsController@index')->where('all', '.*');
 * Route::any('{all}',				'Main\HomeController@index')->where('all', '.*');
 */
