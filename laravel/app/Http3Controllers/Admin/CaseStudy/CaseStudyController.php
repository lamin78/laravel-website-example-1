<?php namespace App\Http\Controllers\Admin\CaseStudy;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\CaseStudyRequest;
use App\Http\Requests\Admin\CaseStudyRequestFull;
use App\Http\Requests\Admin\CaseStudyRequestStat;
use App\Http\Requests\Admin\CaseStudyTextRequest;
use App\Http\Requests\Admin\CaseStudyParallaxRequest;
use App\Http\Requests\Admin\CaseStudyRequestSlider;
use App\Http\Requests\Admin\CaseStudyRequestVideo;
use App\Http\Requests\Admin\CaseStudyRequestCarousel;
use Illuminate\Contracts\Auth\Guard;
use App\Models\CMS_Carousel;
use App\Models\CMS_Case_Study;
use App\Models\CMS_Order;
use App\Models\CMS_Parallax;
use App\Models\CMS_Slide;
use App\Models\CMS_Text;
use App\Models\CMS_Video;
use Admin;
use Log;
use File;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class CaseStudyController extends AdminController {

	public function __construct(
		Guard $guard,
		CMS_Carousel $cms_carousel,
		CMS_Case_Study $cms_case_study,
		CMS_Order $cms_order,
		CMS_Parallax $cms_parallax,
		CMS_Slide $cms_slide,
		CMS_Text $cms_text,
		CMS_Video $cms_video
	) {

		parent::__construct($guard);

		Admin::checkPagePermission("menu_users");

		$this->cms_Carousel = $cms_carousel;
		$this->cms_Case_Study = $cms_case_study;
		$this->cms_Order = $cms_order;
		$this->cms_Parallax = $cms_parallax;
		$this->cms_Slide = $cms_slide;
		$this->cms_Text = $cms_text;
		$this->cms_Video = $cms_video;

		$this->serverCaseStudyPath = public_path() . '/images/cms-case-studies/';
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index()
	{
		$this->context['title'] = 'Case Study Management';
		$cms_Case_Study = $this->cms_Case_Study->all();
		$this->context['cms_Case_Study'] = $cms_Case_Study;
		$this->context['pageViewJS'] = 'admin/casestudy-min';

		return view('admin.casestudy.main', $this->context);
	}

	/**
	 * [create description]
	 * @return [type] [description]
	 */
	public function create()
	{
		$this->context['title'] = 'Create Case Study';
		$this->context['categories'] = [
			'Presentations'=>'Presentations',
			'Digital'=>'Digital',
			'Infosec'=>'Infosec',
			'Video'=>'Video',
			'exhibitions'=>'exhibitions',
			'Campaigns'=>'Campaigns'
		];
		$this->context['pageViewJS'] = 'admin/casestudy-min';

		return view('admin.casestudy.create', $this->context);
	}

	public function generateSlug(Request $request)
	{
		$slug = str_slug($request->title, '-');
		return response()->json(['slug'=>$slug]);
	}

	/**
	 * [store description]
	 * @param  CaseStudyRequest $request [description]
	 * @return [type]                    [description]
	 */
	public function store(CaseStudyRequest $request)
	{
		$this->cms_Case_Study->casestudy_hashed_url = str_random(7);
		switch(true) {
			case($request->casestudy_category === 'Digital'):
			 	$request['casestudy_colour'] = 'orange';
			break;
			case($request->casestudy_category === 'Presentations'):
			 	$request['casestudy_colour'] = 'pink';
			break;
			case($request->casestudy_category === 'Video'):
			 	$request['casestudy_colour'] = 'red';
			break;
			case($request->casestudy_category === 'Campaigns'):
			 	$request['casestudy_colour'] = 'green';
			break;
			case($request->casestudy_category === 'Exhibitions'):
			 	$request['casestudy_colour'] = 'green';
			break;
		}
		$this->cms_Case_Study->fill($request->all());
		$this->cms_Case_Study->save();

		$this->response = array('response_status' => 'success', 'message' => 'Case study created successfully.');

		return redirect('admin/case-study')->with('response', $this->response);
	}

	/**
	 * [edit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function edit($id)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($id);
		$cms_Case_Study->order;

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title;
		$this->context['pageViewJS'] = 'admin/casestudy-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['categories'] = [
			'Presentations'=>'Presentations',
			'Digital'=>'Digital',
			'Infosec'=>'Infosec',
			'Video'=>'Video',
			'exhibitions'=>'exhibitions',
			'Campaigns'=>'Campaigns'
		];

		// dd($this->context['cms_case_study']->toArray());

		return view('admin.casestudy.casestudy_layout', $this->context);
	}

	/**
	 * [edit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function editStatus(Request $request)
	{

		Log::info('EDIT STATUS ==============================================');
		Log::info($request->all());
		$cms_Case_Study = $this->cms_Case_Study->find($request->id);
		$cms_Case_Study->update(['status'=>$request->status]);

		$this->response = array(
			'response_status' => 'success',
			'message' => 'Case study status updated.',
			'status' => $request->status,
		);
		return response()->json($this->response);
	}


	/**
	 * [editHeader description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function editHeader($id)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($id);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title;
		$this->context['pageViewJS'] = 'admin/casestudy-min';
		$this->context['pageViewJS'] = 'admin/vue/vue-header-cms-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['categories'] = [
			'Presentations'=>'Presentations',
			'Digital'=>'Digital',
			'Infosec'=>'Infosec',
			'Video'=>'Video',
			'Exhibitions'=>'Exhibitions',
			'Campaigns'=>'Campaigns'
		];

		// dd($this->context['cms_case_study']->toArray());

		return view('admin.casestudy.edit_header', $this->context);
	}

	/**
	 * [updateHeader description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function updateHeader($id, CaseStudyRequestFull $request)
	{
		$cms_Case_Study = $this->cms_Case_Study->findOrFail($id);

		// Temporarily increase memory limit to 256MB
    // ini_set('memory_limit','256M');

		if ($request->hasFile('header_image')) {

			if($request->file('header_image')->isValid()) {

				$filename = trim($request->casestudy_slug) . '.' . $request->file('header_image')->getClientOriginalExtension();

				if (!is_dir($this->serverCaseStudyPath.$request->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$request->casestudy_slug, 0755);
				}

				$request->file('header_image')->move(
		      $this->serverCaseStudyPath.$request->casestudy_slug, $filename
		    );

				$img = Image::make($this->serverCaseStudyPath.$request->casestudy_slug.'/'.$filename);
				$img->save($this->serverCaseStudyPath.$request->casestudy_slug.'/'.str_replace('.', '@2x.', $filename));

				$normalHeight = $img->height();
				$normalwidth = $img->width();

				$img->resize(($normalwidth/2), ($normalHeight/2));
				$img->save($this->serverCaseStudyPath.$request->casestudy_slug.'/'.$filename);

				$img->resize(($normalwidth/4), ($normalHeight/4));
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.str_replace('.', '@0.3x.', $filename));

				$request['casestudy_header_image'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Header image failed to upload.');
				return redirect('admin/case-study/edit-header/'.$id)->with('response', $this->response);
			}
		}
		else {
			if(!is_dir($this->serverCaseStudyPath.$request->casestudy_slug) && file_exists($this->serverCaseStudyPath.$request->casestudy_slug_hidden.'/'.$cms_Case_Study->casestudy_header_image)) {
				$ext = explode('.', $cms_Case_Study->casestudy_header_image);
				$ext = $ext[1];
				$imageNormal = $this->serverCaseStudyPath.$request->casestudy_slug_hidden.'/'.$cms_Case_Study->casestudy_header_image;
				$imageRetina = $this->serverCaseStudyPath.$request->casestudy_slug_hidden.'/'.str_replace('.', '@2x.', $cms_Case_Study->casestudy_header_image);
				if(file_exists($imageNormal)){
					rename($imageNormal, $this->serverCaseStudyPath.$request->casestudy_slug_hidden.'/'.$request->casestudy_slug.'.'.$ext);
				}
				if(file_exists($imageRetina)){
					rename($imageRetina, $this->serverCaseStudyPath.$request->casestudy_slug_hidden.'/'.$request->casestudy_slug.'@2x.'.$ext);
				}
				rename($this->serverCaseStudyPath.$request->casestudy_slug_hidden, $this->serverCaseStudyPath.$request->casestudy_slug);
				$request['casestudy_header_image'] = $request->casestudy_slug.'.'.$ext ;
			}
		}

		switch(true) {
			case($request->casestudy_category === 'Digital'):
			 	$request['casestudy_colour'] = 'orange';
			break;
			case($request->casestudy_category === 'Presentations'):
			 	$request['casestudy_colour'] = 'pink';
			break;
			case($request->casestudy_category === 'Video'):
			 	$request['casestudy_colour'] = 'red';
			break;
			case($request->casestudy_category === 'Campaigns'):
			 	$request['casestudy_colour'] = 'green';
			break;
			case($request->casestudy_category === 'Exhibitions'):
			 	$request['casestudy_colour'] = 'green';
			break;
		}

		// Log::info('LOG ==============================================');
		// Log::info($request->all());

		$cms_Case_Study->update($request->all());

		$this->response = array('response_status' => 'success', 'message' => 'Case study updated successfully.');

		return redirect('admin/case-study/edit-header/'.$id)->with('response', $this->response);
	}

	/**
	 * [editHeader description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function editStatistics($id)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($id);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title.' statistics';
		$this->context['pageViewJS'] = 'admin/casestudy-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['categories'] = [
			'Presentations'=>'Presentations',
			'Digital'=>'Digital',
			'Infosec'=>'Infosec',
			'Video'=>'Video',
			'exhibitions'=>'exhibitions',
			'Campaigns'=>'Campaigns'
		];

		// dd('statistics');

		// dd($this->context['cms_case_study']->toArray());

		return view('admin.casestudy.edit_statistics', $this->context);
	}

	/**
	 * [updateHeader description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function updateStatistics($id, CaseStudyRequestStat $request)
	{

		$cms_Case_Study = $this->cms_Case_Study->findOrFail($id);

		if ($request->hasFile('casestudy_stat_1_holder')) {

			if($request->file('casestudy_stat_1_holder')->isValid()) {

				$filename = 'stat-1.' . $request->file('casestudy_stat_1_holder')->getClientOriginalExtension();

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$request->file('casestudy_stat_1_holder')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				if($request->file('casestudy_stat_1_holder')->getClientOriginalExtension() !== 'svg') {
					$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
					$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				}

				$request['casestudy_stat_1'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Statistic image 1 failed to upload.');
				return redirect('admin/case-study/edit-statistics/'.$id)->with('response', $this->response);
			}
		}

		if ($request->hasFile('casestudy_stat_2_holder')) {

			if($request->file('casestudy_stat_2_holder')->isValid()) {

				$filename = 'stat-2.' . $request->file('casestudy_stat_2_holder')->getClientOriginalExtension();

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$request->file('casestudy_stat_2_holder')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				if($request->file('casestudy_stat_1_holder')->getClientOriginalExtension() !== 'svg') {
					$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
					$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				}

				$request['casestudy_stat_2'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Statistic image 2 failed to upload.');
				return redirect('admin/case-study/edit-statistics/'.$id)->with('response', $this->response);
			}
		}

		if ($request->hasFile('casestudy_stat_3_holder')) {

			if($request->file('casestudy_stat_3_holder')->isValid()) {

				$filename = 'stat-3.' . $request->file('casestudy_stat_3_holder')->getClientOriginalExtension();

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$request->file('casestudy_stat_3_holder')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				if($request->file('casestudy_stat_1_holder')->getClientOriginalExtension() !== 'svg') {
					$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
					$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				}

				$request['casestudy_stat_3'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Statistic image 3 to upload.');
				return redirect('admin/case-study/edit-statistics/'.$id)->with('response', $this->response);
			}
		}

		$cms_Case_Study->update($request->all());

		$this->response = array('response_status' => 'success', 'message' => 'Statistic images uploaded successfully.');

		return redirect('admin/case-study/edit-statistics/'.$id)->with('response', $this->response);
	}

	/**
	 * [editTextArea description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function editTextArea($cid, $sid)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($cid);
		$cms_Text = $this->cms_Text->find($sid);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title;
		$this->context['pageViewJS'] = 'admin/casestudy-textedit-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['cms_Text'] = $cms_Text;
		$this->context['textLabels'] = [
			'company'=>'Company',
			'approach'=>'Approach',
			'process'=>'Process',
			'result'=>'Result',
		];

		return view('admin.casestudy.edit_text_block', $this->context);
	}

	/**
	 * [updateTextArea description]
	 * @param  [type]               $cid                  [description]
	 * @param  [type]               $sid                  [description]
	 * @param  CaseStudyTextRequest $caseStudyTextRequest [description]
	 * @return [type]                                     [description]
	 */
	public function updateTextArea($cid, $sid, CaseStudyTextRequest $caseStudyTextRequest)
	{
		$cms_Text = $this->cms_Text->find($sid);
		$cms_Text->cid = $cid;
		$cms_Text->text_type = $caseStudyTextRequest->text_type;
		$cms_Text->text_field = $caseStudyTextRequest->text_field;

		$cms_Text->save();

		$this->response = array('response_status' => 'success', 'message' => 'Case study text area updated successfully.');

		return redirect('admin/case-study/'.$cid.'/edit')->with('response', $this->response);
	}

	/**
	 * [editParallax description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function editParallax($cid, $sid)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($cid);
		$cms_Parallax = $this->cms_Parallax->find($sid);

		Log::info('REQUEST ========================================');
		Log::info($cms_Parallax->all());

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title;
		$this->context['pageViewJS'] = 'admin/vue/vue-parallax-cms-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['cms_Parallax'] = $cms_Parallax;

		return view('admin.casestudy.edit_parallax_block', $this->context);
	}

	/**
	 * [updateParallax description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function updateParallax($cid, $sid, CaseStudyParallaxRequest $caseStudyParallaxRequest)
	{

		if($caseStudyParallaxRequest->hasFile('image')) {

			if($caseStudyParallaxRequest->file('image')->isValid()) {

				$cms_Case_Study = $this->cms_Case_Study->find($cid);

				$filename = trim($cms_Case_Study->casestudy_slug) . '-parallax-'.$sid.'.' . $caseStudyParallaxRequest->file('image')->getClientOriginalExtension();

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$caseStudyParallaxRequest->file('image')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.str_replace('.', '@2x.', $filename));

				$normalHeight = $img->height();
				$normalwidth = $img->width();

				$img->resize(($normalwidth/2), ($normalHeight/2));
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);

				$img->resize(($normalwidth/4), ($normalHeight/4));
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.str_replace('.', '@0.3x.', $filename));

				$cms_Parallax = $this->cms_Parallax->find($sid);
				$cms_Parallax->cid = $cid;
				$cms_Parallax->image = $filename;
				$cms_Parallax->save();

				$this->response = array('response_status' => 'success', 'message' => 'Case study parallax image updated successfully.');

				return redirect('admin/case-study/edit-parallax/'.$cid.'/'.$sid)->with('response', $this->response);

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Header image failed to upload.');
				return redirect('admin/case-study/edit-parallax/'.$cid.'/'.$sid)->with('response', $this->response);
			}
		}
		else {
			$this->response = array('response_status' => 'error', 'message' => 'Header image failed to upload.');
			return redirect('admin/case-study/edit-parallax/'.$cid.'/'.$sid)->with('response', $this->response);
		}
	}

	/**
	 * [editVideo description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function editVideo($cid, $sid)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($cid);
		$cms_Video = $this->cms_Video->find($sid);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title.' video';
		$this->context['pageViewJS'] = 'admin/casestudy-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['cms_Video'] = $cms_Video;

		return view('admin.casestudy.edit_video_block', $this->context);
	}

	/**
	 * [updateVideo description]
	 * @param  [type]                 $cid     [description]
	 * @param  [type]                 $sid     [description]
	 * @param  CaseStudyRequestVideo $request [description]
	 * @return [type]                          [description]
	 */
	public function updateVideo($cid, $sid, CaseStudyRequestVideo $request)
	{

		// Log::info('REQUEST ========================================');
		// Log::info($request->all());

		$cms_Video = $this->cms_Video->find($sid);
		$cms_Video->update($request->all());

		$this->response = array('response_status' => 'success', 'message' => 'Case study slider updated successfully.');

		return redirect('admin/case-study/edit-video/'.$cid.'/'. $sid)->with('response', $this->response);
	}

	/**
	 * [editSlider description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function editSlider($cid, $sid)
	{
		$cms_Case_Study = $this->cms_Case_Study->find($cid);
		$cms_Slide = $this->cms_Slide->find($sid);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title.' slider';
		$this->context['pageViewJS'] = 'admin/vue/vue-slider-cms-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['cms_Slide'] = $cms_Slide;

		return view('admin.casestudy.edit_slider_block', $this->context);
	}

	/**
	 * [updateHeader description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function updateSlider($cid, $sid, CaseStudyRequestSlider $request)
	{

		$cms_Case_Study = $this->cms_Case_Study->findOrFail($cid);

		if ($request->hasFile('slide_1_holder')) {

			if($request->file('slide_1_holder')->isValid()) {

				$filename = 'slide-1-'.$sid.'.'.$request->file('slide_1_holder')->getClientOriginalExtension();

				// Log::info('FILENAME ========================================');
				// Log::info($filename);

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$request->file('slide_1_holder')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.str_replace('.', '@2x.', $filename));

				$normalHeight = $img->height();
				$normalwidth = $img->width();

				$img->resize(($normalwidth/2), ($normalHeight/2));
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);

				$request['slide_1'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Slider image 1 failed to upload.');
				return redirect('admin/case-study/edit-slider/'.$cid.'/'. $sid)->with('response', $this->response);
			}
		}

		if ($request->hasFile('slide_2_holder')) {

			if($request->file('slide_2_holder')->isValid()) {

				$filename = 'slide-2-'.$sid.'@2x.'.$request->file('slide_2_holder')->getClientOriginalExtension();

				// Log::info('FILENAME ========================================');
				// Log::info($filename);

				if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
					mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
				}

				$request->file('slide_2_holder')->move(
		      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
		    );

				$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);

				$normalHeight = $img->height();
				$normalwidth = $img->width();
				$filename = str_replace('@2x.', '.', $filename);

				$img->resize(($normalwidth/2), ($normalHeight/2));
				$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);

				$request['slide_2'] = $filename;

			} else {
				$this->response = array('response_status' => 'error', 'message' => 'Slider image 2 failed to upload.');
				return redirect('admin/case-study/edit-slider/'.$cid.'/'. $sid)->with('response', $this->response);
			}
		}

		// Log::info('REQUEST ========================================');
		// Log::info($request->all());

		$cms_Slide = $this->cms_Slide->find($sid);
		$cms_Slide->update($request->all());

		$this->response = array('response_status' => 'success', 'message' => 'Case study slider updated successfully.');

		return redirect('admin/case-study/edit-slider/'.$cid.'/'. $sid)->with('response', $this->response);
	}

	/**
	 * [editCarousel description]
	 * @param  [type] $cid [description]
	 * @param  [type] $sid [description]
	 * @return [type]      [description]
	 */
	public function editCarousel($cid, $sid)
	{
		// Log::info('CAROUSEL ========================================');
		// Log::info($cid);
		// Log::info($sid);

		$cms_Case_Study = $this->cms_Case_Study->find($cid);
		$cms_Carousel = $this->cms_Carousel->where('cid', $cid)->where('csid', $sid)->get();
		// Log::info($cms_Case_Study->id);
		// Log::info($cms_Carousel->first()->id);

		$this->context['title'] = 'Editing: '.$cms_Case_Study->casestudy_title;
		$this->context['pageViewJS'] = 'admin/vue/vue-carousel-cms-min';
		$this->context['cms_case_study'] = $cms_Case_Study;
		$this->context['imageDir'] = '/images/cms-case-studies/'.$cms_Case_Study->casestudy_slug;
		$this->context['cms_Carousel'] = $cms_Carousel;
		$this->context['cid'] = $cid;
		$this->context['csid'] = $sid;

		Log::info('FIRST SECTION ID ========================================');
		Log::info($cms_Carousel->first()->id);

		return view('admin.casestudy.edit_carousel_block', $this->context);
	}

	/**
	 * [updateCarousel description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function addCarousel(Request $request)
	{

		// $cms_Case_Study = $this->cms_Case_Study->findOrFail($cid);

		Log::info('ADD CAROUSEL SECTION ========================================');
		Log::info($request->all());

		$this->cms_Carousel->fill($request->all());
		$this->cms_Carousel->save();

		$insertId = $this->cms_Carousel->id;

		return response()->json(['status'=>'Done', 'insertId'=>$insertId]);
	}

	/**
	 * [updateCarousel description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function updateCarousel($cid, $sid, Request $request)
	{

		Log::info('UP. CAROUSEL REQUEST ========================================');
		Log::info($request->all());

		$data = $request->all();
		$rules = array();
		$messages = array();

		for($x=0;$x<$request['carousel_count'];$x++) {
			if($data['hidden_changed_'.$x] === '1') {
				$rules['image_holder_'.$x] = 'required|mimes:png,jpeg,jpg|max:10240';
				$messages['image_holder_'.$x.'.required'] = 'STOP IT!';
				$messages['image_holder_'.$x.'.mimes'] = 'STOP IT!';
				$messages['image_holder_'.$x.'.max'] = 'STOP IT!';
			}
		}

		Log::info('UP. Validator Messages ========================================');
		Log::info($messages);

		$v = Validator::make($data, $rules, $messages);

		Log::info('Validator Error ========================================');
		Log::info($v->errors());

		if($v->fails()) {
				// return response()->json([
		    //     'success' => false,
		    //     'message' => $v->errors()
		    // ], 422);
				$this->response = array('response_status' => 'error', 'message' => 'Case study carousel did not updated successfully.');
				return redirect('admin/case-study/edit-carousel/'.$cid.'/'. $request['carousel_csid'])->with('response', $this->response);
    }
		else {

			$cms_Case_Study = $this->cms_Case_Study->find($cid);

			for($x=0;$x<$request['carousel_count'];$x++) {

				if($request['hidden_changed_'.$x] === '1') {

					$reqId = $request['hidden_id_'.$x];
					$reqCid = $request['hidden_cid_'.$x];
					$reqCsid = $request['hidden_csid_'.$x];

					$tmpFileName = 'image_holder_'.$x;

					$filename = trim($cms_Case_Study->casestudy_slug).'-carousel-'.$sid.'-'.$x.'.'.$request->file($tmpFileName)->getClientOriginalExtension();

					if (!is_dir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug)) {
						mkdir($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, 0755);
					}

					$request->file($tmpFileName)->move(
			      $this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug, $filename
			    );

					$img = Image::make($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);
					$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.str_replace('.', '@2x.', $filename));

					$normalwidth = $img->width();
					$normalHeight = $img->height();

					$img->resize(($normalwidth/2), ($normalHeight/2));
					$img->save($this->serverCaseStudyPath.$cms_Case_Study->casestudy_slug.'/'.$filename);

					$this->cms_Carousel
							 ->where('id', 	$reqId)
							 ->where('cid', $reqCid)
							 ->where('csid',$reqCsid)
							 ->update(['image' => $filename]);

				}
			}

			// return response()->json([
			// 		'success' => true,
			// 		'message' => 'success'
			// ], 422);
			if(isset($reqCid) && isset($reqCsid)) {
				$this->response = array('response_status' => 'success', 'message' => 'Case study carousel updated successfully.');
			}
			else {
				$reqCid = $cid;
				$reqCsid = $request['carousel_csid'];
				$this->response = array('response_status' => 'success', 'message' => 'Add a new image to upload.');
			}

			return redirect('admin/case-study/edit-carousel/'.$reqCid.'/'. $reqCsid)->with('response', $this->response);
		}
	}

	/**
	 * [updateCarouselOrder description]
	 * @param  [type]  $id      [description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function updateCarouselOrder(Request $request)
	{

		Log::info('UPDATE ORDER CAROUSEL ========================================');
		Log::info($request['cid']);
		Log::info($request['csid']);

		$carouselOrder = $this->cms_Carousel
												->where('csid', $request['csid'])
												->delete();

		Log::info($carouselOrder);

		$data = array();
		foreach($request->input('cms_Carousel') as $req) {
			$data[] = array(
				"cid"			=>$req["cid"]
				,"csid"		=>$req["csid"]
				,"image"	=>$req["image"]
				,"order"	=>$req["order"]
				,"active"	=>$req["active"]
			);
		};

		$this->cms_Carousel->insert($data);

		return response()->json(['status'=>'Done']);
	}

	/**
	 * [updateCarousel description]
	 * @param  [type]               $id      [description]
	 * @param  CaseStudyRequestFull $request [description]
	 * @return [type]                        [description]
	 */
	public function deleteCarouseSectionCarousel(Request $request)
	{

		Log::info('DELETE CAROUSEL SECTION ========================================');
		Log::info($request->all());

		$deleteCarouselSection = $this->cms_Carousel
																->where('id', $request->id)
																->where('cid', $request->cid)
																->where('csid', $request->csid)
																->delete();

		$this->response = array('response_status' => 'success', 'message' => 'Case study carousel section removed.');
		return response()->json($this->response);
	}

	/**
	 * [addSection description]
	 * @param Request $request [description]
	 */
	public function addSection(Request $request) {

		$id = $request->input('id');
		$newSection = $request->input('section');

		switch($newSection['data_type']) {
			case('image'):
				switch($newSection['data_name']) {
					case('parallax'):
						$count = $this->cms_Parallax->where('cid', $id)->count();
						$this->cms_Parallax->fill(['cid'=>$id, 'order'=>($count+1), 'active'=>'1']);
						$this->cms_Parallax->save();
						$insertId = $this->cms_Parallax->id;
					break;
					case('slider'):
						$count = $this->cms_Slide->where('cid', $id)->count();
						$this->cms_Slide->fill(['cid'=>$id, 'order'=>($count+1), 'active'=>'1']);
						$this->cms_Slide->save();
						$insertId = $this->cms_Slide->id;
					break;
				}
			break;
			case('text'):
				$count = $this->cms_Text->where('cid', $id)->count();
				$this->cms_Text->fill(['cid'=>$id, 'order'=>($count+1), 'active'=>'1']);
				$this->cms_Text->save();
				$insertId = $this->cms_Text->id;
			break;
			case('video'):
				$count = $this->cms_Video->where('cid', $id)->count();
				$this->cms_Video->fill(['cid'=>$id, 'order'=>($count+1), 'active'=>'1']);
				$this->cms_Video->save();
				$insertId = $this->cms_Video->id;
			break;
			case('carousel'):
				$count = $this->cms_Carousel->where('cid', $id)->count();
				$this->cms_Carousel->fill(['cid'=>$id, 'csid'=>($count+1), 'order'=>'1', 'active'=>'1']);
				$this->cms_Carousel->save();
				$this->cms_Carousel->update(['csid'=>$this->cms_Carousel->id]);
				$insertId = $this->cms_Carousel->id;
			break;
		}

		return response()->json(['status'=>'Done', 'insertId'=>$insertId]);
	}

	public function updateOrder($id, Request $request)
	{
		$caseOrder = $this->cms_Order->where('cid', $id)->delete();

		Log::info('UPDATE ORDER ===================================================================');
		Log::info($request->all());

		$data = array();
		foreach($request->input('order') as $req) {
			$data[] = array(
				"cid"					=>$req["cid"]
				,"order"			=>$req["order"]
				,"data_ident"	=>$req["data_ident"]
				,"data_name"	=>$req["data_name"]
				,"data_type"	=>$req["data_type"]
				,"sid"				=>$req["sid"]
				,"active"			=>$req["active"]
			);
		};

		$this->cms_Order->insert($data);

		return response()->json(['status'=>'Done']);
	}

	public function updateOrderIdent(Request $request)
	{
		Log::info('UPDATE ORDER IDENT ===================================================================');
		Log::info($request->all());

		$caseOrder = $this->cms_Order
									->where('order', $request->orderId)
									->where('cid', $request->caseStudyId)
									->where('sid', $request->sectionId)
									->update(['data_ident'=>$request->value]);

		Log::info($caseOrder);
									// ->where('sid', $request->sectionId)

		return response()->json(['status'=>'Done']);
	}

	/**
	 * [deleteSection description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function deleteSection(Request $request)
	{

		Log::info('DEL DATA ===================================================================');
		Log::info($request->all());

		$caseOrder = $this->cms_Order
										->where('order', $request->orderOrd)
										->where('cid', $request->caseStudyId)
										->where('sid', $request->sectionId)
										->delete();


		switch($request->orderType) {
			case('image'):
				switch($request->orderName) {
					case('parallax'):
						$parallax = $this->cms_Parallax->where('id', $request->sectionId)->delete();
					break;
					case('slide'):
						$caseOrder = $this->cms_Slide->where('id', $request->sectionId)->delete();
					break;
				}
			break;
			case('text'):
				$caseOrder = $this->cms_Text->where('id', $request->sectionId)->delete();
			break;
			case('video'):
				$caseOrder = $this->cms_Video->where('id', $request->sectionId)->delete();
			break;
			case('carousel'):
				$caseOrder = $this->cms_Carousel->where('csid', $request->sectionId)->delete();
			break;
		}

		return response()->json(['status'=>'Done']);
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		$user = $this->cms_Case_Study->findOrFail($id);
		$user->delete();

		$this->response = array('response_status' => 'success', 'message' => 'Case study deleated successfully.');

		return redirect('admin/case-study')->with('response', $this->response);
	}
}
