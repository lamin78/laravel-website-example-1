<?php namespace App\Libraries;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Illuminate\Http\Request;

class Helpers extends Controller {

	public static $carouselCount = -1;

	public function __construct()
	{
		// self::$carouselCount = -1;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseParallax($cmsData, $order)
	{
		$parallaxResult = $cmsData->parallax->map(function ($item) use($order) {
			if($order->sid === $item['id']) {
				return preg_replace('/.jpg|.JPG|.png|.PNG/i', '', $item['image']);
			}
		})->implode('');

		return $parallaxResult;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseText($cmsData, $order, $type)
	{
		$textResult = $cmsData->textfield->map(function ($item) use($order, $type) {
			if($order->sid == $item['id']) {
				if(isset($type) && $type == 'header') {
					return $item['text_type'];
				}
				elseif(isset($type) && $type == 'content') {
					return $item['text_field'];
				}
				else {
					return $item['text_field'];
				}
			}
		})->implode('');

		return $textResult;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseSlide($cmsData, $order, $slide)
	{
		$slideResult = $cmsData->slide->map(function ($item) use($order, $slide) {
			if($order->sid == $item['id']) {
				return $item['slide_'.$slide];
			}
		})->implode('');

		return $slideResult;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseVideo($cmsData, $order, $type)
	{
		$videoResult = $cmsData->video->map(function ($item) use($order, $type) {
			if($order->sid == $item['id']) {
				return $item[$type];
			}
		})->implode('');

		return $videoResult;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseCarousel()
	{
		self::$carouselCount = self::$carouselCount +1;
		return self::$carouselCount;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public static function parseCarouselTheme($cmsData, $order)
	{
		// Log::info($order);
		// Log::info($cmsData->carousel);

		$theme = $cmsData->carousel
			->filter(function ($item) use($order) {
				if($order->sid == $item['csid'])
					return true;
			})->map(function ($item) use($order) {
				return $item['theme'];
			});

    // Log::info('Result=========================>>');
    // Log::info($theme);
    // Log::info($theme[0]);
		return $theme[0];
	}
}
