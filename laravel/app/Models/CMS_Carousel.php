<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Carousel extends Model {

	protected $table = 'c_m_s__carousels';

	public $timestamps = true;

  protected $fillable = [
    'cid',
    'csid',
    'image',
    'order',
    'active',
  ];
}
