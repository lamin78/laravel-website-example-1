<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Case_Study extends Model {

  protected $table = 'c_m_s__case__studies';

  public $timestamps = true;

  protected $fillable = [
    'casestudy_title',
    'casestudy_subtitle',
    'casestudy_slug',
    'casestudy_hashed_url',
    'casestudy_header_image',
    'casestudy_header',
    'casestudy_sub_header',
    'casestudy_client',
    'casestudy_year',
    'casestudy_location',
    'casestudy_category',
    'casestudy_header_video_link',
    'casestudy_header_video_poster',
    'casestudy_header_video_pre_poster',
    'casestudy_header_video_mobile_poster',
    'header_image_active',
    // 'casestudy_the_company',
    // 'casestudy_the_brief',
    // 'casestudy_the_approach',
    // 'casestudy_the_process',
    // 'casestudy_the_result',
    'casestudy_stat_1',
    'casestudy_stat_2',
    'casestudy_stat_3',
    'casestudy_colour',
    'status',
  ];

  public function textfield() {
    return $this->hasMany('App\Models\CMS_Text', 'cid', 'id');
  }

  public function parallax() {
    return $this->hasMany('App\Models\CMS_Parallax', 'cid', 'id');
  }

  public function slide() {
    return $this->hasMany('App\Models\CMS_Slide', 'cid', 'id');
  }

  public function video() {
    return $this->hasMany('App\Models\CMS_Video', 'cid', 'id');
  }

  public function carousel() {
    return $this->hasMany('App\Models\CMS_Carousel', 'cid', 'id')->orderBy('order');
  }

  public function order() {
    return $this->hasMany('App\Models\CMS_Order', 'cid', 'id')->orderBy('order');
  }
}
