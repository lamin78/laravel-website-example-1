<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Order extends Model {

  protected $table = 'c_m_s__orders';

	public $timestamps = true;

  protected $fillable = [
    'cid',
    'order',
    'data_name',
    'data_type',
    'sid',
    'active',
  ];

  protected $hidden = [
    'created_at',
    'updated_at',
  ];
}
