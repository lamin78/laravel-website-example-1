<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Parallax extends Model {

	protected $table = 'c_m_s__parallaxes';

	public $timestamps = true;

  protected $fillable = [
    'cid',
    'image',
    'order',
    'active',
  ];
}
