<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Slide extends Model {

	protected $table = 'c_m_s__slides';

	public $timestamps = true;

  protected $fillable = [
    'cid',
    'slide_1',
    'slide_2',
    'order',
    'active',
  ];
}
