<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Text extends Model {

	protected $table = "c_m_s__texts";

  public $timestamps = true;

  protected $fillable = [
    'cid',
    'text_type',
    'text_field',
    'order',
    'active',
  ];

  protected $hidden = [
    'created_at',
    'updated_at',
  ];
}
