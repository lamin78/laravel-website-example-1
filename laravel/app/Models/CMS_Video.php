<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMS_Video extends Model {

	protected $table = 'c_m_s__videos';

	public $timestamps = true;

  protected $fillable = [
    'cid',
    'video',
    'poster',
		'order',
    'active',
  ];
}
