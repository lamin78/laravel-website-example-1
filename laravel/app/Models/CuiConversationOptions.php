<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationOptions extends Model {

  protected $table = 'cui_conversation_options';

  public $timestamps = true;

  public function links()
  {
    return $this->belongsToMany('App\Models\CuiConversationUrlData', 'cui_conversation_url_linkages', 'oid', 'lid');
  }
}
