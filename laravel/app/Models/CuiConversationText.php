<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationText extends Model {

  protected $table = 'cui_conversation_text';

  protected $fillable = ['conversation_text'];

  public $timestamps = true;

	public function conversationText() {
    return $this->hasOne('App\Models\CuiConversationTextOptions', 'id', 'tid');
  }

  public function options()
  {
    return $this->belongsToMany('App\Models\CuiConversationOptions', 'cui_conversation_text_options_linkages', 'cid', 'opid');
  }
}
