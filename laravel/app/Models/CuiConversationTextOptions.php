<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationTextOptions extends Model {

  protected $table = 'cui_conversation_text_options';

  public $timestamps = true;

}
