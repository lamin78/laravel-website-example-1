<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationTextOptionsLinkages extends Model {

  protected $table = 'cui_conversation_text_options_linkages';

  public $timestamps = true;

}
