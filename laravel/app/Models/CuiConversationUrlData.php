<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationUrlData extends Model {

  protected $table = 'cui_conversation_url_data';

  public $timestamps = true;

}
