<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuiConversationUrlLinkages extends Model {

  protected $table = 'cui_conversation_url_linkages';

  public $timestamps = true;

}
