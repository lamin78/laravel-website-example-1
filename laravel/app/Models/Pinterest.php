<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pinterest extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['pin_id', 'note', 'link', 'media_url', 'status'];

}
