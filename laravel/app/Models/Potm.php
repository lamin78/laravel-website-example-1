<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Potm extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['uri', 'media_local', 'status'];

}
