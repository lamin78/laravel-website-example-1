<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialImages extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'media_local', 'status'];

}
