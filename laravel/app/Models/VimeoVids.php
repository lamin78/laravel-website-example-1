<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VimeoVids extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vimeo_vids';

	protected $dates = ['created_time'];
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'uri', 'link', 'embed', 'media_url', 'media_local', 'status', 'created_time'];
}
