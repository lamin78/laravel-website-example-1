<?php namespace App\Services;

use Illuminate\Contracts\Auth\Guard as Guard;
use App;

class AdminServices {
		
	protected $permissions = null;
	protected $auth;

	public function register(Guard $guard) {
		
		$this->auth 			= $guard;
		if ($this->auth->user() != null) {
			$this->permissions		= $this->auth->user()->role->permissions;
		}
	}

	public function user($key) {
		if ($this->auth->user() != null) {
			return $this->auth->user()[$key];
		}
		return '';
	}

	public function checkPermission($key) {
		//dd($this->permissions);
		if ($this->permissions != null) {
			foreach ($this->permissions->toArray() as $item)
			{
			    if ($item['name'] == $key) {
			    	return true;
			    }
			}
		}
		return false;
	}

	public function checkPagePermission($key) {
		if ($this->checkPermission($key) == false) {
			view()->share('error','Access Denied');
			App::abort(401);
		}
	}
}