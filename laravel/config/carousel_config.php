<?php

/**
 * Carousel configuration
 *
 */
return [
	
	// castrol
	"castrol-presentation" => [
		[
			 'container'		=> '#castrolCarousel'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/castrol-presentation/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
			]
		]
	],
	
	// castrol
	"entertainment-one-brand-toolkits" => [
		[
			 'container'		=> '#castrolCarousel'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/castrol-presentation/carousel/'
			,'imageArray'		=> []
		]
	],
	
	//sungard animated looping presetation
	"sungard-presentation" => [
		[
			 'container'		=> '#microsoftCarousel1'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> true
			,'imageRoute'		=> '/images/projects/sungard-presentation/carousel/'
			,'imageArray'		=> [
				 'sungard-presentation-header.jpg'
				,'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
			]
		],
		[
			 'container'		=> '#microsoftCarousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/sungard-presentation/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1_alt.jpg'
				,'interactive-carousel-image_2_alt.jpg'
				,'interactive-carousel-image_3_alt.jpg'
			]
		]
	],
	
	//microsoft social engagement
	"microsoft-social-engagement" => [
		[
			 'container'		=> '#microsoftCarousel1'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> true
			,'imageRoute'		=> '/images/projects/microsoft-social-engagement/carousel/'
			,'imageArray'		=> [
				 'microsoft-social-engagement-header.jpg'
				,'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
			]
		],
		[
			 'container'		=> '#microsoftCarousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/microsoft-social-engagement/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1_alt.jpg'
				,'interactive-carousel-image_2_alt.jpg'
				,'interactive-carousel-image_3_alt.jpg'
			]
		]
	],
	
	//article10-bees
	"article10-bees" => [
		[
			 'container'		=> '#microsoftCarousel1'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> true
			,'imageRoute'		=> '/images/projects/article10-bees/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
			]
		],
		[
			 'container'		=> '#microsoftCarousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/microsoft-social-engagement/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1_screen.jpg'
				,'interactive-carousel-image_2_screen.jpg'
				,'interactive-carousel-image_3_screen.jpg'
			]
		]
	],
	
	// microsoft future decoded
	"future-decoded-2014" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/future-decoded-2014/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
				,'interactive-carousel-image_4.jpg'
				,'interactive-carousel-image_5.jpg'
			]
		]
	],
	
	// microsoft future decoded
	"future-decoded-2015" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/future-decoded-2014/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
				,'interactive-carousel-image_4.jpg'
				,'interactive-carousel-image_5.jpg'
			]
		]
	],
	
	// etf-advert
	"etf-advert" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/microsoft-future-decoded/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
				,'interactive-carousel-image_4.jpg'
				,'interactive-carousel-image_5.jpg'
			]
		]
	],
	
	//microsoft universities
	"microsoft-universities" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/microsoft-universities/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image-1.jpg'
				,'interactive-carousel-image-2.jpg'
			]
		]
	],
	
	//microsoft mva
	"microsoft-mva" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> true
			,'playWhenVisible'	=> true
			,'imageRoute'		=> '/images/projects/microsoft-mva/carousel/'
			,'imageArray'		=> []
		]
	],
	
	//omni channel
	"omni-channel" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/omni-channel/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
			]
		]
	],
	
	//steljes interactive experience
	"steljes-interactive-experience" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/steljes-interactive-experience/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
				,'interactive-carousel-image_4.jpg'
				,'interactive-carousel-image_5.jpg'
			]
		]
	],
	
	//stoli video production
	"stoli-video-production" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/projects/stoli-video-production/carousel/'
			,'imageArray'		=> [
				 'interactive-carousel-image_1.jpg'
				,'interactive-carousel-image_2.jpg'
				,'interactive-carousel-image_3.jpg'
				,'interactive-carousel-image_4.jpg'
				,'interactive-carousel-image_5.jpg'
				,'interactive-carousel-image_6.jpg'
			]
		]
	],
	
	//presentation-solutions
	"presentation-solutions" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//digital-solutions
	"digital-solutions" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//information-security
	"information-security" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//video-animation-3d
	"video-animation-3d" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//exhibitions
	"exhibitions" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//campaigns
	"campaigns" => [
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> []
		]
	],
	
	//home services and case studies
	"home" => [
		[
			 'container'		=> '#carousel1'
			,'resizeFlag'		=> true
			,'fullFlag'			=> false
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/services-landing-page/'
			,'imageArray'		=> [
				 'presentations-background.jpg'
				,'campaigns-background.jpg'
				,'digital-background.jpg'
				,'infosec-background.jpg'
				,'exhibitions-background.jpg'
				,'video-background.jpg'
			]
		],
		[
			 'container'		=> '#carousel2'
			,'resizeFlag'		=> false
			,'fullFlag'			=> true
			,'fadeTrans'		=> false
			,'playWhenVisible'	=> false
			,'imageRoute'		=> '/images/case-studies/'
			,'imageArray'		=> [
				'castrol-presentation-foreground.png' 
				//,'elit-foreground.png' 
				//,'microsoft-university-background.jpg' 
				//,'microsoft-university-foreground.png' 
				//,'mipim-background.jpg' 
				//,'old-uppingham-background.jpg' 
				//,'old-uppingham-foreground.png' 
			]
		]
	],
];