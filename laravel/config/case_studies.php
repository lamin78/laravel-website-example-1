<?php

return [

	// home page
	"home" => [
		 'castrol-presentation'
		//,'entertainment-one-brand-toolkits'
		//,'sungard-presentation'
		//,'article10-bees'
		,'sungard-presentation'
		//,'old-uppinghamian-website-redesign'
		//,'stoli-app-design'
		//,'steljes-interactive-experience'
		,'openbet-website-redesign'
		//,'mail-online-exhibition-design'
		//,'thre-tablet-presentation-digital'
		//,'omni-channel'
		,'etf-advert'
		//,'stoli-video-production'
		//,'steljes-interactive-experience'
		//,'bdp-exhibition-design'
		,'thor-exhibition-design'
		//,'mail-online-exhibition-design'
		//,'aarhus'
		,'microsoft-mva'
	],

	// presentations page
	"presentation-solutions" => [
		 "castrol-presentation"
		,"sungard-presentation"
		,'article10-bees'
		,'entertainment-one-brand-toolkits'
	],

	// digital page
	"digital-solutions" => [
		 'openbet-website-redesign'
		,"stoli-app-design"
		,'thre-tablet-presentation-digital'
		,'old-uppinghamian-website-redesign'
	],

	// infosec page
	"information-security" => [
		 'openbet-website-redesign'
		,"stoli-app-design"
		,'thre-tablet-presentation-digital'
		,'old-uppinghamian-website-redesign'
	],

	// motion page
	"video-animation-3d" => [
		 "stoli-video-production"
		,"steljes-interactive-experience"
		,'openbet-website-redesign'
		,'etf-advert'
	],

	// events page
	"exhibitions" => [
		 "mail-online-exhibition-design"
		,"bdp-exhibition-design"
		,"thor-exhibition-design"
	],

	// campaigns page
	"campaigns" => [
		 'aarhus'
		,'microsoft-mva'
	],
];
