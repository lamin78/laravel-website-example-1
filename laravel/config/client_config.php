<?php

/**
 * Carousel configuration
 *
 */
return [

	// clients
	"clients" => [
		 'castrol'
		 ,'microsoft'
		 ,'rb'
		 ,'rolls-royce'
		 ,'viacom'
		 ,'orange'
		 ,'siemens'
		 ,'unilever'
		// ,'kfc'
		// ,'jaguar'
		// ,'sunguard'
		// ,'bp'
		// ,'rac'
		// ,'rmg'
		// ,'tnt'
		// ,'dior'
		// ,'chanel'
		// ,'pepsico'
		// ,'aol'
	],
];
