<?php

return [
		"presentation-solutions"	=> "pink",
		"digital-solutions"				=> "orange",
		"information-security"		=> "teal",
		"video-animation-3d"			=> "red",
		"exhibitions"							=> "yellow",
		"etf-torino-process" 			=> "purple-3",
		"campaigns"								=> "green"
];
