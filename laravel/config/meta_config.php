<?php

/**
 * meta data config
 *
 */
return [

	// main page meta data
	"home" => [
		"meta-title"		=> "Article 10 | Integrated Marketing Agency | London. Amsterdam",
		"meta-description"	=> "We’re an experienced, integrated full-service marketing agency and we want to blow your mind with our capabilities…",
	],

	"projects" => [
		"meta-title"		=> "Our Projects | Article 10 Integrated Marketing",
		"meta-description"	=> "From Microsoft to MailOnline, KFC to Castrol, discover our impressive projects for some of the world's biggest brands",
	],

	"services" => [
		"meta-title"		=> "Our Services | Article 10 Integrated Marketing",
		"meta-description"	=> "We approach every project with fresh eyes and bring new ideas, the latest technologies and contemporary techniques to the table",
	],

	"about" => [
		"meta-title"		=> "About Us  | Article 10 Integrated Marketing ",
		"meta-description"	=> "Connect with us to discover how we can connect you to your audience",
	],

	"social" => [
		"meta-title"		=> "Social | Article 10 Integrated Marketing ",
		"meta-description"	=> "Hear more about our projects, lilkes, loves and latest news on social media",
	],

	"contact" => [
		"meta-title"		=> "Contact Us | Article 10 Integrated Marketing",
		"meta-description"	=> "Got a creative itch you need us to scratch? Want to know more about what we do? Get in touch!",
	],

	// services meta data
	"presentation-solutions" => [
		"meta-title"		=> "Presentations | Article 10 Integrated Marketing",
		"meta-description"	=> "Whatever you want to present, however you want to present it, we can help, with the full range of presentation design services",
	],

	"digital-solutions" => [
		"meta-title"		=> "Digital | Article 10 Integrated Marketing",
		"meta-description"	=> "Across screens of all sizes we design digital solutions that surpass expectation and fulfil your needs",
	],

	"information-security" => [
		"meta-title"		=> "Information Security | Article 10 Integrated Marketing",
		"meta-description"	=> "We deliver engaging information security awareness campaigns to some of the largest organisations in the world",
	],

	"video-animation-3d" => [
		"meta-title"		=> "Video, Animation & 3D | Article 10 Integrated Marketing",
		"meta-description"	=> "Telling stories in the most engaging way is what we’re all about. That’s why brands turn to us when they need captivating video content ",
	],

	"exhibitions" => [
		"meta-title"		=> "Exhibitions | Article 10 Integrated Marketing",
		"meta-description"	=> "Whatever your sector, however large your requirement, our 30 years of combined exhibitions experience will help you make the biggest splash at your next event",
	],

	"campaigns" => [
		"meta-title"		=> "Campaigns | Article 10 Integrated Marketing",
		"meta-description"	=> "Our talented team of writers and designers can turn your ideas into a wide range of dynamic print and digital collateral",
	],

	//projects meta data
	"stoli-app-design" => [
		"meta-title"		=> "Case Study: Stoli iPad App Design | Article 10",
		"meta-description"	=> "Find out how Article 10 designed and produced a stylish, bespoke iPad app for the Stolichnaya Vodka sales team, showcasing the company's products and techniques",
	],

	"microsoft-social-engagement" => [
		"meta-title"		=> "Case Study: Microsoft Social Engagement Presentation | Article 10",
		"meta-description"	=> "See how Article 10 delivered a professional PowerPoint presentation for Microsoft's Social Engagement tool, based around the UK general election",
	],

	"openbet-website-redesign" => [
		"meta-title"		=> "Case Study: OpenBet Microsite Design | Article 10",
		"meta-description"	=> "Discover how Article 10 designed and developed a dynamic and exciting microsite to strict deadlines for OpenBet during the Grand National",
	],

	"etf-advert" => [
		"meta-title"		=> "Case Study: ETF Broadcast Adverts | Article 10",
		"meta-description"	=> "Take a look behind the scenes of a series of stylish, bespoke television adverts created by Article 10 and broadcast globally for the European Training Foundation",
	],

	"kfc-people-promise" => [
		"meta-title"		=> "Case Study: KFC Internal Communications Campaign | Article 10",
		"meta-description"	=> "Find out how Article 10 delivered a unique internal communications campaign for KFC's UK employees",
	],

	"mipim-exhibition-design" => [
		"meta-title"		=> "Case Study: MIPIM Environment Design | Article 10",
		"meta-description"	=> "Learn how Article 10 designed and created bespoke exhibition environments for a range of clients at MIPIM ",
	],

	"bdp-exhibition-design" => [
		"meta-title"		=> "Case Study: BDP Exhibition Stand Design | Article 10",
		"meta-description"	=> "Find out how Article 10 designed and created a unique exhibition space complete with a live art installation for BDP",
	],

	"entertainment-one-brand-toolkits" => [
		"meta-title"		=> "Case Study: Entertainment One Brand Toolkits | Article 10",
		"meta-description"	=> "See how Article 10 streamlined a complex series of brand toolkits for Entertainment One",
	],

	"microsoft-universities" => [
		"meta-title"		=> "Case Study: Microsoft Universities Integrated Campaign | Article 10",
		"meta-description"	=> "Discover how Article 10 delivered a dynamic multimedia campaign for Microsoft, including rebranded marketing materials and brochures",
	],

	"old-uppinghamian-website-redesign" => [
		"meta-title"		=> "Case Study: Old Uppingham Website Redesign | Article 10",
		"meta-description"	=> "Explore Article 10's design and development of a modern new alumni website for Uppingham School",
	],

	"stoli-video-production" => [
		"meta-title"		=> "Case Study: Stoli Video Production | Article 10",
		"meta-description"	=> "Take a look behind the scenes of a high quality video series for Stolichnaya Vodka, shot by Article 10",
	],

	"aarhus" => [
		"meta-title"		=> "Case Study: Aarhus Print & Digital Branding | Article 10",
		"meta-description"	=> "Find out how Article 10 delivered print, digital and social media collateral for the 2017 European City of Culture as part of an ongoing campaign",
	],

	"mail-online-exhibition-design" => [
		"meta-title"		=> "Case Study: MailOnline Interactive Experience | Article 10",
		"meta-description"	=> "See how Article 10 created a unique interactive experience for MailOnline at Nestle's Digital Day",
	],

	"omni-channel" => [
		"meta-title"		=> "Case Study: OpenBet Promotional Video Production | Article 10",
		"meta-description"	=> "Discover how Article 10 delivered a promotional animated video campaign to highlight OpenBet's extensive services",
	],

	"thre-tablet-presentation" => [
		"meta-title"		=> "Case Study: TH Real Estate Interactive 3D Solution | Article 10",
		"meta-description"	=> "Explore Article 10's crafting of a stylish, interactive 3D solution for TH Real Estate",
	],

	"sungard-presentation" => [
		"meta-title"		=> "Case Study: SunGard Bespoke Powerpoint Presentation | Article 10",
		"meta-description"	=> "Find out how Article 10 designed and created a dynamic and engaging looped PowerPoint presentation for use at SunGard events",
	],

	"future-decoded-2014" => [
		"meta-title"		=> "Case Study: Microsoft Future Decoded Animation 2014 | Article 10",
		"meta-description"	=> "See how Article 10 turned hand-drawn sketches and an engaging script into a stunning animation for Microsoft's Future Decoded event",
	],

	"future-decoded-2015" => [
		"meta-title"		=> "Case Study: Microsoft Future Decoded 2015 | Article 10",
		"meta-description"	=> "See how Article 10 transformed a conversations between two technology thought leaders into an engaging video series",
	],

	"thor-exhibition-design" => [
		"meta-title"		=> "Case Study: Thor Equities Dynamic Exhibition Space | Article 10",
		"meta-description"	=> "Discover how Article 10 designed a cutting-edge exhibition space for Thor Equities to use at multiple exhibitions",
	],

	"microsoft-mva" => [
		"meta-title"		=> "Case Study: Microsoft Virtual Academy Campaign & Branding | Article 10",
		"meta-description"	=> "Explore how Article 10 developed a comic-inspired brand identity and campaign collateral for Microsoft Virtual Academy",
	],

	"article10-bees" => [
		"meta-title"		=> "Case Study: Prezi Awards Presentation Submission | Article 10",
		"meta-description"	=> "Take a look at the creation of 'Bees', Article 10's submission to the annual Prezi awards",
	],

	"castrol-presentation" => [
		"meta-title"		=> "Case Study: Castrol Employee Induction Presentation | Article 10",
		"meta-description"	=> "Find out how Article 10 designed and built an impressive induction presentation, complete with video content, for new starters at Castrol",
	],

	"terms-of-business" => [
		"meta-title"		=> "Terms of Business | Article 10 Integrated Marketing",
		"meta-description"	=> "We’re an experienced, integrated full-service marketing agency and we want to blow your mind with our capabilities…",
	],

	"privacy-notice" => [
		"meta-title"		=> "Privacy Notice | Article 10 Integrated Marketing",
		"meta-description"	=> "We’re an experienced, integrated full-service marketing agency and we want to blow your mind with our capabilities…",
	],
];
