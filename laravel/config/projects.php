<?php

return [
			// Presentations

			"microsoft-social-engagement" => [
								"colours" => ["pink"],
								"potm" => false
								],
			"castrol-presentation" => [
								"colours" => ["pink"],
								"potm" => false
								],
			"entertainment-one-brand-toolkits" => [
								"colours" => ["pink"],
								"potm" => false
								],
			"sungard-presentation" => [
								"colours" => ["pink"],
								"potm" => false
								],

			"article10-bees" => [
								"colours" => ["pink"],
								"potm" => false
								],
			"sungard-presentation" => [
								"colours" => ["pink"],
								"potm" => false
								],

			// Digital

			"old-uppinghamian-website-redesign" => [
								"colours" => ["orange"],
								"potm" => false
								],
			"stoli-app-design" => [
								"colours" => ["orange"],
								"potm" => false
								],
			//"steljes-interactive-experience" => [
			//					"colours" => ["orange"],
			//					"potm" => false
			//					],
			"openbet-website-redesign" => [
								"colours" => ["orange"],
								"potm" => false
								],
			//"mail-online" => [
			//					"colours" => ["orange"],
			//					"potm" => false
			//					],
			"thre-tablet-presentation" => [
								"colours" => ["orange"],
								"potm" => false
								],

			// Infosec



			// Video
			"omni-channel" => [
								"colours" => ["red"],
								"potm" => false
								],
			"etf-advert" => [
								"colours" => ["red"],
								"potm" => false
								],
			// "future-decoded-2014" => [
			// 					"colours" => ["red"],
			// 					"potm" => false
			// 					],
			"future-decoded-2015" => [
								"colours" => ["red"],
								"potm" => false
								],
			"stoli-video-production" => [
								"colours" => ["red"],
								"potm" => false
								],
			//"steljes-interactive-experience" => [
			//					"colours" => ["red"],
			//					"potm" => false
			//					],

			// exhibitions

			"bdp-exhibition-design" => [
								"colours" => ["yellow"],
								"potm" => false
								],
			"thor-exhibition-design" => [
								"colours" => ["yellow"],
								"potm" => false
								],

			"mipim-exhibition-design" => [
								"colours" => ["yellow"],
								"potm" => false
								],
			"mail-online-exhibition-design" => [
								"colours" => ["yellow"],
								"potm" => false
								],

			"thre-real-estate-exhibition-design" => [
								"colours" => ["yellow"],
								"potm" => false
								],

			"thre-real-estate-office-environments" => [
								"colours" => ["yellow"],
								"potm" => false
								],

			// Campaigns

			"aarhus" => [
								"colours" => ["green"],
								"potm" => false
								],
			"kfc-people-promise" => [
								"colours" => ["green"],
								"potm" => false
								],
			"microsoft-mva" => [
								"colours" => ["green"],
								"potm" => false
								],
			"microsoft-universities" => [
								"colours" => ["green"],
								"potm" => false
							],
			"etf-torino-process" => [
								"colours" => ["green"],
								"potm" => false
							],
];
