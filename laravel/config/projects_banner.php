<?php

return [
	
	// openbet banner items
	"omni-channel" => [
		 "omni-channel_panel_1"
		,"omni-channel_panel_2"
		,"omni-channel_panel_3"
		,"omni-channel_panel_4"
		,"omni-channel_panel_5"
	],
	
	// microsoft editorial
	"microsoft-universities" => [
		 "microsoft_universities"
		,"microsoft_universities_alt"
	],
	
	// future decoded
	"future-decoded-2014" => [
		 "future-decoded-2014_panel_1"
		,"future-decoded-2014_panel_2"
		,"future-decoded-2014_panel_3"
		,"future-decoded-2014_panel_4"
		,"future-decoded-2014_panel_5"
	
	],
	
	// future decoded
	"future-decoded-2015" => [
		 "future-decoded-2015_panel_1"
		,"future-decoded-2015_panel_2"
		,"future-decoded-2015_panel_3"
		,"future-decoded-2015_panel_4"
		,"future-decoded-2015_panel_5"
	
	],
	
	// etf-advert
	"etf-advert" => [
		 "etf-advert_panel_1"
		,"etf-advert_panel_2"
		,"etf-advert_panel_3"
		,"etf-advert_panel_4"
		,"etf-advert_panel_5"
	
	],
	
	// microsoft-social-engagement
	"microsoft-social-engagement" => [
		 [
			 "microsoft-social-engagement_panel_1"
			,"microsoft-social-engagement_panel_2"
			,"microsoft-social-engagement_panel_3"
		]
		,[
			 "microsoft-social-engagement_panel_1_screen"
			,"microsoft-social-engagement_panel_2_screen"
			,"microsoft-social-engagement_panel_3_screen"
		]
	],
	
	// article10-bees
	"article10-bees" => [
		 [
			 "article10-bees_panel_1"
			,"article10-bees_panel_2"
			,"article10-bees_panel_3"
		]
		,[
			 "article10-bees_panel_1_screen"
			,"article10-bees_panel_2_screen"
			,"article10-bees_panel_3_screen"
		]
	],
	
	// sungard-animated-looping-presentation
	"sungard-presentation" => [
		 [
			 "sungard-presentation_panel_1"
			,"sungard-presentation_panel_2"
			,"sungard-presentation_panel_3"
			,"sungard-presentation_panel_4"
		]
		,[
			 "sungard-presentation_panel_1_screen"
			,"sungard-presentation_panel_2_screen"
			,"sungard-presentation_panel_3_screen"
		]
	],
	
	// microsoft-social-engagement power point 2
	"microsoft-social-engagement-power-point-2" => [
		 "microsoft-social-engagement_panel_1"
		,"microsoft-social-engagement_panel_2"
		,"microsoft-social-engagement_panel_3"
		,"microsoft-social-engagement_panel_4"
	],
	
	// microsoft-social-engagement power point 2
	"microsoft-mva" => [
		 "microsoft-mva_panel_1"
		,"microsoft-mva_panel_2"
		,"microsoft-mva_panel_3"
		,"microsoft-mva_panel_4"
		,"microsoft-mva_panel_5"
		,"microsoft-mva_panel_6"
	],
	
	// steljes interactive experience
	"steljes-interactive-experience" => [
		 "steljes-interactive-experience_panel_1"
		,"steljes-interactive-experience_panel_2"
		,"steljes-interactive-experience_panel_3"
		,"steljes-interactive-experience_panel_4"
		,"steljes-interactive-experience_panel_5"
	
	],
	
	// stoli video production
	"stoli-video-production" => [
		 "stoli-video-production_panel_1"
		,"stoli-video-production_panel_2"
		,"stoli-video-production_panel_3"
		,"stoli-video-production_panel_4"
		,"stoli-video-production_panel_5"
		,"stoli-video-production_panel_6"
		,"stoli-video-production_panel_7"
		,"stoli-video-production_panel_8"
	
	],
	
	// castrol presentation
	"castrol-presentation" => [
		 "castrol-presentation_panel_1_screen"
		,"castrol-presentation_panel_2_screen"
		,"castrol-presentation_panel_3_screen"
	],
];