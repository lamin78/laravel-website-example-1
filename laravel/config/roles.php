<?php

return [

	'all' => [
		'administrator' => [
    			'title'			=> 'Administrator',
                'level'         => 1000,
                'permissions'	=> [
                					'administrator',
                					'manager',

                					'menu_users',
                					'create_user',
                					'edit_user',

                					'reporting'
                					]

    	]
	]

	
];
