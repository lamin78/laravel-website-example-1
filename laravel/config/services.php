<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => 'pGlMO2wktPR4mmcfHHYtCg',
	],

	'ses' => [
		'key'	  => env('AWS_ACCESS_KEY_ID'),
		'secret'  => env('AWS_SECRET_ACCESS_KEY'),
		'region'  => env('AWS_REGION'),						
		//'curl.options' => array(
		//	'CURLOPT_CONNECTTIMEOUT' => 60
		//),				
		'version' => 'latest',
	],

	'stripe' => [
		'model'  => 'App\Models\User',
		'secret' => '',
	],

];
