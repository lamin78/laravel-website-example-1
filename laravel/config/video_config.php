<?php

/**
 * Carousel configuration
 *
 */
return [
	// general video
	"home" => [
		 'video'					=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Home_Page_header.mp4'
		,'poster'					=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Home_Page+showreel+thumbnail.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-home.jpg'
	],

	"video-animation-3d" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/VideoLandingPage_LQ.mp4'
		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Motion_Showreel+Thumbnail+2.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-home.jpg'
	],

	"presentation-solutions" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/pres_landing_full_header_video.mp4'
		,'poster'	=> '/images/services-presentation-page/presentation-landing-page-header@1x.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-presentation.jpg'
	],

	"exhibitions" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/EventsPage_LQ.mp4'
		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Events_Snapshot.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-exhibitions.jpg'
	],

	"default" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/CulturePage_LQ.mp4'
		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/culture_showreel_poster3.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-exhibitions.jpg'
	],

	// "etf-advert" => [
	// 	 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/CulturePage_LQ.mp4'
	// 	,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/culture_showreel_poster3.jpg'
	// 	,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-etf.jpg'
	// ],

	"services" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/culture-still.jpg'
		,'poster'	=> '/images/services-landing-page/services-landing-page-header.jpg'
	],

	"projects" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/HomePage_LQ.mp4'
		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Mixed+showreel+thumbnail.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-projects.jpg'
	],

	"about" => [
		 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/CulturePage_LQ.mp4'
		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/About+showreel+thumbnail+1.jpg'
		,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-about.jpg'
	],

	"microsoft-social-engagement" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/Microsoft_A10Website_Full+Header+Video_RFW.mp4'
	 		,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/images/pres/header/Microsoft-SE+in+situ+header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-microsoft.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Microsoft_A10Website_Single+Animated+Slide_Cropped_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/Microsoft_A10Website_Single%2BAnimated%2BSlide_Cropped_compressed_poster.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Microsoft_A10Website_Full+Video_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/Microsoft_A10Website_Full%2BVideo_compressed_poster.jpg'
		]
	],

	"entertainment-one-brand-toolkits" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/eOne-A10Website_Full+Header+Video_RFW.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/images/pres/header/eOne+in+situ+header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-eone.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/eOne-A10Website_Full+Video_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/eOne-A10Website_Full%2BVideo_compressed_poster.jpg'
		]
	],

	"sungard-presentation" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/SunGard_A10Website_Full+Header+Video_RFW.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/images/pres/header/SunGard+in+situ+header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-sungard.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/SunGard_A10Website_Full_Video_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/SunGard_A10Website_Full%2BVideo_compressed_poster.jpg'
		]
	],

	"article10-bees" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/Bee-Prezi_A10Website_Full+Header+Video_RFW.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/images/pres/header/BEES-in-situ-header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-bees.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Bee-Prezi_A10Website_Full+Video_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/Bee-Prezi_A10Website_Full%2BVideo_compressed_poster.jpg'
		]
	],

	"castrol-presentation" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/pres/Castrol_A10Website_Full+Header+Video_RFW.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/images/pres/header/Castrol+in+situ+header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-castrol-presentation.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Castrol_A10Website_Full+Video_compressed.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/Castrol_A10Website_Full%2BVideo_compressed_poster.jpg'
		],
	],

	"omni-channel" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/OpenBet/OpenBet_Omni-Channel_header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/OpenBet_Snapshot.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-ob.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/OpenBet/OpenBet_Omni-Channel_main+vid.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/OpenBet_Snapshot.jpg'
		],
	],

	"etf-advert" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF_10sec_header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Kayak_Snapshot.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-etf.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF_10sec_header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Kayak_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF-Video+1.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Kayak_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF-Video+2.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Dance_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF-Video+3.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Postman.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF-Video+4.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Chef_Snapshot_1.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/ETF/ETF-Video+5.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/ETF_Farmer_Snapshot.jpg'
		],
	],

	"future-decoded-2014" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_last+year/Microsoft_future_10sec_Header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Future+Decoded_2014.jpg'
		],
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_last+year/Microsoft_future_10sec_Header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Future+Decoded_2014.jpg'
		],
	],

	"future-decoded-2015" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_Header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Future+Decoded_Digital+Visionaries.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-fd-2015.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_1.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/MS_Future_Decoded_2015_Vid_1.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_2.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/MS_Future_Decoded_2015_Vid_2.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_3.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/MS_Future_Decoded_2015_Vid_3.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_4.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/MS_Future_Decoded_2015_Vid_4.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Future_Decoded_this+year/MS_Future_Decoded_2015_Vid_5.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/MS_Future_Decoded_2015_Vid_5.jpg'
		],
	],

	"stoli-video-production" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid_HEader.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Mule_Snapshot.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-stoli.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid1.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Mule_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid2.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Mule_Blueberry+and+Apple_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid3.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Irish+Mule_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid4.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Mule_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid5.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Bom+Bon_Lemonade+snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid6.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Lemonade+Crush_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid7.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Lemonade_Snapshot.jpg'
		],[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/Stoli/Stoli_Cocktails_vid8.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/Stoli_Razberi+and+mint+lemonade_Snapshot.jpg'
		],
	],

	"thre-tablet-presentation" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/THRE_Globe/THRE_Globe_header.mp4'
			,'poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/THRE_Globe_Snapshot.jpg'
			,'mobile-poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/video_poster_frames/THRE_Globe_Snapshot.jpg'
		],
	],

	"thre-real-estate-office-environments" => [
		[
			 'video'					=> 'https://s3-eu-west-1.amazonaws.com/article10/video/THRE_Gallery_Interior.mp4'
			,'poster'					=> 'https://s3-eu-west-1.amazonaws.com/article10/THRE_Office_Environments.jpg'
			,'mobile-poster'	=> 'https://s3-eu-west-1.amazonaws.com/article10/THRE_Office_Environments.jpg'
		],
	],

	"openbet-website-redesign" => [
		[
			 'video'	=> 'https://s3-eu-west-1.amazonaws.com/article10/video/OpenBet_GrandNational_header.mp4'
			,'poster'	=> '/images/projects/openbet-website-redesign/openbet-website-redesign-header.jpg'
			,'mobile-poster'	=> '/images/mobile-header-images/mobile-header-ob.jpg'
		],
	],
];
