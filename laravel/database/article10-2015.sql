-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2017 at 11:22 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `article10-2015`
--

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_options`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_options` (
`id` int(10) unsigned NOT NULL,
  `option_text` text COLLATE utf8_unicode_ci NOT NULL,
  `out_id` text COLLATE utf8_unicode_ci NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_options`
--

INSERT INTO `cui_conversation_options` (`id`, `option_text`, `out_id`, `type`, `active`, `created_at`, `updated_at`) VALUES
(1, '', '2', 'goto', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '', '3', 'goto', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Yes', '26', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'No', '4', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '', '5', 'goto', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '', '6|7', 'input', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '', '8', 'goto', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Yes, tell me about yourself.\n', '27', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'No, I''d rather hear about Article 10', '9', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Actually, I have a specific question', '20', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Digital', '10', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Branding & Print', '12', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Video & Animation', '13', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Presentations', '14', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Office Development', '15', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Content', '16', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Exhibitions', '17', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '', '25', 'input', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'No, thanks', '22', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'Go for it!', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '', '36', 'input', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'That''s fantastic.', '8', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Sales', '28', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Recruitment', '29', 'option', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Internships ', '30', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Information Security', '31', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Ok, lets get back to business.', '8', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'This is intense.', '43', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Yeah? Who sent you?', '44', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Oh, this is deep.', '', 'option', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'So this other guy? He''s a conversation UI like you, right?', '45', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'You mean more advanced than you are?', '46', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'What the hell does that mean?', '49', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Where are we going?', '47', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'I''d just like a quick tour, please.', '9', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Tell me about you, Sage.', '27', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Tell me about Article 10', '9', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Thanks, I''m good.', '25', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '[LINK TO VIDEO]', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '[LINK TO WRITINGS]', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Thanks, I''ve seen enough about that. What else does Article 10 do?', '50', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '[LINKS TO FB, TWITTER ETC.]', '', 'link', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'I just want a general overview.', '51', 'option', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_text`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_text` (
`id` int(10) unsigned NOT NULL,
  `tid` int(10) unsigned NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_text`
--

INSERT INTO `cui_conversation_text` (`id`, `tid`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 12, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 13, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 14, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 15, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 16, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 17, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 18, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 19, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 20, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 21, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 22, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 23, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 24, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 25, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 29, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 30, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 31, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 32, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 33, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 34, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 28, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 35, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 36, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 37, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 38, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 40, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 39, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 27, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 41, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_text_options`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_text_options` (
`id` int(10) unsigned NOT NULL,
  `conversation_text` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_text_options`
--

INSERT INTO `cui_conversation_text_options` (`id`, `conversation_text`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Hi, I''m Sage|Hello, I''m Sage|Whatup, Sage in da house!|Good day to you, my name is Sage', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Welcome to the Article 10 website. I''d be happy to show you around or answer any questions.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'You look familiar... have you been here before?|Do you come here often?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'My mistake!|My apologies, you humans tend to look alike!|Yes you have! I am never wrong!', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'I see you''ve come from {source}... May I ask your name?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'That''s a strange name!|What an odd name...!|You humans really should consider using numbers instead...your the 26th {name} I''ve met today|{name}...reminds me of a chip I once blew|Oh dear {name}, your name resonates prestige!', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'It''s nice to meet you {name}. Would you like to know more about me?|Welcome {name} :)|{name}...a good strong name right there!|I have a serial bus named {name}', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Ok, are you interested in sales, recruitment, internships or our information security awareness offering? Or do you just want to have a general look around?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'No problem! Which area of Article 10''s work would you like to know about?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'We have some great Digital case studies – would you like to see them?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Would you like to check out our other areas of creative expertise?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'We have some great Branding & Print case studies – would you like to see them?', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'We have some great Video & Animation case studies – would you like to see them?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'We have some great Presentations case studies – would you like to see them?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'We have some great Office Development case studies – would you like to see them?', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'We have some great Content case studies – would you like to see them?', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'We have some great Exhibitions case studies – would you like to see them?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Fire away!', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Can I get back to you about that? If you leave your email address, we''ll be in touch soon.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'Sorry, I can''t help you without an email address. Is there anything else I can do for you?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'Thanks! Is there anything else I can do for you?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '*About page GENERAL INFO* + [LINK TO COMPANY BLOG]', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Have you seen enough or can I help you with something else?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Okay! Thanks for coming by. I''d love to hear any feedback you have about your experience on the Article 10 website, and feel free to share or bookmark the site, too!', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Would you like to hear more about this?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'Article 10 is one big team of talent, working across seven creative divisions. Which area of work would you like to know about?', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Thanks, good bye!', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'Then welcome back!|It''s good to see a friendly face :)|Ah yes I remember your IP! ', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'I''m a cybernetic organism. Living tissue over metal endoskeleton.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'Sales text', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'Recruitment text', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'Internships text', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'Information security awareness text', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'My mission is to protect you.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'You did. Thirty-five years from now, you reprogrammed me to be your protector here, in this time.', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Not like me. A T-1000. Advanced prototype.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Yes. A mimetic polyalloy.', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Liquid metal.', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'We have to get out of the city immediately. And avoid the authorities.', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '*About page GENERAL INFO* + [LINK TO COMPANY BLOG]', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_text_options_linkages`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_text_options_linkages` (
`id` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `opid` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_text_options_linkages`
--

INSERT INTO `cui_conversation_text_options_linkages` (`id`, `cid`, `opid`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 3, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 6, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 7, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 8, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 8, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 9, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 9, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 9, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 9, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 9, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 9, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 9, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 11, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 11, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 13, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 13, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 14, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 14, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 17, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 17, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 7, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 20, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 10, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 10, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 25, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 26, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 8, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 8, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 8, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 27, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 43, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 44, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 45, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 46, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 49, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 47, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 22, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 22, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 22, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 12, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 12, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 15, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 15, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 16, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 16, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 17, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 17, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 28, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 29, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 30, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 31, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 31, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 31, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 9, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 30, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 28, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 29, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 27, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 43, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 44, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 45, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 46, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 49, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_url_data`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_url_data` (
`id` int(10) unsigned NOT NULL,
  `link_text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_url_data`
--

INSERT INTO `cui_conversation_url_data` (`id`, `link_text`, `created_at`, `updated_at`) VALUES
(1, '/services/digital-solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '/services/video-animation-3d', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '/services/presentation-solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '/services/exhibitions', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '/services/information-security', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '/contact', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cui_conversation_url_linkages`
--

CREATE TABLE IF NOT EXISTS `cui_conversation_url_linkages` (
`id` int(10) unsigned NOT NULL,
  `oid` int(10) unsigned NOT NULL,
  `lid` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cui_conversation_url_linkages`
--

INSERT INTO `cui_conversation_url_linkages` (`id`, `oid`, `lid`, `created_at`, `updated_at`) VALUES
(1, 19, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 49, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 50, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 51, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 52, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 21, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 22, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 23, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 26, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cui_conversation_options`
--
ALTER TABLE `cui_conversation_options`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cui_conversation_text`
--
ALTER TABLE `cui_conversation_text`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cui_conversation_text_options`
--
ALTER TABLE `cui_conversation_text_options`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cui_conversation_text_options_linkages`
--
ALTER TABLE `cui_conversation_text_options_linkages`
 ADD PRIMARY KEY (`id`), ADD KEY `cui_conversation_text_options_linkages_cid_index` (`cid`), ADD KEY `cui_conversation_text_options_linkages_opid_index` (`opid`);

--
-- Indexes for table `cui_conversation_url_data`
--
ALTER TABLE `cui_conversation_url_data`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cui_conversation_url_linkages`
--
ALTER TABLE `cui_conversation_url_linkages`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cui_conversation_options`
--
ALTER TABLE `cui_conversation_options`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `cui_conversation_text`
--
ALTER TABLE `cui_conversation_text`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `cui_conversation_text_options`
--
ALTER TABLE `cui_conversation_text_options`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `cui_conversation_text_options_linkages`
--
ALTER TABLE `cui_conversation_text_options_linkages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `cui_conversation_url_data`
--
ALTER TABLE `cui_conversation_url_data`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cui_conversation_url_linkages`
--
ALTER TABLE `cui_conversation_url_linkages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cui_conversation_text_options_linkages`
--
ALTER TABLE `cui_conversation_text_options_linkages`
ADD CONSTRAINT `cui_conversation_text_options_linkages_cid_foreign` FOREIGN KEY (`cid`) REFERENCES `cui_conversation_text` (`id`),
ADD CONSTRAINT `cui_conversation_text_options_linkages_opid_foreign` FOREIGN KEY (`opid`) REFERENCES `cui_conversation_options` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
