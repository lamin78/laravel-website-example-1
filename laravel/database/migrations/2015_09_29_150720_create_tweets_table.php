<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tweets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('tweet_id');
			$table->string('text');
			$table->integer('user_id')->unsigned();
			$table->string('media_url');
			$table->string('media_local');
			$table->enum('status', ['pending', 'ready', 'disabled']);
			$table->timestamp('tweeted_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweets');
	}

}
