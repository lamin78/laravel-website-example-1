<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVimeoVidsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vimeo_vids', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('uri');
			$table->string('link');
			$table->string('embed');
			$table->string('media_url');
			$table->string('media_local');
			$table->enum('status', ['pending', 'ready', 'disabled']);
			$table->timestamp('created_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vimeo_vids');
	}

}
