<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkedInsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('linked_ins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('text');
			$table->string('uri');
			$table->string('link');
			$table->enum('status', ['pending', 'ready', 'disabled']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('linked_ins');
	}

}
