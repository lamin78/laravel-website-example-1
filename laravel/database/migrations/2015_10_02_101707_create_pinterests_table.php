<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pinterests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pin_id');
			$table->string('note');
			$table->string('link');
			$table->string('media_url');
			$table->string('media_local');
			$table->enum('status', ['pending', 'ready', 'disabled']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pinterests');
	}

}
