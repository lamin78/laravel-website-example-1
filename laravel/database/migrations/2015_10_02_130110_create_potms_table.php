<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('potms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uri');
			$table->string('media_local');
			$table->enum('status', ['pending', 'ready', 'disabled']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('potms');
	}

}
