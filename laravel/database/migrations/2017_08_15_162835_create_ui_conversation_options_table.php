<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUiConversationOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cui_conversation_options', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('option_text');
			$table->text('out_id');
			$table->text('type');
			$table->enum('active', ['0', '1'])->default('1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cui_conversation_options');
	}

}
