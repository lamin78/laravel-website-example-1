<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUiConversationUrlLinkagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cui_conversation_url_linkages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('oid')->unsigned();
			$table->integer('lid')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cui_conversation_url_linkages');
	}

}
