<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUiConversationTextOptionsLinkages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cui_conversation_text_options_linkages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cid')->unsigned()->index();
			$table->foreign('cid')->references('id')->on('cui_conversation_text');
			$table->integer('opid')->unsigned()->index();
			$table->foreign('opid')->references('id')->on('cui_conversation_options');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cui_conversation_text_options_linkages');
	}
}
