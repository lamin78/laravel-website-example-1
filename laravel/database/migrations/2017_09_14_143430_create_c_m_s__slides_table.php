<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCMSSlidesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('c_m_s__slides', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cid')->references('id')->on('c_m_s__case__studies')->onDelete('cascade');;
			$table->text('slide_1')->nullable();
			$table->text('slide_2')->nullable();
			$table->integer('order')->nullable();
			$table->enum('active', array('0','1'))->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('c_m_s__slides');
	}

}
