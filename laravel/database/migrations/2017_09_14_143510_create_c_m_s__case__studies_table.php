<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCMSCaseStudiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('c_m_s__case__studies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('casestudy_title')->nullable();
			$table->text('casestudy_subtitle')->nullable();
			$table->text('casestudy_slug')->nullable();
			$table->text('casestudy_hashed_url')->nullable();
			$table->text('casestudy_header_image')->nullable();
			$table->text('casestudy_header')->nullable();
			$table->text('casestudy_sub_header')->nullable();
			$table->text('casestudy_client')->nullable();
			$table->text('casestudy_year')->nullable();
			$table->text('casestudy_location')->nullable();
			$table->text('casestudy_category')->nullable();
			// $table->text('casestudy_the_company')->nullable();
			// $table->text('casestudy_the_brief')->nullable();
			// $table->text('casestudy_the_approach')->nullable();
			// $table->text('casestudy_the_process')->nullable();
			// $table->text('casestudy_the_result')->nullable();
			$table->text('casestudy_stat_1')->nullable();
			$table->text('casestudy_stat_2')->nullable();
			$table->text('casestudy_stat_3')->nullable();
			$table->text('casestudy_colour')->nullable();
			$table->enum('status', ['pending', 'live', 'disabled']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('c_m_s__case__studies');
	}

}
