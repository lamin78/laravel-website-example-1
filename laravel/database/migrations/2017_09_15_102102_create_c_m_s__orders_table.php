<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCMSOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('c_m_s__orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('cid')->references('id')->on('c_m_s__case__studies')->onDelete('cascade');
			$table->integer('order')->nullable();
			$table->text('data_ident')->nullable();
			$table->text('data_name')->nullable();
			$table->text('data_type')->nullable();
			$table->integer('sid')->nullable();
			$table->enum('active', array('0','1'))->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('c_m_s__orders');
	}
}
