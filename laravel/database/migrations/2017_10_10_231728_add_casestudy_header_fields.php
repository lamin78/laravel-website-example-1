<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCasestudyHeaderFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('c_m_s__case__studies', function($table) {
        $table->text('casestudy_header_video_link');
				$table->text('casestudy_header_video_poster');
				$table->text('casestudy_header_video_mobile_poster');
				$table->enum('header_image_active', ['true', 'false']);
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('c_m_s__case__studies', function($table) {
				$table->dropColumn('casestudy_header_video_link');
				$table->dropColumn('casestudy_header_video_poster');
				$table->dropColumn('casestudy_header_video_mobile_poster');
				$table->dropColumn('header_image_active', ['true', 'false']);
    });
	}

}
