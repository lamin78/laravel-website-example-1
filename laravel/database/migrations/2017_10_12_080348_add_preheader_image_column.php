<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreheaderImageColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('c_m_s__case__studies', function($table) {
			$table->text('casestudy_header_video_pre_poster');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('c_m_s__case__studies', function($table) {
			$table->dropColumn('casestudy_header_video_pre_poster');
		});
	}

}
