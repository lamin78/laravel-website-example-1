<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThemeFlagToCarousel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('c_m_s__carousels', function(Blueprint $table)
		{
			$table->enum('theme', array('light','dark'))->default('dark');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('c_m_s__carousels', function(Blueprint $table)
		{
			$table->dropColumn('theme');
		});
	}

}
