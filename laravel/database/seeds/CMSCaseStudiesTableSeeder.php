<?php

use Illuminate\Database\Seeder;

class CMSCaseStudiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('c_m_s__case__studies')->delete();
        
        \DB::table('c_m_s__case__studies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'casestudy_title' => 'Test Case Study A',
                'casestudy_subtitle' => 'test A',
                'casestudy_slug' => 'test-case-study-a',
                'casestudy_hashed_url' => 'pgHnsUh',
                'casestudy_header_image' => 'test-case-study-a.jpg',
                'casestudy_header' => 'test A',
                'casestudy_sub_header' => 'test A',
                'casestudy_client' => 'test A',
                'casestudy_year' => 'test A',
                'casestudy_location' => 'test A',
                'casestudy_category' => 'Digital',
                'casestudy_stat_1' => 'test A',
                'casestudy_stat_2' => 'test A',
                'casestudy_stat_3' => 'test A',
                'casestudy_colour' => 'test A',
                'status' => 'live',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '2017-09-22 22:33:21',
            ),
            1 => 
            array (
                'id' => 2,
                'casestudy_title' => 'Test Case Study B',
                'casestudy_subtitle' => 'test B',
                'casestudy_slug' => 'test-case-study-b',
                'casestudy_hashed_url' => '4NoalBG',
                'casestudy_header_image' => NULL,
                'casestudy_header' => 'test B',
                'casestudy_sub_header' => 'test B',
                'casestudy_client' => 'test B',
                'casestudy_year' => 'test B',
                'casestudy_location' => 'test B',
                'casestudy_category' => 'test B',
                'casestudy_stat_1' => 'test B',
                'casestudy_stat_2' => 'test B',
                'casestudy_stat_3' => 'test B',
                'casestudy_colour' => 'test B',
                'status' => 'live',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
        ));
        
        
    }
}
