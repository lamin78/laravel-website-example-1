<?php

use Illuminate\Database\Seeder;

class CMSOrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('c_m_s__orders')->delete();
        
        \DB::table('c_m_s__orders')->insert(array (
            0 => 
            array (
                'id' => 22,
                'cid' => 1,
                'order' => 1,
                'data_name' => 'text',
                'data_type' => 'text',
                'sid' => 1,
                'active' => '1',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            1 => 
            array (
                'id' => 23,
                'cid' => 1,
                'order' => 2,
                'data_name' => 'parallax',
                'data_type' => 'image',
                'sid' => 1,
                'active' => '1',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            2 => 
            array (
                'id' => 24,
                'cid' => 1,
                'order' => 3,
                'data_name' => 'text',
                'data_type' => 'text',
                'sid' => 2,
                'active' => '1',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            3 => 
            array (
                'id' => 25,
                'cid' => 1,
                'order' => 5,
                'data_name' => 'text',
                'data_type' => 'text',
                'sid' => 3,
                'active' => '1',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            4 => 
            array (
                'id' => 26,
                'cid' => 1,
                'order' => 4,
                'data_name' => 'parallax',
                'data_type' => 'image',
                'sid' => 2,
                'active' => '1',
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
        ));
        
        
    }
}
