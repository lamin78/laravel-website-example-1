<?php

use Illuminate\Database\Seeder;

class CMSParallaxesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('c_m_s__parallaxes')->delete();
        
        \DB::table('c_m_s__parallaxes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'cid' => 1,
                'image' => 'test-case-study-a-parallax-1.jpg',
                'order' => 1,
                'active' => '1',
                'created_at' => '2017-09-22 22:51:12',
                'updated_at' => '2017-09-22 22:51:22',
            ),
            1 => 
            array (
                'id' => 2,
                'cid' => 1,
                'image' => 'test-case-study-a-parallax-2.jpg',
                'order' => 2,
                'active' => '1',
                'created_at' => '2017-09-24 22:28:51',
                'updated_at' => '2017-09-24 22:29:13',
            ),
        ));
        
        
    }
}
