<?php

use Illuminate\Database\Seeder;

class CMSTextsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('c_m_s__texts')->delete();
        
        \DB::table('c_m_s__texts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'cid' => 1,
                'text_type' => 'company',
                'text_field' => '<p><strong>About the company</strong></p>
<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
                'order' => 1,
                'active' => '1',
                'created_at' => '2017-09-22 22:33:41',
                'updated_at' => '2017-09-24 19:43:01',
            ),
            1 => 
            array (
                'id' => 2,
                'cid' => 1,
                'text_type' => 'approach',
                'text_field' => '<p><strong>Some approach information</strong></p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'order' => 2,
                'active' => '1',
                'created_at' => '2017-09-22 22:52:00',
                'updated_at' => '2017-09-24 19:43:36',
            ),
            2 => 
            array (
                'id' => 3,
                'cid' => 1,
                'text_type' => 'company',
                'text_field' => '<p><strong>Some result</strong></p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'order' => 3,
                'active' => '1',
                'created_at' => '2017-09-24 22:27:43',
                'updated_at' => '2017-09-24 22:28:41',
            ),
        ));
        
        
    }
}
