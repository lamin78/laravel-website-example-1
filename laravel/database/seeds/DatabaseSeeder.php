<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0');

		// Add truncates here


		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		Model::unguard();

		$this->call('PermissionsTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('SearchesTableSeeder');
    // $this->call('CMSCaseStudiesTableSeeder');
    // $this->call('CMSParallaxesTableSeeder');
    // $this->call('CMSSlidesTableSeeder');
    // $this->call('CMSVideosTableSeeder');
    // $this->call('CMSCarouselsTableSeeder');
    // $this->call('CMSOrdersTableSeeder');
    // $this->call('CMSTextsTableSeeder');
    }

}
