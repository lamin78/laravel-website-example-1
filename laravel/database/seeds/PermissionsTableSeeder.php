<?php

/**
 * Class Dependencies
 *
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permissions;

class PermissionsTableSeeder extends Seeder {

	/**
	 * Run seeder for Permissions table
	 * 
	 */
    public function run() {
       
        $roles_array = Config::get('roles.all');

        $permissions_array = [];

        foreach($roles_array as $key => $data) {
            $permissions_array = array_merge($permissions_array,$data['permissions']);
        }
        $permissions_array = array_unique($permissions_array);

        foreach($permissions_array as $key => $data) {
            $this->command->info('Seeding Permissions table with ' . $data);

    		Permissions::create(
        		array(
                    'name'           => $data
        		)
            );
        }
	
    }

}

?>