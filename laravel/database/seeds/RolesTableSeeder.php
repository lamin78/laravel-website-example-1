<?php

/**
 * Class Dependencies
 *
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Roles;

class RolesTableSeeder extends Seeder {

	/**
	 * Run seeder for roles table
	 * 
	 */
    public function run() {
        
        $roles_array = Config::get('roles.all');

        foreach($roles_array as $key => $data) {
            $this->command->info('Seeding Roles table with ' . $data['title']);
            $role = Roles::create(
                array(
                    'title'         => $data['title'],
                    'ref'           => $key,
                    'level'         => $data['level']
                )
            );

            $role->linkPermissions($data['permissions']);
        }
    }

}

?>