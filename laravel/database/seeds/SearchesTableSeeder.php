<?php

/**
 * Class Dependencies
 *
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Searches;

class SearchesTableSeeder extends Seeder {

	/**
	 * Run seeder for user table
	 * 
	 */
    public function run() {
        $this->command->info('Seeding Searches table with something...');

		$new_search = new Searches;
        $new_search->tweet_id = "47702568536";
        $new_search->save();
    }

}

?>