<?php

/**
 * Class Dependencies
 *
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserTableSeeder extends Seeder {

	/**
	 * Run seeder for user table
	 *
	 */
    public function run() {
      $this->command->info('Seeding Users table with Andy...');

      $user = User::create(
        array(
        	'name'			  => 'Andy Talbot'
        	,'email'		  => 'oindypoind@gmail.com'
        	,'password' 	  => '$2y$10$JG7MobPdcP3niMhNbrHb.uMYzkB.3MlP7vZ2JH3JOmhoPqTKcoDQG'
        )
      );

      $this->command->info('Account created...');
      $user->assignRole('administrator');

      $this->command->info('Seeding Users table with Lamin...');

      $user = User::create(
        array(
          'name'            => 'Lamin Camps'
          ,'email'          => 'lamin@redtangle.co.uk'
          ,'password'       => bcrypt('lamin123')
        )
      );

      $this->command->info('Account created...');
      $user->assignRole('administrator');

      $this->command->info('Seeding Users table with test user...');

      $user = User::create(
        array(
          'name'            => 'Test User'
          ,'email'          => 'test@article10.com'
          ,'password'       => bcrypt('test123')
        )
      );

      $this->command->info('Account created...');
      $user->assignRole('administrator');


      $this->command->info('Seeding Users table with Matt...');

      $user = User::create(
        array(
          'name'            => 'Matt Hooker'
          ,'email'          => 'Matt.H@article10.com'
          ,'password'       => '$2y$10$ieOi6pGM0nExgZ99q/N05uA9n/.LCANNeW.U4BZ8O0frtfjx3mNhy'
        )
      );

      $this->command->info('Account created...');
      $user->assignRole('administrator');
    }
}

?>
