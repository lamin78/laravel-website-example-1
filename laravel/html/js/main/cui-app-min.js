'use strict';

/**
 * cui-app.js
 */
define(['jquery', 'axios', 'lodash', 'vuejs', 'cuiTextBlock', 'cuiInputGroup', 'cuiLoading', 'cuiWatchScreen', 'cuiHeaderBlock'], function ($, axios, _, Vue, CuiTextBlock, CuiInputGroup, CuiLoading, CuiWatchScreen, CuiHeaderBlock) {

	/**
  * Define foundation model view
  */
	var PageView = function PageView() {};

	PageView.prototype = {
		initialize: function initialize() {

			window._eventbus = new Vue({});
			window._watchScreen = new CuiWatchScreen({});

			new Vue({
				el: '#conversation_ui',
				data: {
					typing: false,
					backTrack: false,
					cui_data: [],
					cui_options: [],
					cui_inputs: [],
					name: '',
					username: ''
				},
				components: {
					'cuiheaderblock': CuiHeaderBlock,
					'cuitextblock': CuiTextBlock,
					'cuiloading': CuiLoading,
					'cuiinputgroup': CuiInputGroup
				},
				mounted: function mounted() {

					var innerCui = document.querySelector('.inner-cui');
					var downArrow = document.querySelector('.down-arrow');
					downArrow.classList.add('hidden');

					axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
					_eventbus.$on('send-choice', this.sendChoice);
					_eventbus.$on('send-input', this.sendInput);

					var threshold = false;
					$(".inner-cui").scroll(function (e) {
						if (!threshold) {
							threshold = true;
							setTimeout(function () {
								_eventbus.$emit('scrolling', { scrolltop: e.target.scrollTop });
								threshold = false;
							}, 0.1 * 1000);
						}
					});

					this.typing = true;
					setTimeout(this.getFirstLine, 1 * 1000);
				},

				methods: {

					// SHOW COMMENT SEQUENCE
					showError: function showError() {

						this.cui_data.push({
							type: 'cui_text',
							text: 'I\'m busy at the moment. Please come back later :)'
						});

						this.cui_options.push({
							type: 'option',
							option_text: 'Ok I\'ll come back later',
							out_id: '0'
						});
					},
					getFirstLine: function getFirstLine() {
						var _this = this;

						axios.get('/cui/get-next-line/1').then(function (response) {
							if (typeof response.data.empty !== 'undefined') _this.showError();else {
								_this.parseText(response.data.cui.conversationText);
								_this.parseOption(response.data.cui.options);
							}
							_this.typing = false;
						}).catch(function (error) {

							_this.typing = false;
						});
					},
					getNextLine: function getNextLine(id) {
						var _this2 = this;

						this.typing = true;
						setTimeout(function () {
							axios.get('/cui/get-next-line/' + id).then(function (response) {
								_this2.parseText(response.data.cui.conversationText);
								_this2.parseOption(response.data.cui.options);
								_eventbus.$emit('update-opacity');
								_this2.typing = false;
							}).catch(function (error) {
								_this2.typing = false;
							});
						}, (Math.random() * 2 + 1) * 500);
					},
					parseText: function parseText(string) {
						var textArr = string.split('|');
						var rInd = Math.floor(Math.random() * textArr.length) + 1 - 1;
						var chosenText = textArr.filter(function (text, ind) {
							if (ind === rInd) return text;
						});

						chosenText[0] = chosenText[0].replace('{source}', source);
						chosenText[0] = chosenText[0].replace('{name}', this.username);

						console.log(chosenText[0]);

						this.cui_data.push({
							type: 'cui_text',
							text: chosenText[0]
						});
					},
					parseOption: function parseOption(array) {
						var _this3 = this;

						this.backTrack = false;

						console.log(array);

						if (array[0].type === 'goto') {

							setTimeout(function () {
								if (array[0].out_id.includes('link|')) {

									var link = array[0].out_id;
									if (link.includes('link|')) link = link.replace('link|', 'https://');

									if (!link.includes('https://')) link = link.replace('www', 'https://www');

									var win = window.open(link, '_blank');
									if (win) win.focus();else alert('Please allow popups on this site, then refresh the page.');

									_this3.backTrack = true;
								} else if (array[0].out_id.includes('[condition|name]')) {

									var lineId = array[0].out_id.replace('[condition|name]', '');
									lineId = lineId.split('|');

									console.log(_this3.username);

									if (_this3.username === "") _this3.getNextLine(lineId[0]);else _this3.getNextLine(lineId[1]);
								} else _this3.getNextLine(array[0].out_id);
							}, (Math.random() * 2 + 1) * 300);
						} else {
							this.cui_options = [];
							// console.log(array);
							for (var i = 0; i < array.length; i++) {
								// console.log(array[i]);
								this.cui_options.push({
									type: array[i].type,
									option_text: array[i].option_text,
									out_id: array[i].out_id,
									link_text: typeof array[i].linkData !== 'undefined' ? array[i].linkData.link_text : null
								});

								// console.log(this.cui_options);
							}
						}
					},
					sendInput: function sendInput(params) {
						var _this4 = this;

						this.cui_options = [];
						this.cui_data.push({
							type: 'user_text',
							text: params.text
						});

						console.log(params);
						if (params.hasName === true) this.username = params.text;

						setTimeout(function () {
							_this4.typing = true;
							setTimeout(function () {
								var idRef = params.id.split('|');
								idRef = idRef[Math.floor(Math.random() * idRef.length) + 1 - 1];
								_this4.getNextLine(idRef);
							}, (Math.random() * 2 + 1) * 500);
						}, (Math.random() * 1 + 1) * 500);
					},
					sendChoice: function sendChoice(params) {
						var _this5 = this;

						var idRef = 0;

						this.cui_options = [];
						this.cui_data.push({
							type: 'user_text',
							text: params.text
						});

						console.log(params);

						if (params.id.includes('[condition|name]')) {

							var lineId = params.id.replace('[condition|name]', '');
							lineId = lineId.split('|');

							console.log(this.username);

							if (this.username === "") idRef = lineId[0];else idRef = lineId[1];
						} else {
							idRef = params.id.split('|');
							idRef = idRef[Math.floor(Math.random() * idRef.length) + 1 - 1];
						}

						setTimeout(function () {
							_this5.typing = true;
							setTimeout(function () {
								_this5.getNextLine(idRef);
							}, (Math.random() * 2 + 1) * 500);
						}, 1 * 500);
					}
				},
				computed: {}
			});
		}
	};

	return PageView;
});