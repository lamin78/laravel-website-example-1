'use strict';

/**
 * cui-header-block.js
 */
define(['axios', 'lodash', 'vuejs', 'tweenmax', 'q', 'cuiWatchScreen'], function (axios, _, Vue, TweenMax, Q, CuiWatchScreen) {

	/**
  * Define foundation model view
  */
	return Vue.component('cuiheader', {
		template: '#cui-header-template',
		props: [],
		data: function data() {
			return {
				logoText: {},
				logo: {},
				navInner: {}
			};
		},
		beforeDestroy: function beforeDestroy() {
			_eventbus.$off('hide-header', this.hideAction);
		},
		mounted: function mounted() {
			window._watchScreen = new CuiWatchScreen({});

			this.logoText = this.$el.querySelector('.logo-text > a > img');
			this.logo = this.$el.querySelector('.logo');
			this.navInner = this.$el.querySelector('.nav-inner');
			this.cuiMenu = this.$el.querySelector('.cui-menu');

			_eventbus.$on('hide-header', this.hideAction);
			_eventbus.$on('update-window');
		},

		methods: {
			hideAction: function hideAction() {
				console.log(window.outerWidth);
				this.hideLogoAction();
				if (_watchScreen.screenTest(window.outerWidth, '>=', 'md')) this.hideMenuAction();
			},
			showAction: function showAction() {
				this.showLogoAction();
				this.showMenuAction();
			},
			hideLogoAction: function hideLogoAction() {
				this.hideLogo().then(this.shrinkBox);
			},
			hideMenuAction: function hideMenuAction() {
				this.hideMenu().then(this.showBurger);
			},
			showLogoAction: function showLogoAction() {
				this.growBox().then(this.showLogo);
			},
			showMenuAction: function showMenuAction() {
				this.hideBurger().then(this.showMenu);
			},
			hideMenu: function hideMenu() {
				var _this = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this.navInner, .5, { css: { x: '100%', opacity: 0 }, ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
				});
			},
			showBurger: function showBurger() {
				var _this2 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this2.cuiMenu, .5, { css: { opacity: 1 }, ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
				});
			},
			hideBurger: function hideBurger() {
				var _this3 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this3.cuiMenu, .5, { css: { opacity: 0 }, ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
				});
			},
			showMenu: function showMenu() {
				var _this4 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this4.navInner, .5, { css: { x: '0%', opacity: 1 }, ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
				});
			},
			hideLogo: function hideLogo() {
				var _this5 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this5.logoText, .5, { x: '-90%', ease: Power3.easeInOut, onComplete: function onComplete() {
							_this5.logoText.classList.add('hidden');
							resolve();
						} });
				});
			},
			shrinkBox: function shrinkBox() {
				var _this6 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this6.logo, .5, { scale: '0.5', ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
					resolve();
				});
			},
			growBox: function growBox() {
				var _this7 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this7.logo, .5, { scale: '1.0', ease: Power3.easeInOut, onComplete: function onComplete() {
							_this7.logoText.classList.remove('hidden');
							resolve();
						} });
				});
			},
			showLogo: function showLogo() {
				var _this8 = this;

				return Q.promise(function (resolve, reject, notify) {
					TweenMax.to(_this8.logoText, .5, { x: '20%', ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve();
						} });
				});
			}
		},
		computed: {}
	});
});