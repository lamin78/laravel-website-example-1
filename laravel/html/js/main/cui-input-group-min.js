'use strict';

/**
 * cui-input-group.js
 */
define(['jquery', 'axios', 'lodash', 'vuejs', 'tweenmax', 'q'], function ($, axios, _, Vue, TweenMax, Q) {

	/**
  * Define foundation model view
  */
	return Vue.component('cuibuttongroup', {
		template: '#button-group-template',
		props: ['optionData', 'backTrack'],
		data: function data() {
			return {
				option: 'an option'
			};
		},
		mounted: function mounted() {},
		updated: function updated() {
			var _this = this;

			this.$nextTick(function () {
				console.log('updated');

				if (typeof _this.optionData[0] !== 'undefined') {
					$('.inputField').focus();
				}
			});
		},

		methods: {
			parseLink: function parseLink(link) {
				if (link.includes('link|')) link = link.replace('link|', 'https://');

				if (!link.includes('https://')) link = link.replace('www', 'https://www');

				return link;
			},
			submit: function submit(event) {

				var inputText = "";
				var idOut = "";
				var hasName = false;
				var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

				if (event.target.tagName === 'SPAN') {
					if (event.target.previousElementSibling.value !== '') inputText = event.target.previousElementSibling.value;else inputText = "...";
				} else if (typeof event.target.value !== 'undefined') inputText = event.target.value;else inputText = event.target.previousElementSibling.value;

				if (inputText == "") inputText = "...";

				if (this.optionData[0].option_text === "[condition|email]") {
					if (inputText === "...") idOut = this.optionData[0].out_id.split("|")[2];else if (!emailRegex.test(inputText)) idOut = this.optionData[0].out_id.split("|")[1];else idOut = this.optionData[0].out_id.split("|")[0];
				} else if (this.optionData[0].option_text === "[condition|text]") {
					if (inputText === "...") idOut = this.optionData[0].out_id.split("|")[2];else idOut = this.optionData[0].out_id.split("|")[0];
				} else if (this.optionData[0].option_text === "[condition|name]") {
					if (inputText === "...") idOut = this.optionData[0].out_id.split("|")[2];else {
						idOut = this.optionData[0].out_id.split("|")[0];
						hasName = true;
					}
				} else {
					idOut = event.target.dataset.idOut;
					inputText = event.target.value;
				}

				_eventbus.$emit('send-input', { id: idOut, text: inputText, hasName: hasName });
				_eventbus.$emit('hide-header');
			},
			sendChoice: function sendChoice(event) {
				this.animatebutton(event).then(this.sendEvents);
			},
			animatebutton: function animatebutton(event) {
				var _this2 = this;

				return Q.promise(function (resolve, reject, notify) {
					event.target.style.position = 'absolute';
					var cuiInner = _this2.$el.querySelector('.cui_inner');
					TweenMax.to(event.target, .3, { css: { right: '0', opacity: 0, bottom: cuiInner.getBoundingClientRect().height + 20 + 'px' }, ease: Power3.easeInOut, onComplete: function onComplete() {
							resolve(event);
						} });
				});
			},
			sendEvents: function sendEvents(event) {
				return Q.promise(function (resolve, reject, notify) {
					_eventbus.$emit('send-choice', { id: event.target.dataset.idOut, text: event.target.innerHTML });
					_eventbus.$emit('hide-header');
					resolve();
				});
			},
			optionText: function optionText(string) {
				var stripped = string.replace(/ /g, '');
				if (stripped === '[condition|email]' || stripped === '[condition|text]' || stripped === '[condition|name]') return '';else return string;
			}
		},
		computed: {
			checkOptions: function checkOptions() {
				return this.optionData.type === 'option' ? true : false;
			},
			checkInputs: function checkInputs() {
				return this.optionData.type === 'input' ? true : false;
			},
			checkLink: function checkLink() {
				return this.optionData.type === 'link' ? true : false;
			}
		}
	});
});