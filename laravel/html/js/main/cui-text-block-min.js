'use strict';

/**
 * cui-text-block.js
 */
define(['jquery', 'axios', 'lodash', 'vuejs', 'tweenmax', 'q'], function ($, axios, _, Vue, TweenMax, Q) {

	/**
  * Define foundation model view
  */
	return Vue.component('cuitextblock', {
		template: '#text-block-template',
		props: ['cuiType', 'cuiText'],
		data: function data() {
			return {
				textBoxAlignment: 'left',
				textOpacity: 1,
				innerCuiEle: {}
			};
		},
		beforeDestroy: function beforeDestroy() {
			_eventbus.$off('update-window', this.updateHeight);
			_eventbus.$off('update-opacity', this.updateOpacity);
		},
		mounted: function mounted() {
			this.innerCuiEle = document.querySelector('.inner-cui');
			this.textBoxAlignment = this.cuiType === 'cui_text' ? 'left' : 'right';
			this.scrollWindow();
			this.updateHeight();
			this.updateOpacity();

			// EVENTS LISTNERS
			_eventbus.$on('update-window', this.updateHeight);
			_eventbus.$on('update-opacity', this.updateOpacity);
			_eventbus.$on('scrolling', this.updateOpacity);
		},
		updated: function updated() {
			var _this = this;

			this.$nextTick(function () {
				_this.scrollWindow().then(_this.showText);
				setTimeout(function () {
					_this.updateHeight();
					_this.updateOpacity();
				}, 1 * 1000);
			});
		},

		methods: {
			updateOpacity: function updateOpacity() {
				var header = document.querySelector('#headerWrapper');
				var textBoxTop = this.$el.getBoundingClientRect().top;
				var headerHeight = header.getBoundingClientRect().height;

				this.$el.style.opacity = (1.2 * (textBoxTop / headerHeight)).toFixed(1);
				// this.$el.style.transform = `scale(${(1.4*(textBoxTop/headerHeight)).toFixed(1)})`;
			},
			updateHeight: function updateHeight() {

				var bottomHeight = document.querySelector('.cui-bottom-section').getBoundingClientRect().height;
				var windowHeight = $(window).outerHeight();
				var newHeight = windowHeight - bottomHeight;
				var adjust = 60;

				this.innerCuiEle.style.maxHeight = newHeight - adjust + 'px';
				this.innerCuiEle.style.marginBottom = bottomHeight + adjust + 'px';
			},
			scrollWindow: function scrollWindow() {
				return Q.promise(function (resolve, reject, notify) {
					var $textScroll = $('.inner-cui');
					var $textBlock = $('.inner-cui > #text_block');
					var height = 0;

					for (var i = 0; i < $textBlock.length; i++) {
						height += $textBlock[i].getBoundingClientRect().height;
					}

					$textScroll.animate({ scrollTop: height + 'px' }, 500, 'swing');
					resolve();
				});
			},
			showText: function showText() {
				return Q.promise(function (resolve, reject, notify) {
					// TweenMax.to(this.navInner, .5, {css:{opacity: 1}, ease:Power3.easeInOut, onComplete: ()=>{
					resolve();
					// }});
				});
			}
		},
		computed: {
			textStyle: function textStyle() {
				return {
					'columns small-up-12': true,
					'cui-transform': this.cuiType === 'cui_text',
					'user-transform': this.cuiType !== 'cui_text'
				};
			}
		}
	});
});