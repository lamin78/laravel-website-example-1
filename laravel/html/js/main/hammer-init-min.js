'use strict';

/**
 * Filename: hammer-init.js
 */
define(['jquery', 'hammer', 'jquerythrottle', 'preload'], function ($, Hammer) {

    /**
     * Hammer swipe actions
     */
    var reqAnimationFrame = function () {
        return window[Hammer.prefixed(window, "requestAnimationFrame")] || function (callback) {
            setTimeout(callback, 1000 / 60);
        };
    }();

    var dirProp = function dirProp(direction) {

        return direction & Hammer.DIRECTION_HORIZONTAL ? true : false;
    };

    /**
     * Carousel
     * @param {object} obj
     *
     */
    var HammerCarousel = function HammerCarousel(obj) {

        // Add event dispatcher for carousel load event
        this.CarouselDone = document.createEvent("Event");
        this.CarouselDone.initEvent("carouselDone", true, true);

        // Add event dispatcher for carousel scroll event
        this.CarouselScroll = document.createEvent("Event");
        this.CarouselScroll.initEvent("carouselScroll", true, true);

        this.container = document.querySelector(obj.container);

        if (Boolean(this.container) === false) return;

        this.direction = Hammer.DIRECTION_HORIZONTAL;
        this.imgArray = [];
        this.screenSize = $(window).outerWidth();
        this.resizeFlag = obj.resizeFlag;
        this.fullFlag = obj.fullFlag;
        this.panes = Array.prototype.slice.call(this.container.children, 0);
        this.containerSize = dirProp(this.direction) ? this.container['offsetWidth'] : 0;
        this.orbId;
        this.orbEle;
        this.scrollTrheshold = window.UnisonModel.isMobile ? 30 : 10;
        this.currentIndex = 0;
        this.currentLargeIndex = 0;
        this.orbFactor = 0;
        this.breakPointLarge = 960; // 960px
        this.breakPointMedium = 768; // 768px
        this.breakPointSmall = 480; // 480px // TODO: maybe link to Unison
        this.autoPlayInt = 3; // Length to wait between panel slides
        this.playing = false;
        this.willDrag = true;
        this.fade = obj.fadeTrans;

        /**
         * Initialise hammer Managers
         */
        var hammerOptions = {
            dragLockToAxis: true,
            dragBlockVertical: true,
            preventDefault: true
        };
        this.hammer = new Hammer.Manager(this.container, hammerOptions);

        if (!$(this.container).hasClass('autoplay-banner') && !$(this.container).hasClass('click-slide')) {

            /**
             * Check to see if browser supports transitions
             */
            if (this.supportsTransitions()) {

                /**
                 * Set/initialise pan options
                 */
                var panOptions = {
                    direction: Hammer.DIRECTION_HORIZONTAL,
                    threshold: 1
                };

                this.hammer.add(new Hammer.Pan(panOptions));
                this.hammer.on("panstart panmove panend pancancel", Hammer.bindFn(this.onPan, this));
            }
        }

        /**
         * Set/initialise mouse over options
         */
        if ($(this.container).hasClass('pan-move') && UnisonModel.isIE === false) {
            this.container.addEventListener("mousemove", Hammer.bindFn(this.onPanMove, this));
        }

        if ($(this.container).hasClass('click-slide')) {

            /**
             * Set tap options for click slide nav
             */
            var tapOptions = {
                event: 'tap',
                pointers: 1,
                taps: 1
            };
            this.hammer.add(new Hammer.Tap(tapOptions));
            this.hammer.on("tap", Hammer.bindFn(this.onTap, this));
        }

        if (this.checkBounds(-50, 200) && this.playing === false && $(this.container).hasClass('autoplay-banner')) {

            this.autoPlay(this.panes.length);
            this.playing = true;
        }

        this.show(this.currentIndex);
    };

    /**
     * Determine hammer carousel methods
     */
    HammerCarousel.prototype = {

        /**
         * Method for pre-loading images before initialising
         * @param {string} imgDir | image directory
         * @param {imgArray} imgArray| image array
         * @param {bool} fadeIn | fade in flag
         */
        preLoadImages: function preLoadImages(imgDir, imgArray, fadeIn, obj) {
            var parent = this;
            //$(imgArray).preload(imgDir, function() {
            parent.initialize(obj);

            if (fadeIn) $(parent.container).parent().addClass('fadeInOptimized');
            //});
        },

        initialize: function initialize(obj) {

            var that = this;

            window.dispatchEvent(that.CarouselDone);

            if (Boolean(this.container) === false) return;

            /**
             * build banner from imported array
             */
            if (this.imgArray[0] !== 'undefined') {}

            $(document).on('mouseenter', '.case-banner-nav-left, .services-banner-nav-left, .case-banner-nav-right, .services-banner-nav-right', function () {
                $(this).addClass('reveal-nav');
            }).on('mouseleave', '.case-banner-nav-left, .services-banner-nav-left, .case-banner-nav-right, .services-banner-nav-right', function () {
                $(this).removeClass('reveal-nav');
            });

            if (this.resizeFlag) {

                this.resizeHeight($(window).outerWidth());

                $(document).on('click', '.services-banner-nav-left', function () {

                    if (that.currentIndex > 0) {

                        if (that.screenSize > that.breakPointLarge) that.currentLargeIndex = 0;

                        that.changeIndex('left');
                        that.show(that.currentIndex, 0, true);
                        that.hightlightOrb(that.currentIndex, true);
                    }
                }).on('click', '.services-banner-nav-right', function () {

                    that.changeIndex('right');
                    that.show(that.currentIndex, 0, true);
                    that.hightlightOrb(that.currentIndex, true);
                }).on('touchstart click', '.banner-nav', function () {

                    if ($(this).parent().parent().data('id') !== that.orbId) return;

                    var index = $(this).data('index');

                    that.hightlightOrb(index, true);
                    that.currentLargeIndex = index === 2 ? 2 : 0;
                    that.show(index, 0, true);
                });
            }

            if (this.fullFlag) {

                this.layer2 = $(this.panes[this.currentIndex]).find('.layer2');

                if (typeof this.layer2.offset() !== 'undefined') this.layerXCoord = this.layer2.offset().left;

                $(document).on('click', '.case-banner-nav-left', function () {

                    that.currentIndex = that.currentIndex - 1;
                    that.layer2 = $(that.panes[that.currentIndex]).find('.layer2');
                    that.show(that.currentIndex, 0, true);
                    that.hightlightOrb(that.currentIndex, true);
                }).on('click', '.case-banner-nav-right', function () {

                    that.changeIndex('right');
                    that.layer2 = $(that.panes[that.currentIndex]).find('.layer2');
                    that.show(that.currentIndex, 0, true);
                    that.hightlightOrb(that.currentIndex, true);
                }).on('touchstart click', '.banner-nav', function (event) {

                    event.stopPropagation();
                    event.preventDefault();

                    var flag = false;

                    if (!flag) {

                        flag = true;

                        setTimeout(function () {
                            flag = false;
                        }, 100);

                        if ($(this).parent().parent().data('id') !== that.orbId) return;

                        var index = $(this).data('index');

                        that.hightlightOrb(index, true);
                        that.layer2 = $(that.panes[index]).find('.layer2');
                        that.show(index, 0, true);
                    }

                    return false;
                });
            }

            this.drawOrbs(this.panes);

            /**
             * Throttle window resize event listener
             * resize banner containers
             */
            $(window).resize($.throttle(250, function () {

                that.containerSize = that.container['offsetWidth'];
                that.show(that.currentIndex);

                if (that.resizeFlag) {
                    that.resizeHeight($(this).outerWidth());
                }
                that.drawOrbs(that.panes);
            }));

            /**
             * If playWhenVisible boolean exists listener to window scroll
             * Auto play if element is on screen and isn't already playing
             */
            if (typeof obj !== 'undefined' && obj.playWhenVisible === true) {
                $(window).on('DOMMouseScroll mousewheel scroll', function (ev) {
                    if (that.checkBounds(-50, 200) && that.playing === false) {
                        that.autoPlay(that.panes.length);
                        that.playing = true;
                    }
                });
            };
        },

        /**
         * show a pane
         * @param {int} showIndex
         * @param {int} [percent] percentage visible
         * @param {Boolean} [animate]
         */
        show: function show(showIndex, percent, animate) {

            showIndex = this.analyseShowIndex(Math.max(0, Math.min(showIndex, this.panes.length - 1)));

            percent = percent || 0;

            var className = this.container.className;

            if (animate) {
                if (className.indexOf('animate') === -1) {
                    this.container.className += ' animate';
                }
            } else {
                if (className.indexOf('animate') !== -1) {
                    this.container.className = this.container.className.replace('animate', '').trim();
                }
            }

            var paneIndex, pos, translate;

            for (paneIndex = 0; paneIndex < this.panes.length; paneIndex++) {

                if (this.fade) {
                    this.panes[paneIndex].style.opacity = 0;
                } else {

                    //pos	= this.analysePos((this.containerSize / 100) * (((paneIndex - showIndex) * 100) + percent));
                    pos = this.containerSize / 100 * ((paneIndex - showIndex) * 100 + percent);

                    translate = 'translate(' + pos + 'px, 0)';
                    this.panes[paneIndex].style.transform = translate;
                    this.panes[paneIndex].style.mozTransform = translate;
                    this.panes[paneIndex].style.webkitTransform = translate;
                    this.panes[paneIndex].style.msTransform = translate;
                }
            }

            if (this.fade) this.panes[showIndex].style.opacity = 1;

            this.currentIndex = showIndex;
        },

        /**
         * handle pan
         * @param {Object} ev
         */
        onPan: function onPan(ev) {

            var delta = dirProp(this.direction) ? ev.deltaX : 0,
                percent = 100 / this.containerSize * delta,
                animate = false,
                that = this;

            if ($(ev.target).prop('tagName') === 'DIV') {

                if (ev.type == 'panend' || ev.type == 'pancancel') {

                    if (Math.abs(percent) > this.scrollTrheshold && ev.type == 'panend') {

                        if (percent < 0 === true) this.changeIndex('right');else this.changeIndex('left');

                        this.hightlightOrb(this.currentIndex, true);

                        window.dispatchEvent(this.CarouselScroll);
                    }

                    percent = 0;
                    animate = true;
                    this.willDrag = true;
                    this.layer2 = $(this.panes[this.currentIndex]).find('.layer2');
                }

                reqAnimationFrame(this.show.bind(this, this.currentIndex, percent, animate));
            }

            if ($(ev.target).prop('tagName') === 'polygon' || $(ev.target).prop('tagName') === 'BUTTON') {

                if (ev.type == 'panend' || ev.type == 'pancancel') {
                    this.container.className += ' animate';
                    reqAnimationFrame(this.show.bind(this, this.currentIndex, percent, animate));
                }
            }
        },

        /**
         * handle pan
         * @param {Object} ev
         */
        onPanMove: function onPanMove(ev) {

            if (typeof this.layer2 !== 'undefined') {

                var newDivPositionX = 0.02 * (0.5 * this.containerSize - ev.x);

                this.layer2.css({
                    'transform': 'translate3d(' + (newDivPositionX - $(window).outerWidth() / 2) + 'px, 0, 0)'
                });
                this.layer2.css({
                    '-moz-transform': 'translate3d(' + newDivPositionX + 'px, 0, 0)'
                });
                this.layer2.css({
                    '-webkit-transform': 'translate3d(' + newDivPositionX + 'px, 0, 0)'
                });
            }
        },

        /**
         * adjust index value based screen size
         * @param {int} index
         */
        analyseShowIndex: function analyseShowIndex(index) {

            switch (true) {

                case this.screenSize <= this.breakPointSmall && this.fullFlag && index >= 99:
                    index = this.calcPaneFactor();
                    break;

                case this.screenSize > this.breakPointSmall && this.screenSize <= this.breakPointMedium && !this.fullFlag && index > 4:
                    index = this.calcPaneFactor() + 1;
                    break;

                case this.screenSize > this.breakPointMedium && this.screenSize <= this.breakPointLarge && !this.fullFlag && index > 3:
                    index = this.calcPaneFactor() + 1;
                    break;

                case this.screenSize > this.breakPointLarge && !this.fullFlag && index === 2:
                    index = 2;
                    break;

                default:
                    index = index;
            }

            return index;
        },

        /**
         * adjust pos value based screen size
         * @param {int} pos
         */
        analysePos: function analysePos(pos) {

            switch (true) {
                case this.screenSize <= this.breakPointSmall || this.fullFlag:
                    pos = pos;
                    break;

                case this.screenSize > this.breakPointSmall && this.screenSize <= this.breakPointMedium:
                    pos = pos / 2;
                    break;

                case this.screenSize > this.breakPointMedium && this.screenSize <= this.breakPointLarge:
                    pos = pos / 3;
                    break;

                case this.screenSize > this.breakPointLarge:
                    pos = pos / 4;
                    break;
            }

            return pos;
        },

        /**
         * Centralise change index action
         * @param {string} dir
         */
        changeIndex: function changeIndex(dir) {

            if (dir === 'right') {
                if (this.screenSize > this.breakPointLarge && !this.fullFlag) {
                    this.currentLargeIndex = 2;
                    this.currentIndex = 2;
                } else {
                    this.currentIndex += this.panes.length / this.calcPaneFactor();
                }
            } else {
                if (this.screenSize > this.breakPointLarge && !this.fullFlag) {
                    this.currentLargeIndex = 0;
                    this.currentIndex = 0;
                } else {
                    this.currentIndex -= this.panes.length / this.calcPaneFactor();
                }
            }
        },

        /**
         * Draw navigation orbs based on banner pane.length
         * adjust according to screen width
         *@param {int} n
         */
        drawOrbs: function drawOrbs(n) {

            var j,
                that = this;

            switch (true) {
                case this.screenSize <= this.breakPointSmall && !this.fullFlag:
                    this.orbFactor = 1;
                    j = this.calcPaneFactor();
                    break;

                case this.screenSize > this.breakPointSmall && this.screenSize <= this.breakPointMedium && !this.fullFlag:
                    this.orbFactor = 2;
                    j = this.calcPaneFactor();
                    break;

                case this.screenSize > this.breakPointMedium && this.screenSize <= this.breakPointLarge && !this.fullFlag:
                    this.orbFactor = 3;
                    j = this.calcPaneFactor();
                    break;

                case this.screenSize > this.breakPointLarge && !this.fullFlag:
                    this.orbFactor = 3;
                    j = this.calcPaneFactor();
                    break;

                default:
                    this.orbFactor = 1;
                    j = this.calcPaneFactor();
            }

            this.orbId = $(this.container).attr('id');
            this.orbEle = '.orb-wrap[data-id=' + this.orbId + ']';

            $(this.orbEle).empty();

            $(this.orbEle).append('<ul />');

            for (var i = 0; i < j; i++) {
                var k = this.currentIndex === -1 ? 0 : this.currentIndex;
                $(this.orbEle + ' > ul').append($('<li />', {
                    'class': i === k ? 'banner-nav filled' : 'banner-nav',
                    'data-index': function dataIndex() {

                        if (that.screenSize > that.breakPointLarge && !that.fullFlag) {
                            if (i === j - 1 && !this.fullFlag) return i * 2;else return i;
                        } else return i * that.orbFactor;
                    }
                }));
            }
        },

        /**
         * Highlight orbs depending
         *
         * @param {int} ind | number to test for change and pass
         */
        hightlightOrb: function hightlightOrb(ind) {

            var i = this.analyseShowIndex(ind);

            if (i > this.panes.length - 1) i = this.panes.length - 1;else if (i < 0) i = 0;

            $(this.orbEle).find('.banner-nav').css('border', 0).removeClass('filled');

            $(this.orbEle).find('.banner-nav').each(function () {
                if ($(this).data('index') === i) $(this).addClass('filled');
            });
        },

        /**
         * Determine the amount of cycles needed to create orbs
         */
        calcPaneFactor: function calcPaneFactor() {
            return Math.ceil(this.panes.length / this.orbFactor);
        },

        /**
         * On resize perform class operations and update
         * @param {int} $windowWidth
         */
        resizeHeight: function resizeHeight($windowWidth) {

            if ($windowWidth <= this.breakPointSmall) {
                this.screenSize = $windowWidth;
                $('.pane').removeClass('large-segment');
                $('.pane').removeClass('medium-segment');
                $('.pane').removeClass('small-medium-segment');
            } else if ($windowWidth > this.breakPointSmall && $windowWidth <= this.breakPointMedium) {
                this.screenSize = $windowWidth;
                $('.pane').removeClass('large-segment');
                $('.pane').removeClass('medium-segment');
                $('.pane').addClass('small-medium-segment');
            } else if ($windowWidth > this.breakPointMedium && $windowWidth <= this.breakPointLarge) {
                this.screenSize = $windowWidth;
                $('.pane').removeClass('large-segment');
                $('.pane').removeClass('small-medium-segment');
                $('.pane').addClass('medium-segment');
            } else if ($windowWidth > this.breakPointLarge) {
                this.screenSize = $windowWidth;
                $('.pane').removeClass('medium-segment');
                $('.pane').removeClass('small-medium-segment');
                $('.pane').addClass('large-segment');
            }

            this.show(this.currentIndex);
        },

        /**
         * Autoplay slide if required
         * Will only perform animation if user can see the panel
         *@param {int} n | number of panels
         */
        autoPlay: function autoPlay(n) {

            console.log(1);

            var i = 1,
                that = this;
            var autoCycle = setInterval(function () {

                if (that.checkBounds(-50, 200)) {
                    if (i >= n) clearInterval(autoCycle);
                    reqAnimationFrame(that.show.bind(that, i, 0, true));

                    i = i + 1;
                }
            }, this.autoPlayInt * 1000);
        },

        /**
         * Enable tap/click navigation
         */
        onTap: function onTap() {
            this.currentIndex = this.currentIndex + 1;
            this.show(this.currentIndex, 0, true);
        },

        /**
         * Check to see if scroll falls between supplied parameters
         *@param {int} top
         *@param {int} bottom
         */
        checkBounds: function checkBounds(top, bottom) {

            return $(window).scrollTop() - $(this.container).offset().top > top && $(window).scrollTop() - $(this.container).offset().top < bottom;
        },

        supportsTransitions: function supportsTransitions() {
            var b = document.body || document.documentElement,
                s = b.style,
                p = 'transition';

            if (typeof s[p] == 'string') {
                return true;
            }

            // Tests for vendor specific prop
            var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
            p = p.charAt(0).toUpperCase() + p.substr(1);

            for (var i = 0; i < v.length; i++) {
                if (typeof s[v[i] + p] == 'string') {
                    return true;
                }
            }

            return false;
        }
    };

    return HammerCarousel;
});