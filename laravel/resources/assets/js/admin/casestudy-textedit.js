/**
 * casestudy.js
 */
define(

	['jquery', 'tinyMCE'],

	function($, TinyMCE) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {
			this.init();
		}

		PageView.prototype = ({
			init() {
				new TinyMCE({
					textarea:[
						{id: '#text_field'},
						// {id: '#text_field2'},
					]
				});
			},
		});

		return PageView;
	}
);
