/**
 * casestudy.js
 */
define(

	['jquery', 'vue_casestudy_cms'],

	function($, Vue_casestudy_cms) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {
			this.$generateSlug = $('.generate-slug');
			this.$slugInput = $('#casestudy_slug');
			this.$casestudy_title = $('#casestudy_title');
			this.$submitHeader = $('.submit-header');
			this.$statusSelect = $('.status-select');
			this.$casestudyStat1Holder = $('#casestudy_stat_1_holder');
			this.$casestudyStat2Holder = $('#casestudy_stat_2_holder');
			this.$casestudyStat3Holder = $('#casestudy_stat_3_holder');
			this.alertBoxInterval = 1;
			this.debounce = {};
			this.init();
		}

		PageView.prototype = ({
			init() {

				$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

				this.$submitHeader.on('click', (e)=>this.checkDisabled(e));
				this.$generateSlug.on('click', (e)=>this.processSlug(e));
				this.$casestudy_title.on('keyup', (e)=>this.checkUpdate(e));
				this.$casestudyStat1Holder.on('change', (e)=>this.processFile(e));
				this.$casestudyStat2Holder.on('change', (e)=>this.processFile(e));
				this.$casestudyStat3Holder.on('change', (e)=>this.processFile(e));

				this.$statusSelect.each((index, item)=>{
					$(item).on('click', (e)=>this.updateStatus(e));
				});

				new Vue_casestudy_cms();
			},
			processFile(e) {
				console.log(e.target.previousSibling.previousSibling.innerHTML);
				e.target.previousSibling.previousSibling.innerHTML = e.target.files[0].name;
			},
			processSlug(ele) {

				$.ajax({
            type: 'POST',
            url: '/admin/case-study/generate-slug',
            data: {title: $('#casestudy_title').val()},
            dataType: 'json',
            success: (data) => {
							this.$slugInput.val(data.slug);
            },
            error: (data) => {
              console.log('Error:', data);
            }
        });
			},
			updateStatus(ele) {
				ele.preventDefault();
				$.ajax({
            type: 'POST',
            url: '/admin/case-study/edit-status',
            data: {id: ele.target.dataset.id, status: ele.target.innerHTML},
            dataType: 'json',
            success: (data) => {
							ele.target.parentElement.parentElement.previousSibling.previousSibling.innerHTML = ele.target.innerHTML;
							// FIRE MODAL
							$('#alertModalSuccess').find('.success').html('status updated');
							$('#alertModalSuccess').foundation('reveal', 'open');
							setTimeout(()=>$('#alertModalSuccess').foundation('reveal', 'close'), this.alertBoxInterval*1000);
            },
            error: (data) => {
              console.log('Error:', data);
							// FIRE MODAL
							$('#alertModalFail').find('.fail').html('status update failed');
							$('#alertModalFail').foundation('reveal', 'open');
							setTimeout(()=>$('#alertModalFail').foundation('reveal', 'close'), this.alertBoxInterval*1000);
            }
        });
			},
			checkDisabled(e) {
				console.log(e.target.classList.contains('disabled'));
				if(e.target.classList.contains('disabled'))
					return false;
			},
			checkUpdate(e) {
				clearTimeout(this.debounce);
				this.debounce = setTimeout(()=>{
					this.processSlug();
				}, 0.5*1000)
			}
		});

		return PageView;
	}
);
