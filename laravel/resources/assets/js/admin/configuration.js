/**
 * screen-script.js
 */
define(
	
	[
		 'jquery'
		,'backbone'
	],  
	
	function($, Backbone) {
			
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				
				$("#bUpdateTag").bind("click",this.updateTag);

				$("#bUpdateThresholds").bind("click",this.updateThresholds)

			}

			,updateTag: function(e) {
				e.preventDefault();

				var r = confirm("Are you sure you want to change the tag?\nIt will result in all the database entries tracked so far being wiped.");
				if (r == true) {
					$("#type").attr("value","tag");
					$("#configForm").submit();
				} else {
				    return false;
				}
			}

			,updateThresholds: function(e) {
				e.preventDefault();

				var r = confirm("Are you sure you want to change the thresholds?\nDoing so will make live changes to the frontend status.");
				if (r == true) {
					$("#type").attr("value","threshold");
					$("#configForm").submit();
				} else {
				    return false;
				}
			}
		});
		
		return PageView;
	}
);