/**
 * pinterest-main.js
 */
define(
	[	
		 'jquery'
		,'backbone'
		,'fancybox'
		,'fancybox-media'
	],  
	
	function($, Backbone, FancyBox, FancyBoxMedia) {
			
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({

			initialize: function() {
				trace("Images JS Ready");
				$(".fancybox").bind("click", function(e) {
					e.preventDefault();
					$.fancybox( {'href' : $(this).attr("href")},{});
					return false;
				})
			}
		});
		
		return PageView;
	}
);