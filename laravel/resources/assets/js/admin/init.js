/**
 * init.js
 */
define(
	[
		 'jquery'
		,'foundation'
		,'common'
		,((pageViewJS == '') ? undefined : pageViewJS)
		,'foundation-alert'
		,'foundation-topbar'
		,'foundation-dropdown'
		,'foundation-reveal'
	],

	function($, foundation, common, PageView) {
		var initialize = function() {
			$(document)
				.ready(function() {
					/**
					 * Initialise page actions if available
					 */
					if(typeof PageView !== 'undefined') {
						pageView = new PageView();
					}

				})
				/**
				 * Initialise Foundation Orbit with conditions
				 */
				.foundation({
					reveal: {
						 animation: 'fade'
						,animation_speed: 250
						,close_on_background_click: false
					},
					orbit: {
						timer: false
					},
					equalizer : {
					    // Specify if Equalizer should make elements equal height once they become stacked.
					    equalize_on_stack: true
					}
				});

		}

		return {
			initialize: initialize
		};
})
