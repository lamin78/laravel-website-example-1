/**
 * main.js
 * contents of main.js with extra configuration
 */
require.config({

	baseUrl: site_url + "/js/",

	urlArgs: "bust=" + (new Date()).getTime(),	// halt caching of .js files for development

	waitSeconds: 0, 							// disable script loading timeout

	paths: {
		/**
		 * Dependencies
		 */
    'jquery'		 		: 'libs/jquery/jquery-1.10.2.min'
		,'foundation'  			: 'libs/foundation/foundation.min'
		,'foundation-alert'		: 'libs/foundation/foundation.alert.min'
		,'foundation-topbar'	: 'libs/foundation/foundation.topbar.min'
		,'foundation-dropdown'	: 'libs/foundation/foundation.dropdown.min'
		,'foundation-reveal'	: 'libs/foundation/foundation.reveal.min'
    ,'json2' 				: 'libs/json2/json2-min'
    ,'underscore'	 		: 'libs/underscore/underscore-min'
    ,'backbone'				: 'libs/backbone/backbone-min'
    ,'handlebars'			: 'libs/handlebars/handlebars-v3.0.3'
    ,'highcharts'			: 'libs/highcharts/highcharts'
    ,'vuejs'					: 'libs/vuejs/vue'
    ,'axios'				: '//unpkg.com/axios/dist/axios.min'
		,'sortablejs':        '//cdn.jsdelivr.net/sortable/1.4.2/Sortable.min'
		,'draggable':       '//cdn.rawgit.com/David-Desmaisons/Vue.Draggable/master/dist/vuedraggable.min'
		,'velocity':       '//cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min'
		,'tinymcelib':       	'libs/tinymce/tinymce.min'

    /* fancybox */
    ,'fancybox'             : 'libs/fancybox/jquery.fancybox.pack'
    ,'fancybox-media'       : 'libs/fancybox/helpers/jquery.fancybox-media'

		/**
		 * Application Logic
		 */
		,'common'							: 'admin/common-min'
		,'init'								: 'admin/init-min'
		,'vue_casestudy_cms'  : 'admin/vue/vue-casestudy-cms-min'
		,'component-section'  : 'admin/vue/components/component-section-min'
		,'carousel-section'   : 'admin/vue/components/carousel-section-min'
		,'tinyMCE'  					: 'admin/tinyMCE-min'
		,'pageView' 		 			: pageViewJS
    },

	/**
	 * Use shim for non AMD (Asynchronous Module Definition) compatible scripts
	 */
	shim: {
		'vuejs': {
			exports: 'Vue'
		},
		'velocity': {
			exports: 'Velocity'
		},
		'sortablejs': {
			exports : 'Sortable'
		},
		'draggable': {
			exports : 'draggable'
		},
		'tinymcelib': {
        exports: "tinymcelib",
        init: function() {
            return this.tinyMCE.DOM.events.domLoaded = !0, this.tinyMCE;
        }
    },
		'foundation-reveal': {
             deps	 : ['jquery','foundation']
            ,exports : 'foundation-reveal'
        },

		'foundation-alert': {
             deps	 : ['jquery','foundation']
            ,exports : 'foundation-alert'
        },

        'foundation-topbar': {
             deps	 : ['jquery','foundation']
            ,exports : 'foundation-topbar'
        },

        'foundation-dropdown': {
             deps	 : ['jquery','foundation']
            ,exports : 'foundation-dropdown'
        },

        'foundation': {
             deps	 : ['jquery']
            ,exports : 'foundation'
        },


		'backbone': {
             deps	 : ['underscore', 'jquery']
            ,exports : 'Backbone'
        },

        'underscore': {
            exports : '_'
        },

        'highcharts': {
             deps    : ['jquery']
            ,exports : 'Highcharts'
        },

        'fancybox': {
            deps    : ['jquery']
            ,exports : 'FancyBox'
        },

        'fancybox-media': {
            deps    : ['jquery','fancybox']
            ,exports : 'FancyBoxMedia'
        },
    }
});

/**
 * Load the init module
 */
require(['init'],

	function(App) {
		App.initialize();
	}
);
