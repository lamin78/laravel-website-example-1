/**
 * pinterest-main.js
 */
define(
	[	
		 'jquery'
		,'backbone'
		,'/js/libs/pinterest/sdk.js'
	],  
	
	function($, Backbone, Pinterest) {
			
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({

			initialize: function() {
				trace("Pinterest JS Initialising");

				PDK.init({
		            appId: "4793684297043153046", // Change this
		            redirect_uri: "",
		            cookie: true
		        });

				$("#bFetchPin").bind("click",{'parent':this},this.checkLogin);
			}
			
			,checkLogin: function(e) {
				e.preventDefault();

				var session = PDK.getSession();
				trace(session);
				if (!session) {
					PDK.login({scope : 'read_public, write_public'}, function(session) {
					if (!session) {
					    alert('You chose not to grant permissions or closed the pop-up');
					} else {
					    e.data.parent.checkPin();
					  }
					});
				} else {
					e.data.parent.checkPin();
				}
				return false;
			}

			,checkPin: function() {
				var session = PDK.getSession();
				var pin_id = $("#pin_id").val();
				if (pin_id != "") {
					PDK.request('/pins/'+pin_id+'/', {
				    	access_token: session.accessToken,
				    	fields: 'image[original,large],url,link,created_at,note,color,counts,media,metadata'
				    }, function(response) {
				      if (!response || response.error) {
				        alert('Oops, there was a problem getting your information');
				      } else {
				        $("#preview").removeClass("hidden");

				        trace(response.data);
				        
				        var img = $("<img src='"+response.data.image.large.url+"'/>");
				        $("#preview_image").empty().append(img);

				        $("#preview_pin_id").val(response.data.id);
				        $("#preview_note").val(response.data.note);
				        $("#preview_link").val(response.data.url);
				        $("#preview_media_url").val(response.data.image.original.url);
				      }
				    });
				};
			}
		});
		
		return PageView;
	}
);