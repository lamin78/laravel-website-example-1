/**
 * screen-script.js
 */
define(
	
	[
		 'jquery'
		,'backbone'
		,'highcharts'
	],  
	
	function($, Backbone, Highcharts) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = Backbone.View.extend({
			
			initialize: function() {
				this.getData('tweets');
				this.getData('facebook');
				this.getData('tumblr');
			}

			,getData: function(type) {
		   		var options = {
			        chart: {
			            renderTo: 'hichart_'+type,
			            type: 'line'
			        },
			        title: {
			            text: '',
			            x: 0
			        },
			        xAxis: {
			            categories: [],
			            labels: {
			            	step: 1,
			            	rotation: -45
			            }
			        },
			        yAxis: {
			            title: {
			                text: 'Mentions / Posts'
			            },
			            plotLines: [{
			                value: 0,
			                width: 1,
			                color: '#808080'
			            }],
			            floor: 0
			        },
			        tooltip: {
			            valueSuffix: ''
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
			        series: [{}]
			    }


		   		// Get the CSV and create the chart
			    
			   	var url =  "/admin/summary/data?callback=?";
			    $.getJSON(url, {'type':type})
	   			.done(function( data ) {
			        options.series[0] = data.series;
			        options.xAxis.categories = data.categories;
			        options.title.text = data.title;
			        var chart = new Highcharts.Chart(options);
			    });
		   }
		});
		
		return PageView;
	}
);