/**
 * casestudy.js
 */
define(

	['jquery', 'tinymcelib'],

	function($, tinymce) {

		/**
		 * Define foundation model view
		 */
		var PageView = function(obj) {
			Object.assign(this, obj);
			this.init();
		}

		PageView.prototype = ({
			init() {

				for(let i=0; i<this.textarea.length;i++) {

					tinymce.init({
						selector:this.textarea[i].id,
						theme: 'modern',
						branding: false,
						min_height: 200,
						height : 400,
						plugins: [
							'advlist autolink lists link charmap preview hr anchor',
							'searchreplace wordcount visualblocks visualchars code fullscreen',
							'insertdatetime nonbreaking table contextmenu',
							'paste textcolor colorpicker textpattern help'
						],
						toolbar: 'undo redo | insert | styleselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
						image_prepend_url: "",
						image_dimensions: false,
						style_formats: [ ],
						content_style: "* {color: #000000;}",
						content_css: [
							'/css/main/tinymce_styles.css'
						],
						setup: function (ed) { }
					});
				}

			},
		});

		return PageView;
	}
);
