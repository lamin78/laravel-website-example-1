/**
 * laravel\resources\assets\js\admin\vue\components\carousel-section.js
 */
define(

	['jquery', 'vuejs'],

	function($, Vue) {

    return Vue.component('carousel-section', {
      template: '#template-carousel-section',
      props: ['dataCid', 'dataCsid', 'dataId', 'dataLen', 'dataDropIndex', 'dataImage'],
      data() {
        return {
          id: 0,
          dropId: '',
          type: 'carousel-type',
          fileInputText: 'Add Image',
          fileAdded: false,
        }
      },
      beforeDestroy() {},
      mounted() {
        this.dropId = `drop_0${this.dataDropIndex}`;
      },
      methods: {
				processFile(e) {
					this.$el.querySelector('.icon-upload').innerHTML = e.target.files[0].name;
					this.fileAdded = true;

				},
				deletCarousel() {
					_eventBus.$emit('delete-carousel', {
						id: this.dataId,
						cid: this.dataCid,
						csid: this.dataCsid,
					});
				},
      },
      computed: {
				nameParser() {
					return {
						file_input: `image_holder_${this.dataDropIndex}`,
						hidden_id: `hidden_id_${this.dataDropIndex}`,
						hidden_cid: `hidden_cid_${this.dataDropIndex}`,
						hidden_csid: `hidden_csid_${this.dataDropIndex}`,
						hidden_changed: `hidden_changed_${this.dataDropIndex}`,
					};
				},
				printImage() {
					return `${imageDir}/${this.dataImage}`;
				},
				inputAdded() {
					console.log(this.dataImage);
					return (this.fileAdded === true || this.dataImage === null ? '1' : '0');
				}
      },
    });
	}
);
