/**
 * laravel\resources\assets\js\admin\vue\vue_casestudy_cms.js
 */
define(

	['jquery', 'vuejs', 'axios'],

	function($, Vue, axios) {

    return Vue.component('component-section', {
      template: '#template-component-section',
      props: ['caseStudyId', 'sectionId', 'orderId', 'orderOrd', 'orderIdent', 'orderType', 'orderName', 'dataSection', 'dataDropIndex'],
      data() {
        return {
          id: 2,
          dropId: '',
          type: 'component-type',
					sectionName: '',
					showIdentInput: false,
					alertBoxInterval: 1,
        }
      },
      beforeDestroy() { },
      mounted() {
        this.dropId = `drop_${this.dataDropIndex}`;
				this.sectionName = this.orderIdent;
        console.log('component loaded');
      },
      methods: {
				swapForInput() {
					this.showIdentInput = true;
					setTimeout(()=>$('#sectionName').focus(), 0.5*1000);
				},
				swapBackToText() {
					this.showIdentInput = false;
				},
				submitNewIdent(e) {
					let val = e.target.value;
					axios.post(`/admin/case-study/update-order-ident`, {
							caseStudyId: this.caseStudyId,
							sectionId: this.sectionId,
							orderId: this.orderOrd,
							value: val,
						})
						.then((response) => {
							this.sectionName = e.target.value;
							this.showIdentInput = false;
							// FIRE MODAL
							$('#alertModalSuccess').find('.success').html('section title updated');
							$('#alertModalSuccess').foundation('reveal', 'open');
							setTimeout(()=>$('#alertModalSuccess').foundation('reveal', 'close'), this.alertBoxInterval*1000);
						})
						.catch((error) => {
							console.log(error);
							// FIRE MODAL
							$('#alertModalFail').find('.fail').html('section failed to update');
							$('#alertModalFail').foundation('reveal', 'open');
							setTimeout(()=>$('#alertModalFail').foundation('reveal', 'close'), this.alertBoxInterval*1000);
						});
				},
				deletSection() {
					console.log(this.sectionId);
					_eventBus.$emit('delete-section', {orderId: this.orderId, orderOrd: this.orderOrd, sectionId: this.sectionId, orderType: this.orderType, orderName: this.orderName});
				}
      },
      computed: {
        editLink() {
					let endpoint = '';
					switch(true) {
						case(this.orderType === 'image' && this.orderName === 'parallax'):
						 	endpoint = `/admin/case-study/edit-parallax/${this.caseStudyId}/${this.sectionId}`;
						break;
						case(this.orderType === 'image' && this.orderName === 'slider'):
						 	endpoint = `/admin/case-study/edit-slider/${this.caseStudyId}/${this.sectionId}`;
						break;
						case(this.orderType === 'text'):
						 	endpoint = `/admin/case-study/edit-textarea/${this.caseStudyId}/${this.sectionId}`;
						break;
						case(this.orderType === 'video'):
						 	endpoint = `/admin/case-study/edit-video/${this.caseStudyId}/${this.sectionId}`;
						break;
						case(this.orderType === 'carousel'):
						 	endpoint = `/admin/case-study/edit-carousel/${this.caseStudyId}/${this.sectionId}`;
						break;
					}

          return {endpoint};
        },
				sectionType() {
	        switch(true) {
						case(this.dataSection.data_type === 'carousel'):
							this.sectionName = this.sectionName || 'Carousel Section';
						break;
	          case(this.dataSection.data_type === 'text'):
							this.sectionName = this.sectionName || 'Text Section';
	          break;
	          case(this.dataSection.data_type === 'image' && this.dataSection.data_name === 'parallax'):
							this.sectionName = this.sectionName || 'Parallax Section';
	          break;
	          case(this.dataSection.data_type === 'image' && this.dataSection.data_name === 'slider'):
							this.sectionName = this.sectionName || 'Slider Section';
	          break;
						case(this.dataSection.data_type === 'video'):
							this.sectionName = this.sectionName || 'Video Section';
						break;
	        }
	        return {sectionName: this.sectionName};
				}
      },
    });
	}
);
