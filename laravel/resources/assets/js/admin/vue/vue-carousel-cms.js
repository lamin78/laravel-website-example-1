/**
 * laravel\resources\assets\js\admin\vue\vue_casestudy_cms.js
 */
define(

	['jquery', 'vuejs', 'carousel-section', 'axios', 'draggable', 'velocity'],

	function($, Vue, CarouselSection, axios, draggable, Velocity) {

		var PageView = function() {
			this.init();
		}

		PageView.prototype = ({

			init() {

				console.log('carousel js');

				// EVENT BUS
				window._eventBus = new Vue({});

				// MAIN VUE INSTANCE
        new Vue({
          el: '#carousel_wrap',
          data: {
						cms_Carousel: cmsCarousel,
						carouselCount: 0,
						dragging: false,
						theme: '',
						themeSaved: false
          },
          components: {
            'carousel-section': CarouselSection,
						draggable
          },
          beforeDestroy() {
						_eventBus.$off('delete-carousel', this.deleteSection);
						// _eventBus.$off('add-carousel', this.addCarousel);
					},
          mounted() {

						let token = document.head.querySelector('meta[name="csrf-token"]');

						console.log(token);

			      if (token)
			        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
			      else
			        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');

						this.theme = this.cms_Carousel[0].theme;
						let e = {target:{value:this.theme}};
						this.chooseTheme(e);

						// EVENTS
						_eventBus.$on('delete-carousel', this.deleteSection);
					},
					methods: {
						// TRANSITIONS
						beforeEnter (el) {
					    el.style.opacity = 0
					  },
					  enter (el, done) {
					    Velocity(el, { opacity: 1 }, { duration: 500, complete: done });
					  },
					  beforeLeave (el, done) {
					  },
						leave (el, done) {
							Velocity(el, { opacity: 0 }, { duration: 500, complete: done });
					  },

						chooseTheme(e, save=false) {
							let checkbox = this.$el.querySelectorAll('.checkbox-style');
							let checkboxLen = checkbox.length;
							let theme = e.target.value;

							for(let i=0;i<checkboxLen;i++) {
								if(theme !== checkbox[i].value)
									checkbox[i].checked = false;
								else {
									if(!save)
										checkbox[i].checked = true;
								}
							}

							if(save)
								this.updateTheme(theme)
						},

						updateTheme(theme) {
							axios.post('/admin/case-study/update-theme', {
								theme: theme,
								cid:cid,
								csid:csid
							}).then((response) => {
								this.themeSaved = true;
								setTimeout(()=>{
									this.themeSaved = false;
								}, 3*1000)
							})
							.catch((error) => {
								console.log(error);
							});
						},

						addCarousel(e) {
							e.preventDefault();
							let carouselObj = {
								 active:"1"
								,cid:cid
								,csid:csid
								,image:null
								,order:(this.cms_Carousel[(this.cms_Carousel.length-1)].order +1)
								,theme:this.theme
								,id:(this.cms_Carousel.length +1)
							}
							axios.post('/admin/case-study/add-carousel-image', {
							    cid: carouselObj.cid,
							    csid: carouselObj.csid,
							    order: (this.cms_Carousel[(this.cms_Carousel.length-1)].order +1)
							  })
							  .then((response) => {
									console.log(response);
									carouselObj.id = response.data.insertId;
									this.cms_Carousel.push(carouselObj);
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						},
						deleteSection(params) {

							if(!confirm("Please confirm carousel section removal?"))
								return;

							if(this.cms_Carousel.length < 2)
							 	return;

							axios.post(`/admin/case-study/delete-carousel-section`, {
							    id: params.id,
							    cid: params.cid,
							    csid: params.csid,
							  })
							  .then((response) => {
									let len = this.cms_Carousel.length;
									for(let i=0;i<len;i++) {
										if(this.cms_Carousel[i].id === params.id)
											this.cms_Carousel.splice(i, 1);
									}
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						},
						updateOrder(e) {
							let len = this.cms_Carousel.length;
							for(let i=0; i<len;i++) {
								this.cms_Carousel[i].order = (i+1);
							}

							axios.post(`/admin/case-study/update-carousel-order`, {
							    cid: cid,
							    csid: csid,
									cms_Carousel: this.cms_Carousel,
							  })
							  .then((response) => {
									console.log(response);
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						}
					},
					computed: {
						carouselNum() {
							return this.cms_Carousel.length;
						}
					}
        });
			},
		});

		return PageView;
	}
);
