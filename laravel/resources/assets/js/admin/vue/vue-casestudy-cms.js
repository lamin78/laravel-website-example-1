/**
 * laravel\resources\assets\js\admin\vue\vue_casestudy_cms.js
 */
define(

	['jquery', 'vuejs', 'component-section', 'axios', 'draggable', 'velocity'],

	function($, Vue, ComponentSection, axios, draggable, Velocity) {

		var PageView = function() {
			if($('#casestudy_cms').length > 0)
				this.init();
		}

		PageView.prototype = ({

			init() {
				// EVENT BUS
				window._eventBus = new Vue({});

				// MAIN VUE INSTANCE
        new Vue({
          el: '#casestudy_cms',
					// delimiters: ['${', '}'],
          data: {
						pageData: caseStudyPage,
						order: caseStudyPage.order,
						dragging: false,
						layoutSaved: false,
          },
          components: {
            'component-section': ComponentSection,
						draggable
          },
          beforeDestroy() {
						_eventBus.$off('delete-section', this.deleteSection);
					},
          mounted() {
						this.order = this.pageData.order;

						let len = this.order.length;
						for(let i=0; i<len;i++) {
							this.order[i].order = (i+1);
						}

						let token = document.head.querySelector('meta[name="csrf-token"]');
			      if (token)
			        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
			      else
			        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');

						// EVENTS
						_eventBus.$on('delete-section', this.deleteSection);
					},
					methods: {
						// TRANSITIONS
						beforeEnter (el) {
					    el.style.opacity = 0
					  },
					  enter (el, done) {
					    Velocity(el, { opacity: 1 }, { duration: 500, complete: done });
					  },
					  beforeLeave (el, done) {
					  },
						leave (el, done) {
							Velocity(el, { opacity: 0 }, { duration: 500, complete: done });
					  },

						// OTHER METHODS
						updateOrder(value) {
							let len = this.order.length;
							for(let i=0; i<len;i++) {
								this.order[i].order = (i+1);
							}

							axios.post(`/admin/case-study/update-order/${this.pageData.id}`, {
							    order: this.order,
							  })
							  .then((response) => {
							    console.log(response.data);
									this.layoutSaved = true;
									setTimeout(()=>{
										this.layoutSaved = false;
									}, 3*1000);
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						},
						addSection(e) {

							let sectionObj = {};
							let type = e.target.dataset.type;

							switch(true) {
								case(type === 'text'):
									sectionObj = {
										active:"1"
										,cid:this.pageData.id
										,id:(this.order.length +1)
										,data_ident:null
										,data_name:"text"
										,data_type:"text"
										,order:(this.order.length +1)
										,sid:0
									}
								break;
								case(type === 'parallax'):
									sectionObj = {
										active:"1"
										,id:(this.order.length +1)
										,cid:this.pageData.id
										,data_ident:null
										,data_name:"parallax"
										,data_type:"image"
										,order:(this.order.length +1)
										,sid:0
									}
								break;
								case(type === 'carousel'):
									sectionObj = {
										active:"1"
										,id:(this.order.length +1)
										,cid:this.pageData.id
										,data_ident:null
										,data_name:"carousel"
										,data_type:"carousel"
										,order:(this.order.length +1)
										,sid:0
									}
								break;
								case(type === 'slider'):
									sectionObj = {
										active:"1"
										,id:(this.order.length +1)
										,cid:this.pageData.id
										,data_ident:null
										,data_name:"slider"
										,data_type:"image"
										,order:(this.order.length +1)
										,sid:0
									}
								break;
								case(type === 'video'):
									sectionObj = {
										active:"1"
										,id:(this.order.length +1)
										,cid:this.pageData.id
										,data_ident:null
										,data_name:"video"
										,data_type:"video"
										,order:(this.order.length +1)
										,sid:0
									}
								break;

							}

							axios.post('/admin/case-study/add-section', {
							    id: this.pageData.id,
							    section: sectionObj
							  })
							  .then((response) => {
									sectionObj.sid = response.data.insertId;
									this.order.push(sectionObj);
									this.updateOrder();
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						},
						replaceSection() {
							console.log('replace section');
						},
						deleteSection(params) {

							if(!confirm("Please confirm section removal?"))
								return;

							let len = this.order.length;
							axios.post(`/admin/case-study/delete-section`, {
							    caseStudyId: this.pageData.id,
							    sectionId: params.sectionId,
							    orderId: params.orderId,
							    orderOrd: params.orderOrd,
							    orderType: params.orderType,
							    orderName: params.orderName,
							  })
							  .then((response) => {
									for(let i=0;i<len;i++) {
										console.log(this.order);
										if(this.order[i].id === params.orderId)
											this.order.splice(i, 1);
									}
							  })
							  .catch((error) => {
							    console.log(error);
							  });
						}
					},
					computed: {

					}
        });
			},
		});

		return PageView;
	}
);
