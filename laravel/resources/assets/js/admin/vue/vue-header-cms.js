/**
 * article10\laravel\resources\assets\js\admin\vue\vue-header-cms.js
 */
define(

	['jquery', 'vuejs', 'velocity'],

	function($, Vue, Velocity) {

		var PageView = function() {
			this.init();
		}

		PageView.prototype = ({

			init() {

				// EVENT BUS
				window._eventBus = new Vue({});

				// MAIN VUE INSTANCE
        new Vue({
          el: '#header_wrap',
          data: {
						fileInputText: 'Header Background Image',
						showImage: (headerImageActive === 'true') || false,
          },
          components: {

          },
          beforeDestroy() {

					},
          mounted() {

						this.$generateSlug = $('.generate-slug');
						this.$slugInput = $('#casestudy_slug');
						this.$casestudy_title = $('#casestudy_title');
						this.$submitHeader = $('.submit-header');
						this.$casestudyStat1Holder = $('.casestudy_stat_1_holder');
						this.debounce = {};
						this.debounce = {};

						$.ajaxSetup({
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            }
		        });
						this.$submitHeader.on('click', (e)=>this.checkDisabled(e));
						this.$generateSlug.on('click', (e)=>this.processSlug(e));
						this.$casestudy_title.on('keyup', (e)=>this.checkUpdate(e));
						this.$casestudyStat1Holder.on('change', (e)=>this.processFile(e));

						// EVENTS
					},
					methods: {
						// TRANSITION
						beforeEnter (el) {
					    el.style.opacity = 0
					  },
					  enter (el, done) {
					    Velocity(el, { opacity: 1 }, { duration: 500, complete: done });
					  },
					  beforeLeave (el, done) {
					  },
						leave (el, done) {
							Velocity(el, { opacity: 0 }, { duration: 500, complete: done });
					  },
						// OTHER METHODS
						showImageImput() {
							this.showImage = true;
						},
						showVideoImput() {
							this.showImage = false;
						},
						processFile(e) {
							this.$el.querySelector('.icon-upload').innerHTML = e.target.files[0].name;
						},
						processSlug(ele) {

							if(typeof ele !== 'undefined')
								ele.preventDefault();

							$.ajax({
			            type: 'POST',
			            url: '/admin/case-study/generate-slug',
			            data: {title: $('#casestudy_title').val()},
			            dataType: 'json',
			            success: (data) => {
										this.$slugInput.val(data.slug);
			            },
			            error: (data) => {
			              console.log('Error:', data);
			            }
			        });
						},
						checkDisabled(e) {
							console.log(e.target.classList.contains('disabled'));
							if(e.target.classList.contains('disabled'))
								return false;
						},
						checkUpdate(e) {
							clearTimeout(this.debounce);
							this.debounce = setTimeout(()=>{
								this.processSlug();
							}, 0.5*1000)
						}
					},
					computed: {

					}
        });
			},
		});

		return PageView;
	}
);
