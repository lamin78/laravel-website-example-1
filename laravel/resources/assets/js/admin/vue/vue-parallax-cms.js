/**
 * laravel\resources\assets\js\admin\vue\vue-parallax-cms.js
 */
define(

	['jquery', 'vuejs'],

	function($, Vue) {

		var PageView = function() {
			this.init();
		}

		PageView.prototype = ({

			init() {

				// EVENT BUS
				window._eventBus = new Vue({});

				// MAIN VUE INSTANCE
        new Vue({
          el: '#parallax_wrap',
          data: {
						fileInputText: 'Add Parallax Image',
          },
          components: {

          },
          beforeDestroy() {

					},
          mounted() {

						console.log('parallax');

						// EVENTS
					},
					methods: {
						processFile(e) {
							this.$el.querySelector('.icon-upload').innerHTML = e.target.files[0].name;
						},
					},
					computed: {

					}
        });
			},
		});

		return PageView;
	}
);
