/**
 * article10\laravel\resources\assets\js\admin\vue\vue-slider-cms.js
 */
define(

	['jquery', 'vuejs'],

	function($, Vue) {

		var PageView = function() {
			this.init();
		}

		PageView.prototype = ({

			init() {

				// EVENT BUS
				window._eventBus = new Vue({});

				// MAIN VUE INSTANCE
        new Vue({
          el: '#slider_wrap',
          data: {
						fileInputText1: 'Before Slide',
						fileInputText2: 'After Slide'
          },
          components: {

          },
          beforeDestroy() {

					},
          mounted() {

						console.log('slider js');

						// EVENTS
					},
					methods: {
						processFile(e, ind) {
							this.$el.querySelectorAll('.icon-upload')[ind].innerHTML = e.target.files[0].name;
						},
					},
					computed: {

					}
        });
			},
		});

		return PageView;
	}
);
