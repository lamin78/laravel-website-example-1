/**
 * home.js
 */
define(
	[
		 'jquery',
		 'ScrollMagic',
		 'TweenMax',
		 'TimelineMax',
		 'scrollmagic.gsap',
		 'scrollmagic.indicators',
		 'panelrollover',
	],

	function($, ScrollMagic, TweenMax, TimelineMax, ScrollmagicGsap, ScrollmagicInd, PanelRollover) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {
			this.speed = 0.5;
			this.mobilespeed = 2;
		}

		PageView.prototype = {

			initialize: function() {

				var controllers = new ScrollMagic.Controller();

				TweenMax.set($(".workflow-graphic-1, .workflow-graphic-2, .workflow-graphic-3, .workflow-graphic-4, .workflow-graphic-5"), {css:{scale: '0.0'}});
				TweenMax.set($(".workflow-textbox-1, .workflow-textbox-3, .workflow-textbox-5"), {css:{x: '20%'}});
				TweenMax.set($(".workflow-textbox-2, .workflow-textbox-4"), {css:{x: '-20%'}});
				TweenMax.set($(".workflow-textbox-2, .workflow-textbox-4"), {css:{x: '-20%'}});
				TweenMax.set($(".mid-orb-1, .mid-orb-2, .mid-orb-3, .mid-orb-4, .mid-orb-5"), {css:{scale: '0.0'}});

				var tween1 = new TimelineMax()
	        .add(TweenMax.to($(".line-1"), this.speed, {className: "+=draw"}))
	        .add(TweenMax.to($(".mid-orb-1"), this.speed, {className: "+=draw"}))
	        .add(TweenMax.to($(".workflow-graphic-1"), this.speed, {css:{opacity:1, scale: '1.0'}}))
	        .add(TweenMax.to($(".workflow-textbox-1"), this.speed, {css:{opacity:1, x: '0%'}}))
					.add(TweenMax.to($(".mid-orb-1"), this.speed, {className: "-=draw"}))
					.add(TweenMax.to($(".line-2"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".mid-orb-2"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".workflow-graphic-2"), this.speed, {css:{opacity:1, scale: '1.0'}}))
					.add(TweenMax.to($(".workflow-textbox-2"), this.speed, {css:{opacity:1, x: '0%'}}))
					.add(TweenMax.to($(".mid-orb-2"), this.speed, {className: "-=draw"}))
					.add(TweenMax.to($(".line-3"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".mid-orb-3"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".workflow-graphic-3"), this.speed, {css:{opacity:1, scale: '1.0'}}))
					.add(TweenMax.to($(".workflow-textbox-3"), this.speed, {css:{opacity:1, x: '0%'}}))
					.add(TweenMax.to($(".mid-orb-3"), this.speed, {className: "-=draw"}))
					.add(TweenMax.to($(".line-4"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".mid-orb-4"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".workflow-graphic-4"), this.speed, {css:{opacity:1, scale: '1.0'}}))
					.add(TweenMax.to($(".workflow-textbox-4"), this.speed, {css:{opacity:1, x: '0%'}}))
					.add(TweenMax.to($(".mid-orb-4"), this.speed, {className: "-=draw"}))
					.add(TweenMax.to($(".line-5"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".mid-orb-5"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".workflow-graphic-5"), this.speed, {css:{opacity:1, scale: '1.0'}}))
					.add(TweenMax.to($(".workflow-textbox-5"), this.speed, {css:{opacity:1, x: '0%'}}))
					.add(TweenMax.to($(".mid-orb-5"), this.speed, {className: "-=draw"}))
					.add(TweenMax.to($(".line-6"), this.speed, {className: "+=draw"}))
					.add(TweenMax.to($(".end-orb"), this.speed, {className: "+=draw"}));

				var tween2 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-1"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-workflow-textbox-1"), this.mobilespeed, {css:{opacity:1, y: '0%'}}))
				var tween3 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-2"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-workflow-textbox-2"), this.mobilespeed, {css:{opacity:1, y: '0%'}}))
				var tween4 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-3"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-workflow-textbox-3"), this.mobilespeed, {css:{opacity:1, y: '0%'}}))
				var tween5 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-4"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-workflow-textbox-4"), this.mobilespeed, {css:{opacity:1, y: '0%'}}))
				var tween6 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-5"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-workflow-textbox-5"), this.mobilespeed, {css:{opacity:1, y: '0%'}}))
				var tween7 = new TimelineMax()
					.add(TweenMax.to($(".mobile-line-6"), this.mobilespeed, {className: "+=draw"}))
					.add(TweenMax.to($(".mobile-end-orb"), 1, {className: "+=draw"}));

				var scene1 = new ScrollMagic.Scene({
	          triggerElement: "#trigger1",
	          // triggerHook: "onEnter",
	          // triggerOffset: 400,
	          tweenChanges: true,
	          duration: 1000,
	          offset: 0,
	          reverse: false
	        })
	        .setTween(tween1)
	        // .addIndicators({name: "tween css class 1"})
	        .addTo(controllers);

				var scene2 = new ScrollMagic.Scene({
	          triggerElement: "#trigger2",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween2)
	        // .addIndicators({name: "tween mobile 1"})
	        .addTo(controllers);

				var scene3 = new ScrollMagic.Scene({
	          triggerElement: "#trigger3",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween3)
	        // .addIndicators({name: "tween mobile 2"})
	        .addTo(controllers);

				var scene4 = new ScrollMagic.Scene({
	          triggerElement: "#trigger4",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween4)
	        // .addIndicators({name: "tween mobile 3"})
	        .addTo(controllers);

				var scene5 = new ScrollMagic.Scene({
	          triggerElement: "#trigger5",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween5)
	        // .addIndicators({name: "tween mobile 4"})
	        .addTo(controllers);

				var scene6 = new ScrollMagic.Scene({
	          triggerElement: "#trigger6",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween6)
	        // .addIndicators({name: "tween mobile 5"})
	        .addTo(controllers);

				var scene7 = new ScrollMagic.Scene({
	          triggerElement: "#trigger7",
	          // triggerHook: "onEnter",
	          // triggerOffset: -200,
	          // tweenChanges: true,
	          duration: 200,
	          offset: -0,
	          // reverse: false
	        })
	        .setTween(tween7)
	        // .addIndicators({name: "tween mobile 6"})
	        .addTo(controllers);

				new PanelRollover({
					rollOverEle:['.services-inner', '.projects-inner'],
					background:'.darken-background',
					header:'.header',
					lineFill:'.line-fill',
					plusSymbol:'.plus-symbol',
				});
			}
		};

		return PageView;
	}
);
