/**
 * home.js
 */
define(
	[	
		 'jquery'
		,'quoteEngine'
		,'videoInjector'
	],  
	
	function($, QuoteEngine, VideoInjector) {
			
		/**
		 * Define foundation model view
		 */
		var PageView = function() {}
			
		PageView.prototype = {
			
			initialize: function() {
				$(".button-group .button").bind("click",{'parent':this},this.toggleIntro);
				this.refreshIntro();

				var quoteEngine = new QuoteEngine();
				
				var videoInjector = new VideoInjector({
					 'video':'.video-wrap'
					,'poster':'.poster-wrap'
					,'overlay':'.black-out-layer'
				});
				
				var parent = this;
				$(window).bind("resize", function(event) {
					parent.refreshIntro();
				});
			}

			,toggleIntro: function(e) {
				e.preventDefault();
				var id = $(this).attr("data-id");

				$(".button-group .button[data-id!=\""+id+"\"]").removeClass("active");
				$(".button-group .button[data-id=\""+id+"\"]").addClass("active");

				$(".intro-block[data-id!=\""+id+"\"]").removeClass("active");
				$(".intro-block[data-id=\""+id+"\"]").addClass("active");
				e.data.parent.refreshIntro();
			}

			,refreshIntro: function() {
				$(".blocks-outer").css("height",$(".intro-block.active").innerHeight());
			}			
		};
		
		return PageView;
	}
);