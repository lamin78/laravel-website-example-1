/**
 * Filename: client-engine.js
 * Uses Ecmascript 6
 */
define([
      'jquery', 'underscore', 'preload', 'jquerythrottle'
    ],

    function($, _) {

      /**
       * Attach multiple event listener function
       * @param {object} el
       * @param {string} events
       * @param {fn} fn
       */
      function addListenerMulti(el, events, fn) {
          var evts = events.split(' ');
          for (var i = 0, iLen = evts.length; i < iLen; i++) {
              el.addEventListener(evts[i], fn, false);
          }
      }

        /**
         * Ensure frame draws begin at the start of each frame
         * @param {fn} callback
         */
        var reqAnimationFrame = (function() {
            return window["requestAnimationFrame"] || function(callback) {
                setTimeout(callback, 1000 / 60);
            }
        })();

        /**
         * Initialise client engine values
         * @param {object} obj
         */
        var C_Engine = function(obj) {

            this.container = document.querySelector(obj.container);
            this.panes = Array.prototype.slice.call(this.container.children, 0);
            this.fadeIn = obj.fadeIn;
            this.splitArray = [];
            this.splitIndex = 0;
            this.paddingInt = 0;
            this.sequenceInterval = 10;
            this.breakPointMedium = 768;
            this.clientArray = window.clientsArray;
            this.resizeFlag = ($(window).outerWidth() < this.breakPointMedium ? true : false);
            this.chunk = ($(window).outerWidth() < this.breakPointMedium ? 4 : 8);

            addListenerMulti(window, 'scroll DOMMouseScroll mousewheel resize', (e) => this.parallaxText(e, document.querySelector('.inner-clients-header')));
        }

        /**
         * Add methods
         */
        C_Engine.prototype = {

            /**
             * Preload sprite sheet
             * @param {string} imgDir
             * @param {array} imgArray
             */
            preload: function(imgDir, imgArray) {

                var parent = this;

                $(imgArray).preload(imgDir, function() {

                    parent.buildArray(function() {
                        reqAnimationFrame(parent.render.bind(parent));
                    });

                    if (parent.fadeIn)
                        $(parent.container).parent().addClass('go');
                });

                /**
                 * Throttle window resize event listener
                 * redraw client DOM
                 */
                $(window)
                    .resize($.throttle(250, function() {

                        if ($(window).outerWidth() < parent.breakPointMedium && parent.resizeFlag === false) {

                            clearInterval(parent.sequence);
                            parent.chunk = 4;
                            parent.buildArray(function() {
                                reqAnimationFrame(parent.render.bind(parent));
                                parent.resizeFlag = true;
                            });
                        }

                        if ($(window).outerWidth() > parent.breakPointMedium && parent.resizeFlag === true) {

                            clearInterval(parent.sequence);
                            parent.splitIndex = 0;
                            parent.chunk = 8;
                            parent.buildArray(function() {
                                reqAnimationFrame(parent.render.bind(parent));
                                parent.resizeFlag = false;
                            });
                        }
                    }));
            },

            /**
             * Build working array
             * @param {fn} cb
             */
            buildArray: function(cb) {

                var parent = this;

                this.splitArray = _(_.shuffle(this.clientArray)).groupBy(function(a, b) {
                    return Math.floor(b / parent.chunk);
                });

                cb();
            },

            /**
             * Render to DOM
             */
            render: function() {

                var parent = this,
                    $ele = $(this.container).find('ul');

                this.hideClients(function() {

                    $ele.empty();

                    parent.splitArray[parent.splitIndex].map(function(value, index) {

                        $ele.append($('<li />', {
                                'class': 'columns span-3 large-4 medium-6 small-6',
                                'ref': value
                            })
                            .append($('<a />', {
                                    //'href': ''
                                })
                                .append($('<div />', {
                                    'class': 'logo'
                                }))
                                .append($('<div />', {
                                    'class': 'logo-over'
                                }))));
                    }, parent);

                    parent.showClients();

                    if ($(window).outerWidth() < parent.breakPointMedium)
                        parent.startSequence();

                    if (window.UnisonModel.isMobile)
                        $('.logo').addClass('no-change');
                });
            },

            /**
             * Start swap sequence
             */
            startSequence: function() {

                var parent = this;

                this.sequence = setTimeout(function() {

                    parent.splitIndex = (parent.splitIndex === 0 ? 1 : 0);
                    reqAnimationFrame(parent.render.bind(parent));

                }, this.sequenceInterval * 1000);
            },

            /**
             * fade in
             */
            showClients: function() {

                this.container.className = this.container.className.replace('hide', '').trim();
                this.container.className += ' show';
            },

            /**
             * fade out
             *@param {fn} cb
             */
            hideClients: function(cb) {

                this.container.className = this.container.className.replace('show', '').trim();
                this.container.className += ' hide';

                var waitForFadeOut = setTimeout(function() {
                    cb();
                }, 300)
            },

            parallaxText: function(evt, ele) {
              $(ele).css('top', ( 20 + (parseInt(ele.getBoundingClientRect().top /100) * 3)) + 'px');
            }
        }

        return C_Engine;
    }
);
