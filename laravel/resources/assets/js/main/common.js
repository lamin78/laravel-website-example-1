var app_version = new Date().getTime();
var app_live = false;

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/**
 * Trace (console.log)
 */
function trace(msg, toScreen) {
	if(window.console) {
		console.log(msg);
	} else if ( typeof( jsTrace ) != 'undefined') {
		jsTrace.send(msg);
	} else {
		//alert(msg);
	}
	if(toScreen) {
		$("#st-debug").html(msg);
	}
}

function TrackEventGA(Category, Action, Label, Value) {
	"use strict";
	if (app_live) {
		if (typeof (_gaq) !== "undefined") {
			_gaq.push(['_trackEvent', Category, Action, Label, Value]);
		} else if (typeof (ga) !== "undefined") {
			ga('send', 'event', Category, Action, Label, Value);
		}
	} else {
		trace("GA: " + Category + "," + Action + "," + Label + "," + Value);
	}
}

function scrollToAnchor() {
	var aTag = $("a[name='"+ this.aid +"']");
	$('html,body').animate({
		scrollTop: aTag.offset().top
	}, '300', 'swing');
}

// Perform a smooth scroll, courtesy of css-tricks
function addSmoothScroll() {
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

			if (target.length) {
				var targetY = target.offset().top;
				//trace(targetY);
				$('html,body').animate({
					scrollTop: targetY
				}, 500);

				window.location.hash = this.hash;

				return false;
			}
		}
	});
};
