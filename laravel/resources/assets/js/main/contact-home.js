/**
 * home.js
 */
define(
	[
		 'jquery'
		//,'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyDUeQCQTUI9A_5l2efQx637xKqOl9_C-T0'
		,'async!https://maps.googleapis.com/maps/api/js?key=AIzaSyBM0jJEssTJFF4pTsfs6Z0gnDE-ubF5gQU'
		,'videoInjector'
	],

	function($, gMaps, VideoInjector) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {

			this.map = null;
			this.offices = [
				 ['London Office', 51.52847999999999, -0.09677940000005947, 1]
				,['Reading Office', 51.4249115, -0.9874348, 2]
				,['Amsterdam', 52.365005, 4.906033, 2]
			];
		}

		PageView.prototype = {

			 initialize: function() {

				var parent = this;
				var image = {
				    url: '/images/contact-landing-page/map-marker.png',
				    size: new google.maps.Size(50, 64),
				    origin: new google.maps.Point(0, 0),
				    anchor: new google.maps.Point(25, 64)
				};

				var shape = {
					coords: [10,0,40,0,50,10,50,50,50,40,40,50,30,50,25,64,20,50,10,50,0,40,0,10],
					type: 'poly'
				};

				this.map = new google.maps.Map(document.getElementById('gMap'), {
				    center: {lat: this.offices[0][1], lng: this.offices[0][2]},
				    zoom: 16,
				    scrollwheel: false,
				});

				for (var i = 0; i < this.offices.length; i++) {
					var office = this.offices[i];
					var marker = new google.maps.Marker({
						position: {lat: office[1], lng: office[2]},
						map: this.map,
						icon: image,
						shape: shape,
						title: office[0],
						zIndex: office[3]
				    });
				}

				$(".pick li").bind("click", {'parent':parent}, this.changeMap);

				$(window).bind("resize", function(event) {
					parent.refreshMap();
				});
			}

			,changeMap: function(e) {
				e.preventDefault();
				if (!$(this).hasClass("active")) {
					var id = parseInt($(this).data("id"));
					$(".pick li[data-id!=\""+id+"\"]").removeClass("active");
					$(".pick li[data-id=\""+id+"\"]").addClass("active");

					var map = e.data.parent.map;
					map.panTo({lat: e.data.parent.offices[id][1], lng: e.data.parent.offices[id][2]});
				}
			}

			,refreshMap: function() {
				var id = $(".pick li.active").data("id");
				this.map.panTo({lat: this.offices[id][1], lng: this.offices[id][2]});
			}
		};

		return PageView;
	}
);
