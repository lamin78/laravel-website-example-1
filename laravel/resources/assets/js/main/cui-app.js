/**
 * cui-app.js
 */
define(
	[ 'jquery', 'axios', 'lodash', 'vuejs', 'cuiTextBlock', 'cuiInputGroup', 'cuiLoading', 'cuiWatchScreen', 'cuiHeaderBlock' ],

	function($, axios, _, Vue, CuiTextBlock, CuiInputGroup, CuiLoading, CuiWatchScreen, CuiHeaderBlock) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {

		}

		PageView.prototype = {
			initialize: function() {

				window._eventbus = new Vue({});
				window._watchScreen = new CuiWatchScreen({});

				new Vue({
					el: '#conversation_ui',
					data: {
						typing: false,
						backTrack: false,
						cui_data: [],
						cui_options: [],
						cui_inputs: [],
						name: '',
						username: ''
					},
					components: {
						'cuiheaderblock': CuiHeaderBlock,
						'cuitextblock': CuiTextBlock,
						'cuiloading': CuiLoading,
						'cuiinputgroup': CuiInputGroup,
					},
					mounted() {

						let innerCui = document.querySelector('.inner-cui');
						let downArrow = document.querySelector('.down-arrow');
						downArrow.classList.add('hidden');

						axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
						_eventbus.$on('send-choice', this.sendChoice);
						_eventbus.$on('send-input', this.sendInput);

						let threshold = false;
						$(".inner-cui").scroll((e)=>{
							if(!threshold) {
								threshold = true;
								setTimeout(()=>{
									_eventbus.$emit('scrolling', {scrolltop: e.target.scrollTop});
									threshold = false;
								}, 0.1*1000);
							}
						});

						this.typing = true;
						setTimeout(this.getFirstLine, 1*1000);
					},
					methods: {

						// SHOW COMMENT SEQUENCE
						showError() {

							this.cui_data.push({
								type: 'cui_text',
								text: 'I\'m busy at the moment. Please come back later :)',
							});

							this.cui_options.push({
								type: 'option',
								option_text: 'Ok I\'ll come back later',
								out_id: '0',
							});
						},
						getFirstLine() {
							axios.get('/cui/get-next-line/1')
							  .then((response) => {
									if(typeof response.data.empty !== 'undefined')
										this.showError();
									else {
										this.parseText(response.data.cui.conversationText);
										this.parseOption(response.data.cui.options);
									}
									this.typing = false;
							  })
							  .catch((error) => {

									this.typing = false;
							  });
						},
						getNextLine(id) {
							this.typing = true;
							setTimeout(()=>{
								axios.get(`/cui/get-next-line/${id}`)
								.then((response) => {
									this.parseText(response.data.cui.conversationText);
									this.parseOption(response.data.cui.options);
									_eventbus.$emit('update-opacity');
									this.typing = false;
								})
								.catch((error) => {
									this.typing = false;
								});
							}, ((Math.random() * 2) + 1) * 500);
						},
						parseText(string) {
							let textArr = (string).split('|');
							let rInd = ((Math.floor(Math.random() * textArr.length) + 1)-1);
							let chosenText = textArr.filter((text, ind) => {
								if(ind === rInd)
									return text;
							});

							chosenText[0] = (chosenText[0]).replace('{source}', source);
							chosenText[0] = (chosenText[0]).replace('{name}', this.username);

							console.log(chosenText[0]);

							this.cui_data.push({
								type: 'cui_text',
								text: chosenText[0],
							});
						},
						parseOption(array) {

							this.backTrack = false;

							console.log(array);

							if(array[0].type === 'goto') {

								setTimeout(() => {
									if((array[0].out_id).includes('link|')) {

										let link = array[0].out_id;
										if((link).includes('link|'))
											link = (link).replace('link|', 'https://');

										if(!(link).includes('https://'))
											link = (link).replace('www', 'https://www');

										let win = window.open(link, '_blank');
										if (win)
											win.focus();
										else
											alert('Please allow popups on this site, then refresh the page.');

										this.backTrack = true;
									}
									else if((array[0].out_id).includes('[condition|name]')) {

										let lineId = (array[0].out_id).replace('[condition|name]', '');
										lineId = (lineId).split('|');

										console.log(this.username);

										if(this.username === "")
											this.getNextLine(lineId[0]);
										else
											this.getNextLine(lineId[1]);
									}
									else
										this.getNextLine(array[0].out_id);

								}, ((Math.random() * 2) + 1) * 300);
							}
							else {
								this.cui_options = [];
								// console.log(array);
								for(let i=0; i<array.length; i++) {
									// console.log(array[i]);
									this.cui_options.push({
										type: array[i].type,
										option_text: array[i].option_text,
										out_id: array[i].out_id,
										link_text: (typeof array[i].linkData !== 'undefined' ? array[i].linkData.link_text : null )
									});

									// console.log(this.cui_options);
								}
							}
						},
						sendInput(params) {
							this.cui_options = [];
							this.cui_data.push({
								type: 'user_text',
								text: params.text,
							});

							console.log(params);
							if(params.hasName === true)
								this.username =  params.text;

							setTimeout(()=>{
								this.typing = true;
								setTimeout(()=>{
									let idRef = (params.id).split('|');
									idRef = idRef[((Math.floor(Math.random() * idRef.length) + 1)-1)];
									this.getNextLine(idRef);
								}, ((Math.random() * 2) + 1) * 500);
							}, ((Math.random() * 1) + 1) * 500);
						},
						sendChoice(params) {

							let idRef = 0;

							this.cui_options = [];
							this.cui_data.push({
								type: 'user_text',
								text: params.text,
							});

							console.log(params);

							if((params.id).includes('[condition|name]')) {

								let lineId = (params.id).replace('[condition|name]', '');
								lineId = (lineId).split('|');

								console.log(this.username);

								if(this.username === "")
									idRef = lineId[0];
								else
									idRef = lineId[1];
							}
							else {
								idRef = (params.id).split('|');
								idRef = idRef[((Math.floor(Math.random() * idRef.length) + 1)-1)];
							}

							setTimeout(()=>{
								this.typing = true;
								setTimeout(()=>{
									this.getNextLine(idRef);
								}, ((Math.random() * 2) + 1) * 500);
							}, 1 * 500);
						}
					},
					computed: {},
				});
			}
		};

		return PageView;
	}
);
