/**
 * cui-header-block.js
 */
define(
	[ 'axios', 'lodash', 'vuejs', 'tweenmax', 'q', 'cuiWatchScreen'],

	function(axios, _, Vue, TweenMax, Q, CuiWatchScreen) {

		/**
		 * Define foundation model view
		 */
		return Vue.component('cuiheader', {
			template: '#cui-header-template',
			props: [],
			data() {
				return {
					logoText: {},
					logo: {},
					navInner: {}
				}
			},
			beforeDestroy() {
				_eventbus.$off('hide-header', this.hideAction);
			},
			mounted() {
				window._watchScreen = new CuiWatchScreen({});

				this.logoText = this.$el.querySelector('.logo-text > a > img');
				this.logo = this.$el.querySelector('.logo');
				this.navInner = this.$el.querySelector('.nav-inner');
				this.cuiMenu = this.$el.querySelector('.cui-menu');

				_eventbus.$on('hide-header', this.hideAction);
				_eventbus.$on('update-window');
			},
			methods: {

				hideAction() {
					console.log(window.outerWidth);
					this.hideLogoAction();
					if(_watchScreen.screenTest(window.outerWidth, '>=', 'md'))
						this.hideMenuAction();
				},
				showAction() {
					this.showLogoAction();
					this.showMenuAction();
				},

				hideLogoAction() {
					this.hideLogo()
						.then(this.shrinkBox);
				},
				hideMenuAction() {
					this.hideMenu()
						.then(this.showBurger);
				},
				showLogoAction() {
					this.growBox()
						.then(this.showLogo);
				},
				showMenuAction() {
					this.hideBurger()
						.then(this.showMenu);
				},

				hideMenu() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.navInner, .5, {css:{x:'100%', opacity: 0}, ease:Power3.easeInOut, onComplete: ()=>{
              resolve();
            }});
					});
				},
				showBurger() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.cuiMenu, .5, {css:{opacity:1}, ease:Power3.easeInOut, onComplete: ()=>{
              resolve();
            }});
					});
				},

				hideBurger() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.cuiMenu, .5, {css:{opacity:0}, ease:Power3.easeInOut, onComplete: ()=>{
              resolve();
            }});
					});
				},
				showMenu() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.navInner, .5, {css:{x:'0%', opacity: 1}, ease:Power3.easeInOut, onComplete: ()=>{
							resolve();
						}});
					});
				},

				hideLogo() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.logoText, .5, {x:'-90%', ease:Power3.easeInOut, onComplete: ()=>{
							this.logoText.classList.add('hidden');
              resolve();
            }});
					});
				},
				shrinkBox() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.logo, .5, {scale:'0.5', ease:Power3.easeInOut, onComplete: ()=>{
							resolve();
						}});
						resolve();
					});
				},

				growBox() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.logo, .5, {scale:'1.0', ease:Power3.easeInOut, onComplete: ()=>{
							this.logoText.classList.remove('hidden');
							resolve();
						}});
					});
				},
				showLogo() {
					return Q.promise((resolve, reject, notify)=>{
						TweenMax.to(this.logoText, .5, {x:'20%', ease:Power3.easeInOut, onComplete: ()=>{
              resolve();
            }});
					});
				},

			},
			computed: {},
		});
	}
);
