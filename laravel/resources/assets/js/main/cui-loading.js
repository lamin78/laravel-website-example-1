/**
 * cui-loading.js
 */
define(
	[ 'axios', 'lodash', 'vuejs' ],

	function(axios, _, Vue) {

		/**
		 * Define foundation model view
		 */
		return Vue.component('cuiloading', {
			template: '#loading-cui-text-template',
			props: ['typing'],
			data() {
				return {
					test: 'this',
				}
			},
			mounted() {
				console.log('cui text block');
			},
			methods: {},
			computed: {},
		});
	}
);
