/**
 * cui-text-block.js
 */
define(
	[ 'jquery', 'axios', 'lodash', 'vuejs', 'tweenmax', 'q'],

	function($, axios, _, Vue, TweenMax, Q) {

		/**
		 * Define foundation model view
		 */
		return Vue.component('cuitextblock', {
			template: '#text-block-template',
			props: ['cuiType', 'cuiText'],
			data() {
				return {
					textBoxAlignment: 'left',
					textOpacity: 1,
					innerCuiEle: {},
				}
			},
			beforeDestroy() {
	      _eventbus.$off('update-window', this.updateHeight);
				_eventbus.$off('update-opacity', this.updateOpacity);
	    },
			mounted() {
				this.innerCuiEle = document.querySelector('.inner-cui');
				this.textBoxAlignment = (this.cuiType === 'cui_text' ? 'left' : 'right' );
				this.scrollWindow();
				this.updateHeight();
				this.updateOpacity();

				// EVENTS LISTNERS
				_eventbus.$on('update-window', this.updateHeight);
				_eventbus.$on('update-opacity', this.updateOpacity);
				_eventbus.$on('scrolling', this.updateOpacity);
			},
			updated() {
			  this.$nextTick(()=>{
					this.scrollWindow()
						.then(this.showText);
					setTimeout(()=>{
						this.updateHeight();
						this.updateOpacity();
					}, 1*1000);
			  });
			},
			methods: {
				updateOpacity() {
					let header = document.querySelector('#headerWrapper');
					let textBoxTop = this.$el.getBoundingClientRect().top;
					let headerHeight = header.getBoundingClientRect().height;

					this.$el.style.opacity = (1.2*(textBoxTop/headerHeight)).toFixed(1);
					// this.$el.style.transform = `scale(${(1.4*(textBoxTop/headerHeight)).toFixed(1)})`;
				},
				updateHeight() {

					let bottomHeight = document.querySelector('.cui-bottom-section').getBoundingClientRect().height;
					let windowHeight = $(window).outerHeight();
					let newHeight = (windowHeight - bottomHeight);
					let adjust = 60;

					this.innerCuiEle.style.maxHeight = `${(newHeight - adjust)}px`;
					this.innerCuiEle.style.marginBottom = `${(bottomHeight + adjust)}px`;
				},
				scrollWindow() {
					return Q.promise((resolve, reject, notify)=>{
						let $textScroll = $('.inner-cui');
						let $textBlock = $('.inner-cui > #text_block');
						let height = 0;

						for(let i=0; i<$textBlock.length; i++) {
							height += $textBlock[i].getBoundingClientRect().height;
						}

						$textScroll.animate({scrollTop: `${height}px`}, 500, 'swing');
						resolve();
					});
				},
				showText() {
					return Q.promise((resolve, reject, notify)=>{
						// TweenMax.to(this.navInner, .5, {css:{opacity: 1}, ease:Power3.easeInOut, onComplete: ()=>{
              resolve();
            // }});
					});
				},
			},
			computed: {
				textStyle() {
					return {
						'columns small-up-12': true,
						'cui-transform': (this.cuiType === 'cui_text'),
						'user-transform': (this.cuiType !== 'cui_text')
					}
				}
			},
		});
	}
);
