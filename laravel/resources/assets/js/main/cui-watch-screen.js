// laravel\resources\assets\js\main\cui-watch-screen.js

define(
	[ ],

	function() {

		var WS = function(obj) {

      this.screen = {
        'xsm' : 320,
        'sm'  : 480,
        'md'  : 768,
        'lg'  : 960,
        'xlg' : 1366,
        'xxlg': 1920,
      };

      this.throttle = {};
      Object.assign(this, obj);

      this.init();
    }

    WS.prototype = ({

      init() {
         window.onresize = this.resize;
      },

      resize() {
        clearTimeout(this.throttle)
        this.throttle = setTimeout((i)=>{
          // _eventbus.$emit('update-window');
        }, 150);
      },

      screenTest(screenWidth, op, threshold) {
        switch(true) {
          case (op === '<'):
            if(screenWidth < this.screen[threshold])
              return true;
            else
              return false;
          break;
          case (op === '<='):
            if(screenWidth <= this.screen[threshold])
              return true;
            else
              return false;
          break;
          case (op === '>'):
            if(screenWidth > this.screen[threshold])
              return true;
            else
              return false;
          break;
          case (op === '>='):
            if(screenWidth >= this.screen[threshold])
              return true;
            else
              return false;
          break;
        }
      }
    });

    return WS;
	}
);
