/**
 * dropdown-model.js
 */
define(
	[	
		 'jquery'
		,'backbone'
	],  
	
	function($, Backbone) {
			
		/**
		 * Define foundation model view
		 */
		var DropdownModel = Backbone.Model.extend({
			
			dd:null,
			placeholder:null,
			opts:null,
			val:'',
			index:-1,

			constructor: function(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('ul.dropdown > li');
				this.initEvents();
			},

			initEvents : function() {
				var obj = this;
				obj.dd.bind('click', function(e){
					$(this).toggleClass('active');
					return false;
				});

				obj.opts.bind('click',function(){
					var opt = $(this);
					obj.val = opt.data("value");
					obj.index = opt.index();
					obj.placeholder.text(opt.find("a").text());
					obj.trigger("change");
				});
			},
			getValue : function() {
				return this.val;
			},
			getIndex : function() {
				return this.index;
			}
		});
		
		return DropdownModel;
	}
);