/**
 * error-home.js
 * Uses ecmascript 6
 */
define(
	['jquery'],

	function($) {

		/**
		 * Define foundation model view
		 */
		var Me = function() {
			this.$ele = $('.inner');
			this.$eleH3 = $('.inner > h3');
			this.anim = (Math.floor(Math.random() * 2) + 1 === 1 ? 'reveal' : 'reveal-rotate' );
		}

		Me.prototype = ({
			initialize() {
				// if(window.navigator.userAgent.indexOf('MSIE 9') !== -1) {
				// 	this.$ele.animate({
				// 		'top': '20%',
				// 		'opacity': '1'
				// 	}, 300, () => {
				// 		this.$eleH3.animate({
				// 			'opacity': '1'
				// 		}, 300);
				// 	});
				// }
				// else {
				// 	setTimeout(() => this.$ele.addClass(this.anim), 0.3 * 1000);
				// 	setTimeout(() => this.$eleH3.addClass('reveal'), 0.6 * 1000);
				// };
			}
		});

		return Me;
	}
);
