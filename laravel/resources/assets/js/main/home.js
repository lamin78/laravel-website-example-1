/**
 * home.js
 */
define(
	[	
		 'jquery'
		,'hammerInit'
		,'videoInjector'
		,'clientEngine'
	],  
	
	function($, HammerInit, VideoInjector, ClientEngine) {
		
		/**
		 * Define foundation model view
		 */
		var PageView = function() {}
			
		PageView.prototype = {
			
			initialize: function() {
				
				var videoInjector = new VideoInjector({
					 'video':'.video-wrap'
					,'poster':'.poster-wrap'
					,'overlay':'.black-out-layer'
				});
				
				/**
				 * Build carousel from server object
				 * @param {obj} corouselArray
				 */
				Object.keys(corouselArray).forEach(function(index) {
					// generic carousel load
					var CaseCarousel = new HammerInit({
						 'container'	: corouselArray[index].container
						,'resizeFlag'	: corouselArray[index].resizeFlag
						,'fullFlag'		: corouselArray[index].fullFlag
						,'fadeTrans'	: corouselArray[index].fadeTrans
					});

					CaseCarousel.preLoadImages(
						 corouselArray[index].imageRoute
						,caseStudies
						,true //fade in when image preload complete
						,{'playWhenVisible': corouselArray[index].playWhenVisible}
					);
				});
				
				/**
				 * Initialise client engine if clients div present
				 */
				if($('.clients').length > 0) {
					var clientEngine = new ClientEngine({
						 'container': '.clients'
						,'fadeIn': true
					});
					clientEngine.preload('/images/shared/', ['clients-sprite-sheet.jpg']);						
				}
			}
		};
		
		return PageView;
	}
);