/**
 * init.js
 */
define(
	[
		 'jquery'
		,'common'
		,'animateit'
		,'retina'
		,'unison-model'
		,((pageViewJS == '') ? 'undefined' : pageViewJS)
	],

	function($, common, AnimateIt, Retina, UnisonModel, PageView) {

		// Add event dispatcher for menu open event
		var menuOpen = document.createEvent("Event");
		var menuClose = document.createEvent("Event");

		menuOpen.initEvent("menuOpen", true, true);
		menuClose.initEvent("menuClose", true, true);

		var initialize = function() {

			$.force_appear();


			$(document)
				.ready(function() {

					window.UnisonModel = new UnisonModel();
					/**
					 * Prevent image dragging
					 */
					$('img').on('dragstart', function(event) { event.preventDefault(); });

					/**
					 * Initialise page actions if available
					 */
					if(typeof PageView !== 'undefined') {
						pageView = new PageView();
						pageView.initialize();
					}

					$('.hamburger-menu .button')
						.on('touchstart click', function(event) {

							event.stopPropagation();
							event.preventDefault();

							if (!this.flag) {

								var that = this;
								this.flag = true;

								setTimeout(function(){ that.flag = false; }, 100);

								toggleMobileMenu();
							}

							return false;
						});

					$(".down-arrow")
						//.bind("click", scrollToAnchor.bind({'aid': 'content-section'}));
						.on('touchstart click', function(event) {

							event.stopPropagation();
							event.preventDefault();

							if (!this.flag) {

								var that = this;
								this.flag = true;

								setTimeout(function(){ that.flag = false; }, 100);

								scrollToAnchor.bind({'aid': 'content-section'}).call();
							}

							return false;
						});

					$(".play")
						//.bind("click", scrollToAnchor.bind({'aid': 'video-section'}));
						.on('touchstart click', function(event) {

							event.stopPropagation();
							event.preventDefault();

							if (!this.flag) {

								var that = this;
								this.flag = true;

								setTimeout(function(){ that.flag = false; }, 100);

								scrollToAnchor.bind({'aid': 'video-section'}).call();
							}

							return false;
						});

					/**
					 * Fix earlier ipad squished header bug
					 */
					var iPadSel = /iPad/i;
					var iPad = iPadSel.test(navigator.userAgent);
					if(iPad)
						fixiPadHeight();
				});
		}

		var toggleMobileMenu = function(e) {
			//e.preventDefault();
			$(".nav-outer").toggleClass("open");
			$(".we-are-block, .project-head, #headDown, .play, .black-out-layer, .header-video-controls").toggleClass("hidden");

			if($(".nav-outer").hasClass("open"))
				window.dispatchEvent(menuOpen);
			else
				window.dispatchEvent(menuClose);
		}

		var fixiPadHeight = function() {

			$('.black-out-layer').css({
				 'height': '100%'
				,'top': '0px'
			});

			$('#headerWrapper').css({'height': $(window).innerHeight() + 'px'});

			return $(window).innerHeight();
		}

		return {
			initialize: initialize
		};
	}
)
