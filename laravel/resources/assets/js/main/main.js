/**
 * main.js
 * contents of main.js with extra configuration
 */
require.config({

	baseUrl: site_url + "/js/",

	//urlArgs: "bust=" + (new Date()).getTime(),	// halt caching of .js files for development

	waitSeconds: 0, 							// disable script loading timeout

	paths: {
		/**
		 * Libraries
		 */
    'jquery'		 		: 'libs/jquery/jquery-1.11.3.min'

    //,'json2' 			: 'libs/json2/json2-min'
    ,'underscore'	 	: 'libs/underscore/underscore-min'
    ,'backbone'			: 'libs/backbone/backbone-min'

        //,'handlebars'			: 'libs/handlebars/handlebars-v3.0.3'
		//,'jqueryfb'				: 'libs/jquery-fb/jQuery.fb'
		,'jquerythrottle'		: 'libs/jquery-throttle/jquery-throttle-min'
		,'async'				: 'libs/requirejs/plugins/async'

		/* Triggered animations */
		,'animateit'			: 'libs/animate-it/css3-animate-it'

		/* Unison */
		,'unison'				: 'libs/unison/unison.min'
		,'unison-model'			: 'main/unison-model-min'

		/* hammer */
		,'hammer'				: 'libs/hammer/hammer.min'
		,'hammerInit'			: 'main/hammer-init-min'
		,'hammerCmsInit'			: 'main/hammer-cms-init-min'

		/* packery */
		,'packery'				: 'libs/packery/packery.pkgd.min'
		,'jquery-bridget/jquery.bridget' : 'libs/packery/packery.pkgd.min'
		,'dropdown'				: 'main/dropdown-model-min'

		/* fancybox */
		,'fancybox'				: 'libs/fancybox/jquery.fancybox.pack'
		,'fancybox-media'		: 'libs/fancybox/helpers/jquery.fancybox-media'

		/* retina */
		,'retina'				: 'libs/retina/retina-min'

		/* Preload Script */
		,'preload'				: 'libs/preload/image-preload.min'

		/* Quotes Engine */
		,'quoteEngine'			: 'main/quote-engine-min'

		/* Slider Engine */
		,'sliderEngine'			: 'main/slider-engine-min'

		/* Typed text Engine */
		,'typedEngine'			: 'main/typed-engine-min'

		/* Client logo randomizer */
		,'clientEngine'			: 'main/client-engine-min'

		/* Video injection utility */
		,'videoInjector'		: 'main/video-injector-min'

		/* Video control utility */
		,'videoHeaderControl'	: 'main/video-header-control-min'

		/* Video player engine */
		,'jplayer'				: 'libs/jplayer/jquery.jplayer.min'
		,'videoEngine'			: 'main/video-player-engine-min'


		/* conversation ui	*/
		,'q' : '//cdnjs.cloudflare.com/ajax/libs/q.js/1.5.0/q.min'
		,'tweenmax' : 'libs/gsap/TweenMax.min'
		,'easepack' : 'libs/gsap/easing/EasePack.min'
		,'cssplugin' : 'libs/gsap/plugins/CSSPlugin.min'
		,'axios': 'libs/axios/axios.min'
		,'lodash': 'libs/lodash/lodash.min'
		,'vuejs': 'libs/vuejs/vue'
		,'cuiHeaderBlock': 'main/cui-header-block-min'
		,'cuiTextBlock': 'main/cui-text-block-min'
		,'cuiWatchScreen': 'main/cui-watch-screen-min'
		,'cuiInputGroup': 'main/cui-input-group-min'
		,'cuiLoading': 'main/cui-loading-min'
		,'cuiApp': 'main/cui-app-min'

		/**
		 * Application Logic
		 */
		,'common'				: 'main/common-min'
		,'init'					: 'main/init-min'
		,'pageView' 		: pageViewJS
    },

	/**
	 * Use shim for non AMD (Asynchronous Module Definition) compatible scripts
	 */
	shim: {
		'q': {
				exports : 'Q',
		},
		'tweenmax': {
				exports : 'TweenMax',
		},
		'easepack': {
			deps	 : ['tweenmax'],
			exports : 'EasePack',
		},
		'cssplugin': {
			deps	 : ['tweenmax'],
			exports : 'CSSPlugin',
		},
		'axios': {
				exports : 'Axios',
		},
		'lodash': {
				exports : 'Lodash',
		},
		'vuejs': {
				exports : 'Vue',
		},
		'backbone': {
             deps	 : ['underscore', 'jquery']
            ,exports : 'Backbone'
        },

        'underscore': {
            exports : '_'
        },

				'jquerythrottle': {
        	deps 	: ['jquery']
            ,exports : 'jquerythrottle'
        },

        'animateit': {
         	deps 	: ['jquery']
            ,exports : 'animateit'
        },

        'unison': {
            exports : 'Unison'
        },

        'hammer': {
            exports : 'Hammer'
        },

        'packery': {
            exports : 'Packery'
        },

        'retina': {
            exports : 'Retina'
        },

        'fancybox': {
         	deps 	: ['jquery']
            ,exports : 'FancyBox'
        },

        'fancybox-media': {
         	deps 	: ['jquery','fancybox']
            ,exports : 'FancyBoxMedia'
        },

        'gmaps': {
            deps	: ['jquery']
            ,exports : 'gMaps'
        },
    }
});

/**
 * Load the init module
 */
require(['init'],

	function(App) {
		App.initialize();
	}
);
