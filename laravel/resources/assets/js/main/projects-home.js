/**
 * projects-home.js
 */
define(
	[
		 'jquery'
		,'videoInjector'
	],

	function($, VideoInjector) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {}

		PageView.prototype = {

			initialize: function() {

				var parent = this;

				var videoInjector = new VideoInjector({
					 'video':'.video-wrap'
					,'poster':'.poster-wrap'
					,'overlay':'.black-out-layer'
				});

				if (!window.UnisonModel.isMobile) {
					$(".grid-wall .item").each(function(index,item) {
						$(item).bind("mouseenter", parent.itemEnter.bind(this, { 'obj' : parent }));
					});
					$(".grid-wall .item").each(function(index,item) {
						$(item).bind("mouseleave", parent.itemLeave.bind(this, { 'obj' : parent }));
					})
				}
		 	}

		 	,itemEnter: function() {

				var parent = arguments[0].obj, e = arguments[1];

		 		if (window.UnisonModel.getWidth() > 0) {

		 			var ratio = $(this).width() / $(this).height();
			 		var itemOffset	= $(this).offset();
			 		var itemMidX	= itemOffset.left + ($(this).width()/2);
			 		var itemMidY	= itemOffset.top + ($(this).height()/2);
			 		var angle		= parent.angle(itemMidX,itemMidY,e.pageX,e.pageY);
			 		var side;

			 		var angleMax = 135;
			 		var angleMin = 45;

					if (ratio == 0.5) {
						// Tall
				 		angleMax = 116.5;
			 			angleMin = 63.5;
				 	} else if (ratio == 2) {
						// Wide
				 		angleMax = 153.5;
			 			angleMin = 26.5;
				 	}

				 	if (angle >= -angleMax && angle < -angleMin) {
			 			side = "top";
			 		} else if (angle >= -angleMin && angle < angleMin) {
			 			side = "right";
			 		} else if (angle >= angleMin && angle < angleMax) {
			 			side = "bottom";
			 		} else {
			 			side = "left";
			 		}

			 		$(this).attr("direction",side);

			 		var overlay = $(this).find(".overlay");
			 		switch(side) {
			 			case "top":
			 				overlay.css("left","0%");
			 				overlay.css("top","-101%");
			 				break;
			 			case "right":
			 				overlay.css("left","101%");
			 				overlay.css("top","0%");
			 				break;
			 			case "bottom":
			 				overlay.css("left","0%");
			 				overlay.css("top","101%");
			 				break;
			 			case "left":
			 				overlay.css("left","-101%");
			 				overlay.css("top","0%");
			 				break;
			 		}

			 		overlay.stop().animate({
						top: "0%",
						left: "0%"
					}, 200, function() {
						// Animation complete.
					});
				}
		 	}

		 	,itemLeave: function() {

		 		var side = $(this).attr("direction");
		 		var overlay = $(this).find(".overlay");
		 		var retTop;
		 		var retLeft;

		 		switch(side) {
		 			case "top":
		 				retTop = "-101%";
						retLeft = "0%";
		 				break;
		 			case "right":
		 				retTop = "0%";
						retLeft = "101%";
		 				break;
		 			case "bottom":
		 				retTop = "101%";
						retLeft = "0%";
		 				break;
		 			case "left":
		 				retTop = "0%";
						retLeft = "-101%";
		 				break;
		 		}

				// if (side == "top" && overlay.hasClass("malt") && overlay.hasClass("dblHeight")) {
				// 	retTop = "-201%";
				// }

		 		overlay.stop().animate({
					top: retTop,
					left: retLeft
				}, 200, function() {
					// Animation complete.
				});
		 	}

		 	,angle: function (cx, cy, ex, ey) {
				var dy = ey - cy;
				var dx = ex - cx;
				var theta = Math.atan2(dy, dx); // range (-PI, PI)

				theta *= (180 / Math.PI); // rads to degs, range (-180, 180)
				return theta;
			}
		};

		return PageView;
	}
);
