/**
 * project-view.js
 */
define([
        'jquery', 'hammerInit', 'hammerCmsInit', 'videoInjector', 'videoEngine', 'sliderEngine'
    ],

    function($, HammerInit, HammerCmsInit, VideoInjector, VideoEngine, SliderEngine) {

        /**
         * Ensure frame draws begin at the start of each frame
         * @param {fn} callback
         */
        var reqAnimationFrame = (function() {

            return window["requestAnimationFrame"] || function(callback) {
                setTimeout(callback, 1000 / 60);
            }
        })();

        /**
         * Attach multiple event listener function#
         * @param {object} el
         * @param {string} events
         * @param {fn} fn
         */
        function addListenerMulti(el, events, fn) {
            var evts = events.split(' ');
            for (var i = 0, iLen = evts.length; i < iLen; i++) {
                el.addEventListener(evts[i], fn, false);
            }
        }

        /**
         * Calculate parallax
         * @param {jquery object} bgobj
         */
        var calcParallax = function(bgobj, i) {

            var oPos = bgobj.offset().top;
            var scrolltop = window.pageYOffset;
            var windowHeight = window.outerHeight;

            if (scrolltop > ((oPos - windowHeight) - 100)) {

                var yPos = 0.5 * (scrolltop - oPos);
                var coords = 'translate(0, ' + yPos + 'px)';
                var elem = document.querySelectorAll('.' + bgobj.attr('class').split(' ')[0]);

                elem[i].style.transform = coords;
                elem[i].style.mozTransform = coords;
                elem[i].style.webkitTransform = coords;
                elem[i].style.msTransform = coords;
            }
        }

        /**
         * Calculate mock screem scoll action
         * @param {event object} ev
         */
        var calcMockScroll = function(ev) {

            var $this = $(this),
                scrollTop = this.scrollTop,
                scrollHeight = this.scrollHeight,
                height = $this.height(),
                delta = (ev.type == 'DOMMouseScroll' ?
                    ev.originalEvent.detail * -40 :
                    ev.originalEvent.wheelDelta),
                up = delta > 0;

            if (!up && -delta > scrollHeight - height - scrollTop) {
                // Scrolling down, but this will take us past the bottom.
                $this.scrollTop(scrollHeight);
                return prevent(ev);
            } else if (up && delta > scrollTop) {
                // Scrolling up, but this will take us past the top.
                $this.scrollTop(0);
                return prevent(ev);
            }
        }

        var prevent = function(event) {
            event.stopPropagation();
            event.preventDefault();
            event.returnValue = false;
            return false;
        }

        /**
         * Define page view
         */
        var PageView = function() {

            this.previousScrollTop = 0;
            //this.iPadSel = /iPad/i;
        }

        PageView.prototype = {

            initialize: function() {

                var parent = this;
                var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                //var iPad = this.iPadSel.test(navigator.userAgent);

                /**
                 * Inject video player when carousel load complete
                 */
                // window.addEventListener('carouselDone', function() {
                    if ($('.video-player-wrap').length > 0) {
                        setTimeout(function() {
                            $('.video-player-wrap').each(function(index, ele) {
                                var videoEngine = new VideoEngine({
                                    'videoWrap': $(ele)
                                });
                            });
                        }, 1000);
                    }
                // }, false);

                /**
                 * Build carousel from server object
                 * @param {obj} corouselArray
                 */
                if(typeof corouselArray !== 'undefined') {
                  Object.keys(corouselArray).forEach(function(index) {
                    var CaseCarousel = new HammerInit({
                      'container': corouselArray[index].container,
                      'resizeFlag': corouselArray[index].resizeFlag,
                      'fullFlag': corouselArray[index].fullFlag,
                      'fadeTrans': corouselArray[index].fadeTrans,
                      'orbInd': index
                    });

                    CaseCarousel.preLoadImages(
                      corouselArray[index].imageRoute, corouselArray[index].imageArray, true, {
                        'playWhenVisible': corouselArray[index].playWhenVisible
                      }
                    );
                  });
                }

                let CaseCMSCarousel = [];
                if(typeof carouselCMSArray !== 'undefined') {
                  Object.keys(carouselCMSArray).forEach(function(index) {
                    if(carouselCMSArray[index].imageArray[0] !== null) {
                      CaseCMSCarousel.push(new HammerCmsInit({
                        'container': `.${carouselCMSArray[index].container}`,
                        'resizeFlag': carouselCMSArray[index].resizeFlag,
                        'fullFlag': carouselCMSArray[index].fullFlag,
                        'fadeTrans': carouselCMSArray[index].fadeTrans,
                        'orbInd': parseInt(index+1)
                      }));

                      CaseCMSCarousel[index].preLoadImages(
                        carouselCMSArray[index].imageRoute,
                        carouselCMSArray[index].imageArray,
                        true,
                        {
                          'playWhenVisible': carouselCMSArray[index].playWhenVisible
                        },
                        index,
                      );
                    }

                  });
                }

                /**
                 * Inject video content
                 */
                if ($('.video-wrap').length > 0) {
                  console.log('wrap recognized');
                    var videoInjector = new VideoInjector({
                        'video': '.video-wrap',
                        'poster': '.poster-wrap',
                        'overlay': '.black-out-layer'
                    });
                }

                $('.parallax .bg').each(function(index) {

                    var bgobj = $(this); // assigning the object

                    if (!window.UnisonModel.isMobile) {
                        calcParallax(bgobj, index);
                        bgobj.css('overflow-y', 'hidden');

                        addListenerMulti(window, 'scroll DOMMouseScroll mousewheel', function() {
                            reqAnimationFrame(calcParallax.bind('undefined', bgobj, index));
                        });

                    } else {
                        bgobj.addClass("nofix");
                        bgobj.parent().addClass("reflow-parallax");
                    }
                });

                // switch off header scroll for mobile devices
                if (window.UnisonModel.isMobile) {
                    $("div[class$='-header']").parent().addClass('scroll-header-background');
                    $("div[class$='-header']").addClass('scroll-header-background');
                };

                $('.Scrollable').on('DOMMouseScroll mousewheel', function(ev) {
                    reqAnimationFrame(calcMockScroll.bind(this, ev));
                });

								/**
								 * Inject slider script if image-slider detected
								 */
								if($('.image-slider').length > 0) {
                  $('.image-slider').each((index, ele)=>{
                    new SliderEngine(index);
                  })
								}
            },
        };

        return PageView;
    }
);
