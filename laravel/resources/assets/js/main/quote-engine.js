/**
 * home.js
 */
define(
	[	
		 'jquery'
	],  
	
	function($) {
			
		/**
		 * Define foundation model view
		 */
		var QuoteEngine = function() {
			
			this._quoteVis		= 0;
			this._quoteCount	= 0;
			this._quoteCurrent	= null;
			this._quotePad		= 120;
			this._quoteSpeed	= 10 * 1000;
			this.$quoteEle		= $(".quote")
			this.$blockEle		= $(".block")
			
			this.initialize();
		}
		
		QuoteEngine.prototype = {

			 initialize: function() {
				//this.listenTo(window.UnisonModel, 'change', this.bpChange);
				// Listen for Unison change event
				var  parent = this;
				
				window.addEventListener('unichange', parent.bpChange.bind(parent), false);

				this.$quoteEle.addClass("invisible");
				this.$quoteEle.addClass("transition");
				
				this._quoteCount = this.$quoteEle.length;
				
				$(window).bind("resize", function(event) {
					parent.fixHeights();
				});
				
				this.changeQuote(this);
				this.bpChange();
				
				setInterval(function() {
					parent.changeQuote(parent);
				}, parent._quoteSpeed);
			}

			,bpChange: function() {
				
				if (window.UnisonModel.getWidth() <= 481) {
					this.$blockEle.addClass("forced");
					this._quotePad = 160;
				} else {
					this.$blockEle.removeClass("forced");
					this._quotePad = 130;
				}
			}

			,changeQuote: function(me) {
				
				me._quoteVis++;
				
				if (me._quoteVis > me._quoteCount) {
					me._quoteVis = 1;
				}
				
				if (me._quoteCurrent != null) {
					me._quoteCurrent.addClass("invisible");
				}
				
				me._quoteCurrent = $(".quote[data-id=\""+me._quoteVis+"\"]");
				me._quoteCurrent.removeClass("invisible");
				
				var newHeight = me._quoteCurrent.find(".inner").innerHeight() + me._quotePad;
				$(".quotes").height(newHeight+"px");
			}

			,fixHeights: function() {
				var newHeight = this._quoteCurrent.find(".inner").innerHeight() + this._quotePad;
				$(".quotes").height(newHeight+"px");
			}
			
		};
		
		return QuoteEngine;
	}
);