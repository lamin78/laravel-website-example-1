/**
 * home.js
 */
define(
	[
		 'jquery'
		,'quoteEngine'
		,'videoInjector'
		,'clientEngine'
	],

	function($, QuoteEngine, VideoInjector, ClientEngine) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {
			this._needCount		= 0;
			this._needVis		=  0;
			this._needCurrent	= null;
		}

		PageView.prototype = {

			initialize: function() {
				
				var quoteEngine = new QuoteEngine();
				var parent = this;

				$(window).bind("resize", function(event) {
					parent.refreshView();
				});

				this.refreshView();

				if(/iPad/i.test(navigator.userAgent))
					$(".block").addClass("forced");

				this.enableNeeds();

				$(window).bind("resize", function(event) {
					parent.fixHeights();
				});

				/**
				 * Initialise client engine if clients div present
				 */
				if($('.clients').length > 0) {
					var clientEngine = new ClientEngine({
						 'container': '.clients'
						,'fadeIn': true
					});
					clientEngine.preload('/images/shared/', ['clients-sprite-sheet.jpg']);
				}
			}

			,refreshView: function() {

				$(".services .block .label h2, .services .block .icon").each(function() {
					var w = $(this).width();
					$(this).css("margin-left","-"+(w/2)+"px");
				});
			}

			,enableNeeds: function() {

				this._needCount = $("p.need").length;
				var parent = this;
				setInterval(function() {
					parent.changeNeed(parent);
				},5000);
				this.changeNeed(this);
				$("p.need").addClass("transition");
			}

			,changeNeed: function(me) {

				me._needVis++;
				if (me._needVis > me._needCount) {
					me._needVis = 1;
				}
				if (me._needCurrent != null) {
					me._needCurrent.addClass("invisible");
				}
				me._needCurrent = $("p.need[data-id=\""+me._needVis+"\"]");
				me._needCurrent.removeClass("invisible");

				var newHeight = me._needCurrent.innerHeight() + $(".intro h1").innerHeight();
				$(".intro").height(newHeight+"px");
			}

			,fixHeights: function() {

				var newHeight = this._needCurrent.innerHeight() + $(".intro h1").innerHeight();
				$(".intro").height(newHeight+"px");
			}

		};

		return PageView;
	}
);
