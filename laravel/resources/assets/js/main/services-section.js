/**
 * home.js
 */
define(
	[
		 'jquery'
		,'hammerInit'
		,'videoInjector'
		,'typedEngine'
		,'clientEngine'
	],

	function($, HammerInit, VideoInjector, TypedEngine, ClientEngine) {

		/**
		 * Define foundation model view
		 */
		var PageView = function() {}

		PageView.prototype = {

			initialize: function() {

				/**
				 * Inject video content
				 */
				var videoInjector = new VideoInjector({
					 'video':'.video-wrap'
					,'poster':'.poster-wrap'
					,'overlay':'.black-out-layer'
				});

				/**
				 * Inject slider script if image-slider detected
				 */
				if($('.typed-wrap').length > 0) {
					new TypedEngine({});
				}

				/**
				 * Initialise page specific carousel
				 */
				if(typeof corouselArray !== "undefined") {
					var CaseCarousel = new HammerInit({
						container	: '#carousel2'
						,resizeFlag	: false
						,fullFlag	: true
					});

					CaseCarousel.preLoadImages(
						corouselArray.imageRoute
						,caseStudies
						,true
						,{'playWhenVisible': corouselArray.playWhenVisible}
					);
				}


				/**
				 * Initialise client engine if clients div present
				 */
				if($('.clients').length > 0) {
					var clientEngine = new ClientEngine({
						 'container': '.clients'
						,'fadeIn': true
					});
					clientEngine.preload('/images/shared/', ['clients-sprite-sheet.jpg']);
				}
			}
		};

		return PageView;
	}
);
