/**
 * slider-engine.js
 * Uses ecmascript 6
 */
define(

    ['jquery', 'common'],

    function($) {

        /**
         * Attach multiple event listener function
         * @param {object} el
         * @param {string} events
         * @param {fn} fn
         */
        function addListenerMulti(el, events, fn) {
            var evts = events.split(' ');
            for (var i = 0, iLen = evts.length; i < iLen; i++) {
                el.addEventListener(evts[i], fn, {capture: true});
            }
        }

        /**
         * Define class contructor
         * @param  {object} obj passed from constructor
         * @return {object}
         */
        function SliderEngine(ind) {
            this.$imageSlider = document.querySelectorAll('.image-slider')[ind];
            this.$imageBefore = document.querySelectorAll('.image-slider-image-before')[ind];
            this.$imageAfter  = document.querySelectorAll('.image-slider-image-after')[ind];
            this.$dragControl = document.querySelectorAll('.drag-control')[ind];
            this.mobRegEx			= /Android|Windows Phone|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i;
            this.iPadCheck	  = /iPad/i;
            this.isMobile     = this.mobRegEx.test(navigator.userAgent);
            this.isIpad       = this.iPadCheck.test(navigator.userAgent);
            this.keys         = {37: 1, 38: 1, 39: 1, 40: 1};
            this.width        = 0;

            this.initialize();
        }

        SliderEngine.prototype = ({

            initialize() {

                if(this.isRetinaDisplay()) {
                  var before = (this.$imageSlider.getAttribute('data-before')).replace('.jpg', '@2x.jpg');
                  var after = (this.$imageSlider.getAttribute('data-after')).replace('.jpg', '@2x.jpg');
                }
                else {
                  var before = this.$imageSlider.getAttribute('data-before');
                  var after = this.$imageSlider.getAttribute('data-after');
                }

                $(this.$imageBefore).css('background', 'url(' + before + ')');
                $(this.$imageBefore).css('background-repeat', 'no-repeat');
                $(this.$imageBefore).css('background-size', '100%');

                $(this.$imageAfter).css('background', 'url(' + after + ')');
                $(this.$imageAfter).css('background-repeat', 'no-repeat');
                $(this.$imageAfter).css('background-size', '100%');

                // $(this.$dragControl).on('touchstart touchmove', (e) => this.dragStart(e));
                addListenerMulti(this.$dragControl, 'mousedown touchstart touchmove', (e) => this.dragStart(e));
                // $(this.$dragControl).on('touchstop touchend touchcancel', (e) => this.dragStop(e));
                addListenerMulti(this.$dragControl, 'mouseup touchstop touchend touchcancel', (e) => this.dragStop(e));

                addListenerMulti(window, 'scroll DOMMouseScroll mousewheel resize', (e) => this.setPosition(e));

                document.addEventListener("touchstart", function(){}, true);

                this.setPosition({
                    type: 'resize'
                });
            },

            isRetinaDisplay() {
                if (window.matchMedia) {
                    var mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
                    return (mq && mq.matches || (window.devicePixelRatio > 1));
                }
            },

            setPosition(e) {

                if (typeof e !== 'undefined' && e.type === 'resize') {
                    this.windowWidth = $(window).outerWidth();
                    $(this.$imageSlider).css('height', ($(window).outerWidth() * 0.49));
                    $(this.$dragControl).css('left', (this.windowWidth / 2));
                    $(this.$dragControl).css('top', ($(this.$imageBefore).outerHeight() / 2));

                    $(this.$imageBefore).css('right', (this.windowWidth / 2));
                    $(this.$imageAfter).css('left', (this.windowWidth / 2));

                    if(this.isMobile === true || this.isIpad === true) {
                      $(this.$imageBefore).css('backgroundSize', this.windowWidth);
                      $(this.$imageAfter).css('backgroundSize', this.windowWidth);

                      // $(this.$imageBefore).css('right', -this.width.x);
                      // $(this.$imageAfter).css('left', this.width.x);
                      $(this.$imageAfter).css('backgroundPositionX', -(this.windowWidth/2));
                      // console.log($(this.$imageAfter).css('backgroundPositionX'));
                    }
                    else {
                      $(this.$imageBefore).css('backgroundSize', this.windowWidth);
                      $(this.$imageAfter).css('backgroundSize', this.windowWidth);
                      $(this.$imageAfter).css('backgroundPositionX', -(this.windowWidth/2));
                    }
                }

                // if(this.isMobile === false && this.isIpad === false) {
                  // $(this.$imageBefore).css('backgroundPositionY', this.$imageBefore.getBoundingClientRect().top);
                  // $(this.$imageAfter).css('backgroundPositionY', this.$imageAfter.getBoundingClientRect().top);
                // }
            },

            dragStart(e) {

                this.disableScroll();

                /**
                 * BUG
                 * Rather than making this a document wide assignment clearence, choose a more local context
                 * at the moment this clears all event handlers and so interferes with carousel scrolling actions
                 */
                $('.image-slider').off();
                $('.image-slider').on('mousemove touchmove', (e) => {

                    this.width = this.getMousePos(e);

                    // if(this.isMobile === true || this.isIpad === true)
                    $(this.$imageAfter).css('backgroundPositionX', -this.width.x);

                    $(this.$imageBefore).css('right', -this.width.x);
                    $(this.$imageAfter).css('left', this.width.x);
                    $(this.$dragControl).css('left', this.width.x);
                });

                $('.image-slider').on('mouseup touchstop touchend touchcancel', (e) => this.dragStop(e));
            },

            dragStop(e) {
                this.enableScroll();
                $('.image-slider').off('mousemove touchmove');
            },

            getMousePos(e) {
                return {
                    x: (typeof e.originalEvent.touches !== 'undefined' ? e.originalEvent.touches[0].pageX : e.pageX )
                };
            },

            preventDefault(e) {
              e = e || window.event;
              if (e.preventDefault)
                  e.preventDefault();
              e.returnValue = false;
            },

            preventDefaultForScrollKeys(e) {
                if (this.keys[e.keyCode]) {
                    preventDefault(e);
                    return false;
                }
            },

            disableScroll() {
              if (window.addEventListener) // older FF
                  window.addEventListener('DOMMouseScroll', this.preventDefault, false);
              window.onwheel = this.preventDefault; // modern standard
              window.onmousewheel = document.onmousewheel = this.preventDefault; // older browsers, IE
              window.ontouchmove  = this.preventDefault; // mobile
              document.onkeydown  = this.preventDefaultForScrollKeys;
            },

            enableScroll() {
                if (window.removeEventListener)
                    window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
                window.onmousewheel = document.onmousewheel = null;
                window.onwheel = null;
                window.ontouchmove = null;
                document.onkeydown = null;
            }
        });

        return SliderEngine;
    }
);
