/**
 * home.js
 */
define(
	[	
		 'jquery'
		,'fancybox'
		,'fancybox-media'
		,'videoInjector'
	],  
	
	function($, FancyBox, FancyBoxMedia, VideoInjector) {
			
		/**
		 * Define foundation model view
		 */
		var PageView = function() {}
			
		PageView.prototype = {
			
			initialize: function() {
				trace("Social Loaded");
				
				$(".fancybox").bind("click", function(e) {
					e.preventDefault();
					$.fancybox( {'href' : $(this).attr("href")},{
						helpers : {
							media : {},
							overlay: {
								locked: false
							}
						}
					});
					return false;
				})
			}
		};
		
		return PageView;
	}
);