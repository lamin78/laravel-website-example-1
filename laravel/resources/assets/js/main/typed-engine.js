/**
 * typed-engine.js
 * Uses ecmascript 6
 */
define(

    ['jquery'],

    function($) {

        /**
         * Attach multiple event listener function
         * @param {object} el
         * @param {string} events
         * @param {fn} fn
         */
        function addListenerMulti(el, events, fn) {
            var evts = events.split(' ');
            for (var i = 0, iLen = evts.length; i < iLen; i++) {
                el.addEventListener(evts[i], fn, false);
            }
        }

        /**
         * Define class contructor
         * @param  {object} obj passed from constructor
         * @return {object}
         */
        function TypedEngine(obj) {

            this.$typeText = document.querySelector('.typed-text');

            this.wordArray = [
               String("Sales").split("")
              ,String("Investor").split("")
              ,String("Product").split("")
              ,String("Corporate").split("")
              ,String("Conference").split("")
              ,String("Interactive").split("")
              ,String("Animated").split("")
              ,String("Tablet").split("")
            ];

            this.wordCount = 0;
            this.letterCount = 0;

            this.initialize();
        }

        TypedEngine.prototype = ({

            initialize() {
              this.showText();
            },

            showText() {

              if (this.letterCount <= this.wordArray[this.wordCount].length) {

                let pauseLen = ( this.letterCount === this.wordArray[this.wordCount].length ? 2000 : (Math.floor(Math.random() * 300) + 50) );
                let letter = this.wordArray[this.wordCount][this.letterCount];

                $(this.$typeText).append(letter);

                setTimeout(() => {
                    this.letterCount++;
                    this.showText();
                }, pauseLen);
              }
              else {

                if(this.wordCount < (this.wordArray.length -1)) {
                  this.wordCount++;
                  this.letterCount = 0;
                }
                else {
                  this.wordCount = 0;
                  this.letterCount = 0;
                }

                this.deleteText()
              }
            },

            deleteText() {

              setTimeout(() => {

                $(this.$typeText).text($(this.$typeText).text().slice(0, -1));

                if($(this.$typeText).text() === '')
                  this.showText();
                else
                  this.deleteText();
              }, 50);
            }
        });

        return TypedEngine;
    }
);
