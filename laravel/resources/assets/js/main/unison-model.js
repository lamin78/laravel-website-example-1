/**
 * unison-model.js
 */
define(
	[	
		 'jquery'
		,'unison'
	],  
	
	function($, Unison) {
			
		/**
		 * Define foundation model view
		 */
		var UnisonModel = function() {
			
			this._bp		= null;
			this.isMobile	= false;
			this.isIE		= false;
			this.ltIe10		= false;
		
			this.constructor();
		}

		UnisonModel.prototype = {
			
			constructor: function() {
				 
				// Trigger the unison to load properly with require.js
				var DOMContentLoaded_event = document.createEvent("Event");
				DOMContentLoaded_event.initEvent("DOMContentLoaded", true, true);
				window.document.dispatchEvent(DOMContentLoaded_event);
				
				// Add event dispatcher for unisons change event  
				var UnisonChange = document.createEvent("Event");
				UnisonChange.initEvent("unichange", true, true);

				this._bp = Unison.fetch.now();
				var parent = this;

				Unison.on('change', function(bp) {
					
					parent._bp = bp;
					// broadcast event
					window.document.dispatchEvent(UnisonChange);
					parent.fixMenus();
				});

				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					this.isMobile = true;
				}
				
				if((/rv:11.0|MSIE 10|MSIE 9/i.test(navigator.userAgent))) {
					this.isIE = true;
				}
				
				if((/MSIE 9/i.test(navigator.userAgent))) {
					this.ltIe10 = true;
				}
			}

			,fixMenus: function() {
				if (parseInt(this._bp.width) > 768) {
					$(".nav-outer").removeClass("open");
					$(".we-are-block").removeClass("hidden");
				}
			}

			,getWidth: function() {
				return parseInt(this._bp.width);
			}
		};
		
		return UnisonModel;
	}
);