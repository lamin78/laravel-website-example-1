/**
 * Filename: video-header-control.js
 */
define([],

	function() {

		/**
		 * Initialise video control engine values
		 * @param {object} obj
		 */
		var VC_Engine = function(pause_on_back) {

			this.mobRegEx = /Android|Windows Phone|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;

			if(this.mobRegEx.test(navigator.userAgent))
				return;

			this.preloadArray = ['unmute_white.svg', 'unmute_white.png'];
			this.imageDir	  = '/images/shared/';

			this.overlay =  document.getElementsByClassName('black-out-layer')[0];
			this.headerVideoControls =  document.getElementsByClassName('header-video-controls')[0];
			this.backBtn =  document.getElementsByClassName('back-header')[0];
			this.playBtn =  document.getElementsByClassName('play-header')[0];
			this.muteBtn =  document.getElementsByClassName('mute-header')[0];

			this.textBox	=  (document.querySelector('.tbl > .inner') || 'undefind');
			this.startText	= "WE ARE<br><span class=\"hilite animated growIn go\">VIDEO, ANIMATION &amp; 3D</span><br>OBSESSED";
			this.endText	= "WE<br><span class=\"hilite animated growIn go\">CAN\'T WAIT</span><br>TO WORK WITH YOU";

			this.navBlock   =  document.getElementsByClassName('nav')[0];
			this.titleBlock =  (document.getElementsByClassName('we-are-block')[0] || document.getElementsByClassName('project-head')[0]);
			this.arrowBlock =  document.getElementsByClassName('down-arrow')[0];

			this.videoBlock = document.getElementsByClassName('header-video-file')[0];

			if(typeof pause_on_back !== 'undefined')
				this.pauseOnBack  = pause_on_back;

			this.revealDisplayFlag  = false;
			this.textTriggered		= false;
			this.videoEnded			= false;

			this.assignEvent = null;

			this.fadeTime = 3; // seconds
			this.videoVolume = 0.5; // full 1.0
			this.mouseEventTimeout = 3; // full 1.0

			this.pageUrl = window.location.pathname;

			this.initialise();
		}

		/**
		 * Add methods
		 */
		VC_Engine.prototype = {
			/**
			 * Initialise controls
			 */
			initialise: function() {

				var parent = this;

				this.backBtn.onclick = this.exitVideo.bind(this, {});
				this.playBtn.onclick = this.playVideo.bind(this, {});
				this.muteBtn.onclick = this.muteVideo.bind(this, {});

				this.playBtn.className = (this.playBtn.className).replace('hide', '').trim();

				/**
				 * Preload images
				 */
				$(this.preloadArray).preload(this.imageDir, function() {});

				window.addEventListener('menuOpen', function() {
					parent.revealDisplayFlag = true;
					clearTimeout(parent.clearLayout);
				}, false);

				window.addEventListener('menuClose', function() {
					parent.revealDisplayFlag = false;
				}, false);

				if(window.location.hash === '#play') {
					if(typeof ga !== 'undefined')
						ga('send', 'pageview', location.pathname + location.search + '#play');
					this.playVideo.bind(this, {}).call();
				};
			},
			/**
			 * Rewind and play video
			 */
			playVideo: function() {

				if((this.playBtn.className).indexOf('hide') == -1) {

					var parent = this;

					this.playBtn.className += ' hide';

					this.hideDisplay();
					this.revealControls()

					this.videoBlock.currentTime = 0;
					this.videoBlock.volume		= this.videoVolume;
					this.videoBlock.loop		= false;
					this.videoBlock.play();

					setTimeout(function(){
						parent.videoBlock.muted= false;
					}, 50)

					this.videoBlock.onended = function() {
						parent.videoEnded = true;
						parent.exitVideo();
					}

					/**
					 * Add event listener for menu open event
					 */
					clearTimeout(this.assignEvent);

					this.assignEvent = setTimeout(function() {
						window.onmousemove = parent.mouseMove.bind(parent, {});
					}, parent.mouseEventTimeout * 1000);

					/**
					 * Listen for current time
					 */
					if(this.exhibitionPageTest()) {

						this.videoBlock.addEventListener('timeupdate', function() {

							if(this.currentTime >= 81 && parent.textTriggered === false) {
								parent.changeText();
								parent.revealDisplay();
								parent.textTriggered = true;
							}

						}, false);
					}
				}
			},

			/**
			 * Mute video
			 */
			muteVideo: function() {

				if((this.muteBtn.className).indexOf('unmute') == -1) {
					this.muteBtn.className += ' unmute';
					this.videoBlock.muted = true;
				}
				else {
					this.muteBtn.className = (this.muteBtn.className).replace(' unmute', '').trim();
					this.videoBlock.muted = false;
				}
			},

			/**
			 * Reset to start state
			 */
			exitVideo: function(event) {

				var parent = this;

				clearTimeout(this.assignEvent);

				window.onmousemove			= null;
				this.videoBlock.timeupdate  = null;

				if((this.backBtn.className).indexOf('hide') == -1) {

					clearTimeout(this.clearLayout);

					if(this.videoEnded === false)
						this.changeText();

					if(this.revealDisplayFlag === false) {

						this.hideControls();

						this.playBtn.className = (this.playBtn.className).replace('hide', '').trim();

						if((this.muteBtn.className).indexOf('unmute') != -1)
							this.muteBtn.className = (this.muteBtn.className).replace(' unmute', '').trim();

						if(this.pauseOnBack === true)
							this.videoBlock.currentTime = 0;
						this.videoBlock.pause();

						this.revealDisplay();
					}
					else {
						this.hideControls();
						this.playBtn.className = (this.playBtn.className).replace('hide', '').trim();
						if((this.muteBtn.className).indexOf('unmute') != -1)
							this.muteBtn.className = (this.muteBtn.className).replace(' unmute', '').trim();
					}

					this.videoBlock.muted = true;
					this.videoBlock.loop  = true;

					if(this.pauseOnBack !== true){
						this.videoBlock.play();
					}
					else {
						this.videoBlock.load();
						this.videoBlock.pause();
					}
				}
			},

			/**
			 * Hide header display
			 */
			hideDisplay: function() {

				this.overlay.className	  += this.hideEle(this.overlay);
				this.navBlock.className   += this.hideEle(this.navBlock);
				this.titleBlock.className += this.hideEle(this.titleBlock);
				this.arrowBlock.className += this.hideEle(this.arrowBlock);
				this.revealDisplayFlag = false;
			},

			hideControls: function() {

				this.backBtn.className += this.hideEle(this.backBtn);
				this.muteBtn.className += this.hideEle(this.muteBtn);
			},

			/**
			 * Reveal header display
			 */
			revealDisplay: function() {

				this.overlay.className    = 'black-out-layer push-back see-through';
				this.navBlock.className   = (this.navBlock.className).replace('hide', '').trim();
				this.titleBlock.className = (this.titleBlock.className).replace('hide', '').trim();
				this.arrowBlock.className = (this.arrowBlock.className).replace('hide', '').trim();
				this.revealDisplayFlag	  = true;
			},

			/**
			 * Partial reveal header display for mouse movement whilst playing
			 */
			partialRevealDisplay: function() {

				this.overlay.className    = 'black-out-layer menu-grey-1';
				this.navBlock.className   = (this.navBlock.className).replace('hide', '').trim();
				this.arrowBlock.className = (this.arrowBlock.className).replace('hide', '').trim();
			},

			revealControls: function() {

				this.backBtn.className = (this.backBtn.className).replace(' hide', '').trim();
				this.muteBtn.className = (this.muteBtn.className).replace(' hide', '').trim();
			},

			mouseMove: function() {

				var parent = this;

				if((this.backBtn.className).indexOf('hide') == -1 && this.revealDisplayFlag === false) {
					this.partialRevealDisplay();
					clearTimeout(this.clearLayout);
					this.clearLayout = setTimeout(function() { parent.hideDisplay(); }, this.fadeTime * 1000);
				}
			},

			changeText: function() {
				if(this.exhibitionPageTest() && this.textBox !== 'undefined') {
					this.textBox.innerHTML = this.endText;
				}
			},

			exhibitionPageTest: function() {
				if(Boolean(this.pageUrl !== "/services/video-animation-3d") === true)
					return true;
				else
					return false;
			},

			hideEle: function(ele) {

				return ((ele.className).indexOf('hide') == -1) ? ' hide' : '';
			}

		}

		return VC_Engine;
	}
);
