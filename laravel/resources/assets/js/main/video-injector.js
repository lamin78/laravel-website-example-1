/**
 * Filename: video-injector.js
 */
define([
	 'jquery'
	,'underscore'
	,'videoHeaderControl'
	,'preload'
	,'jquerythrottle'
],

	function($, _, VideoController) {

		/**
		 * Ensure frame draws begin at the start of each frame
		 * @param {fn} callback
		 */
		var reqAnimationFrame = (function() {
      return window["requestAnimationFrame"] || function(callback) {
          setTimeout(callback, 1000 / 60);
      }
    })();

		/**
		 * Initialise client engine values
		 * @param {object} obj
		 */
		var V_Engine = function(obj) {

			if(document.querySelector(obj.video) === null)
				return;

			this.videoContainer		= document.querySelector(obj.video);
			this.posterContainer	= document.querySelector(obj.poster);
			this.videoFile			  = document.querySelector(obj.video).getAttribute('data-file');
			this.posterFile			  = document.querySelector(obj.video).getAttribute('data-poster');
			this.mobilePosterFile	= document.querySelector(obj.video).getAttribute('data-mobile-poster');
			this.posterPosition	  = document.querySelector(obj.video).getAttribute('data-position');
			this.autoPlay					= document.querySelector(obj.video).getAttribute('data-autoplay')
			this.overlay			    = document.querySelector(obj.overlay);
			this.blackOutLayer    = document.querySelector('.black-out-layer');
			this.downArrow			  = document.querySelector('.down-arrow');
			this.breakPointMedium	= 767;
			this.video				= [];
			this.mobRegEx			= /Android|Windows Phone|webOS|iPhone|iPod|ipad|BlackBerry|IEMobile|Opera Mini/i;
			this.iPadSel			= /iPad/i;
			this.iPhone				= /iPhone/i;

			var parent = this;
			var myEvent = window.attachEvent || window.addEventListener;
			var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

			myEvent(chkevent, function(e) {
				if((parent.video).toString() !== '')
					parent.video.pause();
				parent.hideVideo();
    	});

			this.updateCondition.bind(this).call();

			var throttle, parent = this;
			window.addEventListener('resize', function(e) {

				if(!parent.mobRegEx.test(navigator.userAgent))
					return;

				clearTimeout(throttle);
				var throttle = setTimeout(function() {
					if(!parent.iPhone.test(navigator.userAgent))
						parent.renderPoster();
				}, 150);
			});
		}

		/**
		 * Add methods
		 */
		V_Engine.prototype = {

			/**
			 * Preload sprite sheet
			 * @param {string} imgDir
			 * @param {array} imgArray
			 */
			preload: function(type) {

				var parent = this;

				if(!this.mobRegEx.test(navigator.userAgent))
					this.hideVideo();

				$([this.posterFile, this.mobilePosterFile])
					.preload(false, function() {

						parent.clearFrames();

						if(type === 'video') {
							reqAnimationFrame(parent.renderVideo.bind(parent));
						}
						else {
							if(!parent.iPhone.test(navigator.userAgent))
								reqAnimationFrame(parent.renderPoster.bind(parent));
							else {
								$(parent.blackOutLayer).css('display', 'none');
								if($('.ios-insert').length > 0) {
									$('.ios-insert')
										.css('backgroundImage', 'url('+parent.mobilePosterFile+')')
										.css('backgroundPosition', parent.posterPosition);
								}
							}
						}
					});
			},

			/**
			 * Render video to DOM
			 */
			renderVideo: function() {

				var $ele = $(this.videoContainer);
				$ele.append($('<video />', {
						 'autoplay': this.autoPlay
						,'loop': this.autoPlay
						,'muted': true
						,'poster': this.posterFile
						,'class': 'header-video-file'
					})
						.append($('<source />', {
							 'src': this.videoFile
							,'type': 'video/mp4'
						})));

				this.video = document.getElementsByTagName("video")[0];

				if(this.autoPlay !== 'false')
					this.video.play();
				else
					this.video.pause();

				this.video.muted  = true;

				/**
				 * Inject video controller if needed
				 */
				if($('.header-video-controls').length > 0) {
					this.videoController = new VideoController(true); // render video with pause on click back operation
				}

				this.revealVideo();
			},

			/**
			 * Render poster to DOM
			 */
			renderPoster: function() {
				$(this.blackOutLayer).css('display', 'none');
				var $ele = $(this.posterContainer);
				// $ele.empty();

				$ele.css({
					'background': `url(${this.mobilePosterFile})`,
					'background-size': 'cover'
				});
				// $ele.append($('<img />', {
				// 	 'class': 'image-replacement'
				// 	,'src': this.mobilePosterFile
				// 	,'style': 'height: ' + this.getScreenHeight() + 'px;'
				// }));

				if(this.iPadSel.test(navigator.userAgent)) {

					$('#headerWrapper').css({'height': this.getScreenHeight() + 'px'});

					if($(window).outerWidth() > 768) {
						// $('.image-replacement').css({
						// 	 'width': '100%'
						// 	,'left': '0%'
						// });
					}
					else if($(window).outerWidth() <= 768) {
						// $('.image-replacement').css({
						// 	 'width': '100%'
						// 	,'left': '-50%'
						// });
					}
				}

				this.revealVideo();
			},

			updateCondition: function() {

				//may need to tailor this regex to exclude ipad
				if(this.mobRegEx.test(navigator.userAgent)) {
					this.clearFrames();
					reqAnimationFrame(this.checkViewport.bind(this, 'poster'));
				}
				else {
					this.clearFrames();
					reqAnimationFrame(this.checkViewport.bind(this, 'video'));
				}
			},

			checkViewport: function(state) {

				if (!window.UnisonModel.isMobile)
					this.preload(state);
				else
					this.preload(state);
			},

			clearFrames: function() {

				var video = document.getElementsByTagName("video")[0];

				if(typeof video !== 'undefined')
					video.src="";

				if(!this.mobRegEx.test(navigator.userAgent))
					this.hideVideo();

				//$(this.videoContainer).empty();
				//$(this.posterContainer).empty();
			},

			hideVideo: function() {

				$(this.overlay).removeClass('see-through');
				$(this.overlay).addClass('opaque');
			},

			revealVideo: function() {
				$(this.overlay).removeClass('opaque');
				$(this.overlay).addClass('see-through');
				$(".background-scroll")
					.css({'z-index': '0', 'background': 'transparent'});
			},

			getScreenHeight: function() {

				return $(window).innerHeight();
			}
		}

		return V_Engine;
	}
);
