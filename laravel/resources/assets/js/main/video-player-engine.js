/**
 * Filename: video-player-engine.js
 */
define([
	 'jquery'
	,'jplayer'
	,'jquerythrottle'
	,'preload'
],

	function($) {

		/**
		 * Ensure frame draws begin at the start of each frame
		 * @param {fn} callback
		 */
		var reqAnimationFrame = (function() {
            return window["requestAnimationFrame"] || function(callback) {
                setTimeout(callback, 1000 / 60);
            }
        })();

		/**
		 * Initialise video player engine values
		 * @param {object} obj
		 */
		var VP_Engine = function(obj) {

			if(obj.videoWrap.data('video') === null)
				return;

			// Add event dispatcher for mute event
			this.VideoMute = document.createEvent("Event");
			this.VideoMute.initEvent("videoMute", true, true);

			var parent 		  = this;
			this.container	  = obj.videoWrap;
			this.videoWrap	  = obj.videoWrap.data('wrap-id');
			this.videoFile	  = obj.videoWrap.data('video');
			this.videoPoster  = obj.videoWrap.data('poster');
			this.playBtn	  = obj.videoWrap.data('play');
			this.autoPlay	  = obj.videoWrap.data('autoplay');
			this.muteBtn	  = obj.videoWrap.data('mute-btn');
			this.mobRegEx	  = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;
			this.isMobile   = this.mobRegEx.test(navigator.userAgent);
			this.iPadCheck	= /iPad/i;
			this.imageDir	  = '/images/shared/';
			this.preloadArray = ['muteon27.svg', 'muteoff27.svg', 'muteon27.png', 'muteoff27.png',];
			this.playing 	  = false;
			this.mute 	 	  = false;
			//this,videoFile	 = $('#'+parent.videoWrap).parent().data('video')

			var myEvent = window.attachEvent || window.addEventListener;
			var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

			myEvent(chkevent, function(e) {});

			this.init.bind(this).call();
		}

		/**
		 * Add methods to instance
		 */
		VP_Engine.prototype = {

			init: function() {

				var parent = this

				/**
				 * Carousel scroll listener
				 */
				window.addEventListener('carouselScroll', function() {
					parent.playing = false;
				}, false);

				/**
				 * Video mute listener
				 */
				window.addEventListener('videoMute', function(e) {
					parent.mute = e.state;
				}, false);

				/**
				 * Preload images
				 */
				$(parent.preloadArray).preload(parent.imageDir, function() {
					//console.log('preload done');
				});

				/**
				 * Initialise jplayer
				 */
				$('#'+parent.videoWrap).jPlayer({

					ready: function () {

						$(this)
							.jPlayer("setMedia", {
								title: "Article 10 Video",
								m4v: parent.videoFile,
								poster: parent.videoPoster
							});

						$('.'+parent.playBtn)
							.on('touchstart click', function(event) {

								event.stopPropagation();
								event.preventDefault();

								parent.loadingAction($('.video-content'));

								parent.touchAction(function() {

									if(parent.playing === false)
										$('#'+parent.videoWrap).find('video').prop('src', parent.videoFile);

									setTimeout(function() {
										$('#'+parent.videoWrap).jPlayer('play');
										$('.'+parent.playBtn).css({'display': 'none'});
									}, 1*1);
								});
							});

						var thisClassName = $(this).prop('className');
						var resetElements = '.banner-nav, .case-banner-nav-left, .case-banner-nav-right';
						var pauseElements = thisClassName + ', ' + resetElements;

						$(pauseElements)
							.on('touchstart click', function() {

								parent.touchAction(function() {

									$('#'+parent.videoWrap)
										.jPlayer('pause');
								});
							});

						$(resetElements)
							.on('touchstart click', function() {

								parent.touchAction(function() {

									parent.playing = false;
								});
							});

						$('img')
							.on('dragstart', function(event) {
								event.preventDefault();
							});

						$('.'+parent.playBtn)
							.css({'display': 'block'});

						if(parent.muteBtn === true && !parent.iPadCheck.test(navigator.userAgent))
							parent.addMuteBtn.bind(this, {'parent': parent}).call();

						window.addEventListener('scroll', function(e) {

							if(parent.isMobile)
								return;

							if(parent.checkBounds(50, 100) && parent.autoPlay)
								$('#'+parent.videoWrap).jPlayer('play');
						});
					},
					play: function() {

						parent.playing = true;

						var headerVideo = document.querySelector('.header-video-file'),
							obj = this,
							thisId = parseInt($(obj).prop('id').replace('video_', '') -1);

						if($('button[class^="play-content-"]').length > 1) {
							$('button[class^="play-content-"]').each(function(ind, item) {
								if(ind !== thisId)
									$(this).css({'display': 'block'});
								else
									$(this).css({'display': 'none'});
							});
						}

						parent.loadingAction($(this));

						if(Boolean(headerVideo !== null) === true)
							headerVideo.pause();

						$(this).jPlayer("pauseOthers");

						$('.carousel-case').find('video').each(function(ind, item) {

							if(ind !== thisId)
								$(this).prop('src', '');
						});

						parent.addPauseArea(parent);
					},
					playing : function() {
						$('.loader').remove();
					},
					pause : function() {
						$('.'+parent.playBtn)
							.css({'display': 'block'});
					},
					ended : function() {
						parent.playing = false;
						var obj = this;
						$('.'+parent.playBtn)
							.css({'display': 'block'});
					}
					,swfPath: "../../dist/jplayer"
					,supplied: "webmv, ogv, m4v"
					,globalVolume: true
					,useStateClassSkin: false
					,autoBlur: false
					,smoothPlayBar: true
					,keyEnabled: true
					,autoPlay: false
					//,cssSelectorAncestor: '#jp_container_1'
					,solution: 'html, flash'
					,preload: 'none'
					,size : {
						 width: '100%'
						,height: 'auto'
						,cssClass: 'jp-video-720p'
					}
					,remainingDuration: false
					,toggleDuration: false
					,errorAlerts: false
					,warningAlerts: false
					,oggSupport: false
					,nativeSupport: false
					,volume: 1
				});
			},

			/**
			 * Check to see if scroll falls between supplied parameters
			 * @param {int} top
			 * @param {int} bottom
			 */
			checkBounds: function(top, bottom) {
				return (($(window).scrollTop()-$(this.container).offset().top) > top && ($(window).scrollTop()-$(this.container).offset().top) < bottom);
			},

			loadingAction: function($obj) {

				$obj.parent().find('.loader').remove();

				setTimeout(function() {
					$obj.parent().prepend($('<i />', {
						'class' : 'loader large ' + (window.UnisonModel.ltIe10 ? 'lt-ie10' : '' )
					}));
				}, 500);
			},

			addPauseArea: function(parent) {

				$('.pause-btn').remove();

				$('#'+parent.videoWrap)
					.parent()
					.parent()
					.append($('<div />', {
						 'class' : 'pause-btn'
					})
						.on('click', function() {
							$('#'+parent.videoWrap)
								.jPlayer('pause');
							$('.'+parent.playBtn)
								.css({'display': 'block'});
						}));
			},

			addMuteBtn: function(obj) {

				var _this = this;
				var pushRight = ($('#'+obj.parent.videoWrap).parent().parent().prop('className').indexOf('parallax') !== -1 ? ' push-right' : '' );
				var pushUp = ($('#'+obj.parent.videoWrap).parent().parent().parent().prop('className').indexOf('not-mac') !== -1 ? ' push-up' : '' );

				$('#'+obj.parent.videoWrap)
					.parent()
					.parent()
					.append($('<div />', {
						 'class' : 'mute-btn' + pushRight + pushUp
					})
						.on('click', function() {
							if(obj.parent.mute === false) {
								$(_this).jPlayer("mute");
								$('.mute-btn').addClass('unmute');
								obj.parent.mute = true;
								obj.parent.VideoMute.state = obj.parent.mute;
								window.dispatchEvent(obj.parent.VideoMute);
							}
							else {
								$(_this).jPlayer("unmute");
								$('.mute-btn').removeClass('unmute');
								obj.parent.mute = false;
								obj.parent.VideoMute.state = obj.parent.mute;
								window.dispatchEvent(obj.parent.VideoMute);
							}
						}));
			},

			touchAction : function(fn) {

				var flag = false;

				if (!flag) {

					flag = true;

					setTimeout(function() { flag = false; }, 100);

					fn();
				}
			}
		}

		return VP_Engine;
	}
);
