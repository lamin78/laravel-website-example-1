@extends('admin.layouts.main')

@section('content')
<div class="row pt2">
	<div class="small-12 columns">
		
		<div class="row">
			<div class="panel medium-6 medium-offset-3 small-10 small-offset-1 columns">
				<h2>Register</h2>

				@if (count($errors) > 0)
					<div data-alert class="alert-box">
						<a href="#" class="close top">&times;</a>
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						@foreach ($errors->all() as $error)
							{{ $error }}<br/>
						@endforeach
					</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/auth/register') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<label>Name</label>
					<input type="text" name="name" value="{{ old('name') }}">
					
					<label>E-Mail Address</label>
					<input type="email" name="email" value="{{ old('email') }}">
					
					<label>Password</label>
					<input type="password" name="password">
					
					<label>Confirm Password</label>
					<input type="password" name="password_confirmation">
					

					<button type="submit" class="button success small">Register</button>

				</form>

			</div>
		</div>
	</div>
</div>

@endsection
