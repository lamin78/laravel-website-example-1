@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	@include('admin.layouts.partials.response_modal')

  <div class="row">
		<div id="casestudy_cms" class="small-12 columns">

			<!-- BREADCRUMBS -->
			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study List') !!}</li>
			  <li class="current">Edit {{ $cms_case_study->casestudy_title }} Layout</li>
				<transition
					@before-enter="beforeEnter"
					@enter="enter"
					@before-leave="beforeLeave"
					@leave="leave"
					v-bind:css="false">
					<li class="layout-status" v-cloak v-if="layoutSaved"><span class="success label">layout saved</span></li>
				</transition>
			</ul>

      <div class="row">
        <div class="columns small-12">
          <div>
            <div class="row collapse">

							<!-- TOP BUTTON GROUP -->
							<div class="small-12 columns">
								{!! link_to_route('admin.case-study.index', '', [$cms_case_study->casestudy_hashed_url], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
								{!! link_to_route('case-study-preview.{hash}', 'Preview', [$cms_case_study->casestudy_hashed_url], ['class' => 'small button ff', 'target'=>'_blank']) !!}
								<button data-dropdown="top_drop_001" aria-controls="top_drop_001" aria-expanded="false" class="button small button success ff dropdown">Add Section</button>
								<ul id="top_drop_001" data-dropdown-content class="f-dropdown" aria-hidden="true">
									<li><a href="#" data-type="text" @click.prevent="addSection">Text Section</a></li>
									<li><a href="#" data-type="parallax" @click.prevent="addSection">Parallax Section</a></li>
									<li><a href="#" data-type="carousel" @click.prevent="addSection">Carousel Section</a></li>
									<li><a href="#" data-type="slider" @click.prevent="addSection">Slider Section</a></li>
									<li><a href="#" data-type="video" @click.prevent="addSection">Video Section</a></li>
								</ul>
							</div>

							<!-- HEADER SECTION -->
              <div class="columns small-12 line-height-4-rem static-section">
                <div class="row section-block">
                  <div class="columns small-12 medium-8">Header</div>
                  <div class="columns small-12 medium-4 text-align-right">
                    <button href="#" data-dropdown="drop_002" aria-controls="drop_002" aria-expanded="false" class="button tiny dropdown">Options</button>
                    <ul id="drop_002" data-dropdown-content class="f-dropdown text-align-left" aria-hidden="true">
                    	<li>{!! link_to_route('case-study-edit-header.{id}', 'Edit', [$cms_case_study->id]) !!}</li>
	                  </ul>
                	</div>
              	</div>
            	</div>

							<!-- BODY SECTION -->
							<draggable
								v-model="order"
								:element="'div'"
								@update="updateOrder">
								<transition-group
									name="list-complete"
									tag="ul"
									mode="out-in"
									class="row collapse ul-case-study">
			            <component-section
										v-for="(section, section_index) in order"
										:key="section.id"
										class="list-complete-item"
										:case-study-id="section.cid"
										:section-id="section.sid"
										:order-id="section.id"
										:order-ord="section.order"
										:order-ident="section.data_ident"
										:order-type="section.data_type"
										:order-name="section.data_name"
										:data-index="section_index"
										:data-drop-index="section_index"
										draggable="false"
										:data-section="section"></component-section>
								</transition-group>
							</draggable>

							<!-- STATISTICS SECTION -->
							<div class="columns small-12 line-height-4-rem static-section">
                <div class="row section-block">
                  <div class="columns small-12 medium-8">Statistics</div>
                  <div class="columns small-12 medium-4 text-align-right">
                    <button href="#" data-dropdown="drop_09999999999" aria-controls="drop_09999999999" aria-expanded="false" class="button tiny dropdown">Options</button>
                    <ul id="drop_09999999999" data-dropdown-content class="f-dropdown text-align-left" aria-hidden="true">
                    	<li>{!! link_to_route('case-study-edit-statistics.{id}', 'Edit', [$cms_case_study->id]) !!}</li>
	                  </ul>
                	</div>
              	</div>
            	</div>

							<!-- BOTTOM BUTTON GROUP -->
							<div class="small-12 columns mt1">
								{!! link_to_route('admin.case-study.index', '', [$cms_case_study->casestudy_hashed_url], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
								{!! link_to_route('case-study-preview.{hash}', 'Preview', [$cms_case_study->casestudy_hashed_url], ['class' => 'small button ff ', 'target'=>'_blank']) !!}
								<button data-dropdown="bottom_drop_001" aria-controls="bottom_drop_001" aria-expanded="false" class="button small button success ff dropdown">Add Section</button>
								<ul id="bottom_drop_001" data-dropdown-content class="f-dropdown" aria-hidden="true">
									<li><a href="#" data-type="text" @click.prevent="addSection">Text Section</a></li>
									<li><a href="#" data-type="parallax" @click.prevent="addSection">Parallax Section</a></li>
									<li><a href="#" data-type="carousel" @click.prevent="addSection">Carousel Section</a></li>
									<li><a href="#" data-type="slider" @click.prevent="addSection">Slider Section</a></li>
									<li><a href="#" data-type="video" @click.prevent="addSection">Video Section</a></li>
								</ul>
							</div>
          	</div>
        	</div>
      	</div>
    	</div>
  	</div>
  </div>

	@include('admin.casestudy.partials._vue_templates')
@stop
