@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study') !!}</li>
			  <li class="current">Create Case Study</li>
			</ul>

			{!! Form::open(array('route' => ['admin.case-study.store'], 'method' => 'POST') )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.casestudy.partials._create_form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						{!! Form::submit('Create', array('class' => 'button success small ff')) !!}
						{!! link_to_route('admin.case-study.index', 'Cancel', [], ['class' => 'small button alert ff']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
