@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">

			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study List') !!}</li>
			  <li>{!! link_to_route('admin.case-study.edit', 'Edit '.$cms_case_study->casestudy_title.' Layout',  [$cms_case_study->id]) !!}</li>
			  <li class="current">Edit {{ $cms_case_study->casestudy_title }} Carousel Block</li>
			</ul>

			<div id="carousel_wrap" class="row">
				<!-- TOP ADD BUTTON -->
				<div class="small-12 columns">
					{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
					<button class="button small button success ff" @click="addCarousel">Add Panel</button>
				</div>

				<div class="small-12 columns theme">
					<h4>Theme</h4>
					<input type="checkbox" id="light" class="checkbox-style" value="light" @click="chooseTheme($event, true)">
					<label for="light" data-id="light"></label>
					<span class="noselect">Light</span>
					<input type="checkbox" id="dark" class="checkbox-style" value="dark" @click="chooseTheme($event, true)">
					<label for="dark" data-id=""></label>
					<span class="noselect">Dark</span>
					<transition
						@before-enter="beforeEnter"
						@enter="enter"
						@before-leave="beforeLeave"
						@leave="leave"
						v-bind:css="false">
						<span v-cloak v-if="themeSaved" class="success label theme-state">theme saved</span>
					</transition>
				</div>

				<div class="small-12 columns">
				{!! Form::model($cms_Carousel, array('route' => ['case-study-update-carousel.{cid}.{sid}', $cms_case_study->id, $cms_Carousel->first()->id], 'method' => 'PUT', 'files' => true) )!!}
					<input type="hidden" name="carousel_csid" value="{{$cms_Carousel->first()->csid}}">
					<input type="hidden" name="carousel_count" :value="carouselNum">
					@include('admin.casestudy.partials._edit_carousel_form')
					<!-- BOTTOM ADD BUTTON -->
					<!-- <div class="small-12 columns"> -->
					{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
					<button class="button small button success ff" @click="addCarousel">Add Panel</button>
					<!-- </div> -->
					{!! Form::submit('Upload', array('class' => 'button success small')) !!}
					{!! link_to_route('admin.case-study.edit', 'Cancel', [$cms_case_study->id], ['class' => 'small button alert ff']) !!}
				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	@include('admin.casestudy.partials._vue_templates')
@stop
