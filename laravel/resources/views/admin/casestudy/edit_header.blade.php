@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div id="header_wrap" class="small-12 columns">

			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study List') !!}</li>
			  <li>{!! link_to_route('admin.case-study.edit', 'Edit '.$cms_case_study->casestudy_title.' Layout',  [$cms_case_study->id]) !!}</li>
			  <li class="current">Edit {{ $cms_case_study->casestudy_title }} Header</li>
			</ul>


			<!-- TOP ADD BUTTON -->
			{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
			<button data-dropdown="top_drop_001" aria-controls="top_drop_001" aria-expanded="false" class="button small button success ff dropdown">Header Type</button>
			<ul id="top_drop_001" data-dropdown-content class="f-dropdown" aria-hidden="true">
				<li><a href="#" data-type="text" @click="showImageImput">Image</a></li>
				<li><a href="#" data-type="parallax" @click="showVideoImput">Video</a></li>
			</ul>

			{!! Form::model($cms_case_study, array('route' => ['case-study-update-header.{id}', $cms_case_study->id], 'method' => 'PUT', 'files' => true) )!!}
				<input type="hidden" name="header_image_active" :value="showImage">
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.casestudy.partials._edit_header_form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
						{!! Form::submit('Update', array('class' => 'button success small')) !!}
						{!! link_to_route('admin.case-study.edit', 'Cancel', [$cms_case_study->id], ['class' => 'small button alert ff']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
