@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">

			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study List') !!}</li>
			  <li>{!! link_to_route('admin.case-study.edit', 'Edit '.$cms_case_study->casestudy_title.' Layout',  [$cms_case_study->id]) !!}</li>
			  <li class="current">Edit {{ $cms_case_study->casestudy_title }} Slider Block</li>
			</ul>

			<!-- TOP ADD BUTTON -->
			{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}

			{!! Form::model($cms_case_study, array('route' => ['case-study-update-slider.{cid}.{sid}', $cms_case_study->id, $cms_Slide->id], 'method' => 'PUT', 'files' => true) )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.casestudy.partials._edit_slider_form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
						{!! Form::submit('Upload', array('class' => 'button success small')) !!}
						{!! link_to_route('admin.case-study.edit', 'Cancel', [$cms_case_study->id], ['class' => 'small button alert ff']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
