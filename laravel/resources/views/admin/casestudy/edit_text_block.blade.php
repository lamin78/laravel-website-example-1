@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">

			<ul class="breadcrumbs">
			  <li>{!! link_to_route('admin.case-study.index', 'Case Study List') !!}</li>
			  <li>{!! link_to_route('admin.case-study.edit', 'Edit '.$cms_case_study->casestudy_title.' Layout',  [$cms_case_study->id]) !!}</li>
			  <li class="current">Edit {{ $cms_case_study->casestudy_title }} Text Block</li>
			</ul>

			<!-- TOP ADD BUTTON -->
			{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}

			{!! Form::model($cms_Text, array('route' => ['case-study-edit-textarea-update.{cid}.{sid}', $cms_case_study->id, $cms_Text->id], 'method' => 'PUT', 'files' => true) )!!}
				<div class="row">
					<div class="small-12 columns mt1">
						@include('admin.casestudy.partials._edit_text_form')
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						{!! link_to_route('admin.case-study.edit', '', [$cms_case_study->id], ['class' => 'small button warning ff icon3-angle-left thin']) !!}
						{!! Form::submit('Update', array('class' => 'button success small')) !!}
						{!! link_to_route('admin.case-study.edit', 'Cancel', [$cms_case_study->id], ['class' => 'small button alert ff']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
