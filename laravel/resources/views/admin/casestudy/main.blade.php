@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	@include('admin.layouts.partials.response_modal')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li class="current">Case Study List</li>
			</ul>
		</div>

		<div class="medium-12 columns">
			@if (isset($cms_Case_Study) && $cms_Case_Study->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Name</th>
				      <th scope="column">Category</th>
				      <th scope="column">Status</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($cms_Case_Study as $case_study)
						<tr>
				      <td class="table-middle">{{{ $case_study->casestudy_title }}}</td>
				      <td class="table-middle">{{{ $case_study->casestudy_category }}}</td>
				      <td class="table-buttons">
								<button href="#" data-dropdown="drop_status_{{{ $case_study->id }}}" aria-controls="drop_status_{{{ $case_study->id }}}" aria-expanded="false" class="button tiny dropdown">{{{$case_study->status}}}</button>
								<ul id="drop_status_{{{ $case_study->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
									<li><a class="status-select" href="#" data-id="{{{$case_study->id}}}">live</a></li>
									<li><a class="status-select" href="#" data-id="{{{$case_study->id}}}">pending</a></li>
									<li><a class="status-select" href="#" data-id="{{{$case_study->id}}}">disabled</a></li>
								</ul>
							</td>
				      <td class="table-buttons">
				      	<button href="#" data-dropdown="drop_{{{ $case_study->id }}}" aria-controls="drop_{{{ $case_study->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
								<ul id="drop_{{{ $case_study->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
									<li>{!! link_to_route('admin.case-study.edit', 'Edit', [$case_study->id]) !!}</li>
									<li>{!! link_to_route('case-study-preview.{hash}', 'Preview', [$case_study->casestudy_hashed_url], ["target"=>"_blank"]) !!}</li>
									<li>{!! Form::model($case_study, ['route' => ['admin.case-study.destroy', $case_study->id], 'method' => 'delete', 'id' => 'form_'.$case_study->id]) !!}
									  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this case study?"); if (r == true) {document.getElementById("form_'.$case_study->id.'").submit();} return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								</ul>
						  </td>
					   </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No case studies created yet</p>
			@endif

		</div>
		<!-- TOP BUTTON GROUP -->
		<div class="small-12 columns">
			{!! link_to('/admin/case-study/create', 'Create Case Study', ['class' => 'button small button success right']) !!}
		</div>

	</div>
@endsection
