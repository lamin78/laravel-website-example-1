<div class="row collapse">
	<div class="columns small-12">
		{!! Form::label('casestudy_title', 'Header Title') !!}
		{!! Form::text('casestudy_title') !!}
		{!! $errors->first('casestudy_title', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_slug', 'Slug') !!}
		<div class="row collapse">
			<div class="columns small-10">
				{!! Form::text('casestudy_slug') !!}
				{!! $errors->first('casestudy_slug', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns small-2">
				<a href="#" class="button prefix generate-slug">Generate</a>
			</div>
		</div>
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_subtitle', 'Header Sub Title') !!}
		{!! Form::text('casestudy_subtitle') !!}
		{!! $errors->first('casestudy_subtitle', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_category', 'Category') !!}
		{!! Form::select('casestudy_category', $categories, null, ['class' => '']); !!}
		{!! $errors->first('casestudy_category', '<small class="error">:message</small>') !!}
	</div>
</div>
