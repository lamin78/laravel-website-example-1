@if(!isset($cms_Carousel))
	<p>Upload carousel images</p>
@else
	<draggable
		v-model="cms_Carousel"
		:element="'div'"
		@update="updateOrder">
		<transition-group
			name="list-complete"
			tag="ul"
			class="row collapse ul-carousel">
			<carousel-section
				v-for="(carousel, carousel_index) in cms_Carousel"
				:key="carousel.id"
				:id="carousel_index"
				class="list-complete-item"
				:data-cid="carousel.cid"
				:data-csid="carousel.csid"
				:data-id="carousel.id"
				:data-image="carousel.image"
				:data-drop-index="carousel_index"
				:data-len="cms_Carousel.length"
				:data-section="carousel"></carousel-section>
		</transition-group>
	</draggable>
@endif
