<div class="row collapse">

	<transition
		mode="out-in"
		@before-enter="beforeEnter"
    @enter="enter"
		@before-leave="beforeLeave"
    @leave="leave"
    v-bind:css="false">

		<!-- HEADER VIDEO BLOCK -->
		<div v-if="showImage === false" key="video">
			<div class="columns small-12">
				<p>
					<span class="radius success label">Video Header</span>
				</p>
			</div>
			<div class="columns small-12">
				@if(isset($cms_case_study->casestudy_header_video_link) && $cms_case_study->casestudy_header_video_link !== '')
				<video width="100%" controls>
					<source src="{{ $cms_case_study->casestudy_header_video_link }}" type="video/mp4">
						Your browser does not support the video tag.
					</video>
					@else
					<p>Add an AWS video link</p>
					@endif
				</div>
				<div class="columns small-12">
					{!! Form::label('casestudy_header_video_link', 'Header Video (AWS public link)') !!}
					{!! Form::text('casestudy_header_video_link', old('casestudy_header_video_link'), ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/video.mp4']) !!}
					{!! $errors->first('casestudy_header_video_link', '<small class="error">:message</small>') !!}
				</div>

				<!-- HEADER VIDEO POSTER-->
				<div class="columns small-12">
					@if(isset($cms_case_study->casestudy_header_video_poster) && $cms_case_study->casestudy_header_video_poster !== '')
					<img src="{{ $cms_case_study->casestudy_header_video_poster }}" width="100%" alt="">
					@else
					<p>Add an AWS video poster link</p>
					@endif
				</div>
				<div class="columns small-12">
					{!! Form::label('casestudy_header_video_poster', 'Header Poster (AWS public link)') !!}
					{!! Form::text('casestudy_header_video_poster', old('casestudy_header_video_poster'), ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/poster.jpg']) !!}
					{!! $errors->first('casestudy_header_video_poster', '<small class="error">:message</small>') !!}
				</div>

				<!-- HEADER VIDEO PRE POSTER-->
				<div class="columns small-12">
					@if(isset($cms_case_study->casestudy_header_video_pre_poster) && $cms_case_study->casestudy_header_video_pre_poster !== '')
					<img src="{{ $cms_case_study->casestudy_header_video_pre_poster }}" width="100%" alt="">
					@else
					<p>Add an AWS video pre poster link</p>
					@endif
				</div>
				<div class="columns small-12">
					{!! Form::label('casestudy_header_video_pre_poster', 'Header Pre Poster (AWS public link) (389px X 291px)') !!}
					{!! Form::text('casestudy_header_video_pre_poster', old('casestudy_header_video_pre_poster'), ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/pre-poster.jpg']) !!}
					{!! $errors->first('casestudy_header_video_pre_poster', '<small class="error">:message</small>') !!}
				</div>

				<!-- HEADER VIDEO MOBILE POSTER-->
				<div class="columns small-12">
					@if(isset($cms_case_study->casestudy_header_video_mobile_poster) && $cms_case_study->casestudy_header_video_mobile_poster !== '')
					<img src="{{ $cms_case_study->casestudy_header_video_mobile_poster }}" width="100%" alt="">
					@else
					<p>Add an AWS mobile poster link</p>
					@endif
				</div>
				<div class="columns small-12">
					{!! Form::label('casestudy_header_video_mobile_poster', 'Mobile Poster (AWS public link) (640px X 1136px)') !!}
					{!! Form::text('casestudy_header_video_mobile_poster', old('casestudy_header_video_mobile_poster'), ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/mobile-poster.jpg']) !!}
					{!! $errors->first('casestudy_header_video_mobile_poster', '<small class="error">:message</small>') !!}
				</div>
		</div>

		<!-- HEADER IMAGE BLOCK-->
		<div v-if="showImage === true" key="image">
			<div class="columns small-12">
				<p>
					<span class="radius success label">Image Header</span>
				</p>
			</div>
			<div class="columns small-12">
				@if(isset($cms_case_study->casestudy_header_image))
					<img src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_case_study->casestudy_header_image }}?{{rand(5, 15)}}" alt="">
				@else
					<img src="https://placehold.it/500x300&text=Upload header image" alt="">
				@endif
			</div>
			<div class="columns small-12">
				<label for="header_image" class="icon-upload margin-top-0_5-rem margin-bottom-1-rem" v-html="fileInputText"></label>
				<input type="file" name="header_image" id="header_image" @change="processFile($event)">
				{!! $errors->first('header_image', '<small class="error">:message</small>') !!}
			</div>
		</div>

	</transition>

	<div class="columns small-12">
		<hr>
	</div>

	<div class="columns small-12">
		{!! Form::label('casestudy_title', 'Header Title') !!}
		{!! Form::text('casestudy_title', old('casestudy_title')) !!}
		{!! $errors->first('casestudy_title', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_slug', 'Slug') !!}
		<div class="row collapse">
			<div class="columns small-10">
				{!! Form::text('casestudy_slug') !!}
				{!! Form::hidden('casestudy_slug_hidden', $cms_case_study->casestudy_slug) !!}
				{!! $errors->first('casestudy_slug', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns small-2">
				<a href="#" class="button prefix generate-slug">Generate</a>
			</div>
		</div>
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_subtitle', 'Header Sub Title') !!}
		{!! Form::text('casestudy_subtitle', old('casestudy_subtitle')) !!}
		{!! $errors->first('casestudy_subtitle', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_header', 'Main Body Header') !!}
		{!! Form::text('casestudy_header', old('casestudy_header')) !!}
		{!! $errors->first('casestudy_header', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('casestudy_sub_header', 'Main Body Sub Header') !!}
		{!! Form::text('casestudy_sub_header', old('casestudy_sub_header')) !!}
		{!! $errors->first('casestudy_sub_header', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		<div class="row">
			<div class="columns small-3">
				{!! Form::label('casestudy_client', 'Client') !!}
				{!! Form::text('casestudy_client', old('casestudy_client')) !!}
				{!! $errors->first('casestudy_client', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns small-3">
				{!! Form::label('casestudy_year', 'Year') !!}
				{!! Form::text('casestudy_year', old('casestudy_year')) !!}
				{!! $errors->first('casestudy_year', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns small-3">
				{!! Form::label('casestudy_location', 'Location') !!}
				{!! Form::text('casestudy_location', old('casestudy_location')) !!}
				{!! $errors->first('casestudy_location', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns small-3">
				{!! Form::label('casestudy_category', 'Category') !!}
				{!! Form::select('casestudy_category', $categories, null, ['class' => '']); !!}
				{!! $errors->first('casestudy_category', '<small class="error">:message</small>') !!}
			</div>
		</div>
	</div>
	<div class="columns small-12">
		{!! Form::label('status', 'Status') !!}
		@if ($cms_case_study->status === 'live')
			{!! Form::radio('status', 'live', true, ['id' => 'status2', 'checked' => 'checked']); !!}{!! Form::label('status2', 'Live') !!}
			{!! Form::radio('status', 'disabled', null, ['id' => 'status1']); !!}{!! Form::label('status1', 'Disabled') !!}
			{!! Form::radio('status', 'pending', null, ['id' => 'status0']); !!}{!! Form::label('status0', 'Pending') !!}
		@elseif ($cms_case_study->status === 'disabled')
			{!! Form::radio('status', 'live', null, ['id' => 'status2']); !!}{!! Form::label('status2', 'Live') !!}
			{!! Form::radio('status', 'disabled', true, ['id' => 'status1', 'checked' => 'checked']); !!}{!! Form::label('status1', 'Disabled') !!}
			{!! Form::radio('status', 'pending', null, ['id' => 'status0']); !!}{!! Form::label('status0', 'Pending') !!}
		@else
			{!! Form::radio('status', 'live', null, ['id' => 'status2']); !!}{!! Form::label('status2', 'Live') !!}
			{!! Form::radio('status', 'disabled', null, ['id' => 'status1']); !!}{!! Form::label('status1', 'Disabled') !!}
			{!! Form::radio('status', 'pending', true, ['id' => 'status0', 'checked' => 'checked']); !!}{!! Form::label('status0', 'Pending') !!}
		@endif
		{!! $errors->first('status', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
