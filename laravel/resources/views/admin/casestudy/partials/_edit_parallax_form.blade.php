<div id="parallax_wrap" class="row collapse">
	<div class="columns small-12 cms-img-wrap">
		@if(isset($cms_Parallax->image))
			<img src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_Parallax->image }}?{{rand(5, 15)}}" alt="">
		@else
			<p>Upload an image</p>
			<img src="https://placehold.it/500x300&text=Upload a parallax image" alt="">
		@endif
	</div>
	<div class="columns small-12">
		<label for="image" class="icon-upload" v-html="fileInputText"></label>
		<input type="file" name="image" id="image" @change="processFile($event)">
		{!! $errors->first('image', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
