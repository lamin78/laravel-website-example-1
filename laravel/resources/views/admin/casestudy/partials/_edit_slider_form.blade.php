<div id="slider_wrap" class="row collapse">

	<div class="columns small-12">
		@if(isset($cms_Slide->slide_1))
			<img src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_Slide->slide_1 }}?{{rand(5, 15)}}" alt="">
		@else
			<img src="https://placehold.it/500x300&text=Upload before slide" alt="">
		@endif
	</div>

	<div class="columns small-12">
		<label for="slide_1_holder" class="icon-upload margin-top-0_5-rem margin-bottom-1-rem" v-html="fileInputText1"></label>
		<input type="file" name="slide_1_holder" id="slide_1_holder" @change="processFile($event, 0)">
		{!! $errors->first('slide_1_holder', '<small class="error">:message</small>') !!}
	</div>

	<div class="columns small-12">
		@if(isset($cms_Slide->slide_2))
			<img src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_Slide->slide_2 }}?{{rand(5, 15)}}" alt="">
		@else
			<img src="https://placehold.it/500x300&text=Upload after slide" alt="">
		@endif
	</div>

	<div class="columns small-12">
		<label for="slide_2_holder" class="icon-upload margin-top-0_5-rem" v-html="fileInputText2"></label>
		<input type="file" name="slide_2_holder" id="slide_2_holder" @change="processFile($event, 1)">
		{!! $errors->first('slide_2_holder', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
