<div class="row collapse margin-bottom-1-rem">

	<div class="columns small-12">
		@if(isset($cms_case_study->casestudy_stat_1))
			<img class="stat-image" src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_case_study->casestudy_stat_1 }}?{{rand(5, 15)}}" alt="">
			<div class="black-backing"></div>
		@else
			<img src="https://placehold.it/500x300&text=Upload an statistic image 1" alt="">
		@endif
	</div>

	<div class="columns small-12">
		{!! Form::label('casestudy_stat_1_holder', 'Add Statistic Image 1', ['class' => 'icon-upload margin-top-0_5-rem']) !!}
		{!! Form::file('casestudy_stat_1_holder', null) !!}
		{!! $errors->first('casestudy_stat_1_holder', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row collapse margin-bottom-1-rem">

	<div class="columns small-12">
		@if(isset($cms_case_study->casestudy_stat_2))
			<img class="stat-image" src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_case_study->casestudy_stat_2 }}?{{rand(5, 15)}}" alt="">
			<div class="black-backing"></div>
		@else
			<img src="https://placehold.it/500x300&text=Upload an statistic image 2" alt="">
		@endif
	</div>

	<div class="columns small-12">
		{!! Form::label('casestudy_stat_2_holder', 'Add Statistic Image 2', ['class' => 'icon-upload margin-top-0_5-rem']) !!}
		{!! Form::file('casestudy_stat_2_holder', null) !!}
		{!! $errors->first('casestudy_stat_2_holder', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row collapse margin-bottom-1-rem">

	<div class="columns small-12">
		@if(isset($cms_case_study->casestudy_stat_3))
			<img class="stat-image" src="/images/cms-case-studies/{{$cms_case_study->casestudy_slug}}/{{ $cms_case_study->casestudy_stat_3 }}?{{rand(5, 15)}}" alt="">
			<div class="black-backing"></div>
		@else
			<img src="https://placehold.it/500x300&text=Upload an statistic image 3" alt="">
		@endif
	</div>

	<div class="columns small-12">
		{!! Form::label('casestudy_stat_3_holder', 'Add Statistic Image 3', ['class' => 'icon-upload margin-top-0_5-rem']) !!}
		{!! Form::file('casestudy_stat_3_holder', null) !!}
		{!! $errors->first('casestudy_stat_3_holder', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
