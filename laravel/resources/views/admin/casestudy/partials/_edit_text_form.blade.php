<div class="row collapse">
	<div class="columns small-12">
		{!! Form::label('text_type', 'Heading') !!}
		{!! Form::text('text_type') !!}
		{!! $errors->first('text_type', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		{!! Form::label('text_field', 'Content') !!}
		{!! Form::textarea('text_field') !!}
		{!! $errors->first('text_field', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
