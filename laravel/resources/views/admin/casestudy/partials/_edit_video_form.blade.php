<div class="row collapse edit-video-wrap">
	<div class="columns small-12">
		@if(isset($cms_Video->video))
			<video width="100%" controls>
				<source src="{{ $cms_Video->video }}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		@else
			<p>Add an AWS video link</p>
		@endif
	</div>
	<div class="columns small-12">
		{!! Form::label('video', 'Video (AWS public link)') !!}
		{!! Form::text('video', null, ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/video.mp4']) !!}
		{!! $errors->first('video', '<small class="error">:message</small>') !!}
	</div>
	<div class="columns small-12">
		@if(isset($cms_Video->poster))
			<img src="{{ $cms_Video->poster }}" width="100%" alt="">
		@else
			<p>Add an AWS poster link</p>
		@endif
	</div>
	<div class="columns small-12">
		{!! Form::label('poster', 'Poster (AWS public link)') !!}
		{!! Form::text('poster', null, ['placeholder'=>'https://s3-eu-west-1.amazonaws.com/video.jpg']) !!}
		{!! $errors->first('poster', '<small class="error">:message</small>') !!}
	</div>
</div>

<div class="row">
	<div class="small-12 columns mt1">
	</div>
</div>
