<!-- LAYOUT TEMPLATE -->
<script type="text/x-template" id="template-component-section">
  <li id="components_section" class="columns small-12 section-li">
    <div class="row section-block">
      <div class="columns small-12 medium-8">
        <span v-if="showIdentInput === false" @click="swapForInput">@{{sectionType.sectionName}}</span>
        <span v-else-if="showIdentInput === true"><input type="text" id="sectionName" :value="sectionType.sectionName" @keyup.enter="submitNewIdent" @blur="swapBackToText"></span>
      </div>
      <div class="columns small-12 medium-4 text-align-right">
        <a href="#" :data-dropdown="dropId" class="button tiny dropdown" @click.prevent>Options</a>
        <ul :id="dropId" data-dropdown-content class="f-dropdown text-align-left">
          <li><a :href="editLink.endpoint">Edit</a></li>
          <li><a href="#" @click.prevent="deletSection">Delete</a></li>
        </ul>
      </div>
    </div>
  </li>
</script>

<!-- CAROUSEL TEMPLATE -->
<script type="text/x-template" id="template-carousel-section">
  <li id="carousel_section" class="carousel-li">
    <div class="row section-block">
      <div class="columns small-12 medium-8">
        <img v-if="dataImage !== null" :src="printImage" alt="" width="200" height="150">
        <img v-else-if="dataImage === null" src="https://placehold.it/200x150&text=Upload an image" alt="" width="200px" height="150px">
        <br>
        <label :for="nameParser.file_input" class="icon-upload margin-top-0_5-rem" v-html="fileInputText"></label>
        <input type="file"   :name="nameParser.file_input" :id="nameParser.file_input" @change="processFile($event)">
        <input type="hidden" :name="nameParser.hidden_id" :id="nameParser.hidden_id" :value="dataId">
        <input type="hidden" :name="nameParser.hidden_cid" :id="nameParser.hidden_cid" :value="dataCid">
        <input type="hidden" :name="nameParser.hidden_csid" :id="nameParser.hidden_csid" :value="dataCsid">
        <input type="hidden" :name="nameParser.hidden_changed" :id="nameParser.hidden_changed" :value="inputAdded">
      </div>
      <div class="columns small-12 medium-4 text-align-right">
        <a href="#" class="button alert tiny" @click.prevent="deletCarousel" v-if="dataLen > 1">Delete</a>
      </div>
    </div>
  </li>
</script>
