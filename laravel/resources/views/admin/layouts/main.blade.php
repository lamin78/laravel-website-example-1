<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Article 10{{ isset($title) ? ' | '.$title : '' }}</title>

    <link rel="stylesheet" href="/css/admin/foundation.css"/>
    <link rel="stylesheet" href="/css/admin/styles.css"/>

    <script src="/js/libs/modernizr/modernizr.js"></script>
  </head>
  <body>
  	<a name="top"></a>

    @yield('content')

    <script>
      var site_url  = "{{ url('',[]) }}";
      @if (isset($pageViewJS))
        var pageViewJS = "{{ $pageViewJS }}";
      @endif
      @if (isset($cms_case_study->order))
        var caseStudyPage = {!! json_encode($cms_case_study) !!};
      @endif
      @if (isset($imageDir))
        var imageDir = "{{ $imageDir }}";
      @endif
      @if (isset($cms_Carousel))
        var cmsCarousel = {!! json_encode($cms_Carousel) !!};
      @endif
      @if (isset($cid))
        var cid = "{{ $cid }}";
      @endif
      @if (isset($csid))
        var csid = "{{ $csid }}";
      @endif
      @if (isset($cms_case_study->header_image_active))
        var headerImageActive = "{{ $cms_case_study->header_image_active }}";
      @endif
    </script>

    {!! HTML::script('js/libs/requirejs/require.js', ['data-main' => '/js/admin/main-min.js?'.time()]) !!}

    @if (App::environment() == 'production')

    <!-- Google Analytics -->

    @endif

    <div id="hb-templates"></div>
  </body>
</html>
