<nav class="top-bar mb1" data-topbar role="navigation">
	<ul class="title-area">
		<li class="name">
			<h1>{!! HTML::link('/', 'Article 10', ['class'=>'']) !!}</h1>
		</li>
		<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
		<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	</ul>

	<section class="top-bar-section">
		<!-- Right Nav Section -->
		<ul class="right">

			<li class="divider"></li>
			<li class="has-dropdown">
				<a href="#">{!! Admin::user('name') !!}</a>
				<ul class="dropdown">
					<li>{!! link_to_route('admin.users.edit', 'My Account', [Admin::user('id')], null) !!}</li>
					<li class="divider"></li>
					<li class="alert">{!! link_to('admin/auth/logout', 'Logout', [], null) !!}</li>
				</ul>
			</li>
		</ul>

		<!-- Left Nav Section -->
		<ul class="left">
			<li>{!! HTML::link('/admin/summary', 'Summary', ['class'=>'']) !!}</li>
			<li class="has-dropdown not-click">{!! HTML::link('#', 'Social', ['class'=>'']) !!}
				<!-- Dropdown -->
				<ul class="dropdown">
					<li>{!! HTML::link('/admin/social/twitter', 'Twitter', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/social/vimeo', 'Vimeo', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/social/linkedin', 'LinkedIn', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/social/pinterest', 'Pinterest', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/social/potm', 'POTM', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/social/images', 'Images', ['class'=>'']) !!}</li>
				</ul>
			</li>

			<li class="has-dropdown not-click">{!! HTML::link('#', 'Case Studies', ['class'=>'']) !!}
				<!-- Dropdown -->
				<ul class="dropdown">
					<li>{!! HTML::link('/admin/case-study', 'List Case Studies', ['class'=>'']) !!}</li>
					<li>{!! HTML::link('/admin/case-study/create', 'Create Case Study', ['class'=>'']) !!}</li>
				</ul>
			</li>

			@if (Admin::checkPermission('menu_users'))
			<li class="has-dropdown not-click">{!! HTML::link('#', 'Users', ['class'=>'']) !!}
				<!-- Dropdown -->
				<ul class="dropdown">
					<li>{!! HTML::link('/admin/users', 'List Users', ['class'=>'']) !!}</li>
					@if (Admin::checkPermission('create_user'))
					<li>{!! HTML::link('/admin/users/create', 'Create User', ['class'=>'']) !!}</li>
					@endif
				</ul>
			</li>
			@endif
		</ul>
	</section>
</nav>

@include('admin.layouts.partials.responses')
