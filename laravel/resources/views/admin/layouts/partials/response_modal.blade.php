<div id="alertModalSuccess" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <i class="icon2-ok-circled"></i>
  <h2 id="modalTitle" class="success"></h2>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id="alertModalFail" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h2 id="modalTitle" class="fail icon2-cancel-circled"></h2>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
