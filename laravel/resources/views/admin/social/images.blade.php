@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">Images</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="small-3 columns end">
			<a href="/admin/social/images/create" class="button success small ff">Add New Image</a>
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($images) && $images->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Image</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($images as $image)
						<tr class="{{{ $image->status }}}">
					      <td class="table-middle"><a href="/uploads/{{{ $image->media_local }}}" class="fancybox">{!! $image->title !!}</a></td>
					      <td class="table-middle">{{{ $image->created_at->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $image->id }}}" aria-controls="drop_{{{ $image->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $image->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($image->status == 'disabled')
									<li>{!! Form::model($image, ['url' => 'admin/social/images/enable/'.$image->id, 'method' => 'image', 'id' => 'form_enable_'.$image->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$image->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($image->status == 'ready')
									<li>{!! Form::model($image, ['url' => 'admin/social/images/disable/'.$image->id, 'method' => 'image', 'id' => 'form_disable_'.$image->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$image->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								

								<li>{!! Form::model($image, ['url' => 'admin/social/images/destroy/'.$image->id, 'method' => 'delete', 'id' => 'form_delete_'.$image->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this image?"); if (r == true) {document.getElementById("form_delete_'.$image->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No Images yet</p>
			@endif
		</div>


	</div>


@endsection
