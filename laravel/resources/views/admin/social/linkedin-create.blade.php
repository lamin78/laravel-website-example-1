@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li><a href="/admin/social/linkedin">LinkedIn</a></li>
			  <li class="current">Add New Post</li>
			</ul>
		</div>
	</div>

	{!! Form::open(array('url' => ['admin/social/linkedin/store'], 'method' => 'POST') )!!}
		<div class="row">
			<div class="small-12 columns mt1">
				@include('admin.social.partials._linkedin_form')
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				{!! Form::submit('Create', array('class' => 'button success small ff')) !!}
				{!! link_to('admin/social/linkedin', 'Cancel', [], ['class' => 'small button alert ff']) !!}
			</div>
		</div>
	{!! Form::close() !!}

@endsection
