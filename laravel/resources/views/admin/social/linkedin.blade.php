@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">LinkedIn</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="small-2 columns end">
			<a href="/admin/social/linkedin/create" class="button success small ff postfix">Add New Post</a>
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($posts) && $posts->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Post</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($posts as $post)
						<tr class="{{{ $post->status }}}">
					      <td class="table-middle"><a href="{{{ $post->link }}}" target="_blank">{!! $post->text !!}</a></td>
					      <td class="table-middle">{{{ $post->created_at->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $post->id }}}" aria-controls="drop_{{{ $post->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $post->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($post->status == 'disabled')
									<li>{!! Form::model($post, ['url' => 'admin/social/linkedin/enable/'.$post->id, 'method' => 'post', 'id' => 'form_enable_'.$post->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$post->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($post->status == 'ready')
									<li>{!! Form::model($post, ['url' => 'admin/social/linkedin/disable/'.$post->id, 'method' => 'post', 'id' => 'form_disable_'.$post->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$post->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								

								<li>{!! Form::model($post, ['url' => 'admin/social/linkedin/destroy/'.$post->id, 'method' => 'delete', 'id' => 'form_delete_'.$post->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this post?"); if (r == true) {document.getElementById("form_delete_'.$post->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No linkedin posts yet</p>
			@endif
		</div>


	</div>


@endsection
