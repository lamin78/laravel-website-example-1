{!! Form::label('text', 'Text') !!}
{!! Form::textarea('text','',['placeholder' => 'Keep to around 140 characters']) !!}
{!! $errors->first('text', '<small class="error">:message</small>') !!}

{!! Form::label('link', 'Link') !!}
{!! Form::text('link', 'https://www.linkedin.com/company/370727') !!}
{!! $errors->first('link', '<small class="error">:message</small>') !!}