{!! Form::label('uri', 'Project') !!}
{!! Form::select('uri', $projects); !!}
{!! $errors->first('uri', '<small class="error">:message</small>') !!}


{!! Form::label('image','Associated Image') !!}
{!! Form::file('image', null) !!}
{!! $errors->first('image', '<small class="error">:message</small>') !!}