@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	@if (!Request::secure())
	<div class="row">
		<div class="small-12 columns">
			<div data-alert class="alert-box warning radius">
  				Ensure you are using HTTPS when accessing the pinterest API
			</div>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">Pinterest</li>
			</ul>
		</div>
	</div>

	<div class="row mb1">
		<div class="small-12 columns">
			<div class="row collapse">
				<div class="small-5 medium-4 large-3 columns">
			      <span class="prefix">https://pinterest.com/pin/</span>
			    </div>
			    <div class="small-4 columns">
			      <input type="text" name="id" id="pin_id" placeholder="Pin ID">
			    </div>
			    <div class="small-2 columns end">
			    	<a href="#" class="button success small ff postfix" id="bFetchPin">Fetch Pin</a>
		        </div>
			</div>
		</div>
		<div class="small-12 columns hidden" id="preview">
			{!! Form::open(array('url' => ['admin/social/pinterest/store'], 'method' => 'POST') )!!}
			<div class="row panel">
				<div class="small-12 columns mt1">
			  		<div id="preview_image" class="ff"></div>
			  		{!! Form::text('note', '', ['id' => 'preview_note', 'class' => 'ff ml1', 'style' => 'width:50%']) !!}
			  		{!! Form::hidden('pin_id', '', ['id' => 'preview_pin_id']) !!}
			  		{!! Form::hidden('link', '', ['id' => 'preview_link']) !!}
			  		{!! Form::hidden('media_url', '', ['id' => 'preview_media_url']) !!}
			  	</div>

			  	<div class="small-12 columns mt1">
			  		{!! Form::submit('Save', array('class' => 'button success small ff')) !!}
					{!! link_to('#', 'Cancel', ['class' => 'small button alert ff']) !!}
				</div>
			</div>

			{!! Form::close() !!}
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($pins) && $pins->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Pin</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($pins as $pin)
						<tr class="{{{ $pin->status }}}">
					      <td class="table-middle"><a href="{{{ $pin->link }}}" target="_blank">{!! $pin->note !!}</a></td>
					      <td class="table-middle">{{{ $pin->created_at->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $pin->id }}}" aria-controls="drop_{{{ $pin->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $pin->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($pin->status == 'disabled')
									<li>{!! Form::model($pin, ['url' => 'admin/social/pinterest/enable/'.$pin->id, 'method' => 'post', 'id' => 'form_enable_'.$pin->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$pin->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($pin->status == 'ready')
									<li>{!! Form::model($pin, ['url' => 'admin/social/pinterest/disable/'.$pin->id, 'method' => 'post', 'id' => 'form_disable_'.$pin->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$pin->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								

								<li>{!! Form::model($pin, ['url' => 'admin/social/pinterest/destroy/'.$pin->id, 'method' => 'delete', 'id' => 'form_delete_'.$pin->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this pin?"); if (r == true) {document.getElementById("form_delete_'.$pin->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No pins yet</p>
			@endif
		</div>


	</div>


@endsection
