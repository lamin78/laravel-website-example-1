@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li><a href="/admin/social/potm">POTM</a></li>
			  <li class="current">Add New Project</li>
			</ul>
		</div>
	</div>

	{!! Form::open(array('url' => ['admin/social/potm/store'], 'method' => 'POST', 'files' => true) )!!}
		<div class="row">
			<div class="small-12 columns mt1">
				@include('admin.social.partials._potm_form')
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				{!! Form::submit('Create', array('class' => 'button success small ff')) !!}
				{!! link_to('admin/social/potm', 'Cancel', [], ['class' => 'small button alert ff']) !!}
			</div>
		</div>
	{!! Form::close() !!}

@endsection
