@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">POTM</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="small-3 columns end">
			<a href="/admin/social/potm/create" class="button success small ff projectfix">Add New Project</a>
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($projects) && $projects->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Project</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($projects as $project)
						<tr class="{{{ $project->status }}}">
					      <td class="table-middle"><a href="/projects/{{{ $project->uri }}}" target="_blank">{!! ucwords(str_replace("-"," ",$project->uri)) !!}</a></td>
					      <td class="table-middle">{{{ $project->created_at->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $project->id }}}" aria-controls="drop_{{{ $project->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $project->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($project->status == 'disabled')
									<li>{!! Form::model($project, ['url' => 'admin/social/potm/enable/'.$project->id, 'method' => 'project', 'id' => 'form_enable_'.$project->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$project->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($project->status == 'ready')
									<li>{!! Form::model($project, ['url' => 'admin/social/potm/disable/'.$project->id, 'method' => 'project', 'id' => 'form_disable_'.$project->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$project->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								

								<li>{!! Form::model($project, ['url' => 'admin/social/potm/destroy/'.$project->id, 'method' => 'delete', 'id' => 'form_delete_'.$project->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this project?"); if (r == true) {document.getElementById("form_delete_'.$project->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No Projects of the month yet</p>
			@endif
		</div>


	</div>


@endsection
