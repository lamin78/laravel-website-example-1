@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">Twitter</li>
			</ul>
		</div>


		<div class="medium-12 columns">
			@if (isset($tweets) && $tweets->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Tweet</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($tweets as $tweet)
						<tr class="{{{ $tweet->status }}}">
					      <td class="table-middle">{!! Twitter::linkify($tweet->text) !!}</td>
					      <td class="table-middle">{{{ $tweet->tweeted_at->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $tweet->id }}}" aria-controls="drop_{{{ $tweet->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $tweet->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($tweet->status == 'disabled')
									<li>{!! Form::model($tweet, ['url' => 'admin/social/twitter/enable/'.$tweet->id, 'method' => 'post', 'id' => 'form_enable_'.$tweet->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$tweet->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($tweet->status == 'ready')
									<li>{!! Form::model($tweet, ['url' => 'admin/social/twitter/disable/'.$tweet->id, 'method' => 'post', 'id' => 'form_disable_'.$tweet->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$tweet->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								





								<li>{!! Form::model($tweet, ['url' => 'admin/social/twitter/destroy/'.$tweet->id, 'method' => 'delete', 'id' => 'form_delete_'.$tweet->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this tweet?"); if (r == true) {document.getElementById("form_delete_'.$tweet->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No tweets yet</p>
			@endif
		</div>


	</div>


@endsection
