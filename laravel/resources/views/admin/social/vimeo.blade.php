@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="small-12 columns">
			<ul class="breadcrumbs">
			  <li>Social</li>
			  <li class="current">Vimeo</li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			{!! Form::open(array('url' => 'admin/social/vimeo/store', 'method' => 'POST') )!!}
				<div class="row collapse">
					<div class="small-5 medium-4 large-3 columns">
				      <span class="prefix">https://vimeo.com/</span>
				    </div>
				    <div class="small-4 columns">
				      <input type="text" name="id" id="id" placeholder="Video ID">
				    </div>
				    <div class="small-2 columns end">
				    	{!! Form::submit('Add', array('class' => 'button success small ff postfix')) !!}
			        </div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			@if (isset($vids) && $vids->count() != 0)
				<table style="width:100%;">
				  <thead>
				    <tr>
				      <th scope="column">Video</th>
				      <th scope="column" width="150">Date</th>
				      <th width="115"scope="column">Actions</th>
				    </tr>
				  </thead>
				  <tbody>

					@foreach ($vids as $vid)
						<tr class="{{{ $vid->status }}}">
					      <td class="table-middle"><a href="{{{ $vid->link }}}" target="_blank">{!! $vid->name !!}</a></td>
					      <td class="table-middle">{{{ $vid->created_time->format('d-m-Y H:i:s') }}}</td>
					      <td class="table-buttons">
					      	<button href="#" data-dropdown="drop_{{{ $vid->id }}}" aria-controls="drop_{{{ $vid->id }}}" aria-expanded="false" class="button tiny dropdown">Options</button>
							<ul id="drop_{{{ $vid->id }}}" data-dropdown-content class="f-dropdown" aria-hidden="true">
								
								@if ($vid->status == 'disabled')
									<li>{!! Form::model($vid, ['url' => 'admin/social/vimeo/enable/'.$vid->id, 'method' => 'post', 'id' => 'form_enable_'.$vid->id]) !!}
									  		{!! link_to('#', 'Enable', ['onClick' => 'document.getElementById("form_enable_'.$vid->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif

								@if ($vid->status == 'ready')
									<li>{!! Form::model($vid, ['url' => 'admin/social/vimeo/disable/'.$vid->id, 'method' => 'post', 'id' => 'form_disable_'.$vid->id]) !!}
									  		{!! link_to('#', 'Disable', ['onClick' => 'document.getElementById("form_disable_'.$vid->id.'").submit(); return false;']) !!}
									  	{!! Form::close() !!}
									</li>
								@endif
								

								<li>{!! Form::model($vid, ['url' => 'admin/social/vimeo/destroy/'.$vid->id, 'method' => 'delete', 'id' => 'form_delete_'.$vid->id]) !!}
								  		{!! link_to('#', 'Delete', ['onClick' => 'var r = confirm("Are you sure you want to delete this vid?"); if (r == true) {document.getElementById("form_delete_'.$vid->id.'").submit();} return false;']) !!}
								  	{!! Form::close() !!}
								</li>
							</ul>

						  </td>
					    </tr>
					@endforeach
				  </tbody>
				</table>

			@else
				<p>No vimeo videos yet</p>
			@endif
		</div>


	</div>


@endsection
