{!! Form::label('name', 'Name') !!}
{!! Form::text('name') !!}
{!! $errors->first('name', '<small class="error">:message</small>') !!}

{!! Form::label('email', 'Email') !!}
{!! Form::text('email') !!}
{!! $errors->first('email', '<small class="error">:message</small>') !!}

{!! Form::label('password', 'Password') !!}
{!! Form::password('password') !!}
{!! $errors->first('password', '<small class="error">:message</small>') !!}

{!! Form::label('password_confirmation', 'Confirm Password') !!}
{!! Form::password('password_confirmation') !!}
{!! $errors->first('password_confirmation', '<small class="error">:message</small>') !!}

{!! Form::label('roles_id', 'Role') !!}
@if ((isset($user) && $user->id != Admin::user('id')) || !isset($user))
{!! Form::select('roles_id', array_pluck($roles,'title','id'), null, ['class' => '']); !!}
@else
<small>You can not edit your own role.</small>
@endif

<div class="row">
	<div class="small-12 columns mt1">
	{!! Form::label('active', 'Active') !!}

	{!! Form::radio('active', '1', null, ['id' => 'active1']); !!}{!! Form::label('active1', 'Yes') !!}
	{!! Form::radio('active', '0', null, ['id' => 'active0']); !!}{!! Form::label('active0', 'No') !!}

	{!! $errors->first('active', '<small class="error">:message</small>') !!}
	</div>
</div>
