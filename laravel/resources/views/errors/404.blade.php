@extends('errors.layouts.main')

@section('content')
	<div class="row full">
		<div class="column span-12 style-404">
			<div class="inner no-anim">
				<img src="/images/error/404-banner.png" class="error-image" alt="404 Error" title="404 Error" />
				<h3 class="error-text no-anim"><strong>LOST?</strong> WE CAN’T SEEM TO FIND WHAT YOU’RE LOOKING FOR<br>
				<a href="{{ URL::to('/') }}" >RETURN HOME</a></h3>
			</div>
		</div>
	</div>
@endsection
