@extends('errors.layouts.main')

@section('content')
	<div class="row full">
		<div class="column span-12 style-500">
			<div class="inner no-anim">
				<img src="/images/error/500-banner.png" class="error-image" alt="500 Error" title="500 Error" />
				<h3 class="error-text no-anim"><strong>OOPS!</strong> LOOKS LIKE THE SERVER FAILED TO CONNECT<br>
				<a href="{{ URL::to('/') }}" >REFRESH PAGE</a></h3>
			</div>
		</div>
	</div>
@endsection
