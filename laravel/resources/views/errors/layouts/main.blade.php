<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />

		@include('main.layouts.partials._metatags')

		<title>{{ isset($title) ? $title . ' | Article 10' : 'Article 10'  }}</title>

		<link href='https://fonts.googleapis.com/css?family=Raleway:800|Open+Sans:300,600' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/css/main/styles.css"/>

		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{ $pageViewCSS }}.css"/>
		@endif

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/main/animations-ie-fix.css">
		<![endif]-->

	</head>

	<body>
		<a name="top"></a>
		<div id="mainWrapper">

			<div id="contentWrapper">
				<section class="row full grey-background">
					<a name="content-section"></a>
					@yield('content')
				</section>
			</div>
		</div>

		<script>
			var site_url = "{{ URL::to('/') }}";

			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif

			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! HTML::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main-min']) !!}

		@if (App::environment() == 'production')

			<!-- Lead Forensics -->
			<script type="text/javascript" src="/js/main/lead-custom-load-min.js"></script>

			<!-- Google Analytics -->
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-1808135-1', 'auto');
				ga('send', 'pageview');
			</script>
		@endif
	</body>
</html>
