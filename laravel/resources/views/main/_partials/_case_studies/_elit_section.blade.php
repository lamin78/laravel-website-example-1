				<div class="pane">
					<div class="parallax layer1 elit-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 elit-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Stoli</h3>
						<p>Presentation</p>
						<span class="tag {{ isset($colour) ? $colour : 'pink' }}">Presentation</span>
						<a href="/projects/stoli-app-design" class="button view">View Case Study</a>
					</div>
				</div>