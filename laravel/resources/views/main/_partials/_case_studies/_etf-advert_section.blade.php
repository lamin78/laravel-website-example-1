				<div class="pane">
					<div class="parallax layer1 {{ $imageDir }}-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 {{ $imageDir }}-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>ETF</h3>
						<p>Broadcast Adverts</p>
						<span class="tag {{ isset($colour) ? $colour : 'orange' }}">Video</span>
						<a href="/projects/{{ $imageDir }}" class="button view">View Case Study</a>
					</div>
				</div>