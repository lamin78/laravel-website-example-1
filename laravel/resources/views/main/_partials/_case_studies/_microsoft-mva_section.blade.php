				<div class="pane">
					<div class="parallax layer1 {{ $imageDir }}-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Microsoft</h3>
						<p>Integrated Marketing Campaign</p>
						<span class="tag {{ isset($colour) ? $colour : 'green' }}">Campaigns</span>
						<a href="/projects/{{ $imageDir }}" class="button view">View Case Study</a>
					</div>
				</div>