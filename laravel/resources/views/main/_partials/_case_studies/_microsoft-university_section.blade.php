				<div class="pane">
					<div class="parallax layer1 microsoft-university-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 microsoft-university-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Microsoft</h3>
						<p>Integrated Campaign</p>
						<span class="tag {{ isset($colour) ? $colour : 'orange' }}">Campaigns</span>
						<a href="/projects/microsoft-universities" class="button view">View Case Study</a>
					</div>
				</div>