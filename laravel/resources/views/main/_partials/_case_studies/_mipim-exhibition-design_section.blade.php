				<div class="pane">
					<div class="parallax layer1"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 mipim-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Mipim</h3>
						<p>Taking the show by storm</p>
						<span class="tag {{ isset($colour) ? $colour : 'orange' }}">Exhibitions</span>
						<a href="/projects/mipim-environment-design" class="button view">View Case Study</a>
					</div>
				</div>