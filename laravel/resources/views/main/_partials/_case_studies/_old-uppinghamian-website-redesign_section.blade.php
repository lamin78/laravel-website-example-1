				<div class="pane">
					<div class="parallax layer1 {{ $imageDir }}-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 {{ $imageDir }}-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Old Uppingham</h3>
						<p>Website design and build</p>
						<span class="tag {{ isset($colour) ? $colour : 'orange' }}">Digital</span>
						<a href="/projects/{{ $imageDir }}" class="button view">View Case Study</a>
					</div>
				</div>