				<div class="pane">
					<div class="parallax layer1 {{ $imageDir }}-background"></div>
					<div class="side-gradient"></div>
					<div class="parallax layer2 {{ $imageDir }}-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>OpenBet</h3>
						<p>Microsite Design and Build</p>
						<span class="tag {{ isset($colour) ? $colour : 'red' }}">Digital</span>
						<a href="/projects/{{ $imageDir }}" class="button view">View Case Study</a>
					</div>
				</div>