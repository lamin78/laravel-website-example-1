	<div class="columns span-12 case-studies-wrap
		{{ isset($whiteBackground) && $whiteBackground == true ? 'white-background' : '' }}">
		<div class="columns span-12 case-studies
			{{ isset($fixMaxHeight) && $fixMaxHeight == true ? 'fix-max-height' : '' }}
			{{ isset($fixMaxHeightCms) && $fixMaxHeightCms == true ? 'fix-max-height-cms' : '' }}
			{{ isset($increaseWidth) && $increaseWidth == true ? 'increase-width' : '' }}">

			@if (!isset($showNav) || $showNav === true)
				<div class="case-banner-nav-left
					{{ isset($transparent_navs) && $transparent_navs == true ? 'no-background' : '' }}"
					data-id="{{ isset($ind) ? $ind : '-1' }}">
					<svg width="30" height="30">
						@if (isset($theme) && $theme === 'dark')
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left-dark.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left-dark.png' }}" width="30" height="30" />
						@elseif(isset($theme) && $theme === 'light')
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left.png' }}" width="30" height="30" />
						@else
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_left_shadow.png' : '/images/shared/case-study-nav-left.png' }}" width="30" height="30" />
						@endif
					</svg>
				</div>
				<div class="case-banner-nav-right {{ isset($transparent_navs) && $transparent_navs == true ? 'no-background' : '' }}" data-id="{{ isset($ind) ? $ind : '-1' }}">
					<svg width="30" height="30">
						@if (isset($theme) && $theme === 'dark')
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right-dark.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right-dark.png' }}" width="30" height="30" />
						@elseif(isset($theme) && $theme === 'light')
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right.png' }}" width="30" height="30" />
						@@else
							<image xlink:href="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right.png' }}" src="{{ isset($arrow_shadow) && $arrow_shadow == true ? '/images/shared/arrow_right_shadow.png' : '/images/shared/case-study-nav-right.png' }}" width="30" height="30" />
						@endif
					</svg>
				</div>
			@endif

			@if ($page === 'home')
				@include('main.home._partials._case_study_home')
			@elseif ($page === 'services')
				@include('main.services._partials._case_study_banner')
			@elseif ($page === 'projects')
				@include('main.projects._partials._project_banner')
			@elseif ($page === 'project_cms')
				@include('main.projects._partials._project_cms_banner')
			@endif
		</div>

		@if (!isset($showOrbs) || $showOrbs === true)
			<div class="orb-wrap absolute-bottom
				{{ isset($no_orb_background) && $no_orb_background == true ? 'no-orb-background' : '' }}
				{{ isset($theme) && $theme == 'dark' ? 'black-orbs' : '' }}"
				data-id="{{ isset($carouselCMSArray) ? $carouselCMSArray[$ind]['container'] : 'carousel2' }}"></div>
		@endif
	</div>
