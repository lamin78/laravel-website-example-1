<div class="video-wrap" data-file="{{ $video_file }}" data-poster="{{ $video_poster }}" data-mobile-poster="{{ $mobile_poster }}" data-position="{{ isset($posterPosition) ? $posterPosition : '' }}" data-autoplay="{{ isset($auto_play) ? $auto_play : '' }}"></div>
<div class="poster-wrap"></div>
