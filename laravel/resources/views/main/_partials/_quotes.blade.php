	<div class="columns span-12 quote-outer">
		<div class="row full">
			<div class="columns span-6 before-3 small-10 small-before-1 small-after-1 medium-10 medium-before-1 medium-after-1 quotes">
				<div class="quote" data-id="1">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							We find this tool very useful and easy to manage. The general feedback coming from our sales people is very positive - the system allows them to create great customer presentations in a matter of minutes.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Ekaterina Voinova</b> - Content &amp; Customer Communication Manager, //--><b>TNT</b></p>
					</div>
				</div>

				<div class="quote" data-id="2">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							I think we should all be proud; it's a very cool looking website.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>Uppingham School</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="3">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							I was really impressed by the way the filming went, especially the way you guys were so keen to get the best shots, appropriate settings and took the time to highlight key points and themes.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>Microsoft</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="4">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							I wanted to say thank you for everything you have done to help us deliver the re-brand, launching it today internally has made me so proud.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>OpenBet</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="5">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							The MegaMap and animated video created by Article 10 fit our brief perfectly and equipped us with a simple and engaging story to communicate the benefits of using Microsoft technology for running a business
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>Microsoft</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="6">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							I can honestly say this was the best stand and event experience EVER! I have done quite a few exhibitions in my time and this one was painless!
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>Siemens</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="7">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							The brand launch has gone down better than I could ever have hoped internally and even the most cynical people have been raving about the new brand identity.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Caroline Steele</b> - Development Manager,  //--><b>OpenBet</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="8">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							We have been consistently impressed by their expertise in presentation technology and design.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Brian Taylorson</b> -  //--><b>Elementis</b></p>
					</div>
				</div>

				<div class="quote" data-id="9">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							...they are always on hand to provide ideas, opinions and suggestions for us to get the most out of our campaigns and initiatives.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Jake Master</b> -  //--><b>Atkins</b></p>
					</div>
				</div>

				<div class="quote" data-id="10">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							Article 10&rsquo;s professionalism, attention to detail and willingness to work until the small hours went down extremely well...
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Cressida Eccleston</b> -  //--><b>Quintain Estates &amp; Development PLC</b></p>
					</div>
				</div>
				
				<div class="quote" data-id="11">
					<div class="inner">
						<p>
							<img src="/images/shared/quote-left-{{$colour}}.svg" class="ql" data-no-retina />
							I thought the guys from Article 10 were just the business! I was just blown away by the level of passion and professionalism the entire team put into capturing the content.
							<img src="/images/shared/quote-right-{{$colour}}.svg" class="qr" data-no-retina />
						</p>
						<p class="name"><!-- <b>Cressida Eccleston</b> -  //--><b>Microsoft</b></p>
					</div>
				</div>
			</div>
		</div>
	</div>