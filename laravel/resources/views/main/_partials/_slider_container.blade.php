<div class="columns span-12">
  <div class="row full">
    <div class="columns span-12 image-slider" data-before="{{ isset($before) ? $before : '' }}" data-after="{{ isset($after) ? $after : '' }}">
      <div class="image-slider-image-before"></div>
      <div class="image-slider-image-after"></div>
      <div class="drag-control" draggable="false"></div>
    </div>
  </div>
</div>
