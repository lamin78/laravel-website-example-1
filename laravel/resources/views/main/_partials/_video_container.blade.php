	<div class="columns span-12 video-player-wrap {{ isset($transparent) && $transparent == true ? 'transparent-background' : '' }}" style="min-height: {{ $minHeight }}; " data-wrap-id="video_{{ $id }}" data-video="{{ $videoFile }}" data-poster="{{ $videoPoster }}" data-play="play-content-{{ $id }}" data-autoplay="{{ isset($autoPlay) && $autoPlay == true ? 'true' : 'false' }}" data-mute-btn="{{ isset($muteBtn) && $muteBtn == true ? 'true' : 'false' }}">
		<a name="video-section"></a>
		<div id="video_{{ $id }}" class="video-content"></div>
	</div>
	<button id="play" class="play-content-{{ $id }} {{ isset($adjustPlayBtnTop) && $adjustPlayBtnTop == true ? 'adjust-play-btn-top' : '' }} {{ isset($hidePlayBtn) && $hidePlayBtn == true ? 'hide-play-btn' : '' }}" title="Play">
		<div class="play-icon">
			<svg viewBox="0 0 20 20" preserveAspectRatio="xMidYMid" tabindex="-1">
				<polygon points="1,0 20,10 1,20"></polygon>
			</svg>
		</div>
	</button>
