@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll about-background-image-pre">
		<div class="columns span-12 full-height fixed-background background-scroll about-background-image">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">ARTICLE <span class="number-wang">10</span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 white-background">
		<div class="row full">
			<div class="columns span-10 before-1 after-1 medium-12 medium-before-0 medium-after-0 small-12 small-before-0 small-after-0 button-group">
				<div class="button active" data-id="1">Business</div><div class="button" data-id="2">Opportunities</div>
			</div>
		</div>

		<div class="row full blocks-outer">
			<div class="intro-block active" data-id="1">
				<div class="columns span-10 before-1 after-1 intro animatedParent animateOnce delay-1000" data-sequence="100">
					<h1 class="purple">Making connections</h1>
					<h2>(It&rsquo;s something to do with playing with Lego as kids)</h2>
					<p><span class="animated slower fadeIn" data-id="1">We</span> connect <span class="animated slower fadeIn" data-id="2">with you to understand your needs and</span> goals. <span class="animated slower fadeIn" data-id="3">We</span> connect <span class="animated slower fadeIn" data-id="4">to your</span> story <span class="animated slower fadeIn" data-id="5">and tell it in</span> your voice.</p>
					<p class="mt0"><span class="animated slower fadeIn" data-id="6">Our teams</span> connect <span class="animated slower fadeIn" data-id="7">across</span> digital, video, exhibitions, print and presentations <span class="animated slower fadeIn" data-id="8">to deliver</span> integrated campaigns <span class="animated slower fadeIn" data-id="9">across</span> devices and platforms that drive engagement, sales and understanding.</p>
					<p><span class="animated slower fadeIn" data-id="10">So why not</span> connect with us <span class="animated slower fadeIn" data-id="11">to discover how we can</span> connect <span class="animated slower fadeIn" data-id="12">you</span> to your audience?</p>
				</div>
			</div>
			<div class="intro-block" data-id="2">
				<div class="columns span-10 before-1 after-1 intro">
					<h1 class="purple">An astute, authentic, adventurous team with a wealth of experience.</h1>
					<p>Article 10 believes that the key to game-changing marketing is having fun while you do it. Our close knit team of specialists, from account management to creative and beyond, love the work they do and work as one to deliver integrated marketing to some of the world&rsquo;s biggest brands and market-leading organisations. With buzzing offices in London and Amsterdam, we are always talking, always plotting the next big thing, always smiling. We&rsquo;re focused on providing the best results for our clients in a truly collaborative way, which is why we&rsquo;ve been associated with such big names for so many years.</p>

					<p>Sound like a team you could get on board with? Why not get in touch for a chat?</p>

				</div>
			</div>
		</div>
	</div>


	@include('main._partials._quotes', ['colour' => 'white'])

	<div class="columns span-12 grid-wall">
		<div class="row full">
			@include('main._partials._shim_4x4', ['class' => 'bg'])

			@include('main._partials._shim_2x8', ['class' => 'bg-tall'])

			<img src="/images/about-landing-page/image-1.jpg" class="item p1"/>
			<img src="/images/about-landing-page/image-2.jpg" class="item p2"/>
			<img src="/images/about-landing-page/image-3.jpg" class="item p3"/>
			<img src="/images/about-landing-page/image-4.jpg" class="item p4"/>
			<img src="/images/about-landing-page/image-5.jpg" class="item p5"/>
			<img src="/images/about-landing-page/image-6.jpg" class="item p6"/>
			<img src="/images/about-landing-page/image-7.jpg" class="item large p7"/>
			<img src="/images/about-landing-page/image-8.jpg" class="item large p8"/>
			<img src="/images/about-landing-page/image-9.jpg" class="item p9"/>
			<img src="/images/about-landing-page/image-10.jpg" class="item p10"/>
		</div>
	</div>


@endsection
