@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(images/terms-landing-page/terms-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(images/terms-landing-page/terms-landing-page-header@1x.jpg);">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">WAITING TO HEAR FROM YOU</span>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('content')
	<div class="columns span-12 intro">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<p class="mt0">Got a creative itch you need us to scratch? Want to discuss your next project or simply find out more about what we do? It all starts with a quick conversation by phone or email.</p>
				<p>Let us know a little about what you&rsquo;re after using the form below and a member of the team will be in touch sooner than you would expect.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 contact-form">
		<a name="form"></a>
		<div class="row">
			@if (isset($response) && $response != '')
				<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
					<div class="response success">{{{ $response['message'] }}}</div>
				</div>
			@endif

			{!! Form::open(array('url' => 'contact#form', 'method' => 'post')) !!}
			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::label('name', 'Who are you?') !!}
				<div class="half left">
					{!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'full']) !!}
					{!! $errors->first('name', '<small class="error">:message</small>') !!}
				</div>
				<div class="half right">
					{!! Form::text('company', null, ['placeholder' => 'Company', 'class' => 'full']) !!}
					{!! $errors->first('company', '<small class="error">:message</small>') !!}
				</div>
			</div>

			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::label('number', 'Where can we reach you?') !!}
				<div class="half left">
					{!! Form::text('number', null, ['placeholder' => 'Number', 'class' => 'full']) !!}
					{!! $errors->first('number', '<small class="error">:message</small>') !!}
				</div>
				<div class="half right">
					{!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'full']) !!}
					{!! $errors->first('email', '<small class="error">:message</small>') !!}
				</div>

			</div>

			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::label('requirement', 'What do you need?') !!}
				{!! Form::text('requirement', null, ['placeholder' => 'Project, Vacancies etc.', 'class' => 'full']) !!}
				{!! $errors->first('requirement', '<small class="error">:message</small>') !!}

			</div>
			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::label('when', 'When do you need it?') !!}
				{!! Form::select('when', array('Yesterday' => 'Yesterday', 'A week' => 'A week', 'A couple of weeks' => 'A couple of weeks', 'A month' => 'A month', 'We&rsquo;ve got time' => 'We&rsquo;ve got time', 'No time frame' => 'No time frame'), 'Couple of weeks', ['class' => 'full']) !!}
				{!! $errors->first('when', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::label('extra', 'Anything else?') !!}
				{!! Form::textarea('extra', null, ['placeholder' => 'Feel free to provide further information here...', 'class' => 'full']) !!}
				{!! $errors->first('extra', '<small class="error">:message</small>') !!}
			</div>
			<div class="columns span-6 before-3 after-3 large-8 large-before-2 large-after-2 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 formRow">
				{!! Form::submit('Let&rsquo;s Go!', ['class' => 'button']) !!}
			</div>

			{!! Form::close() !!}
		</div>
	</div>

	<div class="columns span-12 address">
		<a name="offices"></a>
		<div class="row">
			<div class="columns span-10 before-1 after-1 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
				<div class="row full divide">
					<div class="columns span-5 before-1 medium-12 medium-before-0 small-12 small-before-0">
						<div class="row full">
							<div class="columns span-11 before-1 medium-before-0 small-before-0">
								<h1>LONDON</h1>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1 before-1 medium-before-0 small-before-0">
								<img src="/images/contact-landing-page/icon-phone.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Number</h2>
							</div>
							<div class="columns span-6">
								<h2>+44 207 749 4450</h2>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1 before-1 medium-before-0 small-before-0">
								<img src="/images/contact-landing-page/icon-globe.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Address</2>
							</div>
							<div class="columns span-6">
								<h2>Ground Floor,<br/>Central House,<br/>142 Central Street,<br/>London,<br/>EC1V 8AR</h2>
							</div>
						</div>
					</div>

					<!-- <div class="columns span-4 medium-12 small-12">
						<div class="row full">
							<div class="columns span-11 after-1 medium-after-0 small-after-0">
								<h1>READING</h1>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1">
								<img src="/images/contact-landing-page/icon-phone.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Number</h2>
							</div>
							<div class="columns span-6 after-1 medium-after-0 small-after-0">
								<h2>+44 118 949 7007</h2>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1">
								<img src="/images/contact-landing-page/icon-globe.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Address</h2>
							</div>
							<div class="columns span-6 after-1 medium-after-0 small-after-0">
								<h2>200 Brook Drive,<br/>Green Park,<br/>Reading,<br/>RG2 6UB</h2>
							</div>
						</div>
					</div> -->

					<div class="columns span-5 after-1 medium-12 medium-after-0 small-12 small-after-0">
						<div class="row full">
							<div class="columns span-11 before-1 medium-before-0 small-before-0">
								<h1>AMSTERDAM</h1>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1 before-1 medium-before-0 small-before-0">
								<img src="/images/contact-landing-page/icon-phone.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Number</h2>
							</div>
							<div class="columns span-6">
								<h2>+31 20 893 2405</h2>
							</div>
						</div>
						<div class="row full">
							<div class="columns span-1 before-1 medium-before-0 small-before-0">
								<img src="/images/contact-landing-page/icon-globe.svg" class="icon" data-no-retina />
							</div>
							<div class="columns span-4">
								<h2>&nbsp;Address</h2>
							</div>
							<div class="columns span-6">
								<h2>Weesperstraat 61,<br/>1018 VN Amsterdam</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 map">
		<a name="map"></a>
		<div class="row full">
			<div class="columns span-12">
				<h3>Pick an Office</h3>
				<ul class="pick">
					<li class="active" data-id="0">London</li>
					<!-- <li>|</li>
					<li data-id="1">Reading</li> -->
					<li>|</li>
					<li data-id="2">Amsterdam</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="columns span-12 canvas">
		<div class="row full">
			<div id="gMap" class="columns span-12"></div>
		</div>
	</div>

@endsection
