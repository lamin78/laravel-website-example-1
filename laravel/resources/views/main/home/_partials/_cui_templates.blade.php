<script type="text/x-template" id="cui-header-template">
  <div id="cui_header">
    <div class="hamburger-menu">
    	<img src="/images/shared/menu-open.svg" class="button open" data-no-retina />
    	<img src="/images/shared/menu-close.svg" class="button close" data-no-retina />
    </div>
    <div class="tel-icon hidden-xlarge hidden-xxlarge">
    	<a href="tel:+44207749 4450"><img src="/images/shared/phone-icon-fs8.png"></a>
    </div>

    <a href="/">
      <img src="/images/cui/logo-cube.png" class="logo" data-no-retina/>
    </a>

    <div class="logo-text">
      <a href="/">
        <img src="/images/cui/logo-text.png" class="logo-text" data-no-retina/>
      </a>
    </div>

    <img src="/images/shared/menu-open.svg" class="button cui-menu open hidden-small hidden-medium hidden-large" @click="showAction" data-no-retina />

    <div class="mobile-menu">
    	<menu>
    		<li class="<?php echo ($section == 'home') ? 'active' : ''; ?>"><a href="/">Home</a></li>
    		<li class="<?php echo ($section == 'projects') ? 'active' : ''; ?>"><a href="/projects">Projects</a></li>
    		<li class="<?php echo ($section == 'services') ? 'active' : ''; ?>"><a href="/services">Services</a></li>
    		<li class="<?php echo ($section == 'about') ? 'active' : ''; ?>"><a href="/about">About</a></li>
    		<li class="<?php echo ($section == 'social') ? 'active' : ''; ?>"><a href="/social">Social</a></li>
    		<li class="<?php echo ($section == 'contact') ? 'active' : ''; ?>"><a href="/contact">Contact</a></li>
    	</menu>
    	<ul class="social hidden-small hidden-medium hidden-large">
    		<li><a href="" target="_blank"><img src="/images/shared/social-twitter.svg" alt="Twitter" title="Twitter" data-no-retina /></a></li>
    		<li><a href="" target="_blank"><img src="/images/shared/social-linkedin.svg" alt="LinkedIn" title="LinkedIn" data-no-retina /></a></li>
    	</ul>
    </div>
    <div class="nav-inner">
    	<div class="info">+44 207&shy; 749 4450 | <a href="mailto:hello@article10.com">hello@article10.com</a></div>
    	<menu>
    		<li class="<?php echo ($section == 'contact') ? 'active' : ''; ?>"><a href="/contact">Contact</a></li>
    		<li class="<?php echo ($section == 'social') ? 'active' : ''; ?>"><a href="/social">Social</a></li>
    		<li class="<?php echo ($section == 'about') ? 'active' : ''; ?>"><a href="/about">About</a></li>
    		<li class="<?php echo ($section == 'services') ? 'active' : ''; ?>"><a href="/services">Services</a></li>
    		<li class="<?php echo ($section == 'projects') ? 'active' : ''; ?>"><a href="/projects">Projects</a></li>
    		<li class="<?php echo ($section == 'home') ? 'active' : ''; ?>"><a href="/">Home</a></li>
    	</menu>
    </div>
  </div>
</script>

<script type="text/x-template" id="text-block-template">
  <div id="text_block" :class="textStyle" :style="{opacity: textOpacity}">
    <div class="cui_inner display-inline max-width full-max-width-mobile" :style="{float: textBoxAlignment}">
      <div class="cui-wrap" v-if="cuiType == 'cui_text'">
        <p>@{{cuiText}}</p>
      </div>
      <div class="user-wrap" v-if="cuiType == 'user_text'">
        <p>@{{cuiText}}</p>
      </div>
    </div>
  </div>
</script>

<script type="text/x-template" id="loading-cui-text-template">
  <div id="loading_cui_text" class="columns small-up-12">
    <div class="cui_inner no-padding">
      <div class="loading-block" v-if="typing === true">
        <ul>
          <li>•</li>
          <li>•</li>
          <li>•</li>
        </ul>
      </div>
    </div>
  </div>
</script>

<script type="text/x-template" id="button-group-template">
  <div id="button_group" class="columns small-up-12">
    <div class="cui_inner">
      <div class="button-wrap" v-if="this.optionData.length > 0 || this.backTrack==true">
        <ul>
          <li v-for="option in optionData">
            <button v-if="option.type=='button'" :data-id-out="option.out_id" @click="sendChoice">@{{ option.option_text }}</button>
            <div class="inputWrap" v-if="option.type=='input'">
              <input class="inputField" :data-id-out="option.out_id" :placeholder="optionText(option.option_text)" type="text" @keyup.enter="submit">
              <span @click="submit"></span>
            </div>
            <a class="cui-link" :href="parseLink(option.link_text)" target="_blank" v-if="option.type=='link'">@{{ option.option_text }}</a>
          </li>
          <li>
            <button v-if="this.backTrack==true" data-id-out="46" @click="sendChoice">Ok, thanks!</button>
          </li>
        </ul>
      </div>
    </div>
  </div>
</script>
