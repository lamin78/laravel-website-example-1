@extends('main.layouts.main')


@section('header')

	@if($show)
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-color: #000; background-image:url(/images/cui/cui-background-image@0.3x.jpg);">
			<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(/images/cui/cui-background-image.jpg);">
				<div class="columns span-12 full-height">
					<div class="black-out-layer"></div>
					<div class="row full full-height nav-outer">
						<div id="conversation_ui" class="cui-wrap">
							<div class="columns span-10 before-1 after-1 nav lift-up-header">
								<cuiheaderblock></cuiheaderblock>
							</div>
							<div class="columns small-up-10 small-up-before-1 small-up-after-1 large-up-8 large-up-before-2 large-up-after-2 cui-block">
								<div class="row full">
									<div class="inner-cui">
										<cuitextblock v-for="(item, index) in cui_data" :key="index" :cui-type="item.type" :cui-text="item.text"></cuitextblock>
									</div>
									<div class="cui-bottom-section">
										<div class="row full">
											<cuiloading :typing="typing"></cuiloading>
											<cuiinputgroup :option-data="cui_options" :back-track="backTrack"></cuiinputgroup>
										</div>
									</div>
								</div>
							</div>
							<svg version="1.1" xmlns='http://www.w3.org/2000/svg'>
					      <filter id='blur'>
					        <feGaussianBlur stdDeviation='6' />
					      </filter>
					    </svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="columns span-12 full-height fixed-background background-scroll home-background-image-pre">
			<div class="columns span-12 full-height fixed-background background-scroll home-background-image">
				<div class="columns span-12 full-height">
					<div class="black-out-layer"></div>

					@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

					<div class="row full full-height nav-outer">
						<div class="columns span-10 before-1 after-1 nav">
							@include('main.layouts.partials._header')
						</div>
						<div class="we-are-block animatedParent animateOnce go">
							<div class="tbl">
								<div class="inner">
									<!-- WE ARE<br/> //-->
									<span class="hilite animated growIn">Astute. Authentic. Adventurous.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection


@section('content')
	<script>
		var source = "{{ $source }}";
	</script>
	<div class="columns span-12 services services-header">
		<h1>Main Content</h1>
	</div>

	@include('main.home._partials._cui_templates')

@endsection
