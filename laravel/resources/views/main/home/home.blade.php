@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll home-background-image-pre">
		<div class="columns span-12 full-height fixed-background background-scroll home-background-image">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								<!-- WE ARE<br/> //-->
								<span class="hilite animated growIn">Astute. Authentic. Adventurous.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 white-background">
		<div class="row">
			<div class="columns span-10 before-1 after-1 intro">
				<h1>Hello, we&rsquo;re Article 10.</h1>
				<p>Astute. Authentic. Adventurous. The three words that define us. Just like the way we work, we sidestep traditional clich&eacute;s like &ldquo;passionate&rdquo;, &ldquo;dedicated&rdquo; and &ldquo;modern&rdquo; (if we weren&rsquo;t all of those things we wouldn&rsquo;t be in this business) to ensure that everything we produce widens eyes, quickens pulses and breaks the mould.&nbsp;</p>
				<p>We&rsquo;re an experienced, integrated full-service marketing and communications agency and we want to blow your mind with our capabilities&hellip;</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 services services-header">
		<h1>Our Services</h1>
	</div>

	<div class="columns span-12 services">

		<div class="carousel-service row full noselect">
			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/presentation-solutions" style="display:block; color:#fff;">
					<div class="pane pane-qtr presentations">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/presentations-icon.svg" src="/images/services-landing-page/presentations-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Presentations</h3>
						<p>PowerPoint or Prezi, internal or external, automated, animated, interactive, with speaker notes, video or all of the above. Whatever you want to present, however you want to present it, we can help, with the full range of presentation design services, starting with copywriting and structure all the way through to artwork, design and animation.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/presentation-solutions" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>

			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/video-animation-3d" style="display:block; color:#fff;">
					<div class="pane video">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/video-icon.svg" src="/images/services-landing-page/video-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Video, Animation &amp; 3D</h3>
						<p>Telling stories in the most engaging way is what we&rsquo;re all about. That&rsquo;s why brands turn to us when they need captivating video content like motion graphics, glorious 3D animation, skilfully filmed and edited video spots, viral videos or informative talking heads.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/video-animation-3d" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>

			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/digital-solutions" style="display:block; color:#fff;">
					<div class="pane digital-solutions">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/digital-icon.svg" src="/images/services-landing-page/digital-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Digital</h3>
						<p>Across screens of all sizes we can design digital solutions that fulfil your needs. Whether you need an app, a fully responsive web presence, eLearning course, immersive experience or a full-scale corporate website, we provide the cutting edge design, technology and rock solid performance to suit.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/digital-solutions" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>

			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/campaigns" style="display:block; color:#fff;">
					<div class="pane campaigns">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/campaigns-icon.svg" src="/images/services-landing-page/campaigns-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Campaigns</h3>
						<p>Whether you want to deliver your message internally or externally, online or in person, our talented team of writers and designers can turn your ideas into a wide range of collateral. With posters, banners, brochures, books and branding just a handful of the options available, our output is always powerful, always professional.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/campaigns" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>

			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/exhibitions" style="display:block; color:#fff;">
					<div class="pane exhibitions">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/exhibitions-icon.svg" src="/images/services-landing-page/exhibitions-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Exhibitions</h3>
						<p>Whatever your sector, however large your requirement, our 30 years of combined event experience has provided us with specialist exhibition knowledge. Across sports media, property and retail and many other sectors, we know the organisers, venues and shows and are ready to help you stand out.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/exhibitions" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>

			<div class="columns span-4 xxlarge-4 xlarge-4 large-6 medium-6 small-12 box-spacing">
				<a href="/services/information-security" style="display:block; color:#fff;">
					<div class="pane infosec">
						<div class="head">
							<span>
								<svg width="100%" height="100px">
									<image class="icon" xlink:href="/images/services-landing-page/infosec-icon.svg" src="/images/services-landing-page/infosec-icon.png" width="100%" height="100%" />
								</svg>
							</span>
						</div>
						<h3>Information<br/>Security</h3>
						<p>While you may know many of the risks associated with information security, do your employees? We deliver engaging information security awareness campaigns to some of the largest organisations in the world, transforming their staff into information security ambassadors and limiting the threat of cyber crime.</p>
						<span class="button fom">Click here to find out more&hellip;</span>
						<!-- <a href="/services/information-security" class="button fom">Click here to find out more&hellip;</a> //-->
					</div>
				</a>
			</div>
		</div>
	</div>

	@include('main._partials._case_study', [
		'page' => 'home',
		'panMove'	=> true,
		'fixMaxHeight' => true,
	])

	@include('main._partials._clients')

@endsection
