<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />

		@include('main.layouts.partials._metatags')

		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ isset($metaData['meta-title']) ? $metaData['meta-title'] : 'Article 10'  }}</title>

		<link href='https://fonts.googleapis.com/css?family=Raleway:400,900,700,300|Open+Sans:300,600' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/css/main/styles.css?{!!getenv('CACHE_BUSTER')!!}"/>

		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{ $pageViewCSS }}.css?{!!getenv('CACHE_BUSTER')!!}"/>
		@endif

		@if (isset($cmsPageViewCSS) && $cmsPageViewCSS === true)
			<link media="all" rel="stylesheet" type="text/css" href="/css/cms_style/{!!$cmsData->id!!}">
		@endif

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/main/animations-ie-fix.css">
		<![endif]-->

	</head>

	<body>
		<a name="top"></a>
		<div id="mainWrapper">

			<div id="headerWrapper">
				<header class="row full">
					@yield('header')
				</header>
				<img src="/images/shared/down-arrow.svg" class="down-arrow" id="headDown" data-no-retina />
			</div>

			<div id="contentWrapper">
				<section class="row full grey-background">
					<a name="content-section"></a>
					@yield('content')
					@include('main.layouts.partials._footer')
				</section>
			</div>
		</div>

		<script>
			var site_url = "{{ $site_url }}";

			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif

			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif

			@if (isset($corouselArray))
				var corouselArray = {!! json_encode($corouselArray) !!};
			@endif

			@if (isset($corouselArray))
				var caseStudies = {!! json_encode($caseStudies) !!};
			@endif

			@if (isset($carouselCMSArray))
				var carouselCMSArray = {!! json_encode($carouselCMSArray) !!};
			@endif

			@if (isset($clientsArray))
				var clientsArray = {!! json_encode($clientsArray) !!};
			@endif

		</script>

		{!! HTML::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main-min']) !!}

		@if (App::environment() == 'production')

			<!-- Lead Forensics -->
			<script type="text/javascript" src="/js/main/lead-custom-load-min.js"></script>

			<!-- Google Analytics -->
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-1808135-1', 'auto');
				ga('send', 'pageview');
			</script>

		@endif
	</body>
</html>
