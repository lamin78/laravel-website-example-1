	<div class="columns span-12 footer">
		<div class="row">
			<div class="columns large-up-6 large-up-before-1 hidden-small hidden-medium">
				<p class="mt0"><b>Visit us at:</b></p>
				<div class="row full">
					<div class="columns small-up-12 small-up-after-0 large-5 large-after-1 xlarge-3 xlarge-after-1 xxlarge-3 xxlarge-after-1">
						<!-- <p>The Penthouse,<br/>23 - 28 Penn Street,<br/>London<br/>N1&nbsp;5DL<br/>+44&nbsp;207&nbsp;749&nbsp;4450</p> -->
						<p>Ground Floor,<br/>Central House,<br/>142 Central Street,<br/>London,<br/>EC1V&nbsp;8AR<br/>+44&nbsp;207&nbsp;749&nbsp;4450</p>
					</div>

					<!-- <div class="columns small-up-12 small-up-after-0 large-5 large-after-1 xlarge-3 xlarge-after-1 xxlarge-3 xxlarge-after-1">
						<p>200 Brook Drive,<br/>Green Park,<br/>Reading,<br/>RG2&nbsp;6UB<br/>+44&nbsp;118&nbsp;949&nbsp;7007</p>
					</div> -->

					<div class="columns small-up-12 small-up-after-0 large-12 xlarge-3 xlarge-after-1 xxlarge-3 xxlarge-after-1">
						<p>Weesperstraat 61,<br/>1018 VN Amsterdam<br/>+31&nbsp;20&nbsp;893&nbsp;2405</p>
					</div>
				</div>
			</div>

			<div class="columns large-up-4 large-up-before-1 hidden-small hidden-medium">
				<div class="row">
					<div class="columns large-12 xlarge-up-5 xlarge-up-after-1 email">
						<p class="mt0"><b>Email us at:</b></p>
						<p><a href="mailto:hello@article10.com">hello@article10.com</a></p>
					</div>

					<div class="columns large-12 xlarge-up-5 xlarge-up-after-1">
						@include('main.layouts.partials._social_links')
					</div>
				</div>
			</div>

			<div class="columns small-up-10 small-up-before-1 hidden vis-small vis-medium mb1">
				<a href="/contact#form" class="footer-button left">Get in touch</a>
				<a href="/contact#map" class="footer-button right">Come and visit</a>
			</div>

			<div class="small-up-12 small-up-after-0 columns large-up-2 large-up-before-0 large-up-after-0 hidden-large hidden-xlarge hidden-xxlarge">
				@include('main.layouts.partials._social_links')
			</div>

		</div>
	</div>
