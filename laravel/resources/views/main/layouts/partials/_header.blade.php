<div class="hamburger-menu">
	<img src="/images/shared/menu-open.svg" class="button open" data-no-retina />
	<img src="/images/shared/menu-close.svg" class="button close" data-no-retina />
</div>
<div class="tel-icon hidden-xlarge hidden-xxlarge">
	<a href="tel:+44207749

	4450"><img src="/images/shared/phone-icon-fs8.png"></a>
</div>
<a href="/"><img src="/images/shared/article10-logo@2x.png" class="logo" data-no-retina/></a>
<div class="mobile-menu">
	<menu>
		<li class="<?php echo ($section == 'home') ? 'active' : ''; ?>"><a href="/">Home</a></li>
		<li class="<?php echo ($section == 'projects') ? 'active' : ''; ?>"><a href="/projects">Projects</a></li>
		<li class="<?php echo ($section == 'services') ? 'active' : ''; ?>"><a href="/services">Services</a></li>
		<li class="<?php echo ($section == 'about') ? 'active' : ''; ?>"><a href="/about">About</a></li>
		<li class="<?php echo ($section == 'social') ? 'active' : ''; ?>"><a href="/social">Social</a></li>
		<li class="<?php echo ($section == 'contact') ? 'active' : ''; ?>"><a href="/contact">Contact</a></li>
	</menu>
	<ul class="social hidden-small hidden-medium hidden-large">
		<li><a href="" target="_blank"><img src="/images/shared/social-twitter.svg" alt="Twitter" title="Twitter" data-no-retina /></a></li>
		<li><a href="" target="_blank"><img src="/images/shared/social-linkedin.svg" alt="LinkedIn" title="LinkedIn" data-no-retina /></a></li>
	</ul>
</div>

<div class="nav-inner">
	<div class="info">+44 207&shy; 749 4450 | <a href="mailto:hello@article10.com">hello@article10.com</a></div>
	<menu>
		<li class="<?php echo ($section == 'contact') ? 'active' : ''; ?>"><a href="/contact">Contact</a></li>
		<li class="<?php echo ($section == 'social') ? 'active' : ''; ?>"><a href="/social">Social</a></li>
		<li class="<?php echo ($section == 'about') ? 'active' : ''; ?>"><a href="/about">About</a></li>
		<li class="<?php echo ($section == 'services') ? 'active' : ''; ?>"><a href="/services">Services</a></li>
		<li class="<?php echo ($section == 'projects') ? 'active' : ''; ?>"><a href="/projects">Projects</a></li>
		<li class="<?php echo ($section == 'home') ? 'active' : ''; ?>"><a href="/">Home</a></li>
	</menu>
</div>
