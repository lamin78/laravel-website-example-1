<!-- Meta Tags
	================================================== -->

<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />

<meta name="description" content="{{ isset($metaData['meta-description']) ? $metaData['meta-description'] : ''  }}">
<meta name="keywords" content="{{ isset($metaData['meta-keywords']) ? $metaData['meta-keywords'] : ''  }}">
<meta name="keyphrases" content="{{ isset($metaData['meta-keyphrases']) ? $metaData['meta-keyphrases'] : ''  }}">

<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Language" content="en-gb, English">
<meta http-equiv="Content-Style-Type" content="text/css">

<meta name="publisher" content="Article 10">
<meta name="copyright" content="Article 10">
@if (isset($noFollow) && $noFollow === true)
  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
@else
  <meta name="robots" content="INDEX, FOLLOW">
@endif
<meta name="classification" content="GENERAL">
<meta name="distribution" content="global">

<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
