					<ul class="social">
						<li><a href="https://twitter.com/article10" target="_blank"><img src="/images/shared/social-twitter.svg" alt="Twitter" title="Twitter" data-no-retina /></a></li>
						<li><a href="https://www.linkedin.com/company/370727?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A131247471449832535307%2CVSRPtargetId%3A370727%2CVSRPcmpt%3Aprimary" target="_blank"><img src="/images/shared/social-linkedin.svg" alt="LinkedIn" title="LinkedIn" data-no-retina /></a></li>
					</ul>
					<p class="copyright">&copy; Article 10 Integrated&nbsp;Marketing&nbsp;Ltd&nbsp;{{ \Carbon\Carbon::now()->format('Y') }}.<br class}"/> All Rights Reserved.<br/></p>
					<p class="copyright"><a href="/terms-of-business">Terms of Business</a>
					<br/><a href="/privacy-notice">Privacy Notice</a></p>
