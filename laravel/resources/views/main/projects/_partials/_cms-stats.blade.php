@if(isset($cmsData->casestudy_stat_1) && isset($cmsData->casestudy_stat_2) && isset($cmsData->casestudy_stat_3))

	<div class="columns span-12 project-stats {{ $cmsData['casestudy_colour'] }}">
		<div class="row full">
			<div class="columns span-12 xxlarge-8 xxlarge-before-2 xxlarge-after-2 xlarge-8 xlarge-before-2 xlarge-after-2 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/cms-case-studies/{{ $imageDir }}/{{ $cmsData->casestudy_stat_1 }}" class="stat" data-no-retina />
					</div>
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/cms-case-studies/{{ $imageDir }}/{{ $cmsData->casestudy_stat_2 }}" class="stat" data-no-retina />
					</div>
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/cms-case-studies/{{ $imageDir }}/{{ $cmsData->casestudy_stat_3 }}" class="stat" data-no-retina />
					</div>
				</div>
			</div>
		</div>
	</div>

@endif
