@foreach($cmsData->order as $key => $order)

	@if(isset($order) && $key === 0)
		@if($order['data_type'] == 'text')
			<div class="columns span-12 project-intro {{ $cmsData['casestudy_colour'] }}">
				<div class="row full">
					@include('main.projects._partials._project_header', [
						'h1' => $cmsData->casestudy_header,
						'h2' => $cmsData->casestudy_sub_header,
						'p' => ( isset($cmsData->casestudy_client) && $cmsData->casestudy_client !== '' ? '<span>Client:</span>&nbsp;'.$cmsData->casestudy_client : '' ).( isset($cmsData->casestudy_year) && $cmsData->casestudy_year !== '' ? '&nbsp;&nbsp;<span>Year:</span>&nbsp;'.$cmsData->casestudy_year : '' ).( isset($cmsData->casestudy_location) && $cmsData->casestudy_location !== '' ? '&nbsp;&nbsp;<span>Location:</span>&nbsp;'.$cmsData->casestudy_location : '' ).( isset($cmsData->casestudy_category) && $cmsData->casestudy_category !== '' ? '&nbsp;&nbsp;<span>Category:</span>&nbsp;'.$cmsData->casestudy_category : '' ),
					])
					<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
						<h3>{!! \Helpers::parseText($cmsData, $order, 'header') !!}</h3>
						{!! \Helpers::parseText($cmsData, $order, 'content') !!}
					</div>
				</div>
			</div>
		@elseif(isset($order) && $order["data_name"] == "parallax")
			<div class="columns span-12 project-intro {{ $cmsData['casestudy_colour'] }}">
				<div class="row full">
					@include('main.projects._partials._project_header', [
						'h1' => $cmsData->casestudy_header,
						'h2' => $cmsData->casestudy_sub_header,
						'p' => ( isset($cmsData->casestudy_client) && $cmsData->casestudy_client !== '' ? '<span>Client:</span>&nbsp;'.$cmsData->casestudy_client : '' ).( isset($cmsData->casestudy_year) && $cmsData->casestudy_year !== '' ? '&nbsp;&nbsp;<span>Year:</span>&nbsp;'.$cmsData->casestudy_year : '' ).( isset($cmsData->casestudy_location) && $cmsData->casestudy_location !== '' ? '&nbsp;&nbsp;<span>Location:</span>&nbsp;'.$cmsData->casestudy_location : '' ).( isset($cmsData->casestudy_category) && $cmsData->casestudy_category !== '' ? '&nbsp;&nbsp;<span>Category:</span>&nbsp;'.$cmsData->casestudy_category : '' ),
					])
				</div>
			</div>
			<div class="columns span-12 parallax">
				<div class="bg {!! \Helpers::parseParallax($cmsData, $order) !!}" data-speed="10"></div>
			</div>
		@elseif(isset($order) && $order["data_name"] == "slider")
			<div class="columns span-12 project-intro {{ $cmsData['casestudy_colour'] }}">
				<div class="row full">
					@include('main.projects._partials._project_header', [
						'h1' => $cmsData->casestudy_header,
						'h2' => $cmsData->casestudy_sub_header,
						'p' => ( isset($cmsData->casestudy_client) && $cmsData->casestudy_client !== '' ? '<span>Client:</span>&nbsp;'.$cmsData->casestudy_client : '' ).( isset($cmsData->casestudy_year) && $cmsData->casestudy_year !== '' ? '&nbsp;&nbsp;<span>Year:</span>&nbsp;'.$cmsData->casestudy_year : '' ).( isset($cmsData->casestudy_location) && $cmsData->casestudy_location !== '' ? '&nbsp;&nbsp;<span>Location:</span>&nbsp;'.$cmsData->casestudy_location : '' ).( isset($cmsData->casestudy_category) && $cmsData->casestudy_category !== '' ? '&nbsp;&nbsp;<span>Category:</span>&nbsp;'.$cmsData->casestudy_category : '' ),
					])
				</div>
			</div>
			@include('main._partials._slider_container', [
				'before'	=> '/images/cms-case-studies/'.$cmsData->casestudy_slug.'/'. \Helpers::parseSlide($cmsData, $order, 1),
				'after'		=> '/images/cms-case-studies/'.$cmsData->casestudy_slug.'/'. \Helpers::parseSlide($cmsData, $order, 2)
			])
		@elseif(isset($order) && $order["data_name"] == "video")
			<div class="columns span-12 project-intro {{ $cmsData['casestudy_colour'] }}">
				<div class="row full">
					@include('main.projects._partials._project_header', [
						'h1' => $cmsData->casestudy_header,
						'h2' => $cmsData->casestudy_sub_header,
						'p' => ( isset($cmsData->casestudy_client) && $cmsData->casestudy_client !== '' ? '<span>Client:</span>&nbsp;'.$cmsData->casestudy_client : '' ).( isset($cmsData->casestudy_year) && $cmsData->casestudy_year !== '' ? '&nbsp;&nbsp;<span>Year:</span>&nbsp;'.$cmsData->casestudy_year : '' ).( isset($cmsData->casestudy_location) && $cmsData->casestudy_location !== '' ? '&nbsp;&nbsp;<span>Location:</span>&nbsp;'.$cmsData->casestudy_location : '' ).( isset($cmsData->casestudy_category) && $cmsData->casestudy_category !== '' ? '&nbsp;&nbsp;<span>Category:</span>&nbsp;'.$cmsData->casestudy_category : '' ),
					])
				</div>
			</div>
			<div class="columns span-12 black-background">
				@include('main._partials._video_container', [
					'id'	  	  => '1',
					'minHeight'   => 'auto',
					'videoFile'   => \Helpers::parseVideo($cmsData, $order, 'video'),
					'videoPoster' => \Helpers::parseVideo($cmsData, $order, 'poster'),
					'transparent' => true,
					'hidePlayBtn' => true,
					'autoPlay' 	  => false,
					'muteBtn'	  => true,
				])
			</div>
		@elseif(isset($order) && $order["data_name"] == "carousel")
			@include('main._partials._case_study', [
				'page' => 'project_cms',
				'panMove'	=> false,
				'no_orb_background'	=> true,
				'theme' => \Helpers::parseCarouselTheme($cmsData, $order),
				'ind'	=> \Helpers::parseCarousel(),
				'fixMaxHeightCms' => true,
			])
		@endif
	@endif

	@if(isset($order) && $order["data_name"] == "parallax" && $key > 0)
		<div class="columns span-12 parallax">
			<div class="bg {!! \Helpers::parseParallax($cmsData, $order) !!}" data-speed="10"></div>
		</div>
	@endif

	@if(isset($order) && $order["data_type"] == "text" && $key > 0)
		<div class="columns span-12 project-more">
			<div class="row">
				<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
					<h3>{!! \Helpers::parseText($cmsData, $order, 'header') !!}</h3>
					{!! \Helpers::parseText($cmsData, $order, 'content') !!}
				</div>
			</div>
		</div>
	@endif

	@if(isset($order) && $order["data_name"] == "slider" && $key > 0)
		@include('main._partials._slider_container', [
			'before'	=> '/images/cms-case-studies/'.$cmsData->casestudy_slug.'/'. \Helpers::parseSlide($cmsData, $order, 1),
			'after'		=> '/images/cms-case-studies/'.$cmsData->casestudy_slug.'/'. \Helpers::parseSlide($cmsData, $order, 2)
		])
	@endif

	@if(isset($order) && $order["data_name"] == "video" && $key > 0)
		<div class="columns span-12 black-background">
			@include('main._partials._video_container', [
				'id'	  	  	=> '1',
				'minHeight'   => 'auto',
				'videoFile'   => \Helpers::parseVideo($cmsData, $order, 'video'),
				'videoPoster' => \Helpers::parseVideo($cmsData, $order, 'poster'),
				'transparent' => true,
				'hidePlayBtn' => true,
				'autoPlay' 	  => false,
				'muteBtn'	  	=> true,
			])
		</div>
	@endif

	@if(isset($order) && $order["data_name"] == "carousel" && $key > 0)
		@include('main._partials._case_study', [
			'page' => 'project_cms',
			'panMove'	=> false,
			'no_orb_background'	=> true,
			'theme' => \Helpers::parseCarouselTheme($cmsData, $order),
			'ind'	=> \Helpers::parseCarousel(),
			'fixMaxHeightCms' => true,
		])
	@endif

@endforeach
