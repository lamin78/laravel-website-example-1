@if(isset($noProjectNav) && $noProjectNav === false )
	<div class="columns span-12 project-footer grey">
		<div class="row full">
			<div class="columns span-3 xxlarge-2 sml-6 project-button prev">
				<a href="{{ $nav_urls['prev'] }}">
					<!-- <i class="demo-icon icon-left-open"></i>
					<img src="/images/shared/arrow-left-white.svg" data-no-retina /> //-->
					<span class="left-arrow">Previous Project</span>
				</a>
			</div>
			<div class="columns span-3 xxlarge-2 sml-6 project-button next">
				<a href="{{ $nav_urls['next'] }}">
					<span class="right-arrow">Next Project</span>
					<!-- <img src="/images/shared/arrow-right-white.svg" data-no-retina /> //-->
				</a>
			</div>
			<div class="columns span-6 xxlarge-8 sml-12 middle">
				<a href="/contact#form"><span>Like what you see? Get in touch</span></a>
			</div>
		</div>
	</div>
@endif
