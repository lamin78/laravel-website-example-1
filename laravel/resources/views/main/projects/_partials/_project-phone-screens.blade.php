	<div class="columns span-12 phones animatedParent animateOnce">
		<div class="row full">
			<div class="columns span-2 before-1 small-10 small-before-1 small-after-1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-1.jpg" class="phone animated fadeIn delay-500"/>
			</div>
			<div class="columns span-2 before-2 small-10 small-before-1 small-after-1"">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-2.jpg" class="phone animated fadeIn"/>
			</div>
			<div class="columns span-2 before-2 after-1 small-10 small-before-1 small-after-1"">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-3.jpg" class="phone animated fadeIn delay-500"/>
			</div>
		</div>
	</div>