	<div class="columns span-12 project-stats {{ $colour }}">
		<div class="row full">
			<div class="columns span-12 xxlarge-8 xxlarge-before-2 xxlarge-after-2 xlarge-8 xlarge-before-2 xlarge-after-2 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/projects/{{ $imageDir }}/stat-1.svg" class="stat" data-no-retina />
					</div>
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/projects/{{ $imageDir }}/stat-2.svg" class="stat" data-no-retina />
					</div>
					<div class="columns span-4 xlarge-4 large-4 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<img src="/images/projects/{{ $imageDir }}/stat-3.svg" class="stat" data-no-retina />
					</div>
				</div>
			</div>
		</div>
	</div>