			<a name="video-section"></a>
			<div class="carousel-case row full {{ isset($increaseHeight) && $increaseHeight == true ? 'increase-height' : '' }} {{ isset($panMove) && $panMove == true ? 'pan-move' : '' }} {{ isset($clickSlide) && $clickSlide == true ? 'click-slide' : '' }} {{ isset($fullHeight) && $fullHeight == true ? 'full-screen-height' : '' }} {{ isset($autoStart) && $autoStart == true ? 'autoplay-banner' : '' }} {{ isset($noPaddingBottom) && $noPaddingBottom == true ? 'no-padding-bottom' : '' }}" id="{{ isset($idTag) ? $idTag : 'carousel2' }}">
				@if(isset($idTag) && $idTag == 'microsoftCarousel1')
					@foreach($bannerArray[0] as $item)	
						@include('main.projects._partials._project_banner_section._'.$item.'_section', ['page' => $item])
					@endforeach
				@elseif(isset($idTag) && $idTag == 'microsoftCarousel2')
					@foreach($bannerArray[1] as $item)	
						@include('main.projects._partials._project_banner_section._'.$item.'_section', ['page' => $item])
					@endforeach
				@else
					@foreach($bannerArray as $item)	
						@include('main.projects._partials._project_banner_section._'.$item.'_section', ['page' => $item])
					@endforeach
				@endif
			</div>