				<div class="pane">
					<div class="parallax layer2 {{ $imageDir }}-video_2">
						@include('main._partials._video_container', [
							'id'	  	  => '2', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[2]['video'], 
							'videoPoster' => $videoPageObj[2]['poster'],
							'transparent' => true,
						])
					</div>
				</div>