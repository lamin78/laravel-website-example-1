				<div class="pane">
					<div class="parallax layer1 {{ $imageDir }}-background_1"></div>
					<div class="parallax layer2 {{ $imageDir }}-video_1">
						@include('main._partials._video_container', [
							'id'	  	  => '1', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[0]['video'], 
							'videoPoster' => $videoPageObj[0]['poster']
						])
					</div>
				</div>