				<div class="pane">
					<div class="parallax layer2 {{ $imageDir }}-video_3">
						@include('main._partials._video_container', [
							'id'	  	  => '3', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[3]['video'], 
							'videoPoster' => $videoPageObj[3]['poster'],
							'transparent' => true,
						])
					</div>
				</div>