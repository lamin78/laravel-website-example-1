				<div class="pane">
					<div class="parallax layer1 microsoft-university-background"></div>
					<div class="parallax layer2 microsoft-university-foreground"></div>
					<div class="parallax layer3 case-study-text-block">
						<h3>Microsoft</h3>
						<p>University Campaign</p>
						<span class="tag">Campaign</span>
						<a href="/projects/old-uppingham" class="button view">View Case Study</a>
					</div>
				</div>