				<div class="pane">
					<div class="parallax layer2 stoli-background-video_1">
						@include('main._partials._video_container', [
							'id'	  	  => '1', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[1]['video'], 
							'videoPoster' => $videoPageObj[1]['poster'],
							'transparent' => true,
						])
					</div>
				</div>