				<div class="pane">
					<div class="parallax layer2 stoli-background-video_4">
						@include('main._partials._video_container', [
							'id'	  	  => '4', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[4]['video'], 
							'videoPoster' => $videoPageObj[4]['poster'],
							'transparent' => true,
						])
					</div>
				</div>