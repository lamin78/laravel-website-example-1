				<div class="pane">
					<div class="parallax layer2 stoli-background-video_5">
						@include('main._partials._video_container', [
							'id'	  	  => '5', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[5]['video'], 
							'videoPoster' => $videoPageObj[5]['poster'],
							'transparent' => true,
						])
					</div>
				</div>