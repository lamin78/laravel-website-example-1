				<div class="pane">
					<div class="parallax layer2 stoli-background-video_5">
						@include('main._partials._video_container', [
							'id'	  	  => '7', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[7]['video'], 
							'videoPoster' => $videoPageObj[7]['poster'],
							'transparent' => true,
						])
					</div>
				</div>