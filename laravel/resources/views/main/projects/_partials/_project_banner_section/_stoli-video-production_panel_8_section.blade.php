				<div class="pane">
					<div class="parallax layer2 stoli-background-video_5">
						@include('main._partials._video_container', [
							'id'	  	  => '8', 
							'minHeight'   => '100%', 
							'videoFile'   => $videoPageObj[8]['video'], 
							'videoPoster' => $videoPageObj[8]['poster'],
							'transparent' => true,
						])
					</div>
				</div>