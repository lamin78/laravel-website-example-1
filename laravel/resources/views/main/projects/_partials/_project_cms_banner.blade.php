			<a name="video-section"></a>
			<div class="carousel-case case-{{$ind}} row full
				{{ isset($increaseHeight) && $increaseHeight == true ? 'increase-height' : '' }}
				{{ isset($panMove) && $panMove == true ? 'pan-move' : '' }}
				{{ isset($clickSlide) && $clickSlide == true ? 'click-slide' : '' }}
				{{ isset($autoStart) && $autoStart == true ? 'autoplay-banner' : '' }}
				{{ isset($noPaddingBottom) && $noPaddingBottom == true ? 'no-padding-bottom' : '' }}"
				id="{{ $carouselCMSArray[$ind]['container'] }}">
				@foreach($carouselCMSArray[$ind]['imageArray'] as $item)
					@include('main._partials._case_studies._cms_carousel_section', ['imgRoute' => $carouselCMSArray[$ind]['imageRoute'], 'image' => $item])
				@endforeach
			</div>
