
<div class="columns span-10 before-1 after-1 sml-12 sml-before-0 sml-after-0 summary">
  <div class="row full">
    <div class="columns span-10 before-1 after-1 sml-10 sml-before-1 sml-after-1">
      <h1 class="h1-light">{!! $h1 !!}</h1>
      <h2>{!! $h2 !!}</h2>
    </div>
    <div class="columns span-10 before-1 after-1 sml-10 sml-before-1 sml-after-1 details">
      @if ($potm)
        <div class="potm">Project of the Month</div>
      @endif
      <p>{!! $p !!}</p>
    </div>
  </div>
</div>
