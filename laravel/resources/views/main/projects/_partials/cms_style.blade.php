.casestudy-header-small {

  background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! $headerImageSmall !!});
  background-size: cover !important;
}

.casestudy-header {
  background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! $headerImage !!});
  background-size: cover !important;
}

@media
  only screen and (-webkit-min-device-pixel-ratio: 2),
  only screen and (   min--moz-device-pixel-ratio: 2),
  only screen and (     -o-min-device-pixel-ratio: 2/1),
  only screen and (        min-device-pixel-ratio: 2),
  only screen and (                min-resolution: 192dpi),
  only screen and (                min-resolution: 2dppx) {
    .casestudy-header {
  		background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! $headerImageLarge !!});
  		background-size: cover !important;
     }
}


@foreach($parallaxArray as $key => $parallax)

  .{!! preg_replace('/.jpg|.JPG|.png|.PNG/i', '', $parallax->image) !!} {
    background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! $parallax->image !!});
    background-size: cover !important;
  }

  @media
    only screen and (-webkit-min-device-pixel-ratio: 2),
    only screen and (   min--moz-device-pixel-ratio: 2),
    only screen and (     -o-min-device-pixel-ratio: 2/1),
    only screen and (        min-device-pixel-ratio: 2),
    only screen and (                min-resolution: 192dpi),
    only screen and (                min-resolution: 2dppx) {
      .{!! preg_replace('/.jpg|.JPG|.png|.PNG/i', '', $parallax->image) !!} {
        background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! str_replace('.', '@2x.', $parallax->image) !!});
        background-size: cover !important;
      }
    }

@endforeach

@foreach($carouselArray as $key => $carousel)
  @foreach($carousel as $key => $image)

    .{!! preg_replace('/.jpg|.JPG|.png|.PNG/i', '', $image) !!} {
      background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! $image !!});
      background-size: cover !important;
      background-position-y: 50% !important;
    }

    @media
      only screen and (-webkit-min-device-pixel-ratio: 2),
      only screen and (   min--moz-device-pixel-ratio: 2),
      only screen and (     -o-min-device-pixel-ratio: 2/1),
      only screen and (        min-device-pixel-ratio: 2),
      only screen and (                min-resolution: 192dpi),
      only screen and (                min-resolution: 2dppx) {
        .{!! preg_replace('/.jpg|.JPG|.png|.PNG/i', '', $image) !!} {
          background-image: url(/images/cms-case-studies/{!! $imageDir !!}/{!! str_replace('.', '@2x.', $image) !!});
          background-size: cover !important;
          background-position-y: 50% !important;
        }
    }
  @endforeach
@endforeach
