@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll projects-background-image-pre">
		<div class="columns span-12 full-height fixed-background background-scroll">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>
				@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">PROUD</span><br/>
								OF OUR PROJECTS
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('content')
	<div class="columns span-12 projects-background-grey">
		<div class="row full grid-wall">
			
			@include('main._partials._shim_8x4', ['class' => ''])
			@include('main._partials._shim_7x5', ['class' => ''])
			@include('main._partials._shim_6x6', ['class' => ''])
			@include('main._partials._shim_3x12', ['class' => ''])
			@include('main._partials._shim_2x18', ['class' => ''])
			@include('main._partials._shim_1x20', ['class' => ''])

			<div class="item cw2 ch2 xxlc1 xxlr1 xlc1 xlr1 lc1 lr1 mc1 mr1 sc1 sr1 xsc1 xsr1 xsalt">
				<img src="/images/projects-landing-page/stoli-app.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/stoli-app-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/stoli-app-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/stoli-app-design" class="btn"></a>
			</div>

			<div class="item cw2 ch1 xxlc3 xxlr1 xlc3 xlr1 lc3 lr1 mc1 mr3 sc1 sr4 xsc1 xsr4 xsalt">
				<img src="/images/projects-landing-page/microsoft-presentation.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/microsoft-presentation-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/microsoft-presentation-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/microsoft-social-engagement" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc5 xxlr1 xlc5 xlr1 lc5 lr1 mc3 mr2 sc2 sr3 xsc1 xsr3">
				<img src="/images/projects-landing-page/openbet-grand-national.jpg" data-pct-size/>
				<img src="/images/projects-landing-page/openbet-grand-national-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/openbet-website-redesign" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc5 xxlr2 xlc7 xlr1 lc3 lr2 mc3 mr1 sc1 sr3 xsc1 xsr2">
				<img src="/images/projects-landing-page/etf-video.jpg" data-pct-size/>
				<img src="/images/projects-landing-page/etf-video-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/etf-advert" class="btn"></a>
			</div>

			<div class="item cw1 ch2 xxlc6 xxlr1 xlc6 xlr1 lc6 lr1 mc3 mr3 sc2 sr5 xsc1 xsr6 xsalt">
				<img src="/images/projects-landing-page/castrol-presentation.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/castrol-presentation-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/castrol-presentation-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/castrol-presentation" class="btn"></a>
			</div>

			<div class="item cw1 ch2 xxlc4 xxlr2 xlc7 xlr2 lc5 lr2 mc2 mr5 sc1 sr5 xsc1 xsr5 xsalt">
				<img src="/images/projects-landing-page/mipim.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/mipim-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/mipim-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/mipim-exhibition-design" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc5 xxlr3 xlc5 xlr3 lc4 lr2 mc1 mr5 sc1 sr8 xsc1 xsr8">
				<img src="/images/projects-landing-page/entertainment-one.jpg" data-pct-size/>
				<img src="/images/projects-landing-page/entertainment-one-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/entertainment-one-brand-toolkits" class="btn"></a>
			</div>

			<div class="item cw2 ch1 xxlc7 xxlr1 xlc4 xlr2 lc1 lr3 mc1 mr4 sc1 sr7 xsc1 xsr7 xsalt">
				<img src="/images/projects-landing-page/bdp-events.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/bdp-events-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/bdp-events-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/bdp-exhibition-design" class="btn"></a>
			</div>

			<div class="item cw1 xlcw2 mcw2 scw2 lcw2 ch1 xxlc5 xxlr4 xlc5 xlr4 lc5 lr4 mc1 mr7 sc1 sr10 xsc1 xsr11 xlalt lalt malt salt">
				<img src="/images/projects-landing-page/microsoft-universities-alt.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-universities.jpg" class="alt xlalt lalt malt salt" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-universities-overlay-alt.png" class="overlay hidden-small" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-universities-overlay.png" class="overlay alt xlalt lalt malt salt" data-pct-size/>
				<a href="/projects/microsoft-universities" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc6 xxlr3 xlc6 xlr3 lc6 lr3 mc1 mr6 sc1 sr9 xsc1 xsr9">
				<img src="/images/projects-landing-page/etf-torino-process.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/etf-torino-process-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/etf-torino-process" class="btn"></a>
			</div>

			<div class="item cw2 ch2 xxlc7 xxlr2 xlc3 xlr3 lc3 lr3 mc2 mr8 sc1 sr12 xsc1 xsr14 xsalt">
				<img src="/images/projects-landing-page/stoli-cocktails.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/stoli-cocktails-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/stoli-cocktails-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/stoli-video-production" class="btn"></a>
			</div>

			<div class="item cw1 ch2 xxlc1 xxlr3 xlc1 xlr3 lc1 lr4 mc3 mr5 sc2 sr8 xsc1 xsr10 xsalt">
				<img src="/images/projects-landing-page/openbet-omni.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/openbet-omni-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/openbet-omni-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/omni-channel" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc2 xxlr3 xlc2 xlr3 lc2 lr5 mc1 mr8 sc1 sr11 xsc1 xsr12">
				<img src="/images/projects-landing-page/mail-online.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/mail-online-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/mail-online-exhibition-design" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc3 xxlr2 xlc3 xlr2 lc2 lr4 mc1 mr9 sc2 sr11 xsc1 xsr13">
				<img src="/images/projects-landing-page/aarhus-2017.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/aarhus-2017-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/aarhus" class="btn"></a>
			</div>

			<div class="item cw2 ch1 xxlc2 xxlr4 xlc1 xlr5 lc4 lr5 mc1 mr11 mcw1 mch2 sc1 sr18 xsc1 xsr20 xsalt malt">
				<img src="/images/projects-landing-page/th-real-estate.jpg" class="regular" data-pct-size/>
				<!-- <img src="/images/projects-landing-page/th-real-estate-alt.jpg" class="alt xsalt" data-pct-size/> -->
				<img src="/images/projects-landing-page/th-real-estate-alt-tall.jpg" class="alt malt" data-pct-size/>
				<img src="/images/projects-landing-page/th-real-estate-overlay.png" class="overlay" data-pct-size/>
				<img src="/images/projects-landing-page/th-real-estate-alt-tall-overlay.png" class="alt malt overlay" data-pct-size/>
				<a href="/projects/thre-tablet-presentation" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc4 xxlr4 xlc2 xlr4 lcw2 lc6 lr5 mc1 mr10 mcw2 scw2 sc1 sr14 xsc1 xsr15 lalt malt salt">
				<img src="/images/projects-landing-page/thre-conference-presentation.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/thre-conference-presentation-alt.jpg" class="alt lalt malt salt" data-pct-size/>
				<img src="/images/projects-landing-page/thre-conference-presentation-overlay.png" class="overlay hidden-small" data-pct-size/>
				<img src="/images/projects-landing-page/thre-conference-presentation-alt-overlay.png" class="alt lalt malt salt overlay dblHeight" data-pct-size/>
				<a href="/projects/thre-office-environments" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc3 xxlr3 xlc7 xlr4 lc3 lr6 mc3 mr7 sc1 sr15 xsc1 xsr16">
				<img src="/images/projects-landing-page/microsoft-future-decoded.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-future-decoded-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/future-decoded-2015" class="btn"></a>
			</div>

			<div class="item cw1 xlcw2 lcw2 mcw2 scw2 ch1 xxlc6 xxlr4 xlc6 xlr5 lc1 lr6 mc2 mr12 sc1 sr17 xsc1 xsr19 xlalt lalt malt salt">
				<img src="/images/projects-landing-page/article10-bees.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/article10-bees-alt.jpg" class="alt xlalt lalt malt salt" data-pct-size/>
				<img src="/images/projects-landing-page/article10-bees-overlay.png" class="overlay hidden-small" data-pct-size/>
				<img src="/images/projects-landing-page/article10-bees-overlay-alt.png" class="overlay alt xlalt lalt malt salt" data-pct-size/>
				<a href="/projects/article10-bees" class="btn"></a>
			</div>

			<div class="item cw1 xlcw2 mcw2 scw2 lcw2 ch1 xxlc8 xxlr4 xlc3 xlr5 lc4 lr6 mc2 mr11 sc1 sr16 xsc1 xsr18 xlalt lalt malt salt">
				<img src="/images/projects-landing-page/microsoft-mva.jpg" class="alt xlalt lalt malt salt" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-mva-alt.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-mva-overlay-alt.png" class="overlay hidden-small" data-pct-size/>
				<img src="/images/projects-landing-page/microsoft-mva-overlay.png" class="overlay alt xlalt lalt malt salt" data-pct-size/>
				<a href="/projects/microsoft-mva" class="btn"></a>
			</div>

			<div class="item cw1 ch1 xxlc7 xxlr4 xlc5 xlr5 lc6 lr6 mc3 mr10 sc2 sr15 xsc1 xsr17">
				<img src="/images/projects-landing-page/thor-equities.jpg" class="regular" data-pct-size/>
				<img src="/images/projects-landing-page/thor-equities-overlay.png" class="overlay" data-pct-size/>
				<a href="/projects/thor-exhibition-design" class="btn"></a>
			</div>

		</div>
	</div>
@endsection
