@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background aarhus-background-image-pre">
		<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>Aarhus 2017</h1>
					<h2>External Promotional Campaign</h2>
					<a href="/services/campaigns" class="tag {{ $colour }}">Campaigns</a>
				</div>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Creatively Championing<br /> The European City Of Culture',
				'h2' => 'Aarhus, <span class="crossed-out">in the middle of</span> right up our street.',
				'p' => '<span>Client:</span>&nbsp;Aarhus 2017    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;Denmark    <span>Category:</span>&nbsp;Campaigns',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Aarhus, the second largest city in Denmark, was elected European Capital of Culture in 2017.</p>
				<h3>The Brief</h3>
				<p>Article 10 helped the Aarhus 2017 team promote the city and its achievements through a variety of media, to generate awareness among Danish people and the wider European population of the goals of its campaign.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-1.jpg"/>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Aarhus 2017 required a selection of print and digital collateral options as well as branding and copywriting services as part of its ongoing campaign. Our creative teams closely researched the city and its culture as well as the requirements of the European Capital of Culture in order to fully understand the project.</p>
				<h3>The Process</h3>
				<p>Article 10 has and continues to deliver a wide range of integrated marketing material for Aarhus in the lead up to its year in the spotlight. Engaging our copywriting, design and project management teams we are fully engaged with the campaign and prepared to deliver on-brand material quickly to support various stages of the city’s preparation.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site">
		<img class="stretch-mobile" src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-2.jpg"/>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>We have so far produced logos, a tagline, posters, roller banners, print adverts and social media imagery as part of an exciting, inspirational and on-going campaign.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
