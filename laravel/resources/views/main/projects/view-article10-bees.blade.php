@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll bees-background-image-pre">
		<div class="black-out-layer"></div>

		@include('main._partials._culture_video_top', [
			'mobile_poster' => $videoObj['mobile-poster']
			,'video_poster' => $videoObj['poster']
			,'video_file' 	=> $videoObj['video']
			,'auto_play' 		=> 'false'
		])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Article 10 Presents “Bees”</h1>
				<h2>Educational Prezi</h2>
				<a href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
			</div>
		</div>
		@include('main.layouts.partials._header_video_controls')
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Saving Bees With The Power Of Prezi',
				'h2' => 'Article 10 picks up buzz for its Prezi Award contender',
				'p' => '<span>Client:</span>&nbsp;Article 10    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Presentation',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Prezi, the cloud-based presentation tool, has almost 200 million presentations in its database and holds an annual competition celebrating the best of those produced by its global community.</p>
				<h3>The Brief</h3>
				<p>While Article 10 is synonymous with cutting-edge PowerPoint presentations, you may not know that we also deliver stunning Prezis too. Over the years we have produced engaging presentations for the likes of Parkinson&rsquo;s UK and the European Training Foundation and we&rsquo;ve also produced our own&hellip;
				<br />As part of the annual Prezi Awards for 2015, the Article 10 presentation studio decided to flex its creative muscle and create a Prezi for submission to the competition across a number of award categories.
				<br />But where to begin? Should our subject be hard hitting, light-hearted, informative or fun? In the end we found the perfect mixture of them all...bees.
				</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>During our research into potential subject matter we uncovered some quite remarkable facts about the effects caused by the decline of the bee population that many may not know about. For that reason, we decided to focus our submission on what is an overlooked but nevertheless important cause. </p>
				<p>Topic chosen, our design and copywriting teams began piecing together information and imagery to tell the story. We came up with a central character, Polly Nation, a cute worker bee, to &ldquo;bee&rdquo; our guide and began crafting a script to explain the cause and effect, plus solutions to the problem in a fun and engaging way. With the script in place, colourful assets were created and typography chosen to bring the narrative to life while the intricate process of linking scenes and assets with on screen text and voiceover was completed.</p>

				<h3>The Result</h3>
				<p>With a range of vibrant scenes, beautiful visual assets, professional voice acting and slick animation, the finished Prezi helped shine light on a cause that doesn&rsquo;t receive the attention it deserves. We encourage you to <a class="pink" href="http://prezi.com/h-pwtgtwpjzb/?utm_campaign=share&utm_medium=copy&rc=ex0share" target="_blank">check out this fun presentation</a> and, most importantly, help to save the bees!</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor not-mac">
					<div class="screen no-scrollbar">
						@include('main._partials._video_container', [
							'id'			=> '2',
							'transparent'	=> true,
							'minHeight' 	=> '100%',
							'videoFile'		=> $videoPageObj[1]['video'],
							'videoPoster'	=> $videoPageObj[1]['poster'],
							'adjustPlayBtnTop'	=> false,
						])
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
