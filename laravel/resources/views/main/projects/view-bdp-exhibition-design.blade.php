@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background bdp-background-image-pre">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>BDP</h1>
				<h2>Stand design and build</h2>
				<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'The Art Of The Exhibition With BDP',
				'h2' => 'An event stand where the centrepiece formed before attendees eyes',
				'p' => '<span>Client:</span>&nbsp;BDP    <span>Year:</span>&nbsp;2014    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Exhibitions',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>BDP is an architectural practice based in Clerkenwell, with creativity at its core.</p>
				<h3>The Brief</h3>
				<p>When the company attended the inaugural MIPIM UK, the UK's first dedicated property trade show, it wanted to create an exhibition space that was about more than simply providing a place for people to meet – and Article 10 helped make it happen.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Article 10 worked with BDP for the event at London&rsquo;s Olympia to design an exhibition environment that both supported and reinforced the company&rsquo;s culture of creativity. And what better way to do that than to watch an artist produce a beautiful, relevant backdrop to the stand.</p>

				<h3>The Process</h3>
				<p>Throughout the three-day show, a fascinating art installation developed before the eyes of attendees. Using only an iPad as a guide, BDP&#39;s artist in residence hand-drew buildings from throughout the company&rsquo;s history, creating a unique cityscape across an entire 6 linear metre wall.</p>
				<p>Article 10 worked with BDP&#39;s design team to produce the stand and provided a range of additional features. Functional elements, including an architectural model, designer furniture pieces, map graphic and 3D logo, all served to enhance BDP&rsquo;s creative message while increasing crucial delegate dwell time by establishing a stylish, comfortable and engaging environment.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>Culminating in a final &ldquo;reveal&rdquo;, the artwork made for a great talking point at the stand and helped generate conversations between BDP and UK developers, property investors, local authority personnel and other real estate professionals.</p>
				<p>And what of the final piece of art? Well it didn&rsquo;t go to waste. It now takes pride of place within BDP&rsquo;s Sheffield office, bringing the company&rsquo;s creative story full circle.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
