@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll castrol-presentation-background-image-pre">
		<div class="black-out-layer"></div>

		@include('main._partials._culture_video_top', [
			'mobile_poster' => $videoObj['mobile-poster']
			,'video_poster' => $videoObj['poster']
			,'video_file' 	=> $videoObj['video']
			,'auto_play' 		=> 'false'
		])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Castrol</h1>
				<h2>Training Presentation</h2>
				<a href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
			</div>
		</div>
		@include('main.layouts.partials._header_video_controls')
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'A Well Oiled Employee Induction<br />Presentation For Castrol',
				'h2' => 'Charting the work and heritage of a global<br />lubricant provider to new recruits',
				'p' => '<span>Client:</span>&nbsp;Castrol    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;UK    <span>Category:</span>&nbsp;Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Castrol is a global industrial and automotive lubricant brand, producing world-renowned oils including Castrol GTX, Edge and Magnatec. It is a major sponsor to global racing teams and partners with major vehicle brands including Ford and Audi.</p>
				<h3>The Brief</h3>
				<p>As a company, Castrol is proud of its work and heritage and, for this reason, wanted to ensure that new starters were invested in and aware of the brand from the outset. To do this, the company turned to Article 10 to create a presentation that collated a wide range of information from across the business as part of an impactful induction. The presentation brief required an on-brand, engaging deck that provided fresh eyes with a sense of loyalty to the Castrol brand, its products and history.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Working closely with Castrol’s Global Brand team to bring together the ideal mix of materials, we proposed a vibrant presentation concept comprising a number of dynamic sections highlighting key components of the Castrol brand. </p>
			</div>
		</div>
	</div>

	@include('main._partials._slider_container', [
	'before'	=> '/images/projects/castrol-presentation/slider/before.jpg',
	'after'		=> '/images/projects/castrol-presentation/slider/after.jpg'
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Through a combination of iconography, high-resolution branded photography and drawn imagery, plus strong messaging to draw viewers in, the presentation covers the vast range of Castrol products and their USPs. Elsewhere, sections cover brand heritage and pioneering achievements as well as an HR-driven induction segment that provides a friendly, informative welcome to newcomers.
				<br />With dynamic builds and emotive embedded videos, the presentation culminates in an exciting final flourish via a photography-led summary of the deck, complete with embedded music.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor not-mac">
					<div class="screen no-scrollbar">
						@include('main._partials._video_container', [
							'id'	  	  		=> '2',
							'minHeight'   		=> '100%',
							'transparent' 		=> true,
							'videoFile'   		=> $videoPageObj[1]['video'],
							'videoPoster' 		=> $videoPageObj[1]['poster'],
							'adjustPlayBtnTop'	=> false,
						])
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The presentation makes for an exceptional tool, enabling Castrol to inform and excite its new staff in an entertaining and engaging way, and is also fully customisable, allowing in-house teams to add content as the company develops.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
