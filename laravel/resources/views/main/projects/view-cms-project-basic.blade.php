@extends('main.layouts.main')


@section('header')
@if(isset($cmsData['header_image_active']) && $cmsData['header_image_active'] == 'false')
	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video" style="background-color:#000; background-image:url({{$cmsData['casestudy_header_video_pre_poster']}});">
		<div class="columns span-12 full-height">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>
			@include('main._partials._culture_video_top', ['mobile_poster' => $cmsData['casestudy_header_video_mobile_poster'], 'video_poster' => $cmsData['casestudy_header_video_poster'], 'video_file' => $cmsData['casestudy_header_video_link']])

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>{{ $cmsData['casestudy_title'] }}</h1>
					<h2>{{ $cmsData['casestudy_subtitle'] }}</h2>
					<a href="/services/video-animation-3d" class="tag  {{ $cmsData['casestudy_colour'] }}">Video</a>
				</div>
			</div>
		</div>
	</div>
@else
	<div class="columns span-12 full-height fixed-background  casestudy-header-small">
		<div class="columns span-12 full-height {{ $cmsData['casestudy_colour'] }} fixed-background casestudy-header">

			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>{{ $cmsData['casestudy_title'] }}</h1>
					<h2>{{ $cmsData['casestudy_subtitle'] }}</h2>
					<a href="/services/digital-solutions" class="tag {{ $cmsData['casestudy_colour'] }}">{{ $cmsData['casestudy_category'] }}</a>
				</div>
			</div>
		</div>
	</div>
@endif
@endsection


@section('content')

	@include('main.projects._partials._cms_section_helper')

	@include('main.projects._partials._cms-stats')

	@include('main.projects._partials._project-footer')

@endsection
