@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll eone-background-image-pre">
		<div class="black-out-layer"></div>

			@include('main._partials._culture_video_top', [
				 'mobile_poster' 	=> $videoObj['mobile-poster']
				,'video_poster'		=> $videoObj['poster']
				,'video_file'			=> $videoObj['video']
				,'auto_play' 			=> 'false'
			])

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>Entertainment One</h1>
					<h2>Presentation Toolkit</h2>
					<a href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
				</div>
			</div>
			@include('main.layouts.partials._header_video_controls')
	</div>

@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Mastering Multiple Brands In One<br />Template For Entertainment One',
				'h2' => 'A complex brand toolkit made fun and easy to use',
				'p' => '<span>Client:</span>&nbsp;Entertainment One    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;UK    <span>Category:</span>&nbsp;Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Entertainment One is an international media corporation specialising in the acquisition, production and distribution of content across film, television, music, family programming, merchandising and licensing, and digital content. Its current rights library includes more than 40,000 film and television titles, 4,500 hours of television programming and 45,000 music tracks.</p>
				<h3>The Brief</h3>
				<p>Entertainment One turned to Article 10 in order to create a toolkit in PowerPoint that covered the many brands across the company’s portfolio, as well as its own corporate branding.
				<br />Each section needed to be fun and reflect the show it referred to, as well as include core slides with set content and boilerplate slides for employees to use as templates.
				<br />With multiple brands within one organisation, the templates had to remain true to a number of brand guidelines while also being easy to use and understand.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>With the goal to build a creative and highly visual master template that was not only robust but also user friendly, Article 10’s presentation team set to work, using PowerPoint and Illustrator to craft the toolkit and combine the multiple types of branding.</p>
			</div>
		</div>
	</div>

	@include('main._partials._slider_container', [
	'before'	=> '/images/projects/entertainment-one-brand-toolkits/slider/before.jpg',
	'after'		=> '/images/projects/entertainment-one-brand-toolkits/slider/after.jpg'
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Incorporating the Entertainment One look and feel as well as those of its multiple shows, each with their own master in the template, colour palettes, aligned to each show, were developed with backgrounds and layouts tailored as needed. The additional suite of boilerplate slides for each brand provided a range of elements that enabled staff to build their own ‘scenes’ within the deck, consistent with brand guidelines.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor not-mac">
					<div class="screen no-scrollbar">
						@include('main._partials._video_container', [
							'id'				=> '2',
							'transparent'		=> true,
							'minHeight' 		=> '100%',
							'videoFile'			=> $videoPageObj[1]['video'],
							'videoPoster'		=> $videoPageObj[1]['poster'],
						])
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more ">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The final toolkit, complete with multiple master templates, required complex PowerPoint work by Article 10’s presentation design teams in order to enable Entertainment One staff to use it easily.
				<br />A training session was held at Entertainment One’s headquarters where Article 10 explained how to make the most of the template which was designed to be simple and fun to use, with on-brand colours and graphical elements available and ready to work with.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
