@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video etf-background-image-pre">
		<div class="columns span-12 full-height">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

			<!--
			<button class="play" title="Play">
				<div class="play-icon">
					<svg viewBox="0 0 20 20" preserveAspectRatio="xMidYMid" tabindex="-1">
						<polygon points="1,0 20,10 1,20"></polygon>
					</svg>
				</div>
			</button>
			//-->
			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>ETF</h1>
					<h2>Broadcast Adverts</h2>
					<a href="/services/video-animation-3d" class="tag {{ $colour }}">Video</a>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">
			
			@include('main.projects._partials._project_header', [
				'h1' => 'Showcasing The ETF To A Global Audience',
				'h2' => 'Stylish television adverts filmed across Europe and shared with the world',
				'p' => '<span>Client:</span>&nbsp;ETF    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Video',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>The European Training Foundation is an EU agency that helps transition and developing countries to harness the potential of their human capital through the reform of vocational education and training, and labour market systems in the context of the EU's external relations policy.</p>
				<h3>The Brief</h3>
				<p>Having worked closely with many of Article 10’s creative divisions over several years, the ETF asked our video team to create a series of short television adverts to provide awareness of the organisation’s initiatives and goals.  </p>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more no-padding-bottom">
		<div class="row full">

			<div class="columns span-12 parallax">
				<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
			</div>
			<!-- <div class="columns span-12 static-video-wrap">
				@include('main.projects._partials._project_banner_section._'.$bannerArray[0].'_section', ['page' => $bannerArray[0], 'muteBtn' => true])
			</div> -->

			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1 padding-top-btm-40">
				<h3>The Approach</h3>
				<p>Article 10 provided a framework for five short videos featuring a range of aspirational business paths that the ETF supports across a number of locations. Each video would feature a different job role complete with dynamic on screen text to highlight key features and benefits provided by the ETF to those in different industries and from different backgrounds. </p>
			</div>

			<div class="columns span-12 parallax">
				<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
			</div>
			<!-- <div class="columns span-12 static-video-wrap">
				@include('main.projects._partials._project_banner_section._'.$bannerArray[1].'_section', ['page' => $bannerArray[1], 'muteBtn' => true])
			</div> -->

			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1 padding-top-btm-40">
				<h3>The Process</h3>
				<p>Shooting in three locations, Croatia, London and Reading, two Article 10 teams made up of producers, editors and directors of photography filmed each video using a matching style, despite the diversity of the subjects and settings. Featuring a kayak instructor, farmer and postman in Croatia, a dancer in London and a chef in Reading, the abstract shots and text narrative, along with a subtle soundtrack and ETF branding, tied the videos together in a seamless, professional series. The on screen text was also translated into multiple languages to reach the broadest possible audience across Europe. </p>
			</div>

			<div class="columns span-12 parallax">
				<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
			</div>
			<!-- <div class="columns span-12 static-video-wrap">
				@include('main.projects._partials._project_banner_section._'.$bannerArray[2].'_section', ['page' => $bannerArray[2], 'muteBtn' => true])
			</div> -->

			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1 padding-top-btm-40">
				<h3>The Result</h3>
				<p>The final adverts were aired on the Euronews channel to a worldwide audience of more than three million viewers, providing significant exposure for the ETF and making a global audience aware of the important role it plays.</p>
			</div>

			<div class="columns span-12 parallax">
				<div class="bg {{ $imageDir }}-body-4" data-speed="10"></div>
			</div>
			<!-- <div class="columns span-12 static-video-wrap">
				@include('main.projects._partials._project_banner_section._'.$bannerArray[3].'_section', ['page' => $bannerArray[3], 'muteBtn' => true])
			</div> -->
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
