@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background etf-torino-background-image-pre">
		<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>ETF Torino Process</h1>
					<h2>Campaign</h2>
					<a href="/services/campaigns" class="tag {{ $colour }}">Integrated Campaign</a>
				</div>
			</div>

		</div>
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'A major international conference and campaign',
				'h2' => 'The Torino Process involves 25 participating countries and <br>culminates every two years with an international conference.',
				'p' => '<span>Client:</span>&nbsp;ETF    <span>Year:</span>&nbsp;2017    <span>Location:</span>&nbsp;Turin, Italy    <span>Category:</span>&nbsp;Motion, Campaigns, Exhibitions, Digital, Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>The European Training Foundation (ETF) is an EU agency that helps transition and developing countries harness the potential of their citizens through the reform of vocational education and training, and labour market systems in the context of the EU's external relations policy.</p>
				<h3>The Brief</h3>
				<p>The Torino Process was first launched by the ETF in 2010. It culminates every two years in an international conference, bringing together 25 partner countries to analyse progress in their vocational education and training (VET) policies and systems. Article 10 has worked alongside the ETF communications team to develop the brand and identity for every cycle and closing conference so far. And this time, we have been working closer than ever: months of detailed planning have gone into helping both the communications and project teams expand the project’s reach and impact yet further, and communicate effectively with its audiences. </p>
			</div>

		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<div class="row full">
			<div class="columns small-up-12 medium-up-6 large-up-4" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-1.jpg); background-size: cover; background-position-y: 50%; background-repeat: no-repeat;">
				<img src="/images/shared/grid-2x1.png" alt="">
			</div>
			<div class="columns small-up-12 medium-up-6 large-up-4" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-2.jpg); background-size: cover; background-position-y: 50%; background-repeat: no-repeat;">
				<img src="/images/shared/grid-2x1.png" alt="">
			</div>
			<div class="columns small-up-12 medium-up-6 large-up-4" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-3.jpg); background-size: cover; background-position-y: 50%; background-repeat: no-repeat;">
				<img src="/images/shared/grid-2x1.png" alt="">
			</div>
			<div class="columns small-up-12 medium-up-6 large-up-12" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-4.jpg); background-size: cover; background-position-y: 50%; background-repeat: no-repeat;">
				<img class="hidden-small hidden-medium" src="/images/shared/grid-6x2.png" alt="">
				<img class="hidden-large hidden-xlarge hidden-xxlarge" src="/images/shared/grid-2x1.png" alt="">
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>New website launch</h3>
				<p>The <a class="purple" href="https://www.torinoprocess.eu/" target="_blank"><strong>newly launched website</strong></a> is a huge milestone, and will shape the future of the Torino Process. Carefully tailored to the team’s requirements, following close, step-by-step consultation, the bespoke design and functionality allows users to build their own reports, extract relevant information and easily compare data and information. </p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<div class="row full padding1rem grey-background">
			<div class="columns small-up-12 medium-up-6 align-right" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-5.png);">
				<img src="/images/shared/grid-3x2.png" alt="">
			</div>
			<div class="columns small-up-12 medium-up-6 align-left" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-6.png);">
				<img src="/images/shared/grid-3x2.png" alt="">
			</div>
			<div class="columns small-up-12 align-center align-center" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-7.png);">
				<img class="hidden-small" src="/images/shared/grid-6x2.png" alt="">
				<img class=" hidden-medium hidden-large hidden-xlarge hidden-xxlarge" src="/images/shared/grid-3x2.png" alt="">
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>Conference live-streaming</h3>
				<p>The final international conference takes place 7–8 June 2017 in Turin. The full conference will be streamed live on the new website in three different languages.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<div class="row full">
			<div class="columns small-up-12 align-center cover" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-parallax-1.jpg);">
				<img class="" src="/images/shared/grid-6x3.png" alt="">
			</div>
		</div>
		<!-- <div class="bg {{ $imageDir }}-body-1" data-speed="10" style="background-position-y: 50%;"></div> -->
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<p>State-of-the-art communications
					For months, Article 10’s digital, campaign, presentation, motion and events teams have been planning and producing materials in preparation for the conference. For example, in alignment with this year’s theme of ‘Innovation’, delegates will be invited to take part in Virtual Reality demonstrations. These communications will all finally come together on 7–8 June; helping to bring the Torino Process’s core messages to life and assisting the ETF in its work to improve Vocational Education and Training (VET) systems and outcomes.
				</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<div class="row full">
			<div class="columns small-up-12 align-center cover" style="background:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-parallax-2.jpg);">
				<img class="" src="/images/shared/grid-6x3.png" alt="">
			</div>
		</div>
		<!-- <div class="bg {{ $imageDir }}-body-2" data-speed="10" style="background-position-y: 50%;"></div> -->
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<p class="p-bold">Watch this space for updates and to see how the conference unfolds.</p>
			</div>
		</div>
	</div>

	<!-- @include('main.projects._partials._project-stats') -->

	@include('main.projects._partials._project-footer')

@endsection
