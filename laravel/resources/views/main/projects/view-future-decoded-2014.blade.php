@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video" style="background-color:#000; background-image:url({{ $videoObj['poster'] }});">
		<div class="columns span-12 full-height">

			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			@include('main._partials._culture_video_top', ['video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])
			<!--
			<button class="play" title="Play">
				<div class="play-icon">
					<svg viewBox="0 0 20 20" preserveAspectRatio="xMidYMid" tabindex="-1">
						<polygon points="1,0 20,10 1,20"></polygon>
					</svg>
				</div>
			</button>
			//-->
			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>Microsoft</h1>
					<h2>Motion Graphics Animation</h2>
					<a href="/services/video-animation-3d" class="tag {{ $colour }}">Video</a>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Decoding The Future With Microsoft',
				'h2' => 'An animation encapsulating Microsoft’s vision shown on stage alongside its CEO. <br />No pressure there then… ',
				'p' => '<span>Client:</span>&nbsp;Microsoft    <span>Year:</span>&nbsp;2014    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Video',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Microsoft has evolved into a forward-thinking services company now it is in the hands of CEO Satya Nadella, with a goal to connect the world and enable us to do more and achieve more with technology. </p>
				<h3>The Brief</h3>
				<p>The future brings about significant challenges but also great opportunity for UK business leaders and is the driving ideology behind Microsoft’s annual Future Decoded event. Microsoft required a range of materials for the event across a number of its business areas, including marketing communications, merchandise, PowerPoint presentations, posters and a simple yet powerful video to showcase the company’s ambitions as part of the CEO’s keynote speech.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 black-background">
		@include('main._partials._video_container', [
			'id'	  	  => '1',
			'minHeight'   => 'auto',
			'videoFile'   => $videoPageObj[1]['video'],
			'videoPoster' => $videoPageObj[1]['poster'],
			'transparent' => true,
			'hidePlayBtn' => false,
			'autoPlay' 	  => false,
			'hidePlayBtn' => true,
		])
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Over the course of a number of meetings with Microsoft, company research and a hefty amount of brainstorming, our copy, design and animation teams crafted the “Your Future Decoded” concept and style that would become the finished animation. </p>

				<h3>The Process</h3>
				<p>Our in-house copywriter penned a well-received draft script and paired it with the storyboard created by our design team, featuring hand-drawn sketches turned into digital assets. Once signed off, our After Effects animator set to work bringing the story to life, adding motion, music and voiceover. </p>

				<h3>The Result</h3>
				<p>The final video was shown on the big screen at Future Decoded alongside Satya Nadella’s keynote speech and received high praise from the wider Microsoft audience. Your Future Decoded paints a picture of a better future brought about by new and emerging Microsoft technologies. Its message is simple: Microsoft will enable and empower more people to do more with smart, unobtrusive technology that understands their needs and becomes an extension of them. With a pure and optimistic tone for both narrative and aesthetics, the Your Future Decoded video will play a crucial role in conveying Microsoft’s core focus and goals.</p>
			</div>
		</div>
	</div>

	<!-- @include('main.projects._partials._project-stats') //-->

	@include('main.projects._partials._project-footer')

@endsection
