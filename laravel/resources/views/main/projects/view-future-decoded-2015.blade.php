@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video fd-2015-background-image-pre">
		<div class="columns span-12 full-height">

			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video'], 'posterPosition' => '80% 50%'])
			<!--
			<button class="play" title="Play">
				<div class="play-icon">
					<svg viewBox="0 0 20 20" preserveAspectRatio="xMidYMid" tabindex="-1">
						<polygon points="1,0 20,10 1,20"></polygon>
					</svg>
				</div>
			</button>
			//-->
			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>Microsoft</h1>
					<h2>Talking Head Discussion Videos</h2>
					<a href="/services/video-animation-3d" class="tag {{ $colour }}">Video</a>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => '5 Videos, 4 Locations, 2 Microsoft Digital Visionaries',
				'h2' => 'Letting the thought leaders do the talking in the run up to Microsoft’s Future Decoded event',
				'p' => '<span>Client:</span>&nbsp;Microsoft    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Video',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Microsoft’s annual Future Decoded conference brings together the world’s brightest minds to discuss the future of not only technology but business, society and more at ExCel London. Welcoming businesses of all sizes as well as significant keynote speakers, including Microsoft CEO Satya Nadella, the two-day event accepts several thousand attendees each year for lively, future-focused debate.</p>
				<h3>The Brief</h3>
				<p>Ahead of Future Decoded 2015, Microsoft wanted to kick start the conversation by creating an engaging video series with two “digital visionaries” discussing relevant Future Decoded topics. The aim was to generate excitement and awareness for the forthcoming event through a natural discussion between two significant thought leaders.</p>
			</div>

		</div>
	</div>

	@include('main._partials._case_study', [
		'page'				=> 'projects',
		'orb_type'			=> 'white',
		'no_orb_background' => true,
		'arrow_shadow'		=> true,
		'transparent_navs'	=> true,
		'hidePlayBtn' 		=> false,
		'increaseWidth'		=> true,
		'whiteBackground' 	=> true,
		'hidePlayBtn' 		=> true,
		'autoPlay' 			=> false,
		'noPaddingBottom'	=> true,
		'muteBtn'			=> true,
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Article 10’s video team met with Microsoft to discuss the requirements of this intimate yet insightful series featuring Chief Envisioning Officer, David Coplin and Ade McCormack, Digital Strategist. Article 10 proposed a “fireside chat” approach, shot across multiple locations with a two-camera set-up and distinctive lighting for each shot. Multiple talking points were chosen for the shoot, covering a range of topics from “Digital Economy” to “Today’s Enterprise”.</p>

				<h3>The Process</h3>
				<p>Following smartly styled rooftop shots and an introduction for both visionaries, the candid, conversational nature of the piece was immediately conveyed by the first of many shooting locations. Two armchairs in an open, airy room filled with natural light provided a suitably positive backdrop for discussion around “Changing The World”. With multiple angles that captured not only the room and its occupants but also their reactions, the filming and editing helped to draw the viewer in to the video naturally, as if they were part for the discussion.</p>
				<p>As the topic of conversation shifted, so did the location with a more intimately lit space for talk of “The Future”, complete with a custom-made lighting rig, and a comfortable kitchen space within which a “People and Technology” dialogue was captured. With a tight schedule and limited access to such busy senior figures, Article 10’s camera, lighting and directorial team was required to quickly set up and film each segment in order to maximise the time available.</p>

				<h3>The Result</h3>
				<p>Appearing on Microsoft’s main Future Decoded page as well as the company’s YouTube channel, the Digital Visionaries videos sparked excitement around the concepts raised as well as the upcoming event. Able to stand alone as interesting, insightful content for viewing beyond Future Decoded’s two-day run, the final output shows the power of conversation between informed thought leaders when placed in the hands of a skilful video production team.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
