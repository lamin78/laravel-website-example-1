@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height {{ $colour }} fixed-background" style="background-image:url(http://lorempixel.com/1480/833/city/);">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Kelway</h1>
				<h2>Corporate Video</h2>
				<a href="/services/video-animation-3d" class="tag {{ $colour }}">Video</a>
			</div>
		</div>

	</div>

@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Bringing Old Uppinghamians Up To Date',
				'h2' => 'A dynamic new website for old boys and girls',
				'p' => '<span>Client:</span>&nbsp;Old Upingham    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Digital',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Organisation</h3>
				<p>Uppingham School was founded in 1584 and is an all boarding school in Rutland. Article 10 has worked on a range of projects with the school&rsquo;s alumni network, Old Uppinghamians.</p>
				<h3>The Brief</h3>
				<p>The OU team was looking for a way to buck the trend of run of the mill alumni websites and provide a modern window into a school steeped in history. Naturally, they turned to Article 10 to design and build a new and up-to-date web presence.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-parallax-1.jpg);" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Article 10&rsquo;s digital team began planning the site with a number of modernization factors in mind. A key priority was to ensure that the site worked across all web-enabled devices and allowed visitors to quickly find information relevant to them. It also had to be highly usable and engage visitors to ensure they remained involved in the school&rsquo;s activities.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 small-12 small-before-0 small-after-0">
				<div class="monitor">
					<div class="screen Scrollable no-scrollbar">
						<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-screen-grab.jpg" class="toScroll"/>
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">Give it a try for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Built using a combination of HTML5, CSS3 and PHP, being an alumni website, the platform not only needed to stand on a rock solid foundation, but also be useable for both administrators and alumni. Behind the scenes, the site uses a custom built, responsive and intuitive CMS, based on the PHP Symfony framework. This modern technology makes updating content quick and easy while enabling OU staff to manage alumni information and ensure details are correct and up-to-date.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-parallax-2.jpg);" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more {{ $colour }}">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The finished site features a homepage video that looks great on the desktop and adjusts to become an equally attractive static image for mobile browsers, along with a number of modern touches that bring the site into step with the websites of today.</p>
				<p>On the launch of the site, Caroline Steele, Development Manager at Uppingham School said: &ldquo;I think we should all be proud; it&rsquo;s a very cool looking website. Fingers crossed the world agrees.&rdquo; Based on the analytics we’ve seen for the Old Uppinghamian website – the world does.</p>
				<a href="http://www.olduppinghamianian.co.uk" target="_blank" class="button">Visit Old Uppinghamian website</a>
			</div>
		</div>
	</div>

	<div class="columns span-12 phones animatedParent">
		<div class="row full">
			<div class="columns span-2 before-1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-1.jpg" class="phone animated fadeIn delay-500"/>
			</div>
			<div class="columns span-2 before-2">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-2.jpg" class="phone animated fadeIn"/>
			</div>
			<div class="columns span-2 before-2 after-1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-3.jpg" class="phone animated fadeIn delay-500"/>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
