@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header@0.3.jpg);">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>KFC</h1>
				<h2>Campaign Identity</h2>
				<a href="/services/campaigns" class="tag {{ $colour }}">Campaigns</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Giving Identity To KFC&rsquo;s People Promise',
				'h2' => 'Logo and brand identity creation for the Colonel&rsquo;s internal teams',
				'p' => '<span>Client:</span>&nbsp;KFC    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;UK    <span>Category:</span>&nbsp;Campaign',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>KFC has over 850 restaurants in the UK serving up tasty fried chicken and our favourite side, delicious gravy. The KFC HR department plays an important role helping to promote awareness across a range of internal campaigns as well as providing traditional human resources support. Article 10 has worked closely with the HR and Marketing teams at KFC on a number of projects. </p>
				<h3>The Brief</h3>
				<p>Article 10 was asked to support the KFC HR team in developing a logo and brand identity for its ‘People Promise’ initiative that could be used across all of its internal facing communications. The desire was to spread this positive message and encourage a ‘Return the Promise’ mentality across the organisation.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-1.jpg"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>In order to appropriately engage KFC staff and reinforce the People Promise message, Article 10’s branding team began research on the initiative. After meeting with stakeholders and studying existing collateral and supporting materials, a range of visual concepts were produced that highlighted the People Promise brand, with the various options shown across a number of example print deliverables in order to fully realise the different applications of the branding.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-2.png"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Once a route was chosen our designers began creating the preferred logos and artwork that included hand drawn graphics and unique fonts that fit perfectly with the KFC brand while retaining their own identity. With the brand identity in place, discussions began on what additional collateral could be created.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-3.jpg"/>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-4.jpg"/>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-5.jpg"/>
	</div>

	<div class="columns span-12 project-more {{ $colour }}">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The artwork that we created was rolled out across the whole organisation and brought KFC’s People Promise to life for its diverse workforce of 25,000 team members and head office employees. Fully embedded into KFC’s existing ways of working, the language of the People Promise has become part of the fabric of the company thanks to the exciting visual language used to enhance its meaning.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-6.jpg"/>
	</div>

	<!-- @include('main.projects._partials._project-stats') //-->

	@include('main.projects._partials._project-footer')

@endsection
