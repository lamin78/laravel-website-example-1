@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background mail-online-background-image-pre">
		<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>MailOnline</h1>
					<h2>Roadshow design and build</h2>
					<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>&nbsp;&nbsp;<a href="/services/digital-solutions" class="tag orange">Digital</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Read All About It – Delivering <br />Scandalous (Mock) Stories For MailOnline',
				'h2' => 'A unique and quickly realised interactive experience for the seriously popular brand at Nestle’s Digital Day.',
				'p' => '<span>Client:</span>&nbsp;MailOnline    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Exhibitions',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>MailOnline is the world&#39;s largest English-language newspaper website with an average 14.2m global browsers per day. Offering editorial excellence, dynamic, addictive content and a highly pictorial, easily navigable format, MailOnline is &ldquo;seriously popular&rdquo; and provides an eye on the world for digital users across devices.</p>
				<h3>The Brief</h3>
				<p>Attending Nestle&rsquo;s Digital Day, MailOnline required an eye-catching and attention-grabbing experience to match the engaging nature of its digital news stories. The goal was to showcase the company&rsquo;s immense advertising opportunities in a fun and unique way while standing out from immediate competition and other major players.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>After much internal discussion and brainstorming, Article 10 produced a proposal, whittled down from dozens of ideas (some genius, many exciting and a few downright extraordinary), to create a branded, interactive space where event attendees could have their photo taken and receive their own custom MailOnline story to be shared on social media. </p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Concept confirmed, a collection of Article 10’s creative divisions dived into the project in order to meet a narrow two-week timeline before the event. With collaboration between design, digital, project management and events teams, further development and planning meetings were held in order to design, detail, develop, build and deliver the exhibition stand complete with the creation of story scenarios conceived in tandem with MailOnline staff in time for the event.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>On the day, a Photo studio environment and newsroom, complete with designer, journalist and, of course, visually appealing MailOnline branding, <br />was set up to engage attendees.
				<br />Visitors could select a theme from a selection of story scenarios (including a red carpet, rugby and the budget day) and be photographed, before our team turned the result into a personalised story. Each unique result could then be posted on a large portrait screen in Newsfeed mode and was also sent to the visitor as an attachment for circulation among friends and followers.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
