@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header@0.3.jpg);">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header" >
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Microsoft</h1>
				<h2>Integrated Marketing Campaign</h2>
				<a href="/services/campaigns" class="tag {{ $colour }}">Campaigns</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

      @include('main.projects._partials._project_header', [
        'h1' => 'Superheroes Of Microsoft Technology, Assemble!',
        'h2' => 'Bringing a comic twist to technical training',
        'p' => '<span>Client:</span>&nbsp;Microsoft    <span>Year:</span>&nbsp;2014    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Campaigns',
      ])

      <div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>The Microsoft Virtual Academy (MVA) offers free training, delivered by experts, to those looking to enhance their skills and prospects. With courses for beginners all the way to expert advice for complex coding languages, it&rsquo;s an impressive training resource for the technically minded.</p>
				<h3>The Brief</h3>
				<p>With an audience of developers, IT professionals, interested amateurs and self-confessed geeks in mind, Microsoft wanted a fun and rewarding way to drive new MVA memberships.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-1.jpg"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>We dug deep and headed back to our youth in order to appeal to such a particular crowd and planned a unique collection of branded collateral and promotional materials to promote MVA.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-2.jpg"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>We started with superheroes. Not your average superheroes, however, these were comic book characters designed for the tech crowd. From Captain Code to Hypervisor, our character design and illustration work on each individual hero represents a particular skill to be derived from MVA training that can be unlocked by keen learners. Working side by side, our editorial and design teams created interesting, relevant technology superheroes that formed the basis of the campaign.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 animatedParent animateOnce" data-sequence="500">
		<div class="row">
			<div class="columns span-12 still-site full-width-image microsoft-anim">

				@include('main._partials._case_study', [
					'page'							=> 'projects',
					'orb_type'					=> 'white',
					'no_orb_background' => true,
					'arrow_shadow'			=> true,
					'transparent_navs'	=> true,
					//'increaseHeight' 	=> true,
					'showNav'						=> false,
					'showOrbs'					=> false,
					'whiteBackground' 	=> true,
					'autoPlay' 					=> true,
					'autoStart' 				=> true,
					'noPaddingBottom'		=> true,
				])

			</div>

			<div class="columns span-12 still-site full-width-image animated fadeIn"  data-id="1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-4.jpg"/>
			</div>

			<div class="columns span-12 still-site full-width-image animated fadeIn"  data-id="2">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-5.jpg"/>
			</div>

			<div class="columns span-12 project-more">
				<div class="row">
					<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
						<h3>The Result</h3>
						<p>We created a raft of collateral for Microsoft Virtual Accademy, including an HTML 5 landing page, collectible stress toys, printed material and merchandise, Top Trumps cards and HTML emails and online social banners as part of a major awareness campaign. The result was an increase in sign ups and activity within the MVA community thanks, in large part we&rsquo;re sure, to the very cool collectibles available.</p>
					</div>
				</div>
			</div>

			<div class="columns span-12 still-site full-width-image animated fadeIn"  data-id="3">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-6.jpg"/>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
