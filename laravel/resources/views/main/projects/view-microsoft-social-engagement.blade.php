@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll microsoft-background-image-pre">
		<div class="black-out-layer"></div>

		@include('main._partials._culture_video_top', [
			'mobile_poster' => $videoObj['mobile-poster']
			,'video_poster' => $videoObj['poster']
			,'video_file' 	=> $videoObj['video']
			,'auto_play' 		=> 'false'
		])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head avoid-play">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Microsoft</h1>
				<h2>High Impact Sales Presentations</h2>
				<a href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
			</div>
		</div>
		@include('main.layouts.partials._header_video_controls')
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'A More Sociable Presentation For Microsoft',
				'h2' => 'Educating the world on Microsoft Social Engagement set to the backdrop of the General Election',
				'p' => '<span>Client:</span>&nbsp;Microsoft    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;UK    <span>Category:</span>&nbsp;Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Microsoft&rsquo;s Dynamics CRM team provides the tools for businesses to understand and engage with their audiences. The latest addition to the Dynamics fold is Social Engagement, a groundbreaking online sentiment analysis tool.</p>
				<h3>The Brief</h3>
				<p>As part of an ongoing series of Social Engagement &ldquo;Snapshot&rdquo; reports delivered by Article 10, Microsoft asked us for a more detailed example of Social Engagement&rsquo;s capabilities using the 2015 UK General Election as the focus for an in-depth report.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Starting 100 days before polling day, our copywriting and PowerPoint teams began gathering research through Social Engagement, using the leaders of four main political parties as data points. With a schedule set to deliver three reports in the run up to the election and a final, larger report after voting was completed, regular internal meetings and planning sessions enabled us to swiftly deliver each report on time, designed and ready to be shared with Microsoft&rsquo;s client network via email.</p>
			</div>
		</div>
	</div>

	@include('main._partials._slider_container', [
		'before'	=> '/images/projects/microsoft-social-engagement/slider/before.jpg',
		'after'		=> '/images/projects/microsoft-social-engagement/slider/after.jpg'
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Three written and designed reports, delivered in a custom-built HTML email template, were created for key milestones during the election campaigns. Each report featured an update on how each party was performing, the key talking points online and the sentiment shown from the general public and media. With hand drawn infographics, digital illustrations and essential statistics, each report provided an informative glance at the unfolding election through the lens of Social Engagement.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor not-mac">
					<div class="screen no-scrollbar">
						@include('main._partials._video_container', [
							'id'				=> '2',
							'transparent'		=> true,
							'minHeight' 		=> '100%',
							'videoFile'			=> $videoPageObj[1]['video'],
							'videoPoster'		=> $videoPageObj[1]['poster'],
							'adjustPlayBtnTop'	=> false,
						])
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>After a gruelling campaign that even pollsters couldn&rsquo;t predict and a long night of Election Day coverage, our final, detailed report brought together the insight derived from Social Engagement. Delivered in just two days and including engaging graphics, annotations and facts, the report highlighted a number of statistics that pointed toward key election outcomes long before the votes were counted. Microsoft shared the final report widely and we received a great deal of positive feedback regarding the project&rsquo;s outcomes and its delivery within a tight time frame.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
