@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background graduate-background-image-pre">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Microsoft</h1>
				<h2>Graduate Recruitment Campaign</h2>
				<a href="/services/campaigns" class="tag {{ $colour }}">Campaigns</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Engaging With The Next<br />Generation Of Microsofties',
				'h2' => 'A fully integrated campaign designed to prick up the ears of budding technology game changers.',
				'p' => '<span>Client:</span>&nbsp;Microsoft    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Campaigns',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Microsoft offers renowned internships and graduate schemes that provide hands on experience and impressive career prospects for school and university leavers. The company’s annual intake is sourced from a variety of institutions with a wide range of exciting roles on offer.</p>
				<h3>The Brief</h3>
				<p>Microsoft approached Article 10 to evolve and enhance existing branding and collateral for its annual intern and graduate recruitment programmes. This included reimagining  and rebranding marketing materials and producing a new graduate and intern brochure.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-1.jpg"/>
	</div>

	<div class="columns span-12 still-site increase-size {{ $colour }}">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-2.png"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Offering the full range of Article 10 services, we proposed a video and photo shoot on Microsoft&rsquo;s Thames Valley Park campus in order to capture fresh new material for a fresh new intake of staff. We also set our copywriter to work, driving new angles for engaging content across the brochure and a range of print and digital collateral.</p>
				<h3>The Process</h3>
				<p>Our work began with the brochure editorial and photo and video shoots in order to bring together all-new content for our design teams to work with. While on location, our video team also recorded interviews with key figures and former interns and graduates as part of a series of talking head videos and animations.</p>
				<p>Content secured, our research and design began, interviewing graduates from the previous year&rsquo;s intake to develop a narrative for the brochure and explaining the many benefits of becoming an &ldquo;early career&rdquo; Microsoft employee, as well as the application process. Our designers developed a vibrant look and feel using artwork crafted specifically for the brand, alongside on-campus photography, and applied it to the brochure as well as a raft of additional collateral, including exhibition display graphics, merchandise design and other print and digital marketing tools.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-3.jpg"/>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-4.jpg"/>
	</div>

	<div class="columns span-12 still-site full-width-image">
		<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-5.jpg"/>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>Microsoft has had another successful year in graduate and intern recruitment, aided by this engaging, integrated campaign. We have continued to work with Microsoft on recruitment projects and look forward to helping them to begin the careers of many more technology stars of the future.</p>
			</div>
		</div>
	</div>

	<!--
	<div class="columns span-12 interactive-site {{ $colour }} scrolling-section-height">
		<div class="row">
			<div class="columns span-10 before-1 after-1 small-12 small-before-0 small-after-0">
				<div class="carousel-wrap">
					@include('main._partials._case_study', ['page' => 'projects'])
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows carousel-arrow" />
				<div class="try carousel-text">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>
	//-->
	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
