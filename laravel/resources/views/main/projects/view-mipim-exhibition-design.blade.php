@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background mipim-background-image-pre">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>MIPIM</h1>
				<h2>Taking the show by storm</h2>
				<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Cannes you feel it? Article 10 at MIPIM',
				'h2' => 'Enabling titans of real estate to showcase their services on the French Riviera',
				'p' => '<span>Client:</span>&nbsp;SCA, THRE, Real Capital Analytics, Invesco Real Estate    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Exhibitions',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>MIPIM is a leading international event for real estate professionals, held annually in Cannes, France.</p>
				<h3>The Brief</h3>
				<p>With five clients exhibiting at the show, our creative and exhibitions teams endured a busy period in the build-up to MIPIM in order to provide their normal high level of service.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Scottish Cities Alliance Brief</h3>
				<p>Article 10 was also appointed to deliver a wide range of digital and video content for Scottish Cities Alliance &ndash; a collaboration of Scotland&#39;s seven cities and the Scottish Government to create conditions for growth and attract inward investment.</p>
				<h3>The Process</h3>
				<p>Collateral created by Article 10 was used by The Scottish Cities Alliance at MIPIM to showcase the investment opportunities, valued at &pound;10bn, available across the cities. Our work brought existing photography and video content to life while conveying key messages through a native iPad app.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Invesco Real Estate Brief</h3>
				<p>Invesco Real Estate tasked us with the design and production of its debut exhibition stand and The Financial Times required our support with branding outside of the show’s main venue.</p>
				<h3>The Process</h3>
				<p>We decided to make the mountain range of Ama Dablam in the Eastern Himalayas the centerpiece of Invesco&rsquo;s stand, creating a 7m wide panoramic backdrop. A number of high-grade exhibition elements were also created to ensure the company&rsquo;s personnel benefited from an ideal meeting environment.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-4" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Real Capital Analytics Brief</h3>
				<p>Real Capital Analytics, a long-standing client of Article 10, required a refresh to its existing exhibition stand design, with new messaging and graphics.</p>
				<h3>The Process</h3>
				<p>For Real Capital Analytics, large screens were utilised to the full, with staff able to interrupt rolling presentations to demonstrate the accuracy and relevance of the company&rsquo;s data to delegates seeking real-time information about property transactions.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-5" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Brief</h3>

				<p>And finally, but by no means last on our agenda, was a fun project for TIAA Henderson Real Estate (TH Real Estate).</p>

				<h3>The Process</h3>

				<p>We also had great fun producing and installing a range of branding elements on a yacht, chartered by TIAA Henderson Real Estate for the show, moored alongside the quayside next to the Palais des Festivals. In order to support the brand and ensure invited guests couldn&rsquo;t miss the company&rsquo;s boat party, our deliverables included a pair of 2.6m high branded inflatable columns, as well as banners and presentation collateral.</p>

				<h3>The Result</h3>

				<p>MIPIM was a great success for all of Article 10&rsquo;s clients exhibiting at the show and, for our new partners, provided a great base for future shows at which we continue to help them showcase their respective offerings.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
