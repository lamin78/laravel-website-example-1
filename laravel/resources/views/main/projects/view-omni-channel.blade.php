@extends('main.layouts.main')


@section('header')
	
	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video" style="background-color:#000; background-image:url({{ $videoObj['poster'] }});">
		<div class="columns span-12 full-height">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>
			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>OpenBet</h1>
					<h2>Promotional Animation</h2>
					<a href="/services/video-animation-3d" class="tag {{ $colour }}">Video</a>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'OpenBet Is Not Multi-Channel,<br />It’s Omni-Channel',
				'h2' => 'OpenBet believes location shouldn’t limit gaming.<br />We helped the company to tell the world.',
				'p' => '<span>Client:</span>&nbsp;OpenBet    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Video',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>OpenBet is a market-leading gaming technology company, providing a robust platform and player-centric services to the biggest names in online and in-store betting. </p>
				<h3>The Brief</h3>
				<p>In order to inform customers how important it is to ensure their services are available anywhere, OpenBet wanted a fun, animated video to convey the benefits of its “always available” solutions. The video was intended for use at major events and online.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 black-background">
		@include('main._partials._video_container', [
			'id'	  	  => '1',
			'minHeight'   => 'auto',
			'videoFile'   => $videoPageObj[1]['video'],
			'videoPoster' => $videoPageObj[1]['poster'],
			'transparent' => true,
			'hidePlayBtn' => true,
			'autoPlay' 	  => false,
			'muteBtn'	  => true,
		])
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>We wanted to highlight OpenBet&rsquo;s player-focused approach and so used a selection of fictional characters to show the different types of customer that gaming providers can cater to. From Slot Sally to Land-based Larry and Casual Chris, the colourful video promoted the many reasons to choose OpenBet&rsquo;s omni-channel solutions that keep players engaged and gaming wherever they happen to be.</p>
				<h3>The Process</h3>
				<p>With a fun voiceover and sparkling animation, the video uses each character to explain the different ways in which OpenBet solutions attract and retain consumers. Created in After Effects, the animated characters keep audiences entertained while educating them on a range of features, including OpenBet&rsquo;s Unity card.</p>
				<h3>The Result</h3>
				<p>OpenBet showed the video on its stand at the ICE 2015 Totally Gaming event and has since used it online and via a range of other media as a showcase video for its platform.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
