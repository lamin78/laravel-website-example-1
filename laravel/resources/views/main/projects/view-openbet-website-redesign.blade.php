@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background ob-background-image-pre">
		<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video ob-background-image">
			<div class="columns span-12 full-height">
				<div class="black-out-layer menu-grey-1"></div>
				<div class="black-out-gradient-layer"></div>
				@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
				</div>

				<div class="row full project-head">
					<div class="columns span-10 before-1 after-1 ">
						<h1>OpenBet</h1>
						<h2>Microsite design and build</h2>
						<a href="/services/digital-solutions" class="tag {{ $colour }}">Digital</a>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'First Past The Finishing Post With OpenBet’s Grand National Microsite',
				'h2' => 'Showcasing the power of the OpenBet platform with a stat-packed site for one of the busiest betting days of the year.',
				'p' => '<span>Client:</span>&nbsp;OpenBet    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Digital',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>OpenBet is a market-leading gaming technology company, providing a robust platform and player-centric services to the biggest names in online and in-store betting.</p>
				<h3>The Brief</h3>
				<p>On a day when thousands of bets are placed in store and online with punters constantly clicking the refresh button to secure their spoils, OpenBet&rsquo;s platform remains rock solid with 0% downtime. While others stumble at the first fence, OpenBet ensures that new accounts can be created and bets can be placed, calculated and cashed out without delay, even during periods of heavy traffic like Grand National Day. To highlight this fact, OpenBet asked us to create a Grand National-themed microsite.  </p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>To convey the fun of betting on the Grand National we designed a dynamic microsite featuring a flowing animation of horses dashing past the site’s main header and, after a quick scroll down, colourful statistics to promote OpenBet’s performance.</p>
			</div>
		</div>
	</div>



	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor">
					<div class="screen Scrollable no-scrollbar">
						<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-screen-grab.jpg" class="toScroll"/>
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Created from scratch with hand drawn, animated characters embedded and statistics artworked, the site sat on its own domain, away from OpenBet&rsquo;s main site location.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more {{ $colour }}">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The microsite was launched in time for the Grand National and provided a convenient and topical example of the performance of OpenBet&rsquo;s platform for interested parties and prospective clients.</p>
				<!-- <a href="#" target="_blank" class="button">Visit OpenBet Grand National website</a> //-->
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 phones animatedParent animateOnce">
		<div class="row full">
			<div class="columns span-2 before-1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-1.jpg" class="phone animated fadeIn delay-500"/>
			</div>
			<div class="columns span-2 before-2">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-2.jpg" class="phone animated fadeIn"/>
			</div>
			<div class="columns span-2 before-2 after-1">
				<img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-phone-3.jpg" class="phone animated fadeIn delay-500"/>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
