@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header@0.3.jpg);">
	<div class="columns span-12 full-height {{ $colour }} fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header.jpg);">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Steljes</h1>
				<h2>3D Animation</h2>
				<a href="/services/video-animation-3d" class="tag {{ $colour }}">Motion</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Showcasing Cutting-Edge Tech With Cutting-Edge Tech',
				'h2' => 'An innovative solution for a brand built on technology innovation',
				'p' => '<span>Client:</span>&nbsp;Steljes    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;3D Animation',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Since 1987, Steljes has been committed to disrupting the way we live and work &ndash; in a good way. Identifying the latest, game-changing technology, from the launch and distribution of the world&rsquo;s first InFocus 8 Colour LCD Panel to a 70% market share in interactive whiteboards, the company aims to shape the way we work and learn.</p>
				<h3>The Brief</h3>
				<p>Steljes wanted a more impressive way to showcase its wide range of technology solutions to its customers and partners.</p>
			</div>

		</div>
	</div>

	@include('main._partials._case_study', ['page' => 'projects', 'orb_type' => 'white', 'no_orb_background' => true, 'arrow_shadow' => true, 'transparent_navs' => true])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>With a brand so deeply immersed in technology, we wanted to ensure that our solution matched Steljes&rsquo; innovative nature. Through collaboration between our creative and digital teams we came up with a dynamic, cutting-edge sales tool.</p>

				<h3>The Process</h3>
				<p>We created a 3D interactive sales tool that allows users to show how individual pieces of technology work within a photorealistic environment. The solution provides full content management and is available as a PC, tablet and web-based application.</p>

				<h3>The Result</h3>
				<p>The tool designed for Steljes is incredibly flexible and allows the company&rsquo;s sales teams to tell a hugely impressive and engaging story using a range of devices in any location while ensuring the content is kept up to date.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
