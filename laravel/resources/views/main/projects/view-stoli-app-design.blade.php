@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header@0.3.jpg);">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">

		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Stoli</h1>
				<h2>App design, development and deployment</h2>
				<a href="/services/digital-solutions" class="tag {{ $colour }}">Digital</a>
			</div>
		</div>
	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'From Black Earth To Bottle With&nbsp;Stoli',
				'h2' => 'The remarkable journey of Stolichnaya vodka in a dynamic&nbsp;app',
				'p' => '<span>Client:</span>&nbsp;Stoli    <span>Year:</span>&nbsp;2014    <span>Location:</span>&nbsp;London,&nbsp;UK    <span>Category:</span>&nbsp;Digital',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Stoli has been producing world-renowned, premium vodka for more than 80 years. With a range of brands under its umbrella, all its vodka is produced to the same traditional, high standards.</p>
				<h3>The Brief</h3>
				<p>Proud of its traditional process, Stoli asked Article 10 to produce an iPad app for sales teams in order to showcase its products, benefits, organise meetings and explain the detailed techniques that go into crafting its famous vodka.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>In order to fully engage audiences and convey the quality of such a premium brand, we needed to create a stylish app that was both interactive and informative. Gathering information, high quality imagery and designing an intuitive interface, we set to work combining all of these elements into a slick finished product suitable for touch screens.</p>
	<!--
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor is-ipad">
					<div class="screen no-scrollbar"><img src="/images/projects/{{ $imageDir }}/{{ $imageDir }}-screen-grab.gif" style="height:100%;float:left;"/></div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
	//-->
				<h3>The Process</h3>
				<p>Our digital design team worked alongside our creative departments to pull together unique illustrations, photography and editorial to fully explain the Stoli brand, its products and its process and includes a fully interactive outline of the entire distillation method from grain in the Black Earth through to bottling. The interface also includes buttons that enable the user to begin a meeting, take photos and view sales collateral.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The application was a huge success and proved a useful asset for Stoli in generating awareness and new business. The application’s stunning process section was later converted into an animation for Stoli by our video production team.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-4" data-speed="10"></div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
