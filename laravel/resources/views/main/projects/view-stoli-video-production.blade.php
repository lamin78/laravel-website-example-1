@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video stoli-background-image-pre">
		<div class="columns span-12 full-height">

			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>

			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video'], 'posterPosition' => '80% 50%'])

			<!--
			<button class="play" title="Play">
				<div class="play-icon">
					<svg viewBox="0 0 20 20" preserveAspectRatio="xMidYMid" tabindex="-1">
						<polygon points="1,0 20,10 1,20"></polygon>
					</svg>
				</div>
			</button>
			//-->

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>Stoli</h1>
					<h2>Video Production</h2>
					<a href="/services/digital-solutions" class="tag {{ $colour }}">Video</a>
				</div>
			</div>

		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'A Lofty Shooting Location For A High Quality Brand',
				'h2' => 'Ensuring Stoli’s new Elit vodka receives the respect it deserves',
				'p' => '<span>Client:</span>&nbsp;Stoli    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Video',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>For more than 80 years, Stolichnaya vodka has been produced using the same traditions and the highest standards. Elit by Stolichnaya takes these traditions and quality to a new level, as crystal-clear vodka with a smooth and harmonious flavour, clean vodka notes and a well rounded, smooth finish.</p>
				<h3>The Brief</h3>
				<p>Stoli wanted to showcase its new, Elit vodka to the people who matter and care the most about quality ingredients – the mixologists pouring the drinks. </p>
			</div>
		</div>
	</div>

	@include('main._partials._case_study', [
		'page'				=> 'projects',
		'orb_type'			=> 'white',
		'no_orb_background' => true,
		'arrow_shadow'		=> true,
		'transparent_navs'	=> true,
		//'increaseHeight' 	=> true,
		'increaseWidth'		=> true,
		'whiteBackground' 	=> true,
		'hidePlayBtn' 		=> true,
		'autoPlay' 			=> false,
		'noPaddingBottom'	=> true,
		'muteBtn'			=> true,
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>From the high perch of the opulent Galvin at Windows, a striking restaurant situated on the 28th floor of the London Hilton on Park Lane, we filmed two videos for Stoli to promote the brand’s premium nature and ideal serving methods.</p>

				<h3>The Process</h3>
				<p>Our video team recorded Stolichnaya’s brand ambassador, Matthew Dakers, during a day shoot in order to show mixologists how best to serve Stoli’s Elit Vodka to produce the best final product for the customer.
The first video demonstrates how to serve a bottle of Elit Vodka to a table of customers in style, with dry ice billowing from a stylish bucket holding the distinctive Elit bottle. The second video covers how to make Stolichnaya’s revolutionary Elit Martini at a staggering -18c, the same temperature the vodka is filtered at.<br />During post-production, we edited and rendered the footage, applying a subtle backing track, lush animated branding and simple yet effective slow motion shots to enhance the theatre of the vodka’s delivery and match the elegance of Elit.</p>

				<h3>The Result</h3>
				<p>Both videos have been extended across the Elit brand and provide an inspiring way to excite and inform key stakeholders ordering and preparing Elit by Stolichnaya.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
