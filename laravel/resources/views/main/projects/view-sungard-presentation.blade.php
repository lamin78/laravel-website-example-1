@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll sungard-background-image-pre">
		<div class="black-out-layer"></div>

		@include('main._partials._culture_video_top', [
			'mobile_poster' => $videoObj['mobile-poster']
			,'video_poster' => $videoObj['poster']
			,'video_file' 	=> $videoObj['video']
			,'auto_play' 		=> 'false'
		])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>SunGard</h1>
				<h2>Conference Presentation</h2>
				<a href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
			</div>
		</div>
		@include('main.layouts.partials._header_video_controls')
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'From Basic Bullet Points To<br />PowerPoint Panache With SunGard',
				'h2' => 'Keeping customers in the loop with a colourful, <br />animated presentation',
				'p' => '<span>Client:</span>&nbsp;SunGard    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;Alabama, USA    <span>Category:</span>&nbsp;Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>With over 30 years of experience, SunGard provides its customers with one of the broadest ranges of solutions in the financial services industry, enabling them to choose options to suit their budget, business model and growth path.</p>
				<h3>The Brief</h3>
				<p>A frequent Article 10 client over several years, SunGard required our presentation team to breathe new life into existing content using PowerPoint. After many successful projects with Article 10, including presentations for internal meetings, conferences and sales teams, SunGard required simple information to be displayed to audiences in a more visual manner while staying true to the company’s recent rebrand.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>As the material to be included in the presentation was to be shown at a SunGard event, it was decided to use seamless slide builds and animation as part of a looping presentation to ensure that the messages were delivered clearly and in the most engaging way. The plan was for the presentation to catch the eye and retain attention as it played throughout the duration of the event.</p>
			</div>
		</div>
	</div>

	@include('main._partials._slider_container', [
	'before'	=> '/images/projects/sungard-presentation/slider/before.jpg',
	'after'		=> '/images/projects/sungard-presentation/slider/after.jpg'
	])

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>Article 10’s PowerPoint designers worked with existing SunGard content, simple titles and bullet point copy, to build the presentation, ensuring that each slide was both visually appealing and fit the new company branding. By harnessing PowerPoint’s great flexibility and customisation options, the presentation was designed to play automatically, with animations between slides, while also displaying at the best quality and in the right dimensions for the display screens at the event.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 interactive-site {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1 xxlarge-6 xxlarge-before-3 xxlarge-after-3 xlarge-8 xlarge-before-2 xlarge-after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-12 small-before-0 small-after-0">
				<div class="monitor not-mac">
					<div class="screen no-scrollbar">
						@include('main._partials._video_container', [
							'id'				=> '2',
							'transparent'		=> true,
							'minHeight' 		=> '100%',
							'videoFile'			=> $videoPageObj[1]['video'],
							'videoPoster'		=> $videoPageObj[1]['poster'],
							'adjustPlayBtnTop'	=> false,
						])
					</div>
				</div>
				<img src="/images/shared/arrows-try.svg" class="try-arrows" data-no-retina />
				<div class="try">See it for <span>yourself</span></div>
			</div>
		</div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>From simple pages of text, a dynamic, rolling presentation was created that looped throughout the event, attracting the attention of attendees. Through fluid movement and beautiful design and animation, the presentation compelled viewers to continue watching, engage and learn more about the SunGard brand. </p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
