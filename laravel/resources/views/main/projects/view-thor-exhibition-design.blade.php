@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background" style="background-image:url(/images/projects/{{ $imageDir }}/{{ $imageDir }}-header@0.3.jpg);">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>Thor Equities</h1>
				<h2>Stand Design and Build</h2>
				<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>
			</div>
		</div>

	</div>
	</div>
@endsection


@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'From Basic Bullet Points To<br />PowerPoint Panache With SunGard',
				'h2' => 'Keeping customers in the loop with a colourful, <br />animated presentation',
				'p' => '<span>Client:</span>&nbsp;SunGard    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;Alabama, USA    <span>Category:</span>&nbsp;Presentations',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>Thor Equities is a leading developer of luxury retail building environments within major global gateway cities across the Unites States, Latin America and Europe.</p>
				<h3>The Brief</h3>
				<p>The company attends many of the premier real estate events worldwide, including shows such as MAPIC, MIPIM UK and Expo Real. In order to maintain its brand’s reputation as a high-quality, luxury real estate business, a flexible stand design was required that boasted an obvious quality with the ability to evolve and adapt.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Article 10&rsquo;s exhibitions team designed and created a stand to match Thor Equities&rsquo; demand for quality while ensuring that it could be easily adapted to suit different environmental and messaging requirements.</p>

				<h3>The Process</h3>
				<p>Appearing in a range of forms, we created a back-lit routed feature map and 20 linear metres of light-box graphics to highlight Thor&rsquo;s key markets and real estate portfolio. A feature reception with embossed logos, interactive devices and screens, as well as an array of bespoke elements and furniture finished to a premium standard, were also created.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>As a beacon highlighting Thor&rsquo;s trademark style and attention to detail, the flexible stands provided fantastic exposure for the company at a range of shows across Europe. Throughout two years of sparkling events, Article 10 was on hand to reimagine and evolve the design in order to match the unique requirements of each location and enable the Thor Equities brand to truly shine.</p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
