@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background thre-background-image-pre">
	<div class="columns span-12 full-height {{ $colour }} fixed-background {{ $imageDir }}-header">
		<div class="black-out-layer menu-grey-1"></div>
		<div class="black-out-gradient-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>TH Real Estate</h1>
				<h2>Exhibition Programme</h2>
				<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>&nbsp;&nbsp;<a href="/services/digital-solutions" class="tag orange">Digital</a>
			</div>
		</div>

	</div>
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Touring Europe With TH Real<br /> Estate’s Evolving Exhibition Stand',
				'h2' => 'Multiple countries and configurations for the re-branded real estate investment business.',
				'p' => '<span>Client:</span>&nbsp;TH Real Estate    <span>Year:</span>&nbsp;2014-Present    <span>Location:</span>&nbsp;Stockholm, Cannes, Munich, London, Manchester, Berlin    <span>Category:</span>&nbsp;Environment Design',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>TH Real Estate is an innovative global investment management company, renowned for pioneering research and thought leadership. Specialising in real estate equity and debt investment, it has a portfolio of over $96.2bn worldwide.</p>
				<h3>The Brief</h3>
				<p>Following a competitive tender, Article 10 was appointed by TH Real Estate to fulfil its 2014 international event programme with a concept that promoted awareness of the brand and communicated the global scale of the company. In 2015, following several successful shows and industry awards, a formal rebrand required a major overhaul of its existing exhibition stand and collateral.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>With six shows on the roster, Article 10 developed its existing modern and interactive designs to reflect the evolution of the brand. The detailed brand refresh was designed to enhance TH Real Estate’s reputation as the 'go-to' investment manager in real estate while remaining true to its unique new style.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>In addition to reformatting all of the company’s digital content to landscape and making more information and tools available to staff and visitors via screens and devices, Article 10 combined dynamic stand architecture with feature shard graphics, angled walls and further branded elements as a spatial and tangible expression of the new look and feel. An extensive inventory of high-end spray-finished furniture pieces were designed and built, comprising modular reception and lounge banquette seating, low and mid height tables, brochure holders, a privacy screen and a plinth for a model showcasing one of the company’s flagship development schemes -  Edinburgh St. James. Highlighting the importance of the scheme, the prominently displayed architectural model was supported by graphics, literature and screen content.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The stand and supporting materials, used in various guises at events across Europe, including Stockholm, Cannes, Munich and London, is constantly evolving and can be deployed to match the event space available. This was the case for TH Real Estate’s attendance at MIPIM in Cannes, where a luxury yacht provided the setting for client meetings and presentations, along with collateral specifically created to draw attention to the unique location. Now in its fourth year of use, our client’s exhibition concept continues to evolve as the company does, with Expo Real 2016 seeing the introduction of a programmable <a class="yellow" href="/projects/thre-office-environments">Tomorrow’s World LED map and bespoke kiosk</a>. Following a successful show debut, the unique visual was extended to cover 77 cities and has been reimagined as a permanent fixture at the company’s HQ in Bishopsgate.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-4" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h1 class="award-title {{ $colour }}">Awards for this Project</h1>
				<ul class="project-more-awards">
					<li><img src="/images/projects/{{ $imageDir }}/estates-award.jpg"></li>
					<li class="spacer">&nbsp;</li>
					<li><img src="/images/projects/{{ $imageDir }}/property-award.jpg"></li>
				<ul>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
