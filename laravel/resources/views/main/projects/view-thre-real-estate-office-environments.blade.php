@extends('main.layouts.main')

@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll thre-office-background-image-pre">
		<div class="black-out-layer"></div>

		@include('main._partials._culture_video_top', [
			'mobile_poster' => $videoObj['mobile-poster']
			,'video_poster' => $videoObj['poster']
			,'video_file' 	=> $videoObj['video']
			,'auto_play' 		=> 'false'
		])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
		</div>

		<div class="row full project-head">
			<div class="columns span-10 before-1 after-1 ">
				<h1>TH Real Estate</h1>
				<h2>Office Environment</h2>
				<a href="/services/exhibitions" class="tag {{ $colour }}">Exhibitions</a>
			</div>
		</div>
		@include('main.layouts.partials._header_video_controls')
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Innovation And Design From Expo To HQ',
				'h2' => 'Charting the journey of TH Real Estate&rsquo;s LED map feature from trade show booth to a unique office interior.',
				'p' => '<span>Client:</span>&nbsp;TH Real Estate <span>Year:</span>&nbsp;2017 <span>Location:</span>&nbsp;London, UK	<span>Category:</span>&nbsp;Office Environments'
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>TH Real Estate is one of the largest real estate investment managers in the world, managing a suite of nearly 80 funds and mandates. With offices in 21 cities throughout the US, Europe and Asia-Pacific, it employs over 450 professionals and offers 60 years of investment experience.</p>
				<h3>The Brief</h3>
				<p>As part of <a href="/services/exhibitions/view/thre-real-estate-exhibition-design">TH Real Estate&rsquo;s exhibition presence</a> at Expo Real 2016, Article 10 created a programmable LED map inspired by the company&rsquo;s Tomorrow&rsquo;s World ethos. Originally designed and developed as part of a bespoke kiosk and transported to shows across Europe, the global map was conceived as six separate shard graphics with LED lights transitioning through city categories. The dynamic and informative exhibition feature was designed to showcase TH Real Estate&rsquo;s research capabilities by identifying future-proof cities and, after a successful debut, the company requested a permanent version for use within its Bishopsgate headquarters. </p>
				<br>
				<br>
				<a href="/services/exhibitions/view/thre-real-estate-exhibition-design" class="button">Previous case study</a>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Situated on the 9th floor of its stylish central London location, the office gallery presented obvious physical and logistical challenges that had to be overcome, along with styling considerations to complement the existing format and décor within the space. An existing five metre kinked wall was earmarked for the new LED map, four metres of which had to be completely reformed to deliver an interior display that not only fitted the surroundings but also merged physically, working in harmony with the existing, distinctive gallery furniture. In line with TH Real Estate&rsquo;s brand and visual guidelines, which feature prominent shards and angles, and following modifications to the kinked wall, distinctly positioned undulating facets were scribed into the walling, with shard graphics overlaid.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Process</h3>
				<p>The Tomorrow&rsquo;s World map was revised and extended to feature 77 cities illuminated with colour changing LED lights set to operate in a pre-set sequence and highlight global locations and their relevance within five specific investment categories. TH Real Estate&rsquo;s interactive kiosk, also designed and built by Article 10 and used as part of the company&rsquo;s broad exhibition presence, features a globe application, which combines with the new installation as part of an interactive and visually engaging public space. To complement the LED map, an explanatory graphic space was also included in the final design, as well as bespoke, on-brand literature holders and lighting to complete the informative and eye-catching interior piece. </p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-3" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The stunning outcome has become the centrepiece of TH Real Estate&rsquo;s London office, with visitors invited to explore the space freely. Taking in architectural models, videos-walls and a library, as well as making use of a café area directly adjacent, the unique installation enables visitors and staff to absorb and interact with exciting visuals while learning about the company and its values. Within a comfortable setting, this impressive interior piece retains all the spectacle of its exhibition booth origins while perfectly matching the office environment that surrounds it.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-4" data-speed="10"></div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
