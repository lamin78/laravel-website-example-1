@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll scroll-to-video" style="background-color:#000; background-image:url({{ $videoObj['poster'] }});">
		<div class="columns span-12 full-height">
			<div class="black-out-layer menu-grey-1"></div>
			<div class="black-out-gradient-layer"></div>
			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
			</div>

			<div class="row full project-head">
				<div class="columns span-10 before-1 after-1 ">
					<h1>TH Real Estate</h1>
					<h2>Interactive web application design and build</h2>
					<a href="/services/digital-solutions" class="tag {{ $colour }}">Digital</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content')
	<div class="columns span-12 project-intro {{ $colour }}">
		<div class="row full">

			@include('main.projects._partials._project_header', [
				'h1' => 'Enabling Users To Get In Touch With <br />TH Real Estate’s R&amp;D',
				'h2' => 'A stylish, interactive digital solution showcasing global trends',
				'p' => '<span>Client:</span>&nbsp;TH Real Estate    <span>Year:</span>&nbsp;2015    <span>Location:</span>&nbsp;London, UK    <span>Category:</span>&nbsp;Digital',
			])

			<div class="columns span-6 before-3 after-3 xxlarge-4 xxlarge-before-4 xxlarge-after-4 sml-10 sml-before-1 sml-after-1 description">
				<h3>The Company</h3>
				<p>TH Real Estate is an innovative global investment management company specialising in real estate equity and debt investment, with a portfolio of over £58.7bn worldwide.</p>
				<h3>The Brief</h3>
				<p>In order to showcase TH Real Estate’s impressive R&D work, Article 10 created an interactive solution, accessible online and across various devices. The stylish, bespoke globe interface enables staff and prospective clients to view key investment insights across a number of locations, alongside insightful facts and beautiful photography.</p>
			</div>

		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-1" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Approach</h3>
				<p>Through engagement with key TH Real Estate stakeholders, Article 10&rsquo;s digital team proposed an interactive 3D globe interface, capable of running on computers, kiosk touch screen devices and tablets, that provided a simple yet modern way to explore the company&rsquo;s R&amp;D work. Starting with the globe view, users could then zoom in to different locations and view key statistics. The style and usability of this futuristic solution would complement TH Real Estate&rsquo;s modern, forward-looking approach to investment and the cutting-edge nature of its research.</p>
				<h3>The Process</h3>
				<p>Article 10&rsquo;s digital design and development team crafted the unique solution making use of a state of the art JavaScript and WebGL framework for the interactive 3D rendering, with a Flash fall-back for older computers. Stunning imagery was included alongside TH Real Estate branding, while content provided by the company, including copy, statistics and graphs for each city, was added throughout.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 parallax">
		<div class="bg {{ $imageDir }}-body-2" data-speed="10"></div>
	</div>

	<div class="columns span-12 project-more">
		<div class="row">
			<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
				<h3>The Result</h3>
				<p>The completed solution enables staff and clients to interact intuitively with the globe interface, tapping on a continent to reveal a map view and then zooming into specific locations. The user can then interact with animated graphs, facts and ratings, plus beautiful photography aligned to the chosen city.
				<br />The interactive solution has been a big hit within TH Real Estate and among its clients as an ultra-modern, high-tech and stylish way to showcase the brand and its capabilities. </p>
			</div>
		</div>
	</div>

	@include('main.projects._partials._project-stats')

	@include('main.projects._partials._project-footer')

@endsection
