@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color: #000; background-image:url(/images/campaigns-landing-page/campaigns-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(/images/campaigns-landing-page/campaigns-page-header@2x.jpg);">
		<div class="black-out-layer"></div>

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
			<div class="we-are-block animatedParent animateOnce go {{ $colour }}">
				<div class="tbl">
					<div class="inner">
						We are<br/>
						<span class="hilite animated growIn">Campaign</span><br/>
						Cultivators<br/>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 intro {{ $colour }}">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>We create collateral you can count on</h1>
				<p>For every campaign, big or small, there are key touch points that often mean the difference between effectively conveying your message and drowning in a sea of mediocre marketing. The former is the type of collateral we pride ourselves on producing. Want a banner to shine online while working equally well in an exhibition hall? Not a problem. Need a powerful publication, pamphlet or promotional posters? All in a day&rsquo;s work. Let&rsquo;s talk about your requirements and we&rsquo;ll come up with the creative to match.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 service-content {{ $colour }} white-background">
		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-campaigns-page/aarhus-campaigns.jpg" class="fix-height"/>
			</div>
			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>Aarhus</h2>
						<h3>External Promotional Campaign</h3>
						<p>Aarhus will be the European City of Culture in 2017, and required integrated marketing in the lead up to its year in the spotlight. Engaging our copywriting, design and project management teams, Article 10 has produced a range of collateral so far including logos, posters, roller banners, print adverts and social media imagery as part of an exciting, inspirational and on-going campaign.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/aarhus" class="button">View Aarhus case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-campaigns-page/kfc-campaigns.png" class="img-left fix-height"/>
			</div>

			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>KFC</h2>
						<h3>Campaign Identity</h3>
						<p>Article 10&rsquo;s relationship with KFC continues to sizzle like a fresh bucket of The Colonel&rsquo;s famous fried chicken. Working across a range of internal KFC teams, Article 10 has provided branding, internal communications, infographics, videos and PowerPoint presentations.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/kfc-people-promise" class="button">View KFC case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-campaigns-page/microsoft-editorial-campaigns.jpg" class="fix-height"/>
			</div>
			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>Microsoft</h2>
						<h3>Graduate Recruitment Campaign</h3>
						<p>Microsoft offers highly sought after intern, graduate and apprentice schemes and delivers an annual campaign to speak to the next generation of employees. Article 10 delivered a fully integrated campaign, evolving the existing visual identity and creating a wealth of material from printed brochures, animated case studies and talking head videos, to photoshoots, exhibition display graphics, merchandise design and print and ambient graphics. Since the 2014 project, Article 10 has continued to work with Microsoft’s recruitment team to deliver vibrant new early in career campaigns and collateral.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/microsoft-universities" class="button">View Microsoft case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-campaigns-page/microsoft-campaigns.jpg" class="img-left fix-height"/>
			</div>
			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>Microsoft</h2>
						<h3>Integrated Marketing Campaign</h3>
						<p>We went to town to promote The Microsoft Virtual Academy across a selection of techie-friendly branded collateral. The comic book-inspired campaign was based around technology themed superheroes and featured visual identity, character design and illustration, plus development of an HTML 5 landing page, creation of collectible stress toys and other printed material and merchandise, including Top Trumps cards. In order to promote the campaign we also designed and developed HTML emails and online social banners.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/microsoft-mva" class="button">View Microsoft case study</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--// Awards Section //-->
	<div class="columns span-12 awards green">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h2>Awards</h2>
				<p>Article 10 has been recognised for the excellence of its information security campaigns by a number of official bodies, including the SC Europe and Real IT awards.</p>
				<div class="row full">
					<div class="columns span-6 before-3 after-3 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<div class="row full">
								<div class="columns span-12"><img src="/images/services-campaigns-page/gold_quill.png" alt="Property Awards 2015 Finalist" title="Property Awards 2015 Finalist" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--
	@include('main._partials._case_study', [
		'page' => 'services',
		'panMove'	=> true,
		'fixMaxHeight'	=> true,
	])
	//-->
@endsection
