@extends('main.layouts.main')


@section('header')
	
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(/images/services-presentation-page/digital-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(/images/services-presentation-page/digital-landing-page-header@1x.jpg);">
			<div class="black-out-layer "></div>
			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
				<div class="we-are-block animatedParent animateOnce go orange">
					<div class="tbl">
						<div class="inner">
							WE ARE<br/>
							<span class="hilite animated growIn">DIGITAL</span><br/>
							DRIVEN
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	
@endsection


@section('content')
	<div class="columns span-12 intro orange">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>Turning 1s and 0s into breath-taking solutions, tailored to your needs</h1>
				<p>Pushing the envelope and redefining the traditional with a thirst for the best ways to use the latest tech, some of our digital designers have even been known to bleed binary.</p>
				<p>We don’t make our lives easy. It’s not about the tools that work, it’s about solutions that work for you. That’s why we don’t rely on single platforms and languages to produce fully responsive websites, intranet solutions integrated with third-party applications, systems and mobile apps, eLearning courses or desktop and social media applications. We seek out the best technology in order to deliver solid, high performance output. We’re also certified Microsoft and Apple developers, which means our solutions always work, whatever the platform.</p>
			</div>
		</div>
	</div>
	
	<div class="columns span-12 service-content orange">
	
		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/digital-landing-page/stoli-ipad.png" class="img-left"/>
			</div>

			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>Stoli</h2>
						<h3>App Design, Development and Deployment</h3>
						<p>The history of Stolichnaya vodka is fascinating; now even more so thanks to an iPad app designed and built by Article 10. Fully interactive and platform native, the app showcases the Stoli brand, provides collateral for sales teams and tells the story of Stolichnaya, its processes and evolution over time in conjunction with breathtaking photography, all packaged in an intuitive interface.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/stoli-app-design" class="button">View Stoli case study</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/digital-landing-page/th-mac.png" class="fix-width" />
			</div>
			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>TH Real Estate</h2>
						<h3>Interactive Web Application Design and Build</h3>
						<p>In order to showcase TH Real Estate’s impressive R&D work, Article 10 created an interactive solution, accessible online and across various devices. The stylish, bespoke globe interface enables staff and prospective clients to view key investment insights across a number of locations, alongside insightful facts and beautiful photography.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/thre-tablet-presentation" class="button">View TH Real Estate case study</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/digital-landing-page/openbet-windows-tablet.png" class="img-left"/>
			</div>

			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2>OpenBet</h2>
						<h3>Microsite Design and Build</h3>
						<p>The sporting calendar rarely sees as many bets placed as on Grand National weekend. For providers of gaming services, this can mean testing times - but not for those working with OpenBet. Its platform remains rock solid, enabling clients to perform for their punters and ensure, without fail, that everyone can have a flutter on the fillies. We built a dynamic microsite to highlight OpenBet’s impressive performance during one of the biggest horse races of the year.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/openbet-website-redesign" class="button">View OpenBet case study</a>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row full project">
			<div class="columns span-6 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/digital-landing-page/old-uppinghamian-ipad.png" class="fix-width" />
			</div>
			<div class="columns span-6 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1">
						<h2 class="hidden-small">Old Uppinghamian</h2>
						<h2 class="hidden-xxlarge hidden-xlarge hidden-large hidden-medium">OU</h2>
						<h3>Website Design and Build</h3>
						<p>A school with such a distinguished history and notable alumni deserves a dynamic website. That was the belief of the Uppingham School alumni network, Old Uppinghamian. Our digital production team consulted on the revamp to the existing site and produced a responsive, modern web presence that looks great across all devices. With a dynamic video running across the home page and an entirely new CMS back end, Article 10&rsquo;s digital work brought Old Uppinghamians into a new online era.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/old-uppinghamian-website-redesign" class="hidden-small button">View Old Uppinghamian case study</a>
						<a href="/{{ $section }}/{{ $subpage }}/view/old-uppinghamian-website-redesign" class="hidden-xxlarge hidden-xlarge hidden-large hidden-medium button">View OU case study</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--
	@include('main._partials._case_study', [
		'page' => 'services',
		'panMove'	=> true,
		'fixMaxHeight'	=> true,
	])
	//-->
@endsection
