@extends('main.layouts.main')

@section('header')

	<!-- <div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(/images/events-landing-page/events-landing-page-header@0.1.jpg);"> //-->
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-color: transparent !important; background-image:url({{ $videoObj['poster'] }});">
			<div class="black-out-layer"></div>

			@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
				<div class="we-are-block animatedParent animateOnce go yellow">
					<div class="tbl">
						<div class="inner">
							WE ARE<br/>
							<span class="hilite animated growIn">EXHIBITION</span><br/>
							EXPERTS
						</div>
					</div>
				</div>
				@include('main.layouts.partials._header_video_controls')
			</div>
		</div>
	<!-- </div> //-->

@endsection


@section('content')
	<div class="columns span-12 intro yellow">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>What&rsquo;s wrong with being true exhibitionists?</h1>
				<p>You&rsquo;d have to go back as far as when Justin Bieber was just a terrifying twinkle in his mother&rsquo;s eye to match the years of combined experience our exhibitions team offers. During that time we&rsquo;ve done it all, from intimate gatherings to in your face interactive arenas and loved every minute. We know the shows, the venues, the producers and the protocol and are backed by our integrated creative team. What more could you need to make the biggest splash at your next event?</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 service-content yellow animatedParent no-constraint">

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/thre-events.jpg" class="no-img-constraint"/>
			</div>
			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>TH Real Estate</h2>
						<h3>Exhibition Programme</h3>
						<p>With a range of exhibitions across Europe as well as a major rebrand, TH Real Estate required the full range of Article 10 services to showcase its significant offering to the industry. Delivering an evolving and adaptable exhibition stand and a range of collateral, our teams fully immersed themselves in the real estate investment business.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/thre-real-estate-exhibition-design" class="button">View TH Real Estate case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/thre-office-events.jpg" class="no-img-constraint"/>
			</div>
			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>TH Real Estate</h2>
						<h3>Office Environment</h3>
						<p>Our design and exhibitions teams worked in harmony to transform TH Real Estate’s programmable LED map feature and kiosk from trade show booth to a unique office interior, reimagining the focal point of the company’s exhibition presence as the centrepiece of its London office.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/thre-office-environments" class="button">View TH Real Estate case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/mipim-equities-events.jpg" class="no-img-constraint"/>
			</div>
			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-right">
						<h2>MIPIM</h2>
						<h3>Taking the Show by Storm</h3>
						<p>With five clients exhibiting at the same time, our creative and exhibitions teams endured a busy period in the build-up to one of the real estate industry’s biggest events. MIPIM, in Cannes, France saw many of Article 10’s clients under one roof, meaning we needed to be on top of our game in order to provide the highest level of service.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/mipim-exhibition-design" class="button">View MIPIM case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/mail-online-events.jpg" class="no-img-constraint"/>
			</div>
			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-right">
						<h2>MailOnline</h2>
						<h3>Roadshow Design and Build</h3>
						<p>Helping to keep event attendees entertained while promoting the MailOnline brand on social media, Article 10 quickly developed, designed and built an interactive concept that demanded attention. Fully staffed and backed by innovative technology, the custom event stand enabled visitors to enjoy a taste of celebrity and share it with friends online.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/mail-online-exhibition-design" class="button">View Mail Online case study</a>
					</div>
				</div>
			</div>
		</div>


		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgR pull-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/thor-equities-events.jpg" class="no-img-constraint"/>
			</div>
			<div class="columns span-6 large-12  medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>Thor Equities</h2>
						<h3>Stand Design and Build</h3>
						<p>Now in its third year of use, Article 10 continues to realise an exhibition concept that is the embodiment of Thor’s distinctive brand. While the stands boast an obvious quality, they are inherently versatile and ever evolving, appearing in various sizes and guises at shows such as MAPIC, MIPIM UK and Expo Real. A back-lit routed feature map and 20 linear metres of light-box graphics highlighting Thor’s key markets and real estate portfolio are just a couple of the professional touches we have delivered.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/thor-exhibition-design" class="button">View Thor Equities case study</a>
					</div>
				</div>
			</div>
		</div>


		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-events-page/bdp-events.jpg" class="no-img-constraint"/>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>BDP</h2>
						<h3>Stand Design and Build</h3>
						<p>When BDP attended the inaugural MIPIM UK, the UK's first dedicated property trade show, it wanted to create an exhibition space that was about more than simply providing a place for people to meet. Article 10 designed an exhibition environment complete with an artist producing a fascinating installation before the eyes of attendees. Using only an iPad as a guide, the artist drew buildings from throughout the company’s history to create a unique cityscape across an entire 6 linear metre wall.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/bdp-exhibition-design" class="button">View BDP case study</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--// Awards Section //-->
	<div class="columns span-12 awards yellow">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h2>Awards</h2>
				<div class="row full">
					<div class="columns span-6 before-3 after-3">
						<div class="row full">
								<div class="columns span-4 medium-12 small-12"><img src="/images/services-events-page/property_awards.jpg" alt="Property Awards 2015 Finalist" title="Property Awards 2015 Finalist" /></div>
								<div class="columns span-4 medium-12 small-12"><img src="/images/services-events-page/estates_awards.png" alt="Estates Gazette Awards 2014 Winner" title="Estates Gazette Awards 2014 Winner" /></div>
								<div class="columns span-4 medium-12 small-12"><img src="/images/services-events-page/clothes_show_award.png" alt="Clothes Show Live" title="Clothes Show Live" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--
	@include('main._partials._case_study', [
		'page' => 'services',
		'panMove'	=> true,
		'fixMaxHeight'	=> true,
	])
	//-->
@endsection
