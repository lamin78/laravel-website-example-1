@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color: #000; background-image:url(images/services-landing-page/services-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(images/services-landing-page/services-landing-page-header@1x.jpg);">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">READY</span><br/>
								TO DELIVER
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12">
		<div class="row">
			<div class="columns span-10 before-1 after-1 intro">
				<h1>What do you need?</h1>
				<p class="need invisible" data-id="1">We approach every project with fresh eyes and bring new ideas, plus the latest technologies and techniques to the table.</p>
				<p class="need invisible" data-id="2">Whatever you want to present, however you want to present it, we can help, with the full range of presentation design services.</p>
				<p class="need invisible" data-id="3">Across screens of all sizes we can design digital solutions that fulfil your needs, with cutting edge technology and rock solid performance.</p>
				<p class="need invisible" data-id="4">Telling stories in the most engaging way is what we’re all about. That’s why brands turn to us when they need captivating video content.</p>
				<p class="need invisible" data-id="5">Our talented team of writers and designers can turn your ideas into a wide range of collateral across print and digital.</p>
				<p class="need invisible" data-id="6">Whatever your sector, however large your requirement, our specialist Exhibitions team is ready to help you stand out.</p>
			</div>
		</div>
	</div>


	<div class="columns span-12 services">
		<div class="row full">
			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="1">
				<a href="/services/presentation-solutions">
					<img class="icon" src="/images/services-landing-page/presentations-icon.svg" data-no-retina />

					<div class="label">
						<h2>Presentations</h2>
						<p>PowerPoint or Prezi, internal or external, automated, animated, interactive, with speaker notes, video or all of the above. Whatever you want to present, however you want to present it, we can help, with the full range of presentation design services, starting with copywriting and structure all the way through to artwork, design and animation.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="2">
				<a href="/services/video-animation-3d">
					<img class="icon" src="/images/services-landing-page/video-icon.svg" data-no-retina />

					<div class="label">
						<h2>Video, Animation &amp; 3D</h2>
						<p>Telling stories in the most engaging way is what we&rsquo;re all about. That&rsquo;s why brands turn to us when they need captivating video content like motion graphics, glorious 3D animation, skilfully filmed and edited video spots, viral videos or informative talking heads.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="3">
				<a href="/services/digital-solutions">
					<img class="icon" src="/images/services-landing-page/digital-icon.svg" data-no-retina />

					<div class="label">
						<h2>Digital</h2>
						<p>Across screens of all sizes we can design digital solutions that fulfil your needs. Whether you need an app, a fully responsive web presence, eLearning course, immersive experience or a full-scale corporate website, we provide the cutting edge design, technology and rock solid performance to suit.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="4">
				<a href="/services/campaigns">
					<img class="icon" src="/images/services-landing-page/campaigns-icon.svg" data-no-retina />

					<div class="label">
						<h2>Campaigns</h2>
						<p>Whether you want to deliver your message internally or externally, online or in person, our talented team of writers and designers can turn your ideas into a wide range of collateral. With posters, banners, brochures, books and branding just a handful of the options available, our output is always powerful, always professional.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="5">
				<a href="/services/exhibitions">
					<img class="icon" src="/images/services-landing-page/exhibitions-icon.svg" data-no-retina />

					<div class="label">
						<h2>Exhibitions</h2>
						<p>Whatever your sector, however large your requirement, our 30 years of combined event experience has provided us with specialist exhibition knowledge. Across sports media, property and retail and many other sectors, we know the organisers, venues and shows and are ready to help you stand out.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>

			<div class="columns span-6 large-12 medium-12 small-12 block" data-id="6">
				<a href="/services/information-security">
					<img class="icon" src="/images/services-landing-page/infosec-icon.svg" data-no-retina />

					<div class="label">
						<h2>Information Security</h2>
						<p>While you may know many of the risks associated with information security, do your employees? We deliver engaging information security awareness campaigns to some of the largest organisations in the world, transforming their staff into information security ambassadors and limiting the threat of cyber crime.</p>
						<span class="button">Click here to find out more&hellip;</span>
					</div>
				</a>
			</div>



		</div>
	</div>

	@include('main._partials._quotes', ['colour' => 'purple'])

	@include('main._partials._clients')

@endsection
