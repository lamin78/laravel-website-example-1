@extends('main.layouts.main')


@section('header')
	
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(/images/services-presentation-page/infosec-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(/images/services-presentation-page/infosec-landing-page-header@1x.jpg);">
			<div class="black-out-layer"></div>
			<div class="row full full-height nav-outer">
				<div class="columns span-10 before-1 after-1 nav">
					@include('main.layouts.partials._header')
				</div>
				<div class="we-are-block animatedParent animateOnce go teal">
					<div class="tbl">
						<div class="inner">
							WE ARE<br/>
							<span class="hilite animated growIn">INFORMATION SECURITY</span><br/>
							SPECIALISTS
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection


@section('content')
	<div class="columns span-12 intro infosec-intro">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>Insider threat no more.</h1>
				<p>While you may know many of the risks associated with information security, do your employees? We deliver engaging information security awareness campaigns to some of the world’s biggest global brands, enabling them to fight the threat of cyber crime and turn their most vulnerable asset – their people, into their strongest defence.</p>
			</div>
		</div>
	</div>
	
	<div class="columns span-12 service-content infosec-content">
		<div class="row">
			<div class="columns span-8 before-2 after-2 infosec-wrap">
				<h1 class="white-text">Let&rsquo;s talk about...</h1>
				<svg class="rotate-image" width="150" height="150">
					<image xlink:href="/images/services-infosec-page/creative-framework.svg" src="/images/services-infosec-page/creative-framework.svg" width="150" height="150" />
				</svg>
				<h2 class="white-text">Creative Framework</h2>
				<p class="white-text">Leverage the power of your brand to create a unique and powerful identifier that will improve the effectiveness of your awareness campaigns.</p>
			</div>
			
			<div class="columns span-8 before-2 after-2 infosec-wrap pad-top50">
				<svg class="reduced-size" width="120" height="150">
					<image xlink:href="/images/services-infosec-page/behavioural-risk.svg" src="/images/services-infosec-page/behavioural-risk.svg" width="120" height="150" />
				</svg>
				<h2 class="white-text">Behavioural Risk Analysis</h2>
				<p class="white-text">How much do your employees already know about information security? Our experience and trusted methodology enables us to accurately analyse information security behaviours and attitudes company-wide.</p>
			</div>
			
			<div class="columns span-8 before-2 after-2 infosec-wrap pad-top50">
				<svg width="150" height="150">
					<image xlink:href="/images/services-infosec-page/awareness.svg" src="/images/services-infosec-page/awareness.svg" width="150" height="150" />
				</svg>
				<h2 class="white-text">Awareness</h2>
				<p class="white-text">Benefit from a bespoke information security awareness programme, tailored to the needs of your organisation, that enhances employee consciousness and drives change.
</p>
			</div>
			
			<div class="columns span-8 before-2 after-2 infosec-wrap pad-top50">
				<svg class="increase-size" width="195" height="195">
					<image xlink:href="/images/services-infosec-page/education.svg" src="/images/services-infosec-page/education.svg" width="195" height="195" />
				</svg>
				<h2 class="white-text">Education</h2>
				<p class="white-text">Ensuring information security best practice is absorbed and implemented throughout your business. Our education solutions include eLearning courses, presentations and interactive digital tools.</p>
			</div>
		</div>
	</div>
	
	<div class="columns span-12 service-content infosec-content-bottom">
		<div class="row">
			<div class="columns span-8 before-2 after-2 infosec-bottom-wrap">
				<h1>Awards</h1>
				<p>Article 10 has been recognised for the excellence of its information security campaigns by a number of official bodies, including the SC Europe and Real IT awards.</p>
				<div class="row full">
					<div class="columns span-6 small-12">
						<img class="pull-right" src="/images/services-infosec-page/sc-awards.jpg" />
					</div>
					<div class="columns span-6 small-12">
						<img class="pull-left" src="/images/services-infosec-page/real-it.jpg" />
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection
