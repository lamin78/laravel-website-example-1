@extends('main.layouts.main')

@section('header')

	<div class="columns span-12 full-height">
		<div class="black-out-layer opaque push-back"></div>
		@include('main._partials._culture_video_top', ['video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

		<div class="row full full-height nav-outer">
			<div class="columns span-10 before-1 after-1 nav">
				@include('main.layouts.partials._header')
			</div>
			<div class="we-are-block animatedParent animateOnce go red">
				<div class="tbl">
					<div class="inner">
						WE ARE<br/>
						<span class="hilite animated growIn">VIDEO, ANIMATION &amp; 3D</span><br/>
						OBSESSED
					</div>
				</div>
			</div>

			@include('main.layouts.partials._header_video_controls')
		</div>
	</div>

@endsection


@section('content')
	<div class="columns span-12 intro red">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>We shoot, we score<br/>(and script, edit and animate)</h1>
				<p>Shooting, scripting, storyboarding, sharing and screening - we love all aspects of digital video and animation and provide a full range of solutions, from animation services to 3D, explainer, corporate and viral videos.</p>
				<p>We produce freshly shot and edited video spots, illustrative motion graphics and photorealistic virtual 3D, plus event openers, corporate campaigns, talking heads and videos for social media. Let&rsquo;s have a chat about your next project and you&rsquo;ll soon realise we&rsquo;re bursting with ideas, backed up by pixel-perfect performance.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 service-content red animatedParent no-constraint">

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL background-image-constraint-open-bet plus-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-motion-page/open-bet.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge"/>
				<!--<img src="/images/services-motion-page/open-bet_1280.jpg" class="no-img-constraint hidden-xxlarge hidden-large hidden-medium hidden-small"/>
				<img src="/images/services-motion-page/open-bet_960.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge hidden-medium hidden-small"/>
				<img src="/images/services-motion-page/open-bet_768.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge hidden-large hidden-medium hidden-small"/>//-->
			</div>
			<div class="columns span-6 large-12 medium-12 small-12 minus-left">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-right">
						<h2>OpenBet</h2>
						<h3>Promotional Animation</h3>
						<p>Betting is no longer just about chaps visiting their local bookie and picking their favourite horse. There’s a new wave of casual gamblers, iPhone flutterers and casino chancers that want to play on their terms and at a time that suits them. OpenBet offers companies an omni-channel platform to provide this and, through a colourful, informative animated video, Article 10 told its story.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/omni-channel" class="button">View OpenBet case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgR pull-right background-image-constraint-kelway">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-motion-page/kelway.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge"/>
				<!-- <img src="/images/services-motion-page/kelway_1280.jpg" class="no-img-constraint hidden-xxlarge hidden-large hidden-medium hidden-small"/> //-->
			</div>
			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>Kelway</h2>
						<h3>Corporate Video</h3>
						<p>In order to deliver Kelway&lsquo;s &rdquo;Beyond Technology&rdquo; message, we needed to think beyond traditional video and motion graphics. In a stirring video spot, our video team matched footage of cityscapes, construction and powerful landmarks with impactful on screen statements to reinforce the benefits of Kelway&rsquo;s IT services and solutions. Featuring beautiful, stylised cinematic footage and a powerful soundtrack, the video stands out as a truly emotive piece.</p>
						<!-- <a href="/{{ $section }}/{{ $subpage }}/view/kelway-corporate-video" class="button">View Kelway case study</a> //-->
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL background-image-constraint-castrol plus-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-motion-page/castrol.jpg" class="no-img-constraint hidden-xlarge"/>
				<img src="/images/services-motion-page/castrol_1280.jpg" class="no-img-constraint hidden-xxlarge hidden-large hidden-medium hidden-small"/>
			</div>
			<div class="columns span-6 large-12 medium-12 small-12 minus-left">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-right">
						<h2>Castrol</h2>
						<h3>Promotional Video</h3>
						<p>Two exhilarating days of high-octane action captured and edited by Article 10 for Castrol. Stunning motion graphics, fast-paced cuts and thrilling in-car footage combined in this action packed promotional video showcasing a two day Independent Workshop Experience. Complete with animated branding and vox pops from satisfied attendees, this video quickens the pulse and shortens the breath.</p>
						<!-- <a href="/{{ $section }}/{{ $subpage }}/view/castrol-presentation" class="button">View Castrol case study</a> //-->
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgR pull-right background-image-constraint-microsoft" >
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-motion-page/microsoft.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge"/>
				<!-- <img src="/images/services-motion-page/microsoft_1280.jpg" class="no-img-constraint hidden-xxlarge hidden-large hidden-medium hidden-small"/>//-->
			</div>

			<div class="columns span-6 large-12 medium-12 small-12">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-left">
						<h2>Microsoft</h2>
						<h3>Motion Graphics Animation</h3>
						<p>We jumped at the chance to create a simple yet powerful video for Microsoft to be used at its annual Future Decoded event. Scripted by our in-house copywriter and brought to life through colourful, hand drawn assets and animation, Your Future Decoded saw collaboration across our creative departments and paints a picture of a future brought about by new and emerging Microsoft technologies.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/future-decoded-2014" class="button">View Microsoft case study</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row full project no-constraint">
			<div class="columns span-6 large-12 medium-12 small-12 sideImgL background-image-constraint-etf plus-right">
				@include('main._partials._shim', ['class' => 'shim'])
				<img src="/images/services-motion-page/etf.jpg" class="no-img-constraint hidden-xxlarge hidden-xlarge"/>
				<!-- <img src="/images/services-motion-page/etf_1280.jpg" class="no-img-constraint hidden-xxlarge hidden-large hidden-medium hidden-small"/> //-->
			</div>
			<div class="columns span-6 large-12 medium-12 small-12 minus-left">
				<div class="row">
					<div class="columns span-8 before-2 after-2 large-10 large-before-1 large-after-1 medium-10 medium-before-1 medium-after-1 small-10 small-before-1 small-after-1 no-constraint-text-right">
						<h2>ETF</h2>
						<h3>Broadcast Adverts</h3>
						<p>In order to promote the ETF across its partner countries and wider territories, Article 10 was enlisted to shoot and edit a series of TV adverts for broadcast globally. The five inspirational videos were shot on location in Croatia, London and Reading and featured a range of job roles to provide awareness of the organisation&lsquo;s initiatives and goals.</p>
						<a href="/{{ $section }}/{{ $subpage }}/view/etf-advert" class="button">View ETF case study</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--
	@include('main._partials._case_study', [
		'page' => 'services',
		'panMove'	=> true,
		'fixMaxHeight'	=> true,
	])
	//-->
@endsection
