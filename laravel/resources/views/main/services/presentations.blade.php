@extends('main.layouts.main')


@section('header')

<div class="columns span-12 full-height fixed-background background-scroll presentation-background-image-pre">
	<div class="columns span-12 full-height fixed-background background-scroll presentation-background-image">
		<div class="columns span-12 full-height">
			<div class="black-out-layer"></div>
				@include('main._partials._culture_video_top', ['mobile_poster' => $videoObj['mobile-poster'], 'video_poster' => $videoObj['poster'], 'video_file' => $videoObj['video']])

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go red">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">PRESENTATION</span><br/>
								PERFECTIONISTS
							</div>
						</div>
					</div>

					@include('main.layouts.partials._header_video_controls')
				</div>
			</div>
		</div>
	<div>

@endsection


@section('content')

	<div class="columns span-12 intro pink">
		<div class="row">
			<div class="columns span-10 before-1 after-1">
				<h1>Taking your PowerPoint deck from outline to outstanding</h1>
				<p>Turning a series of bullet points on slides into a genuinely engaging and memorable presentation doesn't come easily to most. Fortunately for our presentation designers, it's as natural as breathing in and out. You provide us with the raw materials, an idea, an outline or other materials and we'll work with you to create a stunning slideshow, using your brand guidelines and voice, that tells your story. Whether you need an end of year report, sales toolkit, company presentation, proposal or custom PowerPoint template from scratch, let our presentation-perfecting writers and designers transform your message into an audience-grabbing masterpiece, no matter how diverse the topic.</p>
			</div>
		</div>
	</div>

	<div class="columns span-12 service-content pink">
		<div class="row full">
			<div class="columns span-10 before-1 after-1 typed-wrap">
				<h1>What we do</h1>
				<h1><span class="typed-text"></span><span class="blink">|</span><span class="grey2">Presentations</span></h1>
			</div>
		</div>
		<div class="row full facts-wrap animatedParent animateOnce">
			<div class="columns span-3 xlarge-up-3 large-4 large-before-1 large-after-1 medium-6 small-12 fact-box animated fadeIn delay-500">
				<img src="/images/services-presentation-page/fact-1.png" alt="" />
				<p><span>Bring Your Content To Life</span>
				<br>With the help of our 8 full-time specialist presentation designers
				</p>
			</div>
			<div class="columns span-3 xlarge-up-3 large-4 large-before-1 large-after-1 medium-6 small-12 fact-box animated fadeIn delay-1000">
				<img src="/images/services-presentation-page/fact-2.png" alt="" />
				<p><span>End-To-End Support</span>
				<br>From the bare script to the big screen with expert advice and guidance throughout
				</p>
			</div>
			<div class="columns span-3 xlarge-up-3 large-4 large-before-1 large-after-1 medium-6 small-12 fact-box animated fadeIn delay-1500">
				<img src="/images/services-presentation-page/fact-3.png" alt="" />
				<p><span>Transform Your Templates</span>
				<br>Design from scratch, for new branding, a report or special event
				</p>
			</div>
			<div class="columns span-3 xlarge-up-3 large-4 large-before-1 large-after-1 medium-6 small-12 fact-box animated fadeIn delay-2000">
				<img src="/images/services-presentation-page/fact-4.png" alt="" />
				<p><span>Flexible Solutions</span>
				<br>All platforms, all versions, all devices
				</p>
			</div>
		</div>
	</div>

	@include('main._partials._case_study', [
		'page' => 'services',
		'panMove'	=> true,
		'fixMaxHeight'	=> true,
	])

	<div class="columns span-12 small-casestudy">
		<div class="row full">
			<div class="columns span-6 medium-12 small-12 zero-lineheight">
				<img src="/images/services-presentation-page/microsoft-social-small-case-study.jpg" alt="" />
				<h2 class="absPos">Microsoft</h2>
				<a class="absPos tag pink" href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
				<p class="absPos">Social Engagement</p>
				<a href="/projects/microsoft-social-engagement" class="button view">View Case Study</a>
				<!-- <p class="absPos">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula libero mollis mi cursus, in bibendum elit porttitor.</p> -->
			</div>
			<div class="columns span-6 medium-12 small-12 zero-lineheight">
				<img src="/images/services-presentation-page/bees-small-case-study.jpg" alt="" />
				<h2 class="absPos bespoke-bee-text">Article 10 Presents "Bees"</h2>
				<p class="absPos">Prezi  presentation</p>
				<a class="absPos tag pink" href="/services/presentation-solutions" class="tag {{ $colour }}">Presentations</a>
				<a href="/projects/article10-bees" class="button view">View Case Study</a>
				<!-- <p class="absPos">orem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula libero mollis mi cursus, in bibendum elit porttitor.</p> -->
			</div>
		</div>
	</div>

	@include('main._partials._clients')

@endsection
