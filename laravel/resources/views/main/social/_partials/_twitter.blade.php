@if ($tweets[$tweet_id]['media_local'] != "")
	<img src="/uploads/{{ $tweets[$tweet_id]['media_local'] }}" class="spacer"/>
@else
	@include('main._partials._shim', ['class' => 'spacer'])
@endif
<div class="content">
	<p>{!! Twitter::linkify($tweets[$tweet_id]['text']) !!}</p>
</div>
<img src="/images/social-landing-page/icon-twitter.svg" class="icon"/>