@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(images/social-landing-page/social-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(images/social-landing-page/social-landing-page-header.jpg);">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								WE ARE<br/>
								<span class="hilite animated growIn">SERIOUSLY</span><br/>
								SOCIAL
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection


@section('content')
	<div class="columns span-12 grid-wall">
		<div class="row full">
			@include('main._partials._shim_4x3', ['class' => ''])
			@include('main._partials._shim_3x4', ['class' => ''])
			@include('main._partials._shim_2x6', ['class' => ''])
			@include('main._partials._shim_1x12', ['class' => ''])

			<?php
				$tweet_id = 0;
			?>

			<!-- Tweet 1 -->
			<div class="item p1 pink"/>
				@if (isset($tweets[$tweet_id]))
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Vimeo 1 -->
			<div class="item p2 purple"/>
				@if (isset($vids[0]))
				<a href="{{ $vids[0]['link'] }}" class="fancybox">
					<img src="/uploads/{{ $vids[0]['media_local'] }}" class="spacer"/>
					<div class="content">
						<img src="/images/social-landing-page/button-vimeo-play.svg" class="bPlay"/>
					</div>
					<img src="/images/social-landing-page/icon-vimeo.svg" class="icon"/>
				</a>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Tweet 2 -->
			<div class="item p3"/>
				@if (isset($tweets[$tweet_id]))
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Linked In 1 -->
			<div class="item p4 green"/>
				@if (isset($linkedins[0]))
					<a href="{{ $linkedins[0]['link'] }}" target="_blank" class="">
					@include('main._partials._shim', ['class' => 'spacer'])
					<div class="content">
						<p>{{{$linkedins[0]['text']}}}</p>
					</div>
					<img src="/images/social-landing-page/icon-linkedin.svg" class="icon"/>
					</a>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Pinterest 1 -->
			<div class="item p5"/>
				@if (isset($pinterest[0]))
					<a href="{{ $pinterest[0]['link'] }}" target="_blank" class="">
						<img src="/uploads/{{{$pinterest[0]['media_local']}}}" class="spacer"/>
						<div class="content">
							<p>{{ $pinterest[0]['note'] }}</p>
						</div>
						<img src="/images/social-landing-page/icon-pinterest.svg" class="icon"/>
					</a>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Image 1 -->
			<div class="item p6"/>
				@if (isset($socialimages[0]))
					<img src="/uploads/{{{$socialimages[0]['media_local']}}}" class="spacer"/>
					<div class="content"></div>
					<img src="/images/social-landing-page/icon-image.svg" class="icon"/>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Tweet 3 -->
			<div class="item p7 blue"/>
				@if (isset($tweets[$tweet_id]))
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- POTM -->
			<div class="item p8"/>
				@if (isset($potm[0]))
				<a href="/projects/{{ $potm[0]['uri'] }}">
					<img src="/uploads/{{ $potm[0]['media_local'] }}" class="spacer"/>
					<div class="content">

					</div>
					<img src="/images/social-landing-page/icon-featured.svg" class="icon increase-size"/>
				</a>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
				<div class="black-out-gradient-layer"></div>
			</div>

			<!-- Tweet 4 -->
			<div class="item p9 blue"/>
				@if (isset($tweets[$tweet_id]))
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Linked In 2 -->
			<div class="item p10 purple"/>
				@if (isset($linkedins[1]))
					<a href="{{ $linkedins[1]['link'] }}" target="_blank" class="">
					@include('main._partials._shim', ['class' => 'spacer'])
					<div class="content">
						<p>{{{$linkedins[1]['text']}}}</p>
					</div>
					<img src="/images/social-landing-page/icon-linkedin.svg" class="icon"/>
					</a>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Image 2 -->
			<div class="item p11"/>
				@if (isset($socialimages[1]))
					<img src="/uploads/{{{$socialimages[1]['media_local']}}}" class="spacer"/>
					<div class="content"></div>
					<img src="/images/social-landing-page/icon-image.svg" class="icon"/>
				@else
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>

			<!-- Tweet 5 -->
			<div class="item p12"/>
				@if (isset($tweets[$tweet_id]))
					@include('main.social._partials._twitter')
					<?php $tweet_id++; ?>
				@endif
			</div>
		</div>
	</div>

	


	
@endsection
