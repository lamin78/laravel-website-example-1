@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(images/terms-landing-page/terms-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(images/terms-landing-page/terms-landing-page-header@1x.jpg);">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								<span class="hilite animated growIn">Terms of Business</span>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection


@section('content')

	<div class="columns span-12 terms">
		<div class="row">
			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>1. Important</h1>
				
				<p>All business is conducted and orders are accepted subject to the Terms of Business set out below.</p>
			</div>
			
			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>2. Interpretation</h1>
				
				In these Terms: &ldquo;the Company&rdquo; means any Article 10 company with which the Customer is entering into the Contract.

				The companies include: Article 10 Integrated Marketing Ltd, Article 10 Events and Exhibitions Ltd, Article 10 Design Ltd.

				&ldquo;the Customer&rdquo; means the party to whom the Products or Services are supplied by the Company.

				&ldquo;Contract&rdquo; means the quotation of the Company and any order accepted by the Company and these Terms.

				&ldquo;Customer Property&rdquo; means the Customer&rsquo;s originals artwork, samples, software files or other materials supplied to the Company.

				&ldquo;Intellectual Property Rights&rdquo; means all patents trade and service marks registered and unregistered designs copyright know-how confidential information trade or business names, applications for the foregoing and any other similar protected rights.

				&ldquo;Products&rdquo; means the Products supplied under the Contract.

				&ldquo;Products and Services&rdquo; means the Products and or Services supplied under the Contract.

				&ldquo;Services&rdquo; means the Services supplied under the Contract &ldquo;Software&rdquo; means any piece of software supplied by the Company under the Contract whether created specifically for the Customer or for its client base as a whole.

				&ldquo;Specification&rdquo; means any agreed written specification, feature list or note of functionality supplied to the Customer by the Company in advance of commencing work on the Contract whether appended to this contract or otherwise.

				&ldquo;Third Party Creative Works&rdquo; means photography illustrations or other Contracted third party creative Services or Products.

				&ldquo;Third Party Products&rdquo; means any physical or software product owned by any 3rd party which may be utilised in the delivery of the agreed Contract.

				&ldquo;Third Party Software&rdquo; means software used by the Company either as part of the Products and Services supplied under the Contract or to create the Products and Services supplied under the Contract.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>3. Terms</h1>
				
				These Terms apply to all orders placed by the Customer with the Company and supersede any previously published terms and conditions of business and override any terms and conditions stipulated or referred to by the Customer in its order or negotiations (unless otherwise expressly agreed in writing by a Director of the Company).
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>4. Quotations</h1>
				
				All quotations given by us are valid for a period of 30 days only from the date of such quotation (verbal or written). If the Customers&rsquo; order instructions differ from the specification understood when the quotation was given, the Company reserves the right to alter the price and/or terms of any quotations previously given. Errors and omissions are exempted.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>5. Orders</h1>
				
				Verbal instructions are accepted on the understanding that the Company cannot be held responsible for mistakes arising therefrom. The Company may subcontract any service it agrees to perform for the Customer, but will remain responsible for delivery and quality of goods/services supplied
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>6. Standard of Services</h1>
				
				If for any reason the Products or Services supplied do not meet with the Customer&rsquo;s reasonable satisfaction, the Customer must notify the Company in writing within 10 working days of the matter coming to the attention of the Customer, setting out in sufficient detail the matter complained of or the Customer will be deemed to be satisfied with the quality and standard of the Product or Services.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>7. Cancellation and Amendments</h1>
				
				The Customer may by written request to the Company cancel or stop any and all plans, schedules or work-in-progress and the Company shall take all reasonable steps to comply provided that the Company can do so within its Contractual obligations to suppliers and other third parties. In the event of any cancellation by the Customer the Company shall be entitled to recover from the Customer any external and internal charges or expenses incurred on the Customer&rsquo;s behalf and to which the Company is committed and the Company&rsquo;s fees covering the work already carried out on behalf of the Customer or 10% of the Contract value whichever is the greater. The Customer may request amendments to the original specification and the Company will endeavour to comply with the Customer&rsquo;s request. In these circumstances the Company reserves the right to adjust the price quoted to reflect the amendments.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>8. Ownership of Copyright and Other Rights and Materials</h1>
				
				The Company shall retain the copyright in any material contained in any pitch presentation made in competition with any other agency in the event of its presentation being unsuccessful or any other material produced speculatively by the Company and not used, whether or not in competition with another agency. Any images supplied by the Customer to the Company to facilitate any such work will remain the property of the Customer. In such an instance the Company warrants that it will not use these images for any purpose other than to complete its obligations under the Contract. Third Party Creative works remain the intellectual property of the third party supplier or as agreed by the third party supplier and the Company. The Customer shall be entitled to use such works for the purposes supplied under the Contract on payment of the agreed fees but this does not transfer Intellectual Property Rights in the Third Party Creative Works. Additional usage or transfer of Intellectual Property Rights in Third Party Creative works outside of the Contract is subject to prior agreement by the Company and payment of a fee to be agreed with the Company. The Customer accepts sole responsibility for any claim of copyright infringement brought by a third party against the Customer&rsquo;s use of Third Party Creative works outside of that permitted by the Company and indemnifies the Company for any losses or expenses it suffers (including legal costs) in relation to any such claim. Where the Customer provides facts images and/or sound (&ldquo;works&rdquo;) that are to be reproduced by the Company or used in the completion of an order the Customer warrants that it is the proper holder of all copyrights attaching to those works.

				Where the Company creates images or presentation slides for the Customer under the Contract, the Company shall pass Intellectual Property Rights in those images or slides to the Customer without restriction immediately upon payment of the Company&rsquo;s outstanding fees as agreed under the Contract. The Customer accepts sole responsibility for any claim of copyright infringement brought by a third party against the Customer&rsquo;s use of Third Party Creative works outside of that permitted by the Company and indemnifies the Company for any losses or expenses it suffers (including legal costs) in relation to any such claim. Where the Customer provides facts images and/or sound (&ldquo;works&rdquo;) that are to be reproduced by the Company or used in the completion of an order the Customer warrants that it is the proper holder of all copyrights attaching to those works.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>9. Intellectual Property Rights Indemnity</h1>
				
				Each party will indemnify the other against all costs, claims, demands, expenses and liabilities arising out of or in connection with any claim that the normal use or possession of the Products or Third Party Products by the Customer (whether used separately or in combination) infringes the Intellectual Property Rights of any third party.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>10. Software</h1>
				
				The Customer shall be entitled to un-restricted and unlimited, non-exclusive use of the Company&rsquo;s Software supplied under the Contract subject to the following:-
				
				The Software is subject to the copyright protection of the Company. The Customer shall not acquire any Intellectual Property Rights or other rights, express or implied, in or relating to the Software, other than those specified in the Contract and this Section 10. &nbsp;The Company or its licensors shall retain all such rights.
				
				The use shall be restricted to that for which the Software is supplied and the Company warrants that it shall be fit for the purpose defined in the Contract.
				
				No Licence is granted for general reproduction resale or transmission.
				
				The Customer shall not directly or indirectly reverse engineer, disassemble, decompile or translate the Software, except and to the extent expressly allowed by applicable law.
				
				The Customer may not modify, adapt, rent, sell or create derivative works based on the Software in whole or part without the Company&rsquo;s express written authority.
				
				Unless specifically authorised in the Contract, the Customer shall not rent, lease or sublicense the Software, provide third parties with access to the Software through a service bureau, commercial time-sharing arrangement, or ASP arrangement, or use the Software to provide outsourcing or training Services.
				
				The Customer shall only be entitled to use Third Party Software supplied under the Contract in accordance with Terms of any Licences for that Software.
				
				When the Company supplies Software or other computer-based materials it will deliver executable code that the Customer will have full rights to use subject to these Terms. The Company will not include source code, which is the Company&rsquo;s proprietary information. The source code may be available for purchase by the Customer on payment of a separate fee to be agreed between the Company and the Customer. If the Customer wishes to purchase the source code they should inform the Company as soon as possible.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>11. Accounts and Payment</h1>
				
				Approved credit accounts are subject to settlement in full within thirty days of date of the invoice. In all other cases full payment is required with order unless otherwise agreed in writing by the Company. New accounts are opened subject to such criteria as the Company may from time to time determine. The Company reserves the right under the Late Payment of Commercial Debts (Interest) Act 1988 to charge interest at the rate of 8% per annum over the Bank of England base lending rate from time to time for late settlement of accounts. Title to and ownership of all materials, Intellectual Property Rights and moral rights in any work undertaken by the Company shall subject and without prejudice to the other terms set out herein at all times belong to the Company until all accounts relating to the Services have been paid in full by the Customer. The Company shall be entitled to a general lien on all property of the Customer in the Company&rsquo;s possession until all sums due and payable to the Company by the Customer have been paid. Queries regarding invoices raised by The Company shall be notified by The Customer within 7 days of date of issue. Should no such query be raised by The Customer, the invoice will become valid and enforceable without further reference.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>12. Guarantee and Exclusion of Company&rsquo;s Liability</h1>
				
				Subject to the terms set out below the Company warrants:
				
				That the Products will correspond with their Specification at the time of delivery and be free from defects in material and workmanship and;
				
				The Services will be provided with reasonable skill and care.
				
				In the case of Software that the disc on which the Software is recorded will be free of defects in materials and workmanship under normal use for a period of 90 days from the date of this Licence.
				
				The Company shall not be liable under the above warranty: -
				
				Unless a claim for a defective Product is made within 10 days of collection by the Customer or delivery by the Company of the Products by returning all the Products in question with a copy of the written order;
				
				In respect of any defect or fault arising from fair wear and tear wilful damage negligence abnormal storage or working conditions failure to follow the Company&rsquo;s instructions misuse or use of the Products by inexperienced or untrained persons.
				
				If the total price for the Contract has not been paid by the due date for payment.

				In respect of Third Party Products or materials in respect of which the Customer shall only be entitled to such warranty as is given by the Third Party Supplier to the Company.
				
				The Customer expressly acknowledges that the use of the Software is at its sole risk. The Company does not warrant functions contained in the Software will be uninterrupted or error free. Subject as expressly provided in these Terms all warranties conditions or other Terms implied by statute or common law are excluded to the fullest extent permitted by law. Where any valid claim in respect of any of the Products which is based on any defect in the quality of the Products or the failure to meet specification is notified to the Company in accordance with these Terms the Company shall be entitled to replace the Products (or the part in question) free of charge or at the Company&rsquo;s sole discretion refund to the Customer the price of the Product (or a proportional part of the price) and the Company shall have no further liability to the Customer. Except in respect of death or personal injury caused by the Company&rsquo;s negligence the Company shall not be liable to the Customer by reason of any representation (unless fraudulent) or any implied warranty condition or other term or any duty at common law or under the express terms of the Contract for any indirect special or consequential loss or damage (whether for loss of profit or otherwise) which arise out of or in connection with the supply of the Products or Services and the entire liability of the Company under the Contract shall not (except as expressly provided in these Terms) exceed the price of the Contract.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>13. Customer Property</h1>
				
				Whilst every care is taken in handling the Customer Property the Company accepts it on the understanding that this is entirely at the Customer&rsquo;s risk. Unless otherwise agreed in writing the Company&rsquo;s liability if the Customer Property is lost destroyed or damaged will be limited to the replacement cost of the materials. The Company advises the Customer to keep backup copies of all Customer Property and to insure Customer Property for the full value against all risks loss or damage.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>14. Co-Operation</h1>
				
				The Company will co-operate fully with the Customer and will use all reasonable efforts to carry out the Services in an appropriate time and manner. The success of the Services will be dependent on the Customer making available all relevant information and by co-operating fully wherever necessary. The Customer agrees to give a clear brief and ensure that all facts and information given to the Company about Products or Services are complete and accurate and in no way misleading.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>15. Confidential Information</h1>
				
				Parties hereby undertake to each other to keep confidential all information (written or oral) concerning the business and affairs of the others that they shall have obtained under the Contract or which it has been advised is confidential in nature save that which is trivial or obvious or already in its possession or is in the public domain other than as a result of a breach of this clause. The Parties shall use best endeavours to prevent disclosure of confidential information in accordance with this clause.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>16. Non-Solicitation</h1>
				
				The Customer agrees and undertakes to the Company that at no time during or within one year of termination of the Company&rsquo;s appointment to provide the Products and Services will the Customer solicit or offer employment to any of the employees of the Company with whom the Customer has dealt.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>17. Delivery</h1>
				
				Delivery instructions undertaken will be adhered to as closely as possible but no responsibility can be accepted for reasonable delays.</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>18. Entire Agreement</h1>
				
				The Contract constitutes the entire agreement between the Company and the Customer. No variations of or additions shall be of any legal effect and no employee or agent of the Company is authorised to make any representation binding upon the Company unless such variation or addition is made in writing and signed by a Director of the Company.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>19. Force Majeure</h1>
				
				The Company shall not be liable for any failure to perform the Contract due or principally due to any circumstances beyond its control including but not limited to inability to secure labour, materials, supplies or transport, scarcity of fuel power or components, breakdowns in machinery, fire, storm, flood or Act of God, war, civil disturbance, strikes, lock-outs and industrial action in whatsoever form.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>20. Termination</h1>
				
				The Agreement may be terminated forthwith by written notice from either party if: (i) the other commits any material breach of any of the terms of the Agreement and, if capable of remedy, shall have failed to remedy it within 30 days of receipt of a written request from the other party so to do, (such notice to detail the breach and to contain a warning of such party&#39;s intention to terminate); or (ii) the other becomes insolvent or bankrupt or is otherwise unable to pay its debts as they fall due.
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h1>21. Governing Law</h1></h1>
				
				These Terms and Conditions and any questions, disputes or other matters relating to them shall be governed by and determined in accordance with the laws of England.
			</div>
		</div>
	</div>
	
@endsection
