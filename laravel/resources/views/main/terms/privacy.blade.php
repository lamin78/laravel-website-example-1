@extends('main.layouts.main')


@section('header')
	<div class="columns span-12 full-height fixed-background background-scroll" style="background-color:#000; background-image:url(images/terms-landing-page/terms-landing-page-header@0.1.jpg);">
		<div class="columns span-12 full-height fixed-background background-scroll" style="background-image:url(images/terms-landing-page/terms-landing-page-header@1x.jpg);">
			<div class="columns span-12 full-height">
				<div class="black-out-layer"></div>

				<div class="row full full-height nav-outer">
					<div class="columns span-10 before-1 after-1 nav">
						@include('main.layouts.partials._header')
					</div>
					<div class="we-are-block animatedParent animateOnce go">
						<div class="tbl">
							<div class="inner">
								<span class="hilite animated growIn">Privacy Notice</span>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection


@section('content')

	<div class="columns span-12 terms">
		<div class="row">
			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>Introduction</h3>
				<p>Article 10 Integrated Marketing Ltd. (Article 10) takes your privacy very seriously. This Privacy Notice is intended to set out your rights and answer any queries you may have about your personal data. If you need more information, please contact our Privacy Team: <a href="mailto:privacy@article10.com">privacy@article10.com</a>.</p>
				<p>Our personal information handling policy and procedures have been developed in line with the requirements of the 1995 European Union Data Protection Directive (Directive 95/46/EC) and the General Data Protection Regulation (EU) 2016/679 (in force from 25 May 2018).</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>1. What information do we collect?</h3>
				<p>We collect and process personal data about you when services are procured from us. The personal data we process includes parts of the below:</p>
				<ul>
					<li>your name and gender;</li>
					<li>your work address, email address and phone numbers;</li>
					<li>your job title;</li>
					<li>your bank account details where invoices are submitted;</li>
					<li>photographs, where provided to us or taken as part of the services we provide;</li>
					<li>information related to the browser or device (through IP addresses and cookies) you use to access our website or other online services; or engage with us for web and application hosting activities;</li>
					<li>and/or any other information you provide.</li>
				</ul>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>2. How do we use this information and what is the legal basis for this use?</h3>
				<p>We process the personal data for the following purposes:</p>
				<ul>
					<li>as required to establish and fulfil a contract with you, for example, if you make a purchase from us or enter into an agreement to provide or receive services. This may include communicating with you on an 'economic activity' purpose provisioning a range of services. We require this information in order to enter into a contract with you and are unable to do so without it;</li>
					<li>to comply with applicable law and regulation;</li>
					<li>in accordance with our interests in protecting Article 10's legitimate business interests and legal rights, including but not limited to, use in connection with legal claims, compliance, regulatory and investigative purposes (including disclosure of such information in connection with legal process or litigation);</li>
					<li>with your express consent to respond to any business activity that you engage with us or complaints we may receive from you, and/or in accordance with our legitimate interests including to investigate any complaints received from you or from others, about our services to you;</li>
					<li>to personalise (i) our communications to you; (ii) our website; and (iii) products or services for you, in accordance with our legitimate interests;</li>
					<li>to monitor and enhance the use of our hosted web and application services that may contain personal information linked to (i) the business services you are offering or (ii) the business services we offer to you. Through the course of checking, improving and protecting our products, content, services and websites, both online and offline, in accordance with our legitimate interests, we may access certain personal information on you.</li>
				</ul>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>3. With whom and where will we share your personal data?</h3>
				<p>We may share your personal data with our subsidiaries and sub-contractors to process it for the purposes of technical support, administration, management and ongoing improvement of our services.</p>
				<p>Elements of our services are delivered by companies and individuals other than those with which you have directly contracted.</p>
				<p>We use third parties to support us in providing our services and to help provide, run and manage our internal IT systems. The systems provided by those third parties are located in secure data centres around the world.</p>
				<p>We may also share your personal data with the below third parties:</p>
				<ul>
					<li>our professional advisors such as our auditors and external legal and financial advisors;</li>
					<li>our suppliers and business partners.</li>
				</ul>
				<p>Personal data may be shared with government authorities and/or law enforcement officials if required for the purposes above, if mandated by law or if needed for the legal protection of our legitimate interests in compliance with applicable laws.</p>
				<p>In the event that our business or any part of it is sold or integrated with another business, your details will be disclosed to our advisers and those of any prospective purchaser and will be passed to the new owners of the business.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>4. How long will you keep my personal data?</h3>
				<p>We will not keep your personal information for any purpose for longer than is necessary and will only retain the personal information that is necessary in relation to the purpose. We are also required to retain certain information as required by law or for as long as is reasonably necessary to meet regulatory requirements, resolve disputes, or enforce our terms and conditions.</p>
				<p>Where you are a customer, we will keep your information for the length of any contractual relationship you or your employer have with us and after that for a period of 24 months unless otherwise stated in a contract.</p>
				<p>Where you are a prospective customer and you have expressly consented to us contacting you, we will only retain your data (a) while you interact with us and our content; or (b) for 12 months from when you last interacted with us or our content.</p>
				<p>There are some instances where we are legally required to retain your data for longer than outlined above. In these cases, we will retain the data for the time period necessary to meet regulatory requirements, resolve disputes, or enforce our terms and conditions.</p>
				<p>We will retain your data for a short time beyond the specified retention period, to allow for information to be reviewed and any deletion to take place.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>5. Where is my data stored?</h3>
				<p>The personal data that we collect from you is stored within the European Economic Area ('EEA').</p>
				<p>Data may be processed by companies operating outside the EEA who's services we use. In this case the data processors own privacy policy will be in accordance with the General Data Protection Regulation (EU) 2016/679. Further information and copies of policies may be obtained from our Privacy Team.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>6. What are my rights in relation to my personal data?</h3>
				<p>We promise to uphold the rights provided to you as an individual under EU GDPR and the Data Protection Law.</p>
				<p>You have the right to ask us not to process your personal data for marketing purposes.</p>
				<p>Where you have consented to us using your personal data, you can withdraw that consent at any time.</p>
				<p>If the information we hold about you is inaccurate or incomplete, you can notify us and ask us to correct or supplement it.</p>
				<p>You have a right of access to personal data held by us as a data controller. This right may be exercised by emailing us at <a href="mailto:privacy@article10.com">privacy@article10.com</a>.</p>
				<p>We may charge for a request for access in accordance with applicable law. We will aim to respond to any requests for information promptly, and in any event within the legally required time limits (currently 30 days).</p>
				<p>Where you have provided your data to us and it is processed by automated means, you may be able to request that we provide it to you in a structured, machine readable format.</p>
				<p>If you have a complaint about how we have handled your personal data, you may be able to ask us to restrict how we use your personal data while your complaint is resolved. In some circumstances you can ask us to erase your personal data (a) by withdrawing your consent for us to use it; (b) if it is no longer necessary for us to use your personal data; (c) if you object to the use of your personal data and we don't have a good reason to continue to use it; or (d) if we haven't handled your personal data in accordance with our obligations.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>7. Where can I find more information about Article 10's handling of my data?</h3>
				<h3>Data controller</h3>
				<p>The data controller is Article 10 Integrated Marketing Ltd. (company registered in England under registration no. 06683554 and with its registration address at Ground Floor, Central House, 142 Central Street, London, England, EC1V 8AR) and such other contractors or suppliers that are a contracting party for the purposes of providing or receiving services.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>Changes to this privacy statement</h3>
				<p>We recognise that transparency is an ongoing responsibility so we will keep this privacy statement under regular review. This privacy statement was last updated on 24th May 2018.</p>
			</div>

			<div class="columns span-10 before-1 after-1 terms-section">
				<h3>Contact details</h3>
				<p>Should you have any queries regarding this Privacy Notice, the processing of your personal data or wish to exercise your rights you can contact Article 10's Privacy Team using this email address: <a href="mailto:privacy@article10.com">privacy@article10.com</a>.</p>
				<p>If you are not satisfied with our response, you can contact the Information Commissioner's Office: <a href="https://ico.org.uk">https://ico.org.uk<a></p>
			</div>

		</div>
	</div>

@endsection
